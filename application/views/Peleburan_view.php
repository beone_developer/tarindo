<h3>List Pemusnahan</h3>
<div class="portlet light bordered">
  <div class="portlet-title">
      <div class="tools"> </div>
  </div>

<table class="table table-striped table-bordered table-hover" id="sample_1">
        <thead>
          <tr>
              <th><center><small>Nomor</small></center></th>
              <th><center><small>Tanggal</small></center></th>
              <th><center><small>Keterangan</small></center></th>
              <th><center><small>Item</small></center></th>
              <th><center><small>Qty Lebur</small></center></th>
              <th><center>Action</center></th>
          </tr>
        </thead>
        <tbody>
          <?php 	foreach($List_peleburan as $row){ ?>
            <tr>
                <td><?php echo $row['peleburan_no'];?></td>
                <td><?php echo $row['peleburan_date'];?></td>
                <td><?php echo $row['keterangan'];?></td>
                <td><?php echo $row['nitem'];?></td>
                <td><?php echo number_format($row['qty_peleburan'],2);?></td>
                <td>
                    <?php if(helper_security("pemusnahan_delete") == 1){?>
                    <a href="javascript:dialogHapus('<?php echo base_url('Peleburan_controller/delete/'.$row['peleburan_id'].'/'.$row['peleburan_no'].'');?>')" class='btn red'><i class="fa fa-trash-o"></i></a>
                    <?php }?>
                </td>
            </tr>
            <?php
              }
            ?>
        </tbody>
    </table>
</div>

<script>
	function dialogHapus(urlHapus) {
	  if (confirm("Apakah anda yakin ingin menghapus ini ?")) {
		document.location = urlHapus;
	  }
	}
</script>
