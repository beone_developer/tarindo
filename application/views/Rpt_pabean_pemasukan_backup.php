<h3>Report Pabean Pemasukan</h3>
<div class="portlet light bordered">
  <div class="portlet-title">
      <div class="tools"> </div>
  </div>

<table class="table table-striped table-bordered table-hover" id="sample_2">
        <thead>
          <tr>
              <th colspan="1"></th>
              <th colspan="2"><center>Doc</center></th>
              <th colspan="2"><center>Received</center></th>
              <th colspan="1"></th>
              <th colspan="2"><center>Item</center></th>
              <th colspan="4"></th>
          </tr>
          <tr>
							<th width="5%">BC</th>
              <th width="10%">No</th>
              <th width="10%">Date</th>
              <th width="10%">No</th>
              <th width="10%">Date</th>
              <th width="15%">Supplier</th>
              <th width="5%">Kode</th>
              <th width="10%">Nama</th>
              <th width="5%">Sat</th>
              <th width="5%">Qty</th>
              <th width="5%">Val</th>
							<th width="10%">Jml</th>
          </tr>
        </thead>
        <tbody>
          <?php
          $this->mysql = $this ->load -> database('mysql', TRUE);

          $awal = helper_tanggalinsert($_GET['tglawal']);
      		$akhir = helper_tanggalinsert($_GET['tglakhir']);
          $sql = $this->mysql->query("select h.kode_dokumen_pabean, h.nomor_daftar, h.tanggal_daftar, h.nomor_bc11, h.tanggal_aju, h.nama_pemasok, b.uraian, b.jumlah_satuan, b.harga_invoice, b.kode_satuan, b.kode_barang
                                    from tpb_header h inner join tpb_barang b on h.id = b.id_header where h.tanggal_aju between '$awal' and '$akhir'");
								foreach($sql->result_array() as $row){

					?>
            <tr>
                <td><small><?php echo "BC ".$row['kode_dokumen_pabean'];?></small></td>
                <td><small><?php echo $row['nomor_bc11'];?></small></td>
								<td><small><?php echo date('d-m-y',strtotime($row['tanggal_aju']));?></small></td>
                <td><small><?php echo $row['nomor_daftar'];?></small></td>
								<td><small><?php echo $row['tanggal_daftar'];?></small></td>
                <td><small><?php echo $row['nama_pemasok'];?></small></td>
                <td><small><?php echo $row['kode_barang'];?></small></td>
								<td><small><?php echo $row['uraian'];?></small></td>
                <td><small><?php echo $row['kode_satuan'];?></small></td>
                <td><small><?php echo $row['jumlah_satuan'];?></small></td>
                <td><small><?php echo "USD";?></small></td>
								<td><small><?php echo $row['harga_invoice'];?></small></td>
            </tr>
            <?php
              }
            ?>
        </tbody>
    </table>
</div>
