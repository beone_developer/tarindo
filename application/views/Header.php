<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<?php
$session_id = $this->session->userdata('user_id');

$sql_perusahaan = $this->db->query("SELECT nama_perusahaan FROM public.beone_konfigurasi_perusahaan");
$perusahaan = $sql_perusahaan->row_array();

$sql_user = $this->db->query("SELECT * FROM public.beone_user WHERE user_id=".intval($session_id));
$role_id = $sql_user->row_array();

$sql_role = $this->db->query("SELECT * FROM public.beone_role WHERE role_id=".intval($role_id['role_id']));
$security = $sql_role->row_array();
?>

<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head>
        <meta charset="utf-8" />
        <title><?php echo $perusahaan['nama_perusahaan'];?></title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url();?>assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url();?>assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url();?>assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url();?>assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->

        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <link href="<?php echo base_url();?>assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url();?>assets/global/plugins/morris/morris.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url();?>assets/global/plugins/fullcalendar/fullcalendar.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url();?>assets/global/plugins/jqvmap/jqvmap/jqvmap.css" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL PLUGINS -->

        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="<?php echo base_url();?>assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
        <link href="<?php echo base_url();?>assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->

        <!-- BEGIN THEME LAYOUT STYLES -->
        <link href="<?php echo base_url();?>assets/layouts/layout/css/layout.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url();?>assets/layouts/layout/css/themes/darkblue.min.css" rel="stylesheet" type="text/css" id="style_color" />
        <link href="<?php echo base_url();?>assets/layouts/layout/css/custom.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME LAYOUT STYLES -->

        <link href="<?php echo base_url();?>assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url();?>assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />

        <link href="<?php echo base_url();?>assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
       <link href="<?php echo base_url();?>assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />

       <link href="<?php echo base_url();?>assets/global/plugins/bootstrap-sweetalert/sweetalert.css" rel="stylesheet" type="text/css" />
       <link href="<?php echo base_url();?>assets/global/plugins/bootstrap-toastr/toastr.min.css" rel="stylesheet" type="text/css" />

       <link href="<?php echo base_url();?>assets/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css" rel="stylesheet" type="text/css" />
       <link href="<?php echo base_url();?>assets/global/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css" />

       <link href="<?php echo base_url();?>assets/global/plugins/ladda/ladda-themeless.min.css" rel="stylesheet" type="text/css" />
       <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">

      <link rel="shortcut icon" href="favicon.ico" /> </head>

    <!-- END HEAD -->

    <body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white">
        <div class="page-wrapper">
            <!-- BEGIN HEADER -->
            <div class="page-header navbar navbar-fixed-top">
                <!-- BEGIN HEADER INNER -->
                <div class="page-header-inner ">
                    <!-- BEGIN LOGO -->
                    <div class="page-logo">
                        <a href="index.html">
                            <img src="<?php echo base_url();?>assets/apps/img/logo/logo-big.png" alt="logo" class="logo-default" /> </a>
                        <div class="menu-toggler sidebar-toggler">
                            <span></span>
                        </div>
                    </div>
                    <!-- END LOGO -->
                    <!-- BEGIN RESPONSIVE MENU TOGGLER -->
                    <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">
                        <span></span>
                    </a>
                    <!-- END RESPONSIVE MENU TOGGLER -->
                    <!-- BEGIN TOP NAVIGATION MENU -->
                    <div class="top-menu">
                        <ul class="nav navbar-nav pull-right">
                          <li class="dropdown dropdown-user">
                                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                    <!--<img alt="" class="img-circle" src="../assets/layouts/layout/img/avatar3_small.jpg" />-->
                                    <?php $nama_user = $this->session->userdata('username');?>
                                    <span class="username username-hide-on-mobile"> <?php echo $nama_user;?> </span>
                                    <i class="icon-logout"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-menu-default">
                                    <li>
                                        <a href="#">
                                            <i class="icon-user"></i> My Profile </a>
                                    </li>
                                    <li class="divider"> </li>
                                    <li>
                                        <a href="<?php echo base_url('Login_controller/logout'); ?>">
                                            <i class="icon-key"></i> Log Out </a>
                                    </li>
                                </ul>
                            </li>
                            <!-- END USER LOGIN DROPDOWN -->

                        </ul>
                    </div>
                    <!-- END TOP NAVIGATION MENU -->
                </div>
                <!-- END HEADER INNER -->
            </div>
            <!-- END HEADER -->
            <!-- BEGIN HEADER & CONTENT DIVIDER -->
            <div class="clearfix"> </div>
            <!-- END HEADER & CONTENT DIVIDER -->
            <!-- BEGIN CONTAINER -->
            <div class="page-container">
                <!-- BEGIN SIDEBAR -->
                <div class="page-sidebar-wrapper">
                    <!-- BEGIN SIDEBAR -->
                    <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
                    <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
                    <div class="page-sidebar navbar-collapse collapse">
                        <!-- BEGIN SIDEBAR MENU -->
                        <!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
                        <!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
                        <!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
                        <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
                        <!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
                        <!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
                        <ul class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">
                            <!-- DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element -->
                            <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
                            <li class="sidebar-toggler-wrapper hide">
                                <div class="sidebar-toggler">
                                    <span></span>
                                </div>
                            </li>
                            <!-- END SIDEBAR TOGGLER BUTTON -->
                            <!-- DOC: To remove the search box from the sidebar you just need to completely remove the below "sidebar-search-wrapper" LI element -->


                            <li class="nav-item start active open">
                                <a href="javascript:;" class="nav-link nav-toggle">
                                    <i class="icon-home"></i>
                                    <span class="title">Dashboard</span>
                                    <span class="selected"></span>
                                    <span class="arrow open"></span>
                                </a>
                            </li>

                            <li class="heading">
                                <h3 class="uppercase">Module</h3>
                            </li>
                            <!--
                            <li class="nav-item  ">
                                <a href="javascript:;" class="nav-link nav-toggle">
                                    <i class="icon-credit-card"></i>
                                    <span class="title">Purchase</span>
                                    <span class="arrow"></span>
                                </a>
                                <ul class="sub-menu">
                                    <li class="nav-item  ">
                                        <a href="#" class="nav-link ">
                                            <span class="title">Purchase Order</span>
                                        </a>
                                    </li>
                                    <li class="nav-item  ">
                                        <a href="#" class="nav-link ">
                                            <span class="title">Purchase Received</span>
                                        </a>
                                    </li>
                                    <li class="nav-item  ">
                                        <a href="#" class="nav-link ">
                                            <span class="title">Purchase Invoice</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>

                            <li class="nav-item  ">
                                <a href="javascript:;" class="nav-link nav-toggle">
                                    <i class="fa fa-truck"></i>
                                    <span class="title">Sales</span>
                                    <span class="arrow"></span>
                                </a>
                                <ul class="sub-menu">
                                    <li class="nav-item  ">
                                        <a href="#" class="nav-link ">
                                            <span class="title">Sales Order</span>
                                        </a>
                                    </li>
                                    <li class="nav-item  ">
                                        <a href="#" class="nav-link ">
                                            <span class="title">Sales Delivery</span>
                                        </a>
                                    </li>
                                    <li class="nav-item  ">
                                        <a href="#" class="nav-link ">
                                            <span class="title">Sales Invoice</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                          -->
                          <?php
                          if(helper_security("master_menu") == 1){?>
                          <li class="nav-item  ">
                              <a href="javascript:;" class="nav-link nav-toggle">
                                  <i class="fa fa-server"></i>
                                  <span class="title">Master</span>
                                  <span class="arrow"></span>
                              </a>
                              <ul class="sub-menu">
                                  <li class="nav-item  ">
                                      <a href="<?php echo base_url('User_controller'); ?>" class="nav-link ">
                                          <span class="title">User</span>
                                      </a>
                                  </li>
                                  <li class="nav-item  ">
                                      <a href="<?php echo base_url('User_controller/Role_user'); ?>" class="nav-link ">
                                          <span class="title">Role User</span>
                                      </a>
                                  </li>
                                  <li class="nav-item  ">
                                      <a href="<?php echo base_url('Custsup_controller?tipe=2'); ?>" class="nav-link ">
                                          <span class="title">Customer</span>
                                      </a>
                                  </li>
                                  <li class="nav-item  ">
                                      <a href="<?php echo base_url('Custsup_controller?tipe=1'); ?>" class="nav-link ">
                                          <span class="title">Supplier</span>
                                      </a>
                                  </li>
                                  <li class="nav-item  ">
                                      <a href="<?php echo base_url('Item_controller'); ?>" class="nav-link ">
                                          <span class="title">Item</span>
                                      </a>
                                  </li>
                                  <li class="nav-item  ">
                                      <a href="<?php echo base_url('Item_controller/index_item_type'); ?>" class="nav-link ">
                                          <span class="title">Jenis Item</span>
                                      </a>
                                  </li>
                                  <li class="nav-item  ">
                                      <a href="<?php echo base_url('Item_controller/index_satuan'); ?>" class="nav-link ">
                                          <span class="title">Satuan</span>
                                      </a>
                                  </li>
                                  <li class="nav-item  ">
                                      <a href="<?php echo base_url('Gudang_controller'); ?>" class="nav-link ">
                                          <span class="title">Gudang</span>
                                      </a>
                                  </li>
                                  <!--<li class="nav-item  ">
                                      <a href="#" class="nav-link ">
                                          <span class="title">Negara</span>
                                      </a>
                                  </li>-->
                              </ul>
                          </li>
                          <?php }?>

                          <?php
                          if(helper_security("master_import") == 1){?>
                            <li class="nav-item  ">
                                <a href="javascript:;" class="nav-link nav-toggle">
                                    <i class="fa fa-ship"></i>
                                    <span class="title">Impor</span>
                                    <span class="arrow"></span>
                                </a>
                                <ul class="sub-menu">
                                    <li class="nav-item  ">
                                        <a href="<?php echo base_url('Import_controller'); ?>" class="nav-link ">
                                            <span class="title">Impor</span>
                                        </a>
                                    </li>

                                    <li class="nav-item  ">
                                        <a href="<?php echo base_url('Import_controller/incoming_item_manual'); ?>" class="nav-link ">
                                            <span class="title">Kedatangan Manual</span>
                                        </a>
                                    </li>
                                    <!--<li class="nav-item  ">
                                        <a href="<?php echo base_url('Import_controller/index_list'); ?>" class="nav-link ">
                                            <span class="title">List Impor</span>
                                        </a>
                                    </li>-->
                                    <li class="nav-item  ">
                                        <a href="<?php echo base_url('Bc_controller'); ?>" class="nav-link ">
                                            <span class="title">BC 2.3</span>
                                        </a>
                                    </li>
                                    <li class="nav-item  ">
                                        <a href="<?php echo base_url('Bc25_controller'); ?>" class="nav-link ">
                                            <span class="title">BC 2.5</span>
                                        </a>
                                    </li>
                                    <li class="nav-item  ">
                                        <a href="<?php echo base_url('BC_261'); ?>" class="nav-link ">
                                            <span class="title">BC 2.6.1</span>
                                        </a>
                                    </li>
                                    <li class="nav-item  ">
                                        <a href="<?php echo base_url('BC_262'); ?>" class="nav-link ">
                                            <span class="title">BC 2.6.2</span>
                                        </a>
                                    </li>
                                    <li class="nav-item  ">
                                        <a href="<?php echo base_url('Bc27_controller'); ?>" class="nav-link ">
                                            <span class="title">BC 2.7</span>
                                        </a>
                                    </li>
                                    <li class="nav-item  ">
                                        <a href="<?php echo base_url('BC_40'); ?>" class="nav-link ">
                                            <span class="title">BC 4.0</span>
                                        </a>
                                    </li>
                                    <li class="nav-item  ">
                                        <a href="<?php echo base_url('Po_import_controller'); ?>" class="nav-link ">
                                            <span class="title">PO Import</span>
                                        </a>
                                    </li>
                                    <li class="nav-item  ">
                                        <a href="<?php echo base_url('Po_import_controller/index_po_import'); ?>" class="nav-link ">
                                            <span class="title">List PO Import</span>
                                        </a>
                                    </li>
                                    <li class="nav-item  ">
                                        <a href="<?php echo base_url('Inventin_controller'); ?>" class="nav-link ">
                                            <span class="title">Kedatangan Barang</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                          <?php }?>


                          <?php
                          if(helper_security("master_pembelian") == 1){?>
                            <li class="nav-item  ">
                                <a href="javascript:;" class="nav-link nav-toggle">
                                    <i class="fa fa-shopping-cart"></i>
                                    <span class="title">Pembelian</span>
                                    <span class="arrow"></span>
                                </a>
                                <ul class="sub-menu">
                                    <li class="nav-item  ">
                                        <a href="<?php echo base_url('Purchase_controller'); ?>" class="nav-link ">
                                            <span class="title">Pembelian Lokal</span>
                                        </a>
                                    </li>
                                    <li class="nav-item  ">
                                        <a href="<?php echo base_url('Purchase_controller/index_purchase'); ?>" class="nav-link ">
                                            <span class="title">List Pembelian Lokal</span>
                                        </a>
                                    </li>
                                    <li class="nav-item  ">
                                        <a href="<?php echo base_url('Kredit_note_controller'); ?>" class="nav-link ">
                                            <span class="title">Kredit Note</span>
                                        </a>
                                    </li>
                                    <li class="nav-item  ">
                                        <a href="<?php echo base_url('Kredit_note_controller/List_kredit_note'); ?>" class="nav-link ">
                                            <span class="title">List Kredit Note</span>
                                        </a>
                                    </li>
                                    <li class="nav-item  ">
                                        <a href="<?php echo base_url('Hp_controller/Filter_report_hutang'); ?>" class="nav-link ">
                                            <span class="title">Laporan Hutang</span>
                                        </a>
                                    </li>
                                    <li class="nav-item  ">
                                        <a href="<?php echo base_url('Hp_controller/Filter_report_rekap_hutang'); ?>" class="nav-link ">
                                            <span class="title">Rekap Hutang</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                          <?php }?>


                          <?php
                          if(helper_security("master_eksport") == 1){?>
                            <li class="nav-item  ">
                                <a href="javascript:;" class="nav-link nav-toggle">
                                    <i class="fa fa-ship"></i>
                                    <span class="title">Ekspor</span>
                                    <span class="arrow"></span>
                                </a>
                                <ul class="sub-menu">
                                    <li class="nav-item  ">
                                        <a href="<?php echo base_url('Export_controller'); ?>" class="nav-link ">
                                            <span class="title">Ekspor</span>
                                        </a>
                                    </li>
                                    <!--<li class="nav-item  ">
                                        <a href="<?php //echo base_url('Export_controller/index_list'); ?>" class="nav-link ">
                                            <span class="title">List Ekspor</span>
                                        </a>
                                    </li>-->
                                    <li class="nav-item  ">
                                        <a href="<?php echo base_url('Inventin_controller/index_deliverd'); ?>" class="nav-link ">
                                            <span class="title">Keberangkatan Barang</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                          <?php }?>


                          <?php
                          if(helper_security("menu_penjualan") == 1){?>
                            <li class="nav-item  ">
                                <a href="javascript:;" class="nav-link nav-toggle">
                                    <i class="fa fa-truck"></i>
                                    <span class="title">Penjualan</span>
                                    <span class="arrow"></span>
                                </a>
                                <ul class="sub-menu">
                                    <li class="nav-item  ">
                                        <a href="<?php echo base_url('Sales_controller'); ?>" class="nav-link ">
                                            <span class="title">Penjualan Lokal</span>
                                        </a>
                                    </li>
                                    <li class="nav-item  ">
                                        <a href="<?php echo base_url('Sales_controller/index_sales'); ?>" class="nav-link ">
                                            <span class="title">List Penjualan Lokal</span>
                                        </a>
                                    </li>
                                    <li class="nav-item  ">
                                        <a href="<?php echo base_url('Debit_note_controller'); ?>" class="nav-link ">
                                            <span class="title">Debit Note</span>
                                        </a>
                                    </li>
                                    <li class="nav-item  ">
                                        <a href="<?php echo base_url('Debit_note_controller/List_debit_note'); ?>" class="nav-link ">
                                            <span class="title">List Debit Note</span>
                                        </a>
                                    </li>
                                    <li class="nav-item  ">
                                        <a href="<?php echo base_url('Hp_controller/Filter_report_piutang'); ?>" class="nav-link ">
                                            <span class="title">Laporan Piutang</span>
                                        </a>
                                    </li>
                                    <li class="nav-item  ">
                                        <a href="<?php echo base_url('Hp_controller/Filter_report_rekap_piutang'); ?>" class="nav-link ">
                                            <span class="title">Rekap Piutang</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                          <?php }?>

                          <?php
                          if(helper_security("menu_inventory") == 1){?>
                            <li class="nav-item  ">
                                <a href="javascript:;" class="nav-link nav-toggle">
                                    <i class="fa fa-institution"></i>
                                    <span class="title">Inventory</span>
                                    <span class="arrow"></span>
                                </a>
                                <ul class="sub-menu">
                                    <li class="nav-item  ">
                                        <a href="<?php echo base_url('Inventin_controller/Filter_report_kartu_stock'); ?>" class="nav-link ">
                                            <span class="title">Laporan Kartu Stok</span>
                                        </a>
                                    </li>
                                    <li class="nav-item  ">
                                        <a href="<?php echo base_url('Inventin_controller/Filter_report_rekap_item'); ?>" class="nav-link ">
                                            <span class="title">Rekap Item</span>
                                        </a>
                                    </li>
                                    <?php if(helper_security("pindah_gudang") == 1){?>
                                    <li class="nav-item  ">
                                        <a href="<?php echo base_url('Pindah_gudang_controller'); ?>" class="nav-link ">
                                            <span class="title">Pindah Gudang</span>
                                        </a>
                                    </li>
                                    <?php }?>
                                    <li class="nav-item  ">
                                        <a href="<?php echo base_url('Pindah_gudang_controller/List_pindah_gudang'); ?>" class="nav-link ">
                                            <span class="title">List Pindah Gudang</span>
                                        </a>
                                    </li>
                                    <li class="nav-item  ">
                                        <a href="<?php echo base_url('Pindah_gudang_controller/List_pindah_gudang_penerimaan'); ?>" class="nav-link ">
                                            <span class="title">List Penerimaan</span>
                                        </a>
                                    </li>
                                    <li class="nav-item  ">
                                        <a href="<?php echo base_url('Stockopname_controller'); ?>" class="nav-link ">
                                            <span class="title">Stock Opname</span>
                                        </a>
                                    </li>
                                    <li class="nav-item  ">
                                        <a href="<?php echo base_url('Adjustment_penyesuaian_controller'); ?>" class="nav-link ">
                                            <span class="title">Penyesuaian</span>
                                        </a>
                                    </li>
                                    <li class="nav-item  ">
                                        <a href="<?php echo base_url('Adjustment_controller'); ?>" class="nav-link ">
                                            <span class="title">Adjustment</span>
                                        </a>
                                    </li>
                                    <li class="nav-item  ">
                                        <a href="<?php echo base_url('Adjustment_controller/List_adjustment'); ?>" class="nav-link ">
                                            <span class="title">List Adjustment</span>
                                        </a>
                                    </li>
                                    <li class="nav-item  ">
                                        <a href="<?php echo base_url('Peleburan_controller'); ?>" class="nav-link ">
                                            <span class="title">Pemusnahan</span>
                                        </a>
                                    </li>
                                    <li class="nav-item  ">
                                        <a href="<?php echo base_url('Peleburan_controller/List_peleburan'); ?>" class="nav-link ">
                                            <span class="title">List Pemusnahan</span>
                                        </a>
                                    </li>
                                    <?php if(helper_security("recal_inventory") == 1){?>
                                    <li class="nav-item  ">
                                        <a href="<?php echo base_url('Transfer_controller/recal_inventory'); ?>" class="nav-link ">
                                            <span class="title">Recal Inventory</span>
                                        </a>
                                    </li>
                                    <?php }?>
                                </ul>
                            </li>
                          <?php }?>


                          <?php
                          if(helper_security("menu_produksi") == 1){?>
                            <li class="nav-item  ">
                                <a href="javascript:;" class="nav-link nav-toggle">
                                    <i class="fa fa-industry"></i>
                                    <span class="title">Produksi</span>
                                    <span class="arrow"></span>
                                </a>
                                <ul class="sub-menu">
                                    <?php if(helper_security("produksi_add") == 1){?>
                                    <li class="nav-item  ">
                                        <a href="<?php echo base_url('Transfer_controller'); ?>" class="nav-link ">
                                            <span class="title">Bill Of Material</span>
                                        </a>
                                    </li>
                                    <?php }?>
                                    <li class="nav-item  ">
                                        <a href="<?php echo base_url('Transfer_controller/index_transfer'); ?>" class="nav-link ">
                                            <span class="title">List Produksi</span>
                                        </a>
                                    </li>
                                    <!--<li class="nav-item  ">
                                        <a href="#" class="nav-link ">
                                            <span class="title">Process Production</span>
                                        </a>
                                    </li>
                                    <li class="nav-item  ">
                                        <a href="#" class="nav-link ">
                                            <span class="title">Report Finish Good</span>
                                        </a>
                                    </li>
                                    <li class="nav-item  ">
                                        <a href="#" class="nav-link ">
                                            <span class="title">Report Waste</span>
                                        </a>
                                    </li>-->
                                </ul>
                            </li>
                          <?php }?>


                          <?php
                          if(helper_security("menu_asset") == 1){?>
                          <li class="nav-item  ">
                              <a href="javascript:;" class="nav-link nav-toggle">
                                  <i class="fa fa-money"></i>
                                  <span class="title">Fix Asset</span>
                                  <span class="arrow"></span>
                              </a>
                              <ul class="sub-menu">
                                  <li class="nav-item  ">
                                      <a href="<?php echo base_url('Fixasset_controller'); ?>" class="nav-link ">
                                          <span class="title">Periode</span>
                                      </a>
                                  </li>
                                  <li class="nav-item  ">
                                      <a href="<?php echo base_url('Fixasset_controller/fixasset'); ?>" class="nav-link ">
                                          <span class="title">Fix Asset</span>
                                      </a>
                                  </li>
                              </ul>
                          </li>
                          <?php }?>


                          <?php
                          if(helper_security("menu_asset") == 1){?>
                            <li class="nav-item  ">
                                <a href="javascript:;" class="nav-link nav-toggle">
                                    <i class="fa fa-building-o"></i>
                                    <span class="title">Asset</span>
                                    <span class="arrow"></span>
                                </a>
                                <ul class="sub-menu">
                                    <li class="nav-item  ">
                                        <a href="<?php echo base_url('Assets_controller/Assets_bahan_baku'); ?>" class="nav-link ">
                                            <span class="title">Bahan Baku</span>
                                        </a>
                                    </li>
                                    <li class="nav-item  ">
                                        <a href="<?php echo base_url('Assets_controller/Assets_bahan_penolong'); ?>" class="nav-link ">
                                            <span class="title">Bahan Penolong</span>
                                        </a>
                                    </li>
                                    <li class="nav-item  ">
                                        <a href="<?php echo base_url('Assets_controller/Assets_mesin_sparepart'); ?>" class="nav-link ">
                                            <span class="title">Mesin / Sparepart</span>
                                        </a>
                                    </li>
                                    <li class="nav-item  ">
                                        <a href="<?php echo base_url('Assets_controller/Assets_peralatan_pabrik'); ?>" class="nav-link ">
                                            <span class="title">Peralatan Pabrik</span>
                                        </a>
                                    </li>
                                    <li class="nav-item  ">
                                        <a href="<?php echo base_url('Assets_controller/Assets_wip'); ?>" class="nav-link ">
                                            <span class="title">Semi Finish Good</span>
                                        </a>
                                    </li>
                                    <li class="nav-item  ">
                                        <a href="<?php echo base_url('Assets_controller/Assets_barang_jadi'); ?>" class="nav-link ">
                                            <span class="title">Finish Good</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                          <?php }?>


                          <?php
                          if(helper_security("menu_jurnal_umum") == 1){?>
                            <li class="nav-item  ">
                                <a href="javascript:;" class="nav-link nav-toggle">
                                    <i class="fa fa-book"></i>
                                    <span class="title">Jurnal Umum</span>
                                    <span class="arrow"></span>
                                </a>
                                <ul class="sub-menu">
                                    <li class="nav-item  ">
                                        <a href="<?php echo base_url('Gj_controller'); ?>" class="nav-link ">
                                            <span class="title">Jurnal Umum</span>
                                        </a>
                                    </li>
                                    <li class="nav-item  ">
                                        <a href="<?php echo base_url('Gj_controller/List_gj'); ?>" class="nav-link ">
                                            <span class="title">List Jurnal Umum</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                          <?php }?>

                          <?php
                          if(helper_security("menu_kas_bank") == 1){?>
                            <li class="nav-item  ">
                                <a href="javascript:;" class="nav-link nav-toggle">
                                    <i class="fa fa-money"></i>
                                    <span class="title">Kas & Bank</span>
                                    <span class="arrow"></span>
                                </a>
                                <ul class="sub-menu">
                                    <li class="nav-item  ">
                                        <a href="<?php echo base_url('Cashbank_controller'); ?>" class="nav-link ">
                                            <span class="title">Transaksi Kas Bank</span>
                                        </a>
                                    </li>
                                    <li class="nav-item  ">
                                        <a href="<?php echo base_url('Cashbank_controller/index_voucher'); ?>" class="nav-link ">
                                            <span class="title">List Voucher Kas Bank</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                          <?php }?>


                          <?php
                          if(helper_security("menu_laporan_inventory") == 1){?>
                            <li class="nav-item  ">
                                <a href="javascript:;" class="nav-link nav-toggle">
                                    <i class="fa fa-bar-chart-o"></i>
                                    <span class="title">Laporan Inventory</span>
                                    <span class="arrow"></span>
                                </a>
                                <ul class="sub-menu">
                                    <li class="nav-item  ">
                                        <a href="<?php echo base_url('Bc_controller/filter_pabean_pemasukan'); ?>" class="nav-link ">
                                            <span class="title">RPT Pabean Pemasukan</span>
                                        </a>
                                    </li>
                                    <li class="nav-item  ">
                                        <a href="<?php echo base_url('Bc_controller/filter_pabean_pengeluaran'); ?>" class="nav-link ">
                                            <span class="title">RPT Pabean Pengeluaran</span>
                                        </a>
                                    </li>
                                    <li class="nav-item  ">
                                        <a href="<?php echo base_url('Bc_controller/filter_pabean_wip'); ?>" class="nav-link ">
                                            <span class="title">RPT WIP</span>
                                        </a>
                                    </li>
                                    <li class="nav-item  ">
                                        <a href="<?php echo base_url('Bc_controller/filter_mutasi_bahan_baku'); ?>" class="nav-link ">
                                            <span class="title">RPT Mutasi Bahan Baku & Penolong</span>
                                        </a>
                                    </li>
                                    <li class="nav-item  ">
                                        <a href="<?php echo base_url('Bc_controller/filter_pabean_mutasi_scrapt'); ?>" class="nav-link ">
                                            <span class="title">RPT Mutasi Scrap & Sisa</span>
                                        </a>
                                    </li>
                                    <li class="nav-item  ">
                                        <a href="<?php echo base_url('Bc_controller/filter_mutasi_barang_jadi'); ?>" class="nav-link ">
                                            <span class="title">RPT Mutasi Barang Jadi</span>
                                        </a>
                                    </li>
                                    <li class="nav-item  ">
                                        <a href="<?php echo base_url('Bc_controller/filter_mutasi_mesin'); ?>" class="nav-link ">
                                            <span class="title">RPT Mutasi Mesain & Sparepart</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                          <?php }?>

                          <?php
                          if(helper_security("menu_laporan_keuangan") == 1){?>
                            <li class="nav-item  ">
                                <a href="javascript:;" class="nav-link nav-toggle">
                                    <i class="fa fa-bar-chart-o"></i>
                                    <span class="title">Laporan Keuangan</span>
                                    <span class="arrow"></span>
                                </a>
                                <ul class="sub-menu">
                                  <li class="nav-item  ">
                                      <a href="<?php echo base_url('Gj_controller/Filter_report_general_journal'); ?>" class="nav-link ">
                                          <span class="title">Laporan Buku Besar</span>
                                      </a>
                                  </li>
                                  <li class="nav-item  ">
                                      <a href="<?php echo base_url('Gj_controller/Filter_report_neraca_mutasi'); ?>" class="nav-link ">
                                          <span class="title">Laporan Neraca Mutasi</span>
                                      </a>
                                  </li>
                                  <li class="nav-item  ">
                                      <a href="<?php echo base_url('Gj_controller/Filter_report_laba_rugi'); ?>" class="nav-link ">
                                          <span class="title">Laporan Laba Rugi</span>
                                      </a>
                                  </li>
                                  <li class="nav-item  ">
                                      <a href="<?php echo base_url('Gj_controller/Filter_report_calk'); ?>" class="nav-link ">
                                          <span class="title">Laporan CALK</span>
                                      </a>
                                  </li>
                                  <li class="nav-item  ">
                                      <a href="<?php echo base_url('Gj_controller/Filter_report_neraca'); ?>" class="nav-link ">
                                          <span class="title">Laporan Neraca</span>
                                      </a>
                                  </li>
                                  <li class="nav-item  ">
                                      <a href="<?php echo base_url('Gj_controller/Filter_report_modal'); ?>" class="nav-link ">
                                          <span class="title">Laporan Perubahan Modal</span>
                                      </a>
                                  </li>
                                  <li class="nav-item  ">
                                      <a href="<?php echo base_url('Gj_controller/Filter_report_hpp'); ?>" class="nav-link ">
                                          <span class="title">Laporan HPP</span>
                                      </a>
                                  </li>
                                </ul>
                            </li>
                          <?php }?>


                          <?php
                          if(helper_security("menu_konfigurasi") == 1){?>
                            <li class="nav-item  ">
                                <a href="javascript:;" class="nav-link nav-toggle">
                                    <i class="fa fa-gear"></i>
                                    <span class="title">Konfigurasi</span>
                                    <span class="arrow"></span>
                                </a>
                                <ul class="sub-menu">
                                    <li class="nav-item  ">
                                        <a href="<?php echo base_url('Perusahaan_controller'); ?>" class="nav-link ">
                                            <span class="title">Konfigurasi Perusahaan</span>
                                        </a>
                                    </li>
                                    <li class="nav-item  ">
                                        <a href="<?php echo base_url('COA_controller'); ?>" class="nav-link ">
                                            <span class="title">Chart Of Account</span>
                                        </a>
                                    </li>
                                    <li class="nav-item  ">
                                        <a href="<?php echo base_url('Cashbank_controller/index_kode_cashbank'); ?>" class="nav-link ">
                                            <span class="title">Kode Voucher Kas Bank</span>
                                        </a>
                                    </li>
                                    <li class="nav-item  ">
                                        <a href="<?php echo base_url('COA_controller/index_coa_jurnal'); ?>" class="nav-link ">
                                            <span class="title">COA Transaksi</span>
                                        </a>
                                    </li>
                                    <li class="nav-item  ">
                                        <a href="<?php echo base_url('Log_controller'); ?>" class="nav-link ">
                                            <span class="title">Log Program</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                          <?php }?>
                        </ul>
                        <!-- END SIDEBAR MENU -->
                        <!-- END SIDEBAR MENU -->
                    </div>
                    <!-- END SIDEBAR -->
                </div>
                <!-- END SIDEBAR -->
                <!-- BEGIN CONTENT -->
                <div class="page-content-wrapper">
                    <!-- BEGIN CONTENT BODY -->
                    <div class="page-content">
                        <!-- BEGIN PAGE HEADER-->

                        <!-- BEGIN PAGE BAR -->
                        <div class="page-bar">
                            <!--<ul class="page-breadcrumb">
                                <li>
                                    <a href="index.html">Home</a>
                                    <i class="fa fa-circle"></i>
                                </li>
                                <li>
                                    <span>Dashboard</span>
                                </li>
                            </ul>-->
                            <div class="page-toolbar">
                                <div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
                                    <i class="icon-calendar"></i>&nbsp;
                                    <span class="thin uppercase hidden-xs"></span>&nbsp;
                                    <i class="fa fa-angle-down"></i>
                                </div>
                            </div>
                        </div>
                        <!-- END PAGE BAR -->
