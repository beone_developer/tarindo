<div class="row">
	
	
</div>
<a href="<?php echo base_url('Bc25_detail_controller/next/'.$default['ID_HEADER'].'/'.$default['ID'].'');?>" class="btn btn-primary btn-sm" role="button" aria-pressed="true">next</a>
<a href="<?php echo base_url('Bc25_detail_controller/prev/'.$default['ID_HEADER'].'/'.$default['ID'].'');?>" class="btn btn-primary btn-sm" role="button" aria-pressed="true">prev</a>

<br>
<div class="portlet light bordered">
	<div class="portlet-title">
	
	<form action="" method="">
		
		<div class="form-body">
			<div class="row">
				<label for="status" class="col-sm-2 col-form-label">Status</label>
				<div class="col-sm-2">
					<input style="border: none; margin-top: -5px;" type="text" readonly class="form-control" id="status" name="status" value="LENGKAP">
				</div>
			</div>
		</div>
		<br>
		<div class="form-body">
			<div class="row">
				<div class="col-sm-1">
					<label>Detail Ke</label>
					<input type="text" class="form-control" name="detail_ke" value="1">
					<br>
					<label>Penggunaan</label>
					<input type="text" class="form-control" name="penggunaan" value="<?=isset($default['KODE_GUNA'])? $default['KODE_GUNA'] : ""?>">
				</div>

				<div class="col-sm-2">
					<label>Dari</label>
					<input type="text" class="form-control" name="dari" value="11">
					<br>
					<p style="font-size: 15px; margin-top: 30px;">Barang Lainnya</p>
				</div>

				<div class="col-sm-2 col-sm-offset-2">
					<label>Kategori Barang</label>
					<input type="text" class="form-control" name="kategori_barang" value="<?=isset($default['KATEGORI_BARANG'])? $default['KATEGORI_BARANG'] : ""?>">
					<br>
					<label>Kondisi Barang</label>
					<input type="text" class="form-control" name="kondisi_barang" value="<?=isset($default['KONDISI_BARANG'])? $default['KONDISI_BARANG'] : ""?>">
				</div>
					
				<div class="col-sm-3">
					<p style="font-size: 15px; margin-top: 30px;">Sisa Proses Produksi</p>
					<p style="font-size: 15px; margin-top: 57px;">Rusak</p>
				</div>
			</div>
		</div>
		<br>
		<div class="form-body">
			<div class="row">
				<div class="col-sm-6">
					<label><b>DATA BARANG BC 2.5</b></label>
				</div>
			</div>
		</div>
		<br>
		<div class="form-body">
			<div class="row">
				<div class="col-sm-4">
					<label>Kode</label>
					<input type="text" class="form-control" name="kode" value="<?=isset($default['KODE_BARANG'])? $default['KODE_BARANG'] : ""?>">
				</div>

				<div class="col-sm-4">
					<label>Nomor HS</label>
					<input type="text" class="form-control" name="nomor_hs" value="<?=isset($default['POS_TARIF'])? $default['POS_TARIF'] : ""?>">
				</div>

				<div class="col-sm-4">
					<label>Negara Asal</label>
					<input type="text" class="form-control" name="negara_asal" value="<?=isset($default['KODE_NEGARA_ASAL'])? $default['KODE_NEGARA_ASAL'] : ""?>">
				</div>
			</div>
		</div>
		<br>
		<div class="form-body">
			<div class="row">
				<div class="col-sm-12">
					<label>Uraian Barang</label>
					<input type="text" class="form-control" name="uraian_barang" value="<?=isset($default['URAIAN'])? $default['URAIAN'] : ""?>">
				</div>
			</div>
		</div>
		<br>
		<div class="form-body">
			<div class="row">
				<div class="col-sm-3">
					<label>Tipe</label>
					<input type="text" class="form-control" name="tipe" value="<?=isset($default['TIPE'])? $default['TIPE'] : ""?>">
				</div>
				<div class="col-sm-3">
					<label>Ukuran</label>
					<input type="text" class="form-control" name="ukuran" value="<?=isset($default['UKURAN'])? $default['UKURAN'] : ""?>">
				</div>
				<div class="col-sm-3">
					<label>Spf Lain</label>
					<input type="text" class="form-control" name="spf_lain" value="<?=isset($default['SPESIFIKASI_LAIN'])? $default['SPESIFIKASI_LAIN'] : ""?>">
				</div>
				<div class="col-sm-3">
					<label>Merk</label>
					<input type="text" class="form-control" name="merk" value="<?=isset($default['MERK'])? $default['MERK'] : ""?>">
				</div>
			</div>
		</div>
		<br>
		<div class="form-body">
			<div class="row">
				<div class="col-sm-6">
					<label><b>SATUAN & HARGA</b></label>
				</div>
			</div>
		</div>
		<br>
		<div class="form-body">
			<div class="row">
				<div class="col-sm-4">
					<label>Jumlah Satuan</label>
					<input type="text" class="form-control" name="jumlah_satuan" value="<?=isset($default['JUMLAH_SATUAN'])? $default['JUMLAH_SATUAN'] : ""?>">
				</div>
				<div class="col-sm-4">
					<label>Jumlah Kemasan</label>
					<input type="text" class="form-control" name="jumlah_kemasan" value="<?=isset($default['JUMLAH_KEMASAN'])? $default['JUMLAH_KEMASAN'] : ""?>">
				</div>
				<div class="col-sm-4">
					<label>Nilai CIF</label>
					<input type="text" class="form-control" name="nilai_cif" value="<?=isset($default['CIF'])? $default['CIF'] : ""?>">
				</div>
			</div>
		</div>
		<br>
		<div class="form-body">
			<div class="row">
				<div class="col-sm-2">
					<label>Jenis Satuan</label>
					<input type="text" class="form-control" name="jenis_satuan" value="<?=isset($default['KODE_SATUAN'])? $default['KODE_SATUAN'] : ""?>">
				</div>
				<div class="col-sm-2">
					<input style="margin-top: 23px; border: none;" readonly type="text" class="form-control" name="uraian_satuan" value="<?=isset($default[''])? $default[''] : ""?>" placeholder="KILOGRAM">
				</div>
				<div class="col-sm-2">
					<label>Jenis Kemasan</label>
					<input type="text" class="form-control" name="jenis_kemasan" value="<?=isset($default['KODE_KEMASAN'])? $default['KODE_KEMASAN'] : ""?>">
				</div>
				<div class="col-sm-2">
					<input style="margin-top: 23px; border: none;" readonly type="text" class="form-control" name="uraian_kemasan" value="<?=isset($default[''])? $default[''] : ""?>">
				</div>
				<div class="col-sm-4">
					<label>CIF Rupiah</label>
					<input type="text" class="form-control" name="cif_rupiah" value="<?=isset($default['CIF_RUPIAH'])? $default['CIF_RUPIAH'] : ""?>">
				</div>
			</div>
		</div>
		<br>
		<div class="form-body">
			<div class="row">
				<div class="col-sm-4">
					<label>Netto (Kgm)</label>
					<input type="text" class="form-control" name="netto" value="<?=isset($default['NETTO'])? $default['NETTO'] : ""?>">
				</div>
				<div class="col-sm-4">
					<label>Volume (M3)</label>
					<input type="text" class="form-control" name="volume" value="<?=isset($default['VOLUME'])? $default['VOLUME'] : ""?>">
				</div>
				<div class="col-sm-4">
					<label>Harga Penyerahan Rp</label>
					<input type="text" class="form-control" name="harga_penyerahan" value="<?=isset($default['HARGA_PENYERAHAN'])? $default['HARGA_PENYERAHAN'] : ""?>">
				</div>
			</div>
		</div>
		<br>
		<div class="form-body">
			<div class="row">
				<div class="col-sm-6">
					<label>Kode Perhitungan</label>
					<select id="kode_perhitungan" name="kode_perhitungan" class="form-control">
				        <option value="">Choose..</option>
                        
			        </select>
				</div>
			</div>
		</div>
		
		<div class="form-body">
			<div class="row">
				<!-- FORM LEFT SIDE -->
				<div class="col-sm-6">
					



					<div class="form-group">
						<div class="row">
							
							<div class="col-sm-4">
								<label>Jumlah Satuan</label>
								<input type="text" class="form-control" name="harga_penyerahan" value="<?=isset($default[''])? $default[''] : ""?>">
							</div>

							<div class="col-sm-4">
								<input style="margin-top: 25px;" type="text" class="form-control" name="harga_penyerahan" value="<?=isset($default[''])? $default[''] : ""?>">
							</div>

							<div class="col-sm-3">
								<input style="margin-top: 25px;" type="text" class="form-control" name="harga_penyerahan" value="<?=isset($default[''])? $default[''] : ""?>">
							</div>

							<div class="col-sm-1">
								<p style="margin-top: 30px; font-size: 15px;">%</p>
							</div>
						</div>
					</div>
					

				</div>
				<!-- TUTUP FORM LEFT SIDE -->
				
				<!-- FORM RIGHT SIDE -->
				<div class="col-sm-6">
					<div class="form-group">
						<div class="row">
							<div class="col-sm-6">
								<label>Dokumen</label>
								
							</div>

						</div>
					</div>

					<div class="form-group">
						<div class="row">
							<div class="col-sm-12">
								<table class="table table-sm">
								  <thead>
								    <tr class="bg-success">
								      <th scope="col">Jenis</th>
								      <th scope="col">Nomor</th>
								      <th scope="col">Tanggal</th>
								    </tr>
								  </thead>
								  <tbody>
								    
								    <tr>
								      <th>1</th>
								      <td>2</td>
								      <td>3</td>
								    </tr>

								    <tr>
								      <th>1</th>
								      <td>2</td>
								      <td>3</td>
								    </tr>

								    <tr>
								      <th>1</th>
								      <td>2</td>
								      <td>3</td>
								    </tr>

								    <tr>
								      <th>1</th>
								      <td>2</td>
								      <td>3</td>
								    </tr>

								    <tr>
								      <th>1</th>
								      <td>2</td>
								      <td>3</td>
								    </tr>
								    
						  		  </tbody>
				 				</table>
							</div>	
						</div>
					</div>

					
				</div>
				<!-- TUTUP FORM RIGHT SIDE -->
			</div>
		</div>


	</form>

	</div>
</div>