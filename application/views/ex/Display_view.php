<script>
function startTime() {
  var today = new Date();
  var h = today.getHours();
  var m = today.getMinutes();
  var s = today.getSeconds();
  m = checkTime(m);
  s = checkTime(s);
  document.getElementById('txt').innerHTML =
  h + ":" + m + ":" + s;
  var t = setTimeout(startTime, 500);
}
function checkTime(i) {
  if (i < 10) {i = "0" + i};  // add zero in front of numbers < 10
  return i;
}
</script>

<body style="background-image: url(<?php echo base_url();?>assets/green/img/Orthopedi_blur.jpg); background-size: cover;" onload="startTime()">

   <!-- Start Header Top Area
    <div class="header-top-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    <div class="logo-area">
						<a href="#"><img src="<?php echo base_url();?>assets/green/img/logo/beone.png" alt="" /></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Header Top Area -->


<!-- Start Status area -->
    		
			<div class="notika-status-area">
				<div class="container" >
				<div class="material-design-btn">
						<a href="<?php echo base_url();?>Beranda_controller" class="btn notika-btn-green btn-reco-mg btn-button-mg" style="padding:10px;" ><h2 style="font-family: Times new Roman;"><i class="fa fa-arrow-circle-left" style="font-size: 70px;"></i></h2></a>
				</div>
				</div>
			</div>
		   
		   <!-- BUTTON AREA -->
			<div class="notika-status-area">
				<div class="container" >
					<div class="row">
						<div class="col-lg-3 col-md-4 col-sm-12 col-xs-12" style="margin-right:10px; margin-top:10px; padding:0px; box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);">
							<div class="color-single nk-teal mg-t-30" style="width:100%; margin:0px;">
								<h1 style="font-size:50px; font-family:EB+Garamond; text-shadow: 0px 5px 6px rgba(38,16,10,0.55); color:white;"><div id="txt"></div></h1>
							</div>
							<br />
							<div class="color-single nk-teal mg-t-30" style="width:100%; margin:0px; padding:20px;">
								<h1 style="font-size:40px; font-family:EB+Garamond; text-shadow: 0px 5px 6px rgba(38,16,10,0.55); color:white;">03 / 06 / 2019</h1>
							</div>
						</div>
						
						<div class="col-lg-8 col-md-4 col-sm-12 col-xs-12" style="margin:0px; padding:0px; box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);">
							<div class="color-single nk-teal mg-t-30" style="width:100%; height:30px; margin:0px;"></div>
							
							<div style="background-color:white; width:100%; height:270px; padding:10px; background-image: url(<?php echo base_url();?>assets/green/img/logo/logo-soth-trans.png); background-size: cover;">
								<center>
								<h3 style="font-family:EB+Garamond; text-shadow: 0px 5px 6px rgba(38,16,10,0.55);">NOMOR ANTRIAN</h3>
								<hr />
								<h1 style="font-size:130px; font-family:EB+Garamond; text-shadow: 0px 5px 6px rgba(38,16,10,0.55);"><?php echo $display_antrian_terakhir['nomor_antrian'];?></h1>
								<h4 style="font-family:EB+Garamond; text-shadow: 0px 5px 6px rgba(38,16,10,0.55);">- <?php echo $display_antrian_terakhir['nama'];?> -</h4>
								</center>
							</div>
							
							<div class="color-single nk-teal mg-t-30" style="width:100%; height:30px; margin: 0px;"></div>
						</div>
					</div>
					<br />
					<br />
					<br />
					<div class="row">
					<center>
						<?php
							foreach($display_antrian as $row){
						?>
						<div class="col-lg-2 col-md-4 col-sm-12 col-xs-12" style="margin:10px; padding:0px; box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);">
							<div class="nk-teal" style="width:100%; height:10px; margin: 0px;"></div>
							<div style="background-color:white; width:100%; height:120px; padding:5px;">
								<center>
								<h3 style="font-size:15px; font-family:EB+Garamond; text-shadow: 0px 5px 6px rgba(38,16,10,0.55);"><?php echo $row['nama'];?></h3>
								<hr />
								<h1 style="font-size:35px; font-family:EB+Garamond; text-shadow: 0px 5px 6px rgba(38,16,10,0.55);"><?php echo $row['nomor_antrian'];?></h1>
								</center>
							</div>
							<div class="nk-teal" style="width:100%; height:10px; margin: 0px;"></div>
						</div>
						<?php
							}
						?>
						<!--<div class="col-lg-2 col-md-4 col-sm-12 col-xs-12" style="margin:10px; padding:0px; box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);">
							<div class="nk-teal" style="width:100%; height:10px; margin: 0px;"></div>
							<div style="background-color:white; width:100%; height:120px; padding:5px;">
								<center>
								<h3 style="font-size:15px; font-family:EB+Garamond; text-shadow: 0px 5px 6px rgba(38,16,10,0.55);">POLI ORTHOPEDI</h3>
								<hr />
								<h1 style="font-size:35px; font-family:EB+Garamond; text-shadow: 0px 5px 6px rgba(38,16,10,0.55);">PA001</h1>
								</center>
							</div>
							<div class="nk-teal" style="width:100%; height:10px; margin: 0px;"></div>
						</div>
						
						<div class="col-lg-2 col-md-4 col-sm-12 col-xs-12" style="margin:10px; padding:0px; box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);">
							<div class="nk-teal" style="width:100%; height:10px; margin: 0px;"></div>
							<div style="background-color:white; width:100%; height:120px; padding:5px;">
								<center>
								<h3 style="font-size:15px; font-family:EB+Garamond; text-shadow: 0px 5px 6px rgba(38,16,10,0.55);">POLI ORTHOPEDI</h3>
								<hr />
								<h1 style="font-size:35px; font-family:EB+Garamond; text-shadow: 0px 5px 6px rgba(38,16,10,0.55);">PA001</h1>
								</center>
							</div>
							<div class="nk-teal" style="width:100%; height:10px; margin: 0px;"></div>
						</div>
						
						<div class="col-lg-2 col-md-4 col-sm-12 col-xs-12" style="margin:10px; padding:0px; box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);">
							<div class="nk-teal" style="width:100%; height:10px; margin: 0px;"></div>
							<div style="background-color:white; width:100%; height:120px; padding:5px;">
								<center>
								<h3 style="font-size:15px; font-family:EB+Garamond; text-shadow: 0px 5px 6px rgba(38,16,10,0.55);">POLI ORTHOPEDI</h3>
								<hr />
								<h1 style="font-size:35px; font-family:EB+Garamond; text-shadow: 0px 5px 6px rgba(38,16,10,0.55);">PA001</h1>
								</center>
							</div>
							<div class="nk-teal" style="width:100%; height:10px; margin: 0px;"></div>
						</div>
						
						<div class="col-lg-2 col-md-4 col-sm-12 col-xs-12" style="margin:10px; padding:0px; box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);">
							<div class="nk-teal" style="width:100%; height:10px; margin: 0px;"></div>
							<div style="background-color:white; width:100%; height:120px; padding:5px;">
								<center>
								<h3 style="font-size:15px; font-family:EB+Garamond; text-shadow: 0px 5px 6px rgba(38,16,10,0.55);">POLI ORTHOPEDI</h3>
								<hr />
								<h1 style="font-size:35px; font-family:EB+Garamond; text-shadow: 0px 5px 6px rgba(38,16,10,0.55);">PA001</h1>
								</center>
							</div>
							<div class="nk-teal" style="width:100%; height:10px; margin: 0px;"></div>
						</div>-->
					</center>
					</div>
				</div>
			</div>
			<!-- End BUTTON AREA -->
			
			
		
    <!-- End Status area-->