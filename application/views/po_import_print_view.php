        <!-- END PAGE HEADER-->
        <?php
            $no_po = "";
            $list_item = "";
            $total_qty = 0;
          foreach($data_po_import as $row){
            $no_po = $row['purchase_no'];
            $nama_supplier = $row['nsupplier'];
            $alamat = $row['alamat'];
            $list_item = $row['nitem'].", ".$list_item;
            $total_qty = $total_qty + $row['qty'];
          }
        ?>

        <div class="invoice">
            <center>
            <table>
                <td>
                  <img src="<?php echo base_url();?>assets/global/img/Logo PO Tarindo.png">&nbsp;&nbsp;&nbsp;&nbsp;
                </td>
                <td>
                  <center><h2 style="color:#191970;"><b>PT. TARINDO UNIMETAL UTAMA<b></h2></center>
                  <center style="color:#191970;">Dismantling & Smelter of non-ferrous metals</center>
                  <center style="color:#FF0000;">Kawasan Berikat Ngoro Industri Persada Kav F-8,</center>
                  <center style="color:#FF0000;">Ds./kec, Ngoro, Mojokerto 61385, Jawa Timur - Indonesia</center>
                  <center style="color:#FF0000;">Tel. : (0321) 6819067, 6819068 Fax : (0321) 6819069</center>
                </td>
            </table>
          </center>

            <hr/ style="border-color:#FF0000;">
            <br />

            <center><u><h3><b>PURCHASE ORDER</b></h3></u></center>
            <center><h4><b><?php echo $no_po;?></b></h4></center>

            <br />
            <br />
            <div class="row">
                <div class="col-xs-2">
                    <ul class="list-unstyled">
                        <li> <h4> Supplier </h4> </li>
                        <br />
                        <li> <h4> Address </h4> </li>
                        <br />
                        <li> <h4> Comodity </h4> </li>
                        <br />
                        <li> <h4> Quantity </h4> </li>
                        <li> <h4>  </h4> </li>
                        <br />
                        <br />
                        <li> <h4> Unit Price </h4> </li>
                        <br />
                        <br />
                        <br />
                        <li> <h4> Packing </h4> </li>
                        <br />
                        <li> <h4> Payment Term </h4> </li>
                        <br />
                        <li> <h4> Latest Shipment </h4> </li>
                        <br />
                        <li> <h4> Remarks </h4> </li>

                    </ul>
                </div>
                <div class="col-xs-10">
                    <ul class="list-unstyled">

                        <li><h4>: <?php echo $nama_supplier;?> </h4> </li>
                        <br />
                        <li><h4>: <?php echo $alamat;?> </h4> </li>
                        <br />
                        <li><h4>: <?php echo $list_item;?> </h4> </li>
                        <br />
                        <li><h4>: <?php echo $list_item." ".$total_qty." Kgs";?> </h4> </li>
                        <li><h4>  <?php echo "(+/- 5% in Quantity is Allowed)";?> </h4> </li>
                        <br />

                        <?php
                          foreach($data_po_import as $row){
                            $nama_item = $row['nitem'];
                            $unit_price = $row['price'];
                            echo "<li><h4>: ".$nama_item." US$ ".number_format($unit_price, 3)." / Kgs";
                            echo "<br />";
                          }
                        ?>
                        <br />

                        <li><h4>:  <?php echo "Case / Pallet";?> </h4> </li>
                        <br />
                        <li><h4>:  <?php echo "T/T Remittance Against Faxed copies of a set of Shipping documents";?> </h4> </li>
                        <br />
                        <li><h4>:  <?php echo " ";?> </h4> </li>
                        <br />
                        <li><h4>:  <?php echo "1. Loaded into Container";?> </h4> </li>
                        <li><h4>:  <?php echo "2. The quantity of correct melts is tolerance +/- 5% not to be treated";?> </h4> </li>
                        <li><h4>:  <?php echo "3. Secondary Quality";?> </h4> </li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                </div>
            </div>


            <br />
            <br />
            <br />
            <br />
            <br />
            <div class="row">
                <div class="col-xs-4">
                      <?php
                        $bln = substr(date('d-m-Y'), 3, 2);
                        $tgl = substr(date('d-m-Y'), 0, 2);
                        $thn = substr(date('d-m-Y'), 6, 4);

                        if ($bln == 1){
                          $bulan = "Januari";
                        }else if ($bln == 2){
                          $bulan = "Februari";
                        }else if ($bln == 3){
                          $bulan = "Maret";
                        }else if ($bln == 4){
                          $bulan = "April";
                        }else if ($bln == 5){
                          $bulan = "Mei";
                        }else if ($bln == 6){
                          $bulan = "Juni";
                        }else if ($bln == 7){
                          $bulan = "Juli";
                        }else if ($bln == 8){
                          $bulan = "Agustus";
                        }else if ($bln == 9){
                          $bulan = "September";
                        }else if ($bln == 10){
                          $bulan = "Oktober";
                        }else if ($bln == 11){
                          $bulan = "November";
                        }else if ($bln == 12){
                          $bulan = "Desember";
                        }
                      ?>

                      <h4><?php echo $tgl." ".$bulan." ".$thn;?></h4>
                      <h4>PT. Tarindo Unimetal Utama,</h4>
                      <br/>
                      <br/>
                      <br />
                      <br/>
                      <h4><u>Lee Guat Kheng</u></h4>
                      <h4>Manager</h4>
                </div>
            </div>
            <div class="row">
              <a class="btn btn-lg blue hidden-print margin-bottom-5" onclick="javascript:window.print();"> Print
                  <i class="fa fa-print"></i>
              </a>
            </div>
        </div>
