<div class="row">
    <div class="col-sm-3">
        <a href="#" class="btn btn-primary btn-sm active" role="button" aria-pressed="true">Detail Barang</a>
    </div>

</div>
<a href="<?php echo base_url('Bc261_detail_controller/next/' . $default['ID_HEADER'] . '/' . $default['ID'] . ''); ?>" class="btn btn-primary btn-sm" role="button" aria-pressed="true">next</a>
<a href="<?php echo base_url('Bc261_detail_controller/prev/' . $default['ID_HEADER'] . '/' . $default['ID'] . ''); ?>" class="btn btn-primary btn-sm" role="button" aria-pressed="true">prev</a>

<br>
<div class="portlet light bordered">
    <div class="portlet-title">

        <form action="" method="">

            <div class="form-body">
                <div class="row">
                    <label for="status" class="col-sm-2 col-form-label">Status</label>
                    <div class="col-sm-2">
                        <input style="border: none; margin-top: -5px;" type="text" readonly class="form-control" id="status" name="status" value="LENGKAP">
                    </div>
                </div>
            </div>
            <br>
            <div class="form-body">
                <div class="row">
                    <div class="col-sm-1">
                        <label>Detail Ke</label>
                        <input type="text" class="form-control" name="detail_ke" value="1">
                        <br>
                    </div>

                    <div class="col-sm-2">
                        <label>Dari</label>
                        <input type="text" class="form-control" name="dari" value="">
                        <br>

                    </div>

                    <div class="col-sm-2 col-sm-offset-2">
                        <label>Kategori Barang</label>
                        <input type="text" class="form-control" name="kategori_barang" value="<?= isset($default['KATEGORI_BARANG']) ? $default['KATEGORI_BARANG'] : "" ?>">
                        <br>
                    </div>

                    <div class="col-sm-3">
                        <p style="font-size: 15px; margin-top: 30px;">Sisa Proses Produksi</p>

                    </div>
                </div>
            </div>
            <br>
            <div class="form-body">
                <div class="row">
                    <div class="col-sm-6">
                        <label><b>DATA BARANG BC 2.5</b></label>
                    </div>
                </div>
            </div>
            <br>
            <div class="form-body">
                <div class="row">
                    <div class="col-sm-4">
                        <label>Kode</label>
                        <input type="text" class="form-control" name="kode" value="<?= isset($default['KODE_BARANG']) ? $default['KODE_BARANG'] : "" ?>">
                    </div>

                    <div class="col-sm-4">
                        <label>Nomor HS</label>
                        <input type="text" class="form-control" name="nomor_hs" value="<?= isset($default['POS_TARIF']) ? $default['POS_TARIF'] : "" ?>">
                    </div>


                </div>
            </div>
            <br>
            <div class="form-body">
                <div class="row">
                    <div class="col-sm-12">
                        <label>Uraian Barang</label>
                        <input type="text" class="form-control" name="uraian_barang" value="<?= isset($default['URAIAN']) ? $default['URAIAN'] : "" ?>">
                    </div>
                </div>
            </div>
            <br>
            <div class="form-body">
                <div class="row">
                    <div class="col-sm-3">
                        <label>Tipe</label>
                        <input type="text" class="form-control" name="tipe" value="<?= isset($default['TIPE']) ? $default['TIPE'] : "" ?>">
                    </div>
                    <div class="col-sm-3">
                        <label>Ukuran</label>
                        <input type="text" class="form-control" name="ukuran" value="<?= isset($default['UKURAN']) ? $default['UKURAN'] : "" ?>">
                    </div>
                    <div class="col-sm-3">
                        <label>Spf Lain</label>
                        <input type="text" class="form-control" name="spf_lain" value="<?= isset($default['SPESIFIKASI_LAIN']) ? $default['SPESIFIKASI_LAIN'] : "" ?>">
                    </div>
                    <div class="col-sm-3">
                        <label>Merk</label>
                        <input type="text" class="form-control" name="merk" value="<?= isset($default['MERK']) ? $default['MERK'] : "" ?>">
                    </div>
                </div>
            </div>
            <br>
            <div class="form-body">
                <div class="row">
                    <div class="col-sm-6">
                        <label><b>SATUAN & HARGA</b></label>
                    </div>
                </div>
            </div>
            <br>
            <div class="form-body">
                <div class="row">
                    <div class="col-sm-4">
                        <label>Total Detil/FOB</label>
                        <input type="text" class="form-control" name="fob" value="<?= isset($default['FOB']) ? $default['FOB'] : "" ?>">
                    </div>
                    <div class="col-sm-4">
                        <label>BT.Diskon</label>
                        <input type="text" class="form-control" name="diskon" value="<?= isset($default['DISKON']) ? $default['DISKON'] : "" ?>">
                    </div>
                    <div class="col-sm-4">
                        <label>Jumlah Satuan</label>
                        <input type="text" class="form-control" name="jumlah_satuan" value="<?= isset($default['JUMLAH_SATUAN']) ? $default['JUMLAH_SATUAN'] : "" ?>">
                    </div>
                    <div class="col-sm-4">
                        <label>Harga Satuan</label>
                        <input type="text" class="form-control" name="harga_satuan" value="<?= isset($default['HARGA_SATUAN']) ? $default['HARGA_SATUAN'] : "" ?>">
                    </div>
                    <div class="col-sm-4">
                        <label>Harga Detil</label>
                        <input type="text" class="form-control" name="harga_detil" value="<?= isset($default['FOB']) ? $default['FOB'] : "" ?>">
                    </div>
                    <div class="col-sm-4">
                        <label>Freight</label>
                        <input type="text" class="form-control" name="freight" value="<?= isset($default['FREIGHT']) ? $default['FREIGHT'] : "" ?>">
                    </div>
                    <div class="col-sm-4">
                        <label>Asuransi</label>
                        <input type="text" class="form-control" name="asuransi" value="<?= isset($default['ASURANSI']) ? $default['ASURANSI'] : "" ?>">
                    </div>

                    <div class="col-sm-4">
                        <label>Jumlah Kemasan</label>
                        <input type="text" class="form-control" name="jumlah_kemasan" value="<?= isset($default['JUMLAH_KEMASAN']) ? $default['JUMLAH_KEMASAN'] : "" ?>">
                    </div>
                    <div class="col-sm-4">
                        <label>Nilai CIF</label>
                        <input type="text" class="form-control" name="nilai_cif" value="<?= isset($default['CIF']) ? $default['CIF'] : "" ?>">
                    </div>
                </div>
            </div>
            <br>
            <div class="form-body">
                <div class="row">

                    <div class="col-sm-2">
                        <label>Jenis Satuan</label>
                        <input type="text" class="form-control" name="jenis_satuan" value="<?= isset($default['KODE_SATUAN']) ? $default['KODE_SATUAN'] : "" ?>">
                    </div>
                    <div class="col-sm-2">
                        <input style="margin-top: 23px; border: none;" readonly type="text" class="form-control" name="uraian_satuan" value="<?= isset($default['']) ? $default[''] : "" ?>" placeholder="KILOGRAM">
                    </div>
                    <div class="col-sm-2">
                        <label>Jenis Kemasan</label>
                        <input type="text" class="form-control" name="jenis_kemasan" value="<?= isset($default['KODE_KEMASAN']) ? $default['KODE_KEMASAN'] : "" ?>">
                    </div>
                    <div class="col-sm-2">
                        <input style="margin-top: 23px; border: none;" readonly type="text" class="form-control" name="uraian_kemasan" value="<?= isset($default['']) ? $default[''] : "" ?>">
                    </div>
                    <div class="col-sm-4">
                        <label>CIF Rupiah</label>
                        <input type="text" class="form-control" name="cif_rupiah" value="<?= isset($default['CIF_RUPIAH']) ? $default['CIF_RUPIAH'] : "" ?>">
                    </div>
                </div>
            </div>
            <br>
            <div class="form-body">
                <div class="row">
                    <div class="col-sm-4">
                        <label>Netto (Kgm)</label>
                        <input type="text" class="form-control" name="netto" value="<?= isset($default['NETTO']) ? $default['NETTO'] : "" ?>">
                    </div>
                    <div class="col-sm-4">
                        <label>Negara Asal</label>
                        <input type="text" class="form-control" name="negara_asal" value="<?= isset($default['KODE_NEGARA_ASAL']) ? $default['KODE_NEGARA_ASAL'] : "" ?>">
                    </div>

                </div>
            </div>
            <br>

            <br>
            <div class="form-body">
                <div class="row">
                    <div class="col-sm-6">
                        <label><b>TARIF & FASILITAS</b></label>
                    </div>

                    <div class="col-sm-6">
                        <label><b>FASILITAS & SKEMA TARIF</b></label>
                    </div>
                </div>
            </div>
            <br>
            <div class="form-body">
                <div class="row">
                    <!-- FORM LEFT SIDE -->
                    <div class="col-sm-6">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" name="harga_penyerahan" value="<?= isset($default_tarifBM['']) ? $default_tarifBM[''] : "" ?>">
                                </div>

                                <div class="col-sm-4">
                                    <input type="text" class="form-control" name="kode_tarif_bm" value="<?= isset($default_tarifBM['KODE_TARIF']) ? $default_tarifBM['KODE_TARIF'] : "" ?>">
                                </div>

                                <div class="col-sm-4">
                                    <input type="text" class="form-control" name="tarif_bm" value="<?= isset($default_tarifBM['TARIF']) ? $default_tarifBM['TARIF'] : "" ?>">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="kode_fasilitas_bm" value="<?= isset($default_tarifBM['KODE_FASILITAS']) ? $default_tarifBM['KODE_FASILITAS'] : "" ?>">
                                </div>

                                <div class="col-sm-4">
                                    <input type="text" class="form-control" name="tarif_fasilitas_bm" value="<?= isset($default_tarifBM['TARIF_FASILITAS']) ? $default_tarifBM['TARIF_FASILITAS'] : "" ?>">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">

                                <div class="col-sm-3">
                                    <label>PPN</label>
                                    <input type="text" class="form-control" name="tarif_ppn" value="<?= isset($default_tarifPPN['TARIF']) ? $default_tarifPPN['TARIF'] : "" ?>">
                                </div>

                                <div class="col-sm-1">
                                    <p style="margin-top: 30px; font-size: 15px;">%</p>
                                </div>

                                <div class="col-sm-4">
                                    <input style="margin-top: 25px;" type="text" class="form-control" name="kode_fasilitas_ppn" value="<?= isset($default_tarifPPN['KODE_FASILITAS']) ? $default_tarifPPN['KODE_FASILITAS'] : "" ?>">
                                </div>

                                <div class="col-sm-3">
                                    <input style="margin-top: 25px;" type="text" class="form-control" name="tarif_fasilitas_ppn" value="<?= isset($default_tarifPPN['TARIF_FASILITAS']) ? $default_tarifPPN['TARIF_FASILITAS'] : "" ?>">
                                </div>

                                <div class="col-sm-1">
                                    <p style="margin-top: 30px; font-size: 15px;">%</p>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">

                                <div class="col-sm-3">
                                    <label>PPnBM</label>
                                    <input type="text" class="form-control" name="tarif_ppnbm" value="<?= isset($default_tarifPPNBM['TARIF']) ? $default_tarifPPNBM['TARIF'] : "" ?>">
                                </div>

                                <div class="col-sm-1">
                                    <p style="margin-top: 30px; font-size: 15px;">%</p>
                                </div>

                                <div class="col-sm-4">
                                    <input style="margin-top: 25px;" type="text" class="form-control" name="kode_fasilitas_ppnbm" value="<?= isset($default_tarifPPNBM['KODE_FASILITAS']) ? $default_tarifPPNBM['KODE_FASILITAS'] : "" ?>">
                                </div>

                                <div class="col-sm-3">
                                    <input style="margin-top: 25px;" type="text" class="form-control" name="tarif_fasilitas_ppnbm" value="<?= isset($default_tarifPPNBM['TARIF_FASILITAS']) ? $default_tarifPPNBM['TARIF_FASILITAS'] : "" ?>">
                                </div>

                                <div class="col-sm-1">
                                    <p style="margin-top: 30px; font-size: 15px;">%</p>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">

                                <div class="col-sm-3">
                                    <label>PPh</label>
                                    <input type="text" class="form-control" name="tarif_pph" value="<?= isset($default_tarifPPH['TARIF']) ? $default_tarifPPH['TARIF'] : "" ?>">
                                </div>

                                <div class="col-sm-1">
                                    <p style="margin-top: 30px; font-size: 15px;">%</p>
                                </div>

                                <div class="col-sm-4">
                                    <input style="margin-top: 25px;" type="text" class="form-control" name="kode_fasilitas_pph" value="<?= isset($default_tarifPPH['KODE_FASILITAS']) ? $default_tarifPPH['KODE_FASILITAS'] : "" ?>">
                                </div>

                                <div class="col-sm-3">
                                    <input style="margin-top: 25px;" type="text" class="form-control" name="tarif_fasilitas_pph" value="<?= isset($default_tarifPPH['TARIF_FASILITAS']) ? $default_tarifPPH['TARIF_FASILITAS'] : "" ?>">
                                </div>

                                <div class="col-sm-1">
                                    <p style="margin-top: 30px; font-size: 15px;">%</p>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-12">
                                    <label>Cukai</label>
                                    <input type="text" class="form-control" name="cukai" value="<?= isset($default_tarifcukai['TARIF']) ? $default_tarifcukai['TARIF'] : "" ?>">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">

                                <div class="col-sm-4">
                                    <input style="margin-top: 30px;" type="text" class="form-control" name="harga_penyerahan" value="<?= isset($default['']) ? $default[''] : "" ?>">
                                </div>

                                <div class="col-sm-4">
                                    <input style="margin-top: 30px;" type="text" class="form-control" name="harga_penyerahan" value="<?= isset($default['']) ? $default[''] : "" ?>">
                                </div>

                                <div class="col-sm-1">
                                    <p style="margin-top: 30px; font-size: 15px;">%</p>
                                </div>

                                <div class="col-sm-3">
                                    <input style="margin-top: 30px;" type="text" class="form-control" name="harga_penyerahan" value="<?= isset($default['']) ? $default[''] : "" ?>">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">

                                <div class="col-sm-4">
                                    <label>Jumlah Satuan</label>
                                    <input type="text" class="form-control" name="jumlah_satuan" value="<?= isset($default_tarifcukai['JUMLAH_SATUAN']) ? $default_tarifcukai['JUMLAH_SATUAN'] : "" ?>">
                                </div>

                                <div class="col-sm-4">
                                    <input style="margin-top: 25px;" type="text" class="form-control" name="harga_penyerahan" value="<?= isset($default['']) ? $default[''] : "" ?>">
                                </div>

                                <div class="col-sm-3">
                                    <input style="margin-top: 25px;" type="text" class="form-control" name="harga_penyerahan" value="<?= isset($default['']) ? $default[''] : "" ?>">
                                </div>

                                <div class="col-sm-1">
                                    <p style="margin-top: 30px; font-size: 15px;">%</p>
                                </div>
                            </div>
                        </div>


                    </div>
                    <!-- TUTUP FORM LEFT SIDE -->

                    <!-- FORM RIGHT SIDE -->
                    <div class="col-sm-6">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-6">
                                    <label>Fasilitas</label>
                                    <input type="text" class="form-control" name="harga_penyerahan" value="<?= isset($default_tarifcukai['KODE_FASILITAS']) ? $default_tarifcukai['KODE_FASILITAS'] : "" ?>">
                                </div>

                                <div class="col-sm-6">
                                    <label>Skm Trf</label>
                                    <input type="text" class="form-control" name="skema_tarif" value="<?= isset($default_tarifcukai['JENIS_TARIF']) ? $default_tarifcukai['JENIS_TARIF'] : "" ?>">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                 <li>
                            <a href="#modal4" class="btn btn-primary btn-sm" role="button" data-target="#modal4" data-toggle="modal">DOKUMEN</a>
                        </li>
                                <div class="col-sm-12">
                                   
                                    <table class="table table-sm">
                                        <thead>
                                            <tr class="bg-success">
                                                <th scope="col">Jenis</th>
                                                <th scope="col">Nomor</th>
                                                <th scope="col">Tanggal</th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                            <tr>
                                                <?php
                                                $n = 0;
                                                if ($ptb_other) {
                                                    foreach ($ptb_other as $data) {
                                                        $n = $n + 1;
                                                        ?>
                                                    <tr>
                                                        <td width="30%"><?php echo $data->URAIAN_DOKUMEN; ?></td>
                                                        <td width="30%"><?php echo $data->NOMOR_DOKUMEN; ?></td>
                                                        <td width="30%"><?php echo $data->TANGGAL_DOKUMEN; ?></td>
                                                        </tr>
                                                    <?php
                                                }
                                            }
                                            ?>
                                            </tr>

                                        </tbody>
                                    </table>
                                </div>	
                            </div>
                        </div>


                    </div>
                    <!-- TUTUP FORM RIGHT SIDE -->
                </div>
            </div>


        </form>

    </div>
</div>
<div id="modal4" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog">
    <div class="modal-content">
        <div class="modal-header">
            <h3 class="modal-title"><b>DOKUMEN</b></h3>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-sm-12">
                    <form action="preview_BC261/update_modal_dokumen" method = "POST">
                        <div class="form-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <input type="hidden" class="form-control" id="id" name="id" value="">
                                </div>
                            </div>
                        </div>
                        <div class="form-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <label>Jenis Dokumen</label>
                                    <input type="text" class="form-control" id="dokumen" name="dokumen" value="">
                                </div>
                            </div>
                        </div>

                        <div class="form-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <label>Nomor Dokumen</label>
                                    <input type="text" class="form-control" id="nomor" name="nomor" value="">
                                </div>
                            </div>
                        </div>
                        <div class="form-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <label>Tanggal Dokumen</label>
                                    <input type="text" class="form-control" id="tanggal" name="tanggal" value="">
                                </div>
                            </div>
                        </div>

                        <div class="modal-footer">
                            <button type="submit" class="btn blue" name="edit_bc261">Save</button>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        </div>
                    </form>
                </div>	
            </div>
            <br></br>
            <div class="row">
                <div class="table-wrapper-scroll-y my-custom-scrollbar">
                    <table class="table table-striped table-bordered table-hover">

                        <thead>
                            <tr class="bg-success">
                                <th scope="col">No.</th>
                                <th scope="col">Jenis Dokumen</th>
                                <th scope="col">Nomor Dokumen</th>
                                <th scope="col">Tanggal</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <?php
                                $n = 0;
                                if ($ptb_other) {
                                    foreach ($ptb_other as $data) {
                                        $n = $n + 1;
                                        ?>
                                    <tr><td width="30%"><?php echo $n; ?></td>
                                        <td width="30%"><?php echo $data->URAIAN_DOKUMEN; ?></td>
                                        <td width="30%"><?php echo $data->NOMOR_DOKUMEN; ?></td>
                                        <td width="30%"><?php echo $data->TANGGAL_DOKUMEN; ?></td>
                                        <td><button type="button" class="btn btn-dark" onclick="tampilkanEditDokumen(<?php echo $data->id_dokumen; ?>)"><i class="fa fa-info"></i></button>
                                            <a href="javascript:dialogHapus('<?php echo base_url('preview_BC261/delete_dokumen/' . $data->id_dokumen . ''); ?>')" class='btn red'><i class="fa fa-trash-o"></i></a></td>
                                    </tr>
                                    <?php
                                }
                            }
                            ?>
                            </tr>	    

                        </tbody>
                    </table>



                </div>
            </div>	
        </div>

        <div class="modal2-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
    </div>
    <script>
        function dialogHapus(urlHapus) {
            if (confirm("Apakah anda yakin ingin menghapus ini ?")) {
                document.location = urlHapus;
            }
        }
    </script>
</div>

<script type="text/javascript">
    function tampilkanEditDokumen(id) {
// alert(id);
        $.ajax({
            url: "<?php echo base_url('preview_BC261/editmodal/') ?>" + id,
            type: "GET",
            dataType: "JSON",
            success: function (data)
            {

                $("#dokumen").empty();
                $("#dokumen").val(data.dokumenedit.URAIAN_DOKUMEN);
                $("#nomor").empty();
                $("#nomor").val(data.dokumenedit.NOMOR_DOKUMEN);
                $("#tanggal").empty();
                $("#tanggal").val(data.dokumenedit.TANGGAL_DOKUMEN);
                $("#id").empty();
                $("#id").val(data.dokumenedit.ID);
                $("#modal4").modal('show');

            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error get data from ajax');
            }
        });
// $('#modal').modal();

// var url= '<?php echo base_url(); ?>Bc25_controller/editmodal/'+id;
// $.get( url, function( data ) {
// // console.log(data);
// $("#dokumen").empty();
// $("#dokumen").val(data.KODE_JENIS_DOKUMEN);
// $("#nomor").empty();
// $("#nomor").val(data.NOMOR_DOKUMEN);
// $("#tgl").empty();
// $("#tgl").val(data.TANGGAL_DOKUMEN);
// });
    }
</script>