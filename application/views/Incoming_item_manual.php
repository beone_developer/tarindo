<h3>Incoming Item Manual</h3>
<div class="portlet light bordered">
  <div class="portlet-title">
      <div class="tools"> </div>
  </div>

<table class="table table-striped table-bordered table-hover" id="sample_2">
        <thead>
          <tr>
              <th width="5%"><center><small>No Daftar</small></center></th>
              <th width="20%"><center><small>No Pengajuan</small></center></th>
              <th width="10%"><center><small>Tgl Pengajuan</small></center></th>
              <th width="5%"><center><small>Kode <br/> Pabean</small></center></th>
              <th width="15%"><center><small>Supplier</small></center></th>
              <th width="10%"><center><small>Berat</small></center></th>
			        <th width="10%"><center><small>Currency</small></center></th>
              <th width="10%"><center><small>Amount</small></center></th>
			        <th width="15%"><center><small>Action</small></center></th>
          </tr>
        </thead>
        <tbody>
          <?php
                $no=0;
								foreach($list_import_header as $row){
					?>
            <tr>
                <td><small><?php echo $row['nomor_daftar'];?></small></td>
				        <td><small><?php echo $row['nomor_aju'];?></small></td>
                <td><small><?php echo $row['tanggal_aju'];?></small></td>
								<td><small><?php echo "BC ".$row['kode_dokumen_pabean'];?></small></td>
                <td><small><?php echo $row['nsupplier'];?></small></td>
                <td><small><?php echo number_format($row['berat'], 2);?></small></td>
        				<td><small><?php
        						if ($row['currency'] == 1){
                                      echo 'USD';
                                  }else{
                                      echo 'IDR';
                                  }
        				?></small></td>
				        <td><small><?php echo number_format($row['amount'], 2);?></small></td>
                <?php
              		  if ($row['status'] == 0){
                ?>
                    <td><a href='#' class='btn btn-circle green'><small>Sudah Terima</small></a> </td>
              <?php }else{?>
                    <td><?php if(helper_security("terima_barang") == 1){?><a href='<?php echo base_url('Inventin_controller/Received_manual/'.$row['import_manual_header_id'].'');?>' class='btn btn-circle red'><small>Terima</small></a><?php }?> </td>
              <?php }?>
            </tr>
            <?php
              }
            ?>
        </tbody>
    </table>
</div>

<script>
	function dialogHapus(urlHapus) {
	  if (confirm("Apakah anda yakin ingin menghapus ini ?")) {
		document.location = urlHapus;
	  }
	}
</script>
