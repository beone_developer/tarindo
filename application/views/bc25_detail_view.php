<div class="row">
	<div class="col-sm-3">
		<a href="#" class="btn btn-primary btn-sm active" role="button" aria-pressed="true">Detail Barang</a>
	</div>
	<div style="margin-left: -176px;" class="col-sm-3">
		<a href="<?php echo base_url('Bc25_detail_penggunaan_bb_impor_controller/form/'.$default['ID_HEADER'].'/'.$default['ID'].'');?>" class="btn btn-primary btn-sm" role="button" aria-pressed="true">Penggunaan Bahan Baku Impor</a>
	</div>
	<!-- <div style="margin-left: -75px;" class="col-sm-3">
		<a href="<?php echo base_url('Bc25_detail_penggunaan_bb_lokal_controller/form/'.$default['ID_HEADER'].'/'.$default['ID'].'');?>" class="btn btn-primary btn-sm" role="button" aria-pressed="true">Penggunaan Bahan Baku Lokal</a>
	</div> -->	
</div>


<br>

<a href="<?php echo base_url('Bc25_detail_controller/prev/'.$default['ID_HEADER'].'/'.$default['ID'].'');?>" class="btn btn-primary btn-sm" role="button" aria-pressed="true">Prev</a>

<a href="<?php echo base_url('Bc25_detail_controller/next/'.$default['ID_HEADER'].'/'.$default['ID'].'');?>" class="btn btn-primary btn-sm" role="button" aria-pressed="true">Next</a>


<br>
<div class="portlet light bordered">
	<div class="portlet-title">
	
	<form role="form" method="post">
			<input type="hidden" class="form-control" id="ID" 
						name="ID" value="<?=isset($default['ID'])? $default['ID'] : ""?>">
		
		<div class="form-body">
			<div class="row">
				<label for="status" class="col-sm-2 col-form-label">Status</label>
				<div class="col-sm-2">
					<input style="border: none; margin-top: -5px;" type="text" readonly class="form-control" id="status" name="status" value="LENGKAP">
				</div>
			</div>
		</div>
		<br>
		<div class="form-body">
			<div class="row">
				<div class="col-sm-1">
					<label>Detail Ke</label>
					<input type="text" class="form-control" name="detail_ke" value="<?=isset($default[''])? $default[''] : ""?>">
					<br>
					<label>Penggunaan</label>
					<input type="text" class="form-control" name="penggunaan" value="<?=isset($default['KODE_GUNA'])? $default['KODE_GUNA'] : ""?>">
				</div>

				<div class="col-sm-2">
					<label>Dari</label>
					<input type="text" class="form-control" name="dari" value="<?=isset($default[''])? $default[''] : ""?>">
					<br>
					<input style="margin-top: 23px; border: none;" readonly type="text" class="form-control" name="uraian_guna" value="<?=isset($default['URAIAN_GUNA'])? $default['URAIAN_GUNA'] : ""?>">
				</div>

				<div class="col-sm-2 col-sm-offset-2">
					<label>Kategori Barang</label>
					<input type="text" class="form-control" name="kategori_barang" value="<?=isset($default['KATEGORI_BARANG'])? $default['KATEGORI_BARANG'] : ""?>">
					<br>
					<label>Kondisi Barang</label>
					<input type="text" class="form-control" name="kondisi_barang" value="<?=isset($default['KONDISI_BARANG'])? $default['KONDISI_BARANG'] : ""?>">
				</div>
					
				<div class="col-sm-3">
					<input style="margin-top: 23px; border: none;" readonly type="text" class="form-control" name="uraian_kategori" value="<?=isset($default['URAIAN_KATEGORI'])? $default['URAIAN_KATEGORI'] : ""?>">
					<br>
					<input style="margin-top: 23px; border: none;" readonly type="text" class="form-control" name="uraian_kondisi" value="<?=isset($default['URAIAN_KONDISI'])? $default['URAIAN_KONDISI'] : ""?>">
				</div>
			</div>
		</div>
		<br>
		<div class="form-body">
			<div class="row">
				<div class="col-sm-6">
					<label><b>DATA BARANG BC 2.5</b></label>
				</div>
			</div>
		</div>
		<br>
		<div class="form-body">
			<div class="row">
				<div class="col-sm-4">
					<label>Kode</label>
					<input type="text" class="form-control" name="kode" value="<?=isset($default['KODE_BARANG'])? $default['KODE_BARANG'] : ""?>">
				</div>

				<div class="col-sm-4">
					<label>Nomor HS</label>
					<input type="text" class="form-control" name="nomor_hs" value="<?=isset($default['POS_TARIF'])? $default['POS_TARIF'] : ""?>">
				</div>

				<div class="col-sm-4">
					<label>Negara Asal</label>
					<input type="text" class="form-control" name="negara_asal" value="<?=isset($default['KODE_NEGARA_ASAL'])? $default['KODE_NEGARA_ASAL'] : ""?>">
				</div>
			</div>
		</div>
		<br>
		<div class="form-body">
			<div class="row">
				<div class="col-sm-12">
					<label>Uraian Barang</label>
					<input type="text" class="form-control" name="uraian_barang" value="<?=isset($default['URAIAN'])? $default['URAIAN'] : ""?>">
				</div>
			</div>
		</div>
		<br>
		<div class="form-body">
			<div class="row">
				<div class="col-sm-3">
					<label>Tipe</label>
					<input type="text" class="form-control" name="tipe" value="<?=isset($default['TIPE'])? $default['TIPE'] : ""?>">
				</div>
				<div class="col-sm-3">
					<label>Ukuran</label>
					<input type="text" class="form-control" name="ukuran" value="<?=isset($default['UKURAN'])? $default['UKURAN'] : ""?>">
				</div>
				<div class="col-sm-3">
					<label>Spf Lain</label>
					<input type="text" class="form-control" name="spf_lain" value="<?=isset($default['SPESIFIKASI_LAIN'])? $default['SPESIFIKASI_LAIN'] : ""?>">
				</div>
				<div class="col-sm-3">
					<label>Merk</label>
					<input type="text" class="form-control" name="merk" value="<?=isset($default['MERK'])? $default['MERK'] : ""?>">
				</div>
			</div>
		</div>
		<br>
		<div class="form-body">
			<div class="row">
				<div class="col-sm-6">
					<label><b>SATUAN & HARGA</b></label>
				</div>
			</div>
		</div>
		<br>
		<div class="form-body">
			<div class="row">
				<div class="col-sm-4">
					<label>Jumlah Satuan</label>
					<input type="text" class="form-control" name="jumlah_satuan" value="<?=isset($default['JUMLAH_SATUAN'])? $default['JUMLAH_SATUAN'] : ""?>">
				</div>
				<div class="col-sm-4">
					<label>Jumlah Kemasan</label>
					<input type="text" class="form-control" name="jumlah_kemasan" value="<?=isset($default['JUMLAH_KEMASAN'])? $default['JUMLAH_KEMASAN'] : ""?>">
				</div>
				<div class="col-sm-4">
					<label>Nilai CIF</label>
					<input type="text" class="form-control" name="nilai_cif" value="<?=isset($default['CIF'])? $default['CIF'] : ""?>">
				</div>
			</div>
		</div>
		<br>
		<div class="form-body">
			<div class="row">
				<div class="col-sm-2">
					<label>Jenis Satuan</label>
					<input type="text" class="form-control" name="jenis_satuan" value="<?=isset($default['KODE_SATUAN'])? $default['KODE_SATUAN'] : ""?>">
				</div>
				<div class="col-sm-2">
					<input style="margin-top: 23px; border: none;" readonly type="text" class="form-control" name="uraian_satuan" value="<?=isset($default['URAIAN_SATUAN'])? $default['URAIAN_SATUAN'] : ""?>">
				</div>
				<div class="col-sm-2">
					<label>Jenis Kemasan</label>
					<input type="text" class="form-control" name="jenis_kemasan" value="<?=isset($default['KODE_KEMASAN'])? $default['KODE_KEMASAN'] : ""?>">
				</div>
				<div class="col-sm-2">
					<input style="margin-top: 23px; border: none;" readonly type="text" class="form-control" name="uraian_kemasan" value="<?=isset($default['URAIAN_KEMASAN'])? $default['URAIAN_KEMASAN'] : ""?>">
				</div>
				<div class="col-sm-4">
					<label>CIF Rupiah</label>
					<input type="text" class="form-control" name="cif_rupiah" value="<?=isset($default['CIF_RUPIAH'])? $default['CIF_RUPIAH'] : ""?>">
				</div>
			</div>
		</div>
		<br>
		<div class="form-body">
			<div class="row">
				<div class="col-sm-4">
					<label>Netto (Kgm)</label>
					<input type="text" class="form-control" name="netto" value="<?=isset($default['NETTO'])? $default['NETTO'] : ""?>">
				</div>
				<div class="col-sm-4">
					<label>Volume (M3)</label>
					<input type="text" class="form-control" name="volume" value="<?=isset($default['VOLUME'])? $default['VOLUME'] : ""?>">
				</div>
				<div class="col-sm-4">
					<label>Harga Penyerahan Rp</label>
					<input type="text" class="form-control" name="harga_penyerahan" value="<?=isset($default['HARGA_PENYERAHAN'])? $default['HARGA_PENYERAHAN'] : ""?>">
				</div>
			</div>
		</div>
		<br>
		<div class="form-body">
			<div class="row">
				<div class="col-sm-6">
					<label>Kode Perhitungan</label>
					<select id="kode_perhitungan" name="kode_perhitungan" class="form-control">
				        <option value="">Choose..</option>
                        
			        </select>
				</div>
			</div>
		</div>
		<br>
		<div class="form-body">
			<div class="row">
				<div class="col-sm-6">
					<label><b>TARIF & FASILITAS</b></label>
				</div>

				<div class="col-sm-6">
					<label><b>FASILITAS & SKEMA TARIF</b></label>
				</div>
			</div>
		</div>
		<br>
		<div class="form-body">
			<div class="row">
				<!-- FORM LEFT SIDE -->
				<div class="col-sm-6">
					<div class="form-group">
						<div class="row">
							<div class="col-sm-4">
								<input type="text" class="form-control" name="harga_penyerahan" value="<?=isset($default_tarifBM[''])? $default_tarifBM[''] : ""?>">
							</div>
							<div class="col-sm-4">
								<input type="hidden" class="form-control" name="id_bm" value="<?=isset($default_tarifBM['ID'])? $default_tarifBM['ID'] : ""?>">
							</div>

							<div class="col-sm-4">
								<input type="text" class="form-control" name="kode_tarif_bm" value="<?=isset($default_tarifBM['KODE_TARIF'])? $default_tarifBM['KODE_TARIF'] : ""?>">
							</div>

							<div class="col-sm-4">
								<input type="text" class="form-control" name="tarif_bm" value="<?=isset($default_tarifBM['TARIF'])? $default_tarifBM['TARIF'] : ""?>">
							</div>
						</div>
					</div>
				
					<div class="form-group">
						<div class="row">
							<div class="col-sm-8">
								<input type="text" class="form-control" name="kode_fasilitas_bm" value="<?=isset($default_tarifBM['KODE_FASILITAS'])? $default_tarifBM['KODE_FASILITAS'] : ""?>">
							</div>

							<div class="col-sm-4">
								<input type="text" class="form-control" name="tarif_fasilitas_bm" value="<?=isset($default_tarifBM['TARIF_FASILITAS'])? $default_tarifBM['TARIF_FASILITAS'] : ""?>">
							</div>
						</div>
					</div>
				
					<div class="form-group">
						<div class="row">
							
							<div class="col-sm-3">
								<label>PPN</label>
								<input type="hidden" class="form-control" name="id_ppn" value="<?=isset($default_tarifPPN['ID'])? $default_tarifPPN['ID'] : ""?>">
								<input type="text" class="form-control" name="tarif_ppn" value="<?=isset($default_tarifPPN['TARIF'])? $default_tarifPPN['TARIF'] : ""?>">
							</div>
							
							<div class="col-sm-1">
								<p style="margin-top: 30px; font-size: 15px;">%</p>
							</div>

							<div class="col-sm-4">
								<input style="margin-top: 25px;" type="text" class="form-control" name="kode_fasilitas_ppn" value="<?=isset($default_tarifPPN['KODE_FASILITAS'])? $default_tarifPPN['KODE_FASILITAS'] : ""?>">
							</div>

							<div class="col-sm-3">
								<input style="margin-top: 25px;" type="text" class="form-control" name="tarif_fasilitas_ppn" value="<?=isset($default_tarifPPN['TARIF_FASILITAS'])? $default_tarifPPN['TARIF_FASILITAS'] : ""?>">
							</div>
							
							<div class="col-sm-1">
								<p style="margin-top: 30px; font-size: 15px;">%</p>
							</div>
						</div>
					</div>
					
					<div class="form-group">
						<div class="row">
							
							<div class="col-sm-3">
								<label>PPnBM</label>
								<input type="hidden" class="form-control" name="id_ppnbm" value="<?=isset($default_tarifPPNBM['ID'])? $default_tarifPPNBM['ID'] : ""?>">
								<input type="text" class="form-control" name="tarif_ppnbm" value="<?=isset($default_tarifPPNBM['TARIF'])? $default_tarifPPNBM['TARIF'] : ""?>">
							</div>
							
							<div class="col-sm-1">
								<p style="margin-top: 30px; font-size: 15px;">%</p>
							</div>

							<div class="col-sm-4">
								<input style="margin-top: 25px;" type="text" class="form-control" name="kode_fasilitas_ppnbm" value="<?=isset($default_tarifPPNBM['KODE_FASILITAS'])? $default_tarifPPNBM['KODE_FASILITAS'] : ""?>">
							</div>

							<div class="col-sm-3">
								<input style="margin-top: 25px;" type="text" class="form-control" name="tarif_fasilitas_ppnbm" value="<?=isset($default_tarifPPNBM['TARIF_FASILITAS'])? $default_tarifPPNBM['TARIF_FASILITAS'] : ""?>">
							</div>
							
							<div class="col-sm-1">
								<p style="margin-top: 30px; font-size: 15px;">%</p>
							</div>
						</div>
					</div>
					
					<div class="form-group">
						<div class="row">
							
							<div class="col-sm-3">
								<label>PPh</label>
								<input type="hidden" class="form-control" name="id_pph" value="<?=isset($default_tarifPPH['ID'])? $default_tarifPPH['ID'] : ""?>">
								<input type="text" class="form-control" name="tarif_pph" value="<?=isset($default_tarifPPH['TARIF'])? $default_tarifPPH['TARIF'] : ""?>">
							</div>
							
							<div class="col-sm-1">
								<p style="margin-top: 30px; font-size: 15px;">%</p>
							</div>

							<div class="col-sm-4">
								<input style="margin-top: 25px;" type="text" class="form-control" name="kode_fasilitas_pph" value="<?=isset($default_tarifPPH['KODE_FASILITAS'])? $default_tarifPPH['KODE_FASILITAS'] : ""?>">
							</div>

							<div class="col-sm-3">
								<input style="margin-top: 25px;" type="text" class="form-control" name="tarif_fasilitas_pph" value="<?=isset($default_tarifPPH['TARIF_FASILITAS'])? $default_tarifPPH['TARIF_FASILITAS'] : ""?>">
							</div>
							
							<div class="col-sm-1">
								<p style="margin-top: 30px; font-size: 15px;">%</p>
							</div>
						</div>
					</div>

					<div class="form-group">
						<div class="row">
							<div class="col-sm-12">
								<label>Cukai</label>
								<input type="hidden" class="form-control" name="id_cukai" value="<?=isset($default_tarifcukai['ID'])? $default_tarifcukai['ID'] : ""?>">
								<input type="text" class="form-control" name="kode_komoditi_cukai" value="<?=isset($default_tarifcukai['KODE_KOMODITI_CUKAI'])? $default_tarifcukai['KODE_KOMODITI_CUKAI'] : ""?>">
							</div>
						</div>
					</div>

					<div class="form-group">
						<div class="row">
							
							<div class="col-sm-4">
								<input style="margin-top: 30px;" type="text" class="form-control" name="kode_tarif_cukai" value="<?=isset($default_tarifcukai['KODE_TARIF'])? $default_tarifcukai['KODE_TARIF'] : ""?>">
							</div>

							<div class="col-sm-4">
								<input style="margin-top: 30px;" type="text" class="form-control" name="tarif_cukai" value="<?=isset($default_tarifcukai['TARIF'])? $default_tarifcukai['TARIF'] : ""?>">
							</div>

							<div class="col-sm-1">
								<p style="margin-top: 30px; font-size: 15px;">%</p>
							</div>

							<div class="col-sm-3">
								<input style="margin-top: 30px;" type="text" class="form-control" name="kode_satuan_cukai" value="<?=isset($default_tarifcukai['KODE_SATUAN'])? $default_tarifcukai['KODE_SATUAN'] : ""?>">
							</div>
						</div>
					</div>

					<div class="form-group">
						<div class="row">
							
							<div class="col-sm-4">
								<label>Jumlah Satuan</label>
								<input type="text" class="form-control" name="jumlah_satuan_cukai" value="<?=isset($default_tarifcukai['JUMLAH_SATUAN'])? $default_tarifcukai['JUMLAH_SATUAN'] : ""?>">
							</div>

							<div class="col-sm-4">
								<input style="margin-top: 25px;" type="text" class="form-control" name="kode_fasilitas_cukai" value="<?=isset($default_tarifcukai['KODE_FASILITAS'])? $default_tarifcukai['KODE_FASILITAS'] : ""?>">
							</div>

							<div class="col-sm-3">
								<input style="margin-top: 25px;" type="text" class="form-control" name="tarif_fasilitas_cukai" value="<?=isset($default_tarifcukai['TARIF_FASILITAS'])? $default_tarifcukai['TARIF_FASILITAS'] : ""?>">
							</div>

							<div class="col-sm-1">
								<p style="margin-top: 30px; font-size: 15px;">%</p>
							</div>
						</div>
					</div>
					

				</div>
				<!-- TUTUP FORM LEFT SIDE -->
				
				<!-- FORM RIGHT SIDE -->
				<div class="col-sm-6">
					<div class="form-group">
						<div class="row">
							<div class="col-sm-6">
								<label>Fasilitas</label>
								<input type="text" class="form-control" name="harga_penyerahan" value="<?=isset($default[''])? $default[''] : ""?>">
							</div>

							<div class="col-sm-6">
								<label>Skm Trf</label>
								<input type="text" class="form-control" name="harga_penyerahan" value="<?=isset($default[''])? $default[''] : ""?>">
							</div>
						</div>
					</div>

					<div class="form-group">
						<div class="row">
							<div class="col-sm-12">
								<table class="table table-sm">
								  <thead>
								    <tr class="bg-success">
								      <th scope="col">Jenis</th>
								      <th scope="col">Nomor</th>
								      <th scope="col">Tanggal</th>
								    </tr>
								  </thead>
								  <tbody>
								    
								    <tr>
								      <th></th>
								      <td></td>
								      <td></td>
								    </tr>
								    
						  		  </tbody>
				 				</table>
							</div>	
						</div>
					</div>

					<div class="form-group">
						<div class="row">
							<div class="col-sm-12">
								<label>Jumlah Bahan Baku</label>
								<input type="text" class="form-control" name="jumlah_bahan_baku" value="<?=isset($default['JUMLAH_BAHAN_BAKU'])? $default['JUMLAH_BAHAN_BAKU'] : ""?>">
							</div>
						</div>
					</div>
				</div>
				<!-- TUTUP FORM RIGHT SIDE -->
			</div>
		</div>
		<div class="form-actions">
            <!-- <a href='<?php echo base_url('Bc25_detail_controller');?>' class='btn default'> Cancel</a> -->
            <button type="submit" class="btn blue" name="submit_detail">Simpan</button>
        </div>

	</form>

	</div>
</div>