<?php

// memanggil library FPDF
//require_once(APPPATH.'assets\global\plugins\fpdf\fpdf.php');
require_once(APPPATH.'libraries/fpdf/fpdf.php');
// intance object dan memberikan pengaturan halaman PDF
$pdf = new FPDF('l','mm','A5');
// membuat halaman baru
$pdf->AddPage();
// setting jenis font yang akan digunakan
$pdf->SetFont('Arial','B',12);
// mencetak string
$pdf->Cell(190,7,'LAPORAN PEMASUKAN BARANG PER DOKUMEN PABEAN',0,1,'C');
$pdf->SetFont('Arial','B',12);
$pdf->Cell(190,7,'TARINDO UNIMETAL UTAMA, PT',0,1,'C');
$pdf->SetFont('Arial','B',10);
$pdf->Cell(190,7,'PERIODE',0,1,'C');

// Memberikan space kebawah agar tidak terlalu rapat
$pdf->Cell(10,7,'',0,1);

$pdf->SetFont('Arial','B',10);
$pdf->Cell(20,6,'BC',1,0);
$pdf->Cell(85,6,'BC NO',1,0);
$pdf->Cell(27,6,'BC DATE',1,0);
$pdf->Cell(85,6,'Receive NO',1,0);
$pdf->Cell(27,6,'Receive DATE',1,0);
$pdf->Cell(27,6,'SUPPLIER',1,0);
$pdf->Cell(27,6,'CODE',1,0);
$pdf->Cell(27,6,'NAMA',1,0);
$pdf->Cell(27,6,'SAT',1,0);
$pdf->Cell(27,6,'QTY',1,0);
$pdf->Cell(27,6,'VAL',1,0);
$pdf->Cell(27,6,'JML',1,0);

$pdf->SetFont('Arial','',10);

$this->mysql = $this ->load -> database('mysql', TRUE);

//$awal = helper_tanggalinsert($_GET['tglawal']);
//$akhir = helper_tanggalinsert($_GET['tglakhir']);

$awal = '2019-01-01';
$akhir = '2019-09-09';
$sql = $this->mysql->query("select h.kode_dokumen_pabean, h.nomor_daftar, h.tanggal_daftar, h.nomor_bc11, h.tanggal_aju, h.nama_pemasok, b.uraian, b.jumlah_satuan, b.harga_invoice, b.kode_satuan, b.kode_barang
                          from tpb_header h inner join tpb_barang b on h.id = b.id_header where h.tanggal_aju between '$awal' and '$akhir'");
foreach($sql->result_array() as $row){
    $pdf->Cell(20,6,$row['kode_dokumen_pabean'],1,0);
    $pdf->Cell(85,6,$row['nomor_bc11'],1,0);
    $pdf->Cell(27,6,date('d-m-y',strtotime($row['tanggal_aju'])),1,0);
    $pdf->Cell(25,6,$row['nomor_daftar'],1,1);
    $pdf->Cell(27,6,$row['tanggal_daftar'],1,0);
    $pdf->Cell(27,6,$row['nama_pemasok'],1,0);
    $pdf->Cell(27,6,$row['kode_barang'],1,0);
    $pdf->Cell(27,6,$row['uraian'],1,0);
    $pdf->Cell(27,6,$row['kode_satuan'],1,0);
    $pdf->Cell(27,6,$row['jumlah_satuan'],1,0);
    $pdf->Cell(27,6,'USD',1,0);
    $pdf->Cell(27,6,$row['harga_invoice'],1,0);
}

$pdf->Output();
?>
