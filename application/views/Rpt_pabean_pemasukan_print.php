<?php
$awal = $_GET['tglawal'];
$tgl_awal = substr($awal,8,2);
$bulan_awal = substr($awal,5,2);
$tahun_awal = substr($awal,0,4);

$bln_awal = $bulan_awal * 1;

$akhir = $_GET['tglakhir'];
$tgl_akhir = substr($akhir,8,2);
$bulan_akhir = substr($akhir,5,2);
$tahun_akhir = substr($akhir,0,4);

$bln_akhir = $bulan_akhir * 1;

if ($bln_awal == 1){
	$spell_bulan_awal = "Januari";
}else if($bln_awal == 2){
	$spell_bulan_awal = "Februari";
}else if($bln_awal == 3){
	$spell_bulan_awal = "Maret";
}else if($bln_awal == 4){
	$spell_bulan_awal = "April";
}else if($bln_awal == 5){
	$spell_bulan_awal = "Mei";
}else if($bln_awal == 6){
	$spell_bulan_awal = "Juni";
}else if($bln_awal == 7){
	$spell_bulan_awal = "Juli";
}else if($bln_awal == 8){
	$spell_bulan_awal = "Agustus";
}else if($bln_awal == 9){
	$spell_bulan_awal = "September";
}else if($bln_awal == 10){
	$spell_bulan_awal = "Oktober";
}else if($bln_awal == 11){
	$spell_bulan_awal = "November";
}else if($bln_awal == 12){
	$spell_bulan_awal = "Desember";
}



if ($bln_akhir == 1){
	$spell_bulan_akhir = "Januari";
}else if($bln_akhir == 2){
	$spell_bulan_akhir = "Februari";
}else if($bln_akhir == 3){
	$spell_bulan_akhir = "Maret";
}else if($bln_akhir == 4){
	$spell_bulan_akhir = "April";
}else if($bln_akhir == 5){
	$spell_bulan_akhir = "Mei";
}else if($bln_akhir == 6){
	$spell_bulan_akhir = "Juni";
}else if($bln_akhir == 7){
	$spell_bulan_akhir = "Juli";
}else if($bln_akhir == 8){
	$spell_bulan_akhir = "Agustus";
}else if($bln_akhir == 9){
	$spell_bulan_akhir = "September";
}else if($bln_akhir == 10){
	$spell_bulan_akhir = "Oktober";
}else if($bln_akhir == 11){
	$spell_bulan_akhir = "November";
}else if($bln_akhir == 12){
	$spell_bulan_akhir = "Desember";
}


// intance object dan memberikan pengaturan halaman PDF
$pdf = new FPDF('l','mm','A4');


// membuat halaman baru
$pdf->AddPage();
// setting jenis font yang akan digunakan
$pdf->SetFont('Arial','B',12);
// mencetak string
$pdf->Cell(270,6,'LAPORAN PEMASUKAN BARANG PER DOKUMEN PABEAN',0,1,'C');
$pdf->SetFont('Arial','B',12);
$pdf->Cell(270,6,'TARINDO UNIMETAL UTAMA, PT',0,1,'C');
$pdf->SetFont('Arial','B',12);
$pdf->Cell(270,6,'PERIODE '.$tgl_awal.'-'.$spell_bulan_awal.'-'.$tahun_awal.' S.D '.$tgl_akhir.'-'.$spell_bulan_akhir.'-'.$tahun_akhir,0,1,'C');

// Memberikan space kebawah agar tidak terlalu rapat
$pdf->Cell(10,7,'',0,1);

$pdf->SetFont('Arial','B',8);
$pdf->Cell(10,12,'NO',1,0, 'C');
$pdf->Cell(47,6,'Dokumen Pabean',1,0,'C');
$pdf->Cell(40,6,'Bukti Penerimaan Barang',1,0, 'C');
$pdf->Cell(60,12,'Supplier',1,0, 'C');
$pdf->Cell(20,12,'Kode Barang',1,0, 'C');
$pdf->Cell(35,12,'Uraian Barang',1,0, 'C');
$pdf->Cell(10,12,'Sat',1,0, 'C');
$pdf->Cell(20,12,'Jml',1,0, 'C');
$pdf->Cell(10,12,'Valas',1,0, 'C');
$pdf->Cell(20,12,'Nilai Barang',1,0, 'C');
$pdf->Cell(5,6,'',0,0);
$pdf->Ln();
$pdf->Cell(10,0,' ',0,0);
$pdf->Cell(15,6,'Jenis',1,0, 'C');
$pdf->Cell(12,6,'Nomor',1,0, 'C');
$pdf->Cell(20,6,'Tanggal',1,0, 'C');
$pdf->Cell(20,6,'Nomor',1,0, 'C');
$pdf->Cell(20,6,'Tanggal',1,0, 'C');

$pdf->Cell(10,10,'',0,1);

$pdf->SetFont('Arial','',7);

$this->mysql = $this ->load -> database('mysql', TRUE);

$sql_received = $this->db->query("SELECT tpb_header_id, nomor_aju, nomor_received, tanggal_received FROM public.beone_received_import WHERE tanggal_received between '$awal' and '$akhir' ORDER BY tanggal_received ASC");
$no = 0;
foreach($sql_received->result_array() as $row_received){

$no_received = $row_received['nomor_received'];
$tgl_received = $row_received['tanggal_received'];

$sql = $this->mysql->query("select h.id, h.nomor_aju, h.kode_dokumen_pabean, h.nomor_daftar, h.tanggal_daftar, h.nomor_bc11, h.tanggal_aju, h.nama_pemasok, h.nama_pengirim, b.uraian, b.jumlah_satuan, b.harga_invoice, b.harga_penyerahan, b.kode_satuan, b.kode_barang
                          from tpb_header h inner join tpb_barang b on h.id = b.id_header where h.id = ".intval($row_received['tpb_header_id']));

//$no = 0;
$y = 0;
foreach($sql->result_array() as $row){
		$no = $no + 1;

		/*$sql_received = $this->db->query("SELECT nomor_received, tanggal_received FROM public.beone_received_import WHERE nomor_aju = '$row[nomor_aju]'");
		$hasil_received = $sql_received->row_array();
		$no_received = $hasil_received['nomor_received'];
		$tgl_received = $hasil_received['tanggal_received'];*/

		/*$detail_item = $this->db->query("SELECT * FROM public.beone_item WHERE item_code = '$row[kode_barang]'");
		$hasil_item = $detail_item->row_array();
		$hasil_item_id = $hasil_item['item_id'];*/

		$detail_qty = $this->db->query("SELECT * FROM public.beone_inventory WHERE intvent_trans_no = '$row_received[nomor_aju]' AND keterangan = 'IMPOR'");
		$hasil_qty = $detail_qty->row_array();
		$hasil_qty_terima = $hasil_qty['qty_in'];

		if($row['kode_dokumen_pabean'] == '23'){
		$harga_invoice = $row['harga_invoice'] * 1;
		}else{
			$harga_invoice = $row['harga_penyerahan'] * 1;
		}
		$jml_satuan = $row['jumlah_satuan'] * 1;

		$cellWidth=60; //lebar sel
		$cellWidthUraian=35;
		$cellHeight=4; //tinggi sel satu baris normal
		$over = 0;

		if($pdf->GetStringWidth($row['nama_pemasok']) < $cellWidth AND $pdf->GetStringWidth($row['uraian']) < $cellWidthUraian){
			$line=1;		//tidak melakukan apa2
			$over=1;		//isi terserah asal terisi
		}else if($pdf->GetStringWidth($row['uraian']) > $cellWidthUraian){
			$textLength=strlen($row['uraian']);	//total panjang teks
			$errMargin=5;		//margin kesalahan lebar sel, untuk jaga-jaga
			$startChar=0;		//posisi awal karakter untuk setiap baris
			$maxChar=0;			//karakter maksimum dalam satu baris, yang akan ditambahkan nanti
			$textArray=array();	//untuk menampung data untuk setiap baris
			$tmpString="";		//untuk menampung teks untuk setiap baris (sementara)

				while($startChar < $textLength){ //perulangan sampai akhir teks
					//perulangan sampai karakter maksimum tercapai
					while(
					$pdf->GetStringWidth( $tmpString ) < ($cellWidthUraian-$errMargin) &&
					($startChar+$maxChar) < $textLength ) {
						$maxChar++;
						$tmpString=substr($row['uraian'],$startChar,$maxChar);
					}
					//pindahkan ke baris berikutnya
					$startChar=$startChar+$maxChar;
					//kemudian tambahkan ke dalam array sehingga kita tahu berapa banyak baris yang dibutuhkan
					array_push($textArray,$tmpString);
					//reset variabel penampung
					$maxChar=0;
					$tmpString='';

				}
				//dapatkan jumlah baris
				$line=count($textArray);
				$over = 1; //uraian yg over
		}else if($pdf->GetStringWidth($row['nama_pemasok']) > $cellWidth){
			$textLength=strlen($row['nama_pemasok']);	//total panjang teks
			$errMargin=5;		//margin kesalahan lebar sel, untuk jaga-jaga
			$startChar=0;		//posisi awal karakter untuk setiap baris
			$maxChar=0;			//karakter maksimum dalam satu baris, yang akan ditambahkan nanti
			$textArray=array();	//untuk menampung data untuk setiap baris
			$tmpString="";		//untuk menampung teks untuk setiap baris (sementara)

				while($startChar < $textLength){ //perulangan sampai akhir teks
					//perulangan sampai karakter maksimum tercapai
					while(
					$pdf->GetStringWidth( $tmpString ) < ($cellWidth-$errMargin) &&
					($startChar+$maxChar) < $textLength ) {
						$maxChar++;
						$tmpString=substr($row['nama_pemasok'],$startChar,$maxChar);
					}
					//pindahkan ke baris berikutnya
					$startChar=$startChar+$maxChar;
					//kemudian tambahkan ke dalam array sehingga kita tahu berapa banyak baris yang dibutuhkan
					array_push($textArray,$tmpString);
					//reset variabel penampung
					$maxChar=0;
					$tmpString='';

				}
				//dapatkan jumlah baris
				$line=count($textArray);
				$over = 2; //pemasok yg over
		}


		if ($over == 1){// uraian yg over
			$pdf->Cell(10,($line * $cellHeight),$no,1,0,'C');
	    $pdf->Cell(15,($line * $cellHeight),'BC '.$row['kode_dokumen_pabean'],1,0);
	    $pdf->Cell(12,($line * $cellHeight),$row['nomor_daftar'],1,0);
	    $pdf->Cell(20,($line * $cellHeight),date('d-m-y',strtotime($row['tanggal_aju'])),1,0);
	    $pdf->Cell(20,($line * $cellHeight),$no_received,1,0);
	    $pdf->Cell(20,($line * $cellHeight),date('d-m-y',strtotime($tgl_received)),1,0);

			if($row['kode_dokumen_pabean'] == '23'){
						$pdf->Cell(60,($line * $cellHeight),$row['nama_pemasok'],1,0);
			}else{
						$pdf->Cell(60,($line * $cellHeight),$row['nama_pengirim'],1,0);
			}

			$pdf->Cell(20,($line * $cellHeight),$row['kode_barang'],1,0);
			$xPos=$pdf->GetX();
			$yPos=$pdf->GetY();
			$pdf->MultiCell($cellWidthUraian,$cellHeight,$row['uraian'],1, 'L');
			$pdf->SetXY($xPos + $cellWidthUraian , $yPos);
	    $pdf->Cell(10,($line * $cellHeight),$row['kode_satuan'],1,0);
	    $pdf->Cell(20,($line * $cellHeight),number_format($hasil_qty_terima, 4),1,0, 'R');

			if($row['kode_dokumen_pabean'] == '23'){
	    $pdf->Cell(10,($line * $cellHeight),'USD',1,0);
			}else{
			$pdf->Cell(10,($line * $cellHeight),'IDR',1,0);
			}

	    $pdf->Cell(20,($line * $cellHeight),number_format($harga_invoice, 4),1,0, 'R');
		}else if ($over == 2){// pemasok yg over
			$pdf->Cell(10,($line * $cellHeight),$no,1,0,'C');
	    $pdf->Cell(15,($line * $cellHeight),'BC '.$row['kode_dokumen_pabean'],1,0);
	    $pdf->Cell(12,($line * $cellHeight),$row['nomor_daftar'],1,0);
	    $pdf->Cell(20,($line * $cellHeight),date('d-m-y',strtotime($row['tanggal_aju'])),1,0);
	    $pdf->Cell(20,($line * $cellHeight),$no_received,1,0);
	    $pdf->Cell(20,($line * $cellHeight),date('d-m-y',strtotime($tgl_received)),1,0);
			$xPos=$pdf->GetX();
			$yPos=$pdf->GetY();

			if($row['kode_dokumen_pabean'] == '23'){
				$pdf->MultiCell($cellWidth,$cellHeight,$row['nama_pemasok'],1, 'L');
			}else{
				$pdf->MultiCell($cellWidth,$cellHeight,$row['nama_pengirim'],1, 'L');
			}

			$pdf->SetXY($xPos + $cellWidth , $yPos);
	    $pdf->Cell(20,($line * $cellHeight),$row['kode_barang'],1,0);
			$pdf->Cell(35,($line * $cellHeight),$row['uraian'],1,0);
	    $pdf->Cell(10,($line * $cellHeight),$row['kode_satuan'],1,0);
	    $pdf->Cell(20,($line * $cellHeight),number_format($hasil_qty_terima, 4),1,0, 'R');
			if($row['kode_dokumen_pabean'] == '23'){
	    $pdf->Cell(10,($line * $cellHeight),'USD',1,0);
			}else{
			$pdf->Cell(10,($line * $cellHeight),'IDR',1,0);
			}
	    $pdf->Cell(20,($line * $cellHeight),number_format($harga_invoice, 4),1,0, 'R');
		}
    $pdf->Ln();
		$y = $y + $cellHeight;


		if ($y >= 110){
			$pdf->AddPage();
			$pdf->SetFont('Arial','B',8);
			$pdf->Cell(10,12,'NO',1,0, 'C');
			$pdf->Cell(47,6,'Dokumen Pabean',1,0,'C');
			$pdf->Cell(40,6,'Bukti Penerimaan Barang',1,0, 'C');
			$pdf->Cell(60,12,'Supplier',1,0, 'C');
			$pdf->Cell(20,12,'Kode Barang',1,0, 'C');
			$pdf->Cell(35,12,'Uraian Barang',1,0, 'C');
			$pdf->Cell(10,12,'Sat',1,0, 'C');
			$pdf->Cell(20,12,'Jml',1,0, 'C');
			$pdf->Cell(10,12,'Valas',1,0, 'C');
			$pdf->Cell(20,12,'Nilai Barang',1,0, 'C');
			$pdf->Cell(5,6,'',0,0);
			$pdf->Ln();
			$pdf->Cell(10,0,' ',0,0);
			$pdf->Cell(15,6,'Jenis',1,0, 'C');
			$pdf->Cell(12,6,'Nomor',1,0, 'C');
			$pdf->Cell(20,6,'Tanggal',1,0, 'C');
			$pdf->Cell(20,6,'Nomor',1,0, 'C');
			$pdf->Cell(20,6,'Tanggal',1,0, 'C');

			$pdf->Cell(10,10,'',0,1);

			$pdf->SetFont('Arial','',7);
			$y = 0;
		}
}
}

date_default_timezone_set('Asia/Jakarta');
$jam=date("H_i_s");
$pdf->Output('','MASUK_PERIODE_'.$tgl_awal.'-'.$spell_bulan_awal.'-'.$tahun_awal.'_S.D_'.$tgl_akhir.'-'.$spell_bulan_akhir.'-'.$tahun_akhir.'_'.$jam.'.pdf');
?>
