<!-- BEGIN PAGE TITLE-->
<!-- END PAGE TITLE-->
<!-- END PAGE HEADER-->
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js"></script>
<?php $list_item = $this->db->query("SELECT * FROM public.beone_item");?>

<div class="portlet box blue ">
      <div class="portlet-title">
          <div class="caption">
              <i class="fa fa-gift"></i> Form Import Manual</div>
          <div class="tools">
              <a href="" class="collapse"> </a>
              <a href="#portlet-config" data-toggle="modal" class="config"> </a>
              <a href="" class="reload"> </a>
              <a href="" class="remove"> </a>
          </div>
      </div>
      <div class="portlet-body form">
          <form role="form" method="post">
              <div class="form-body">

                <div class="row">
                  <div class="col-sm-4">
                    <div class="form-group">
                        <label>Nomor Aju</label>
                        <input type="text" class="form-control" placeholder="Nomor Aju" id="nomor_aju" name="nomor_aju" required>
                      </div>
                  </div>

                    <div class="col-sm-2">
                      <div class="form-group">
                          <label>Nomor Daftar</label>
                          <input type="text" class="form-control" placeholder="Nomor Daftar" id="nomor_daftar" name="nomor_daftar" required>
                        </div>
                    </div>

                  <div class="col-sm-4">
                    <div class="form-group">
                          <label>Tanggal Aju</label>
                          <div class="input-group">
                            <span class="input-group-addon input-circle-left">
                                <i class="fa fa-calendar"></i>
                            </span>
                            <input class="form-control form-control-inline input-medium date-picker input-circle-right" size="16" type="text" name="tanggal" value="<?php echo date('m/d/Y');?>" readonly required/>
                            <span class="help-block"></span>
                          </div>
                    </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-sm-4">
                      <div class="form-group">
                            <label>Supplier</label>
                            <div class="input-group">
                               <select id="select2-single-input-sm" class="form-control input-sm select2-multiple" name="supplier" required>
                                 <?php
                                   $supplier = $this->db->query("SELECT * FROM public.beone_custsup WHERE custsup_id =".intval($default['supplier_id']));
                                   $hasil_supplier = $supplier->row_array();

                                   foreach($list_supplier as $row){
                                   ?>
                                    <option value='<?php echo $row['custsup_id'];?>'><?php echo $row['nama'];?></opiton>
                                    <?php
                                    }
                                    ?>
                               </select>
                         </div>
                       </div>
                    </div>

                      <div class="col-sm-2">
                        <div class="form-group">
                              <label>Kode Dokumen Pabean</label>
                              <div class="input-group">
                                 <select id="select2-single-input-sm" class="form-control input-sm select2-multiple" name="kode_dokumen_pabean" required>
                                     <option value='23'>BC 23</opiton>
                                     <option value='40'>BC 40</opiton>
                                 </select>
                           </div>
                         </div>
                      </div>

                      <div class="col-sm-2">
                        <div class="form-group">
                              <label>Currency</label>
                              <div class="input-group">
                                 <select id="select2-single-input-sm" class="form-control input-sm select2-multiple" name="currency" required>
                                     <option value='1'>USD</opiton>
                                     <option value='2'>IDR</opiton>
                                 </select>
                           </div>
                         </div>
                      </div>

                      <div class="col-sm-2">
                      </div>

                    </div>


                    <div class="row">
                      <div class="col-sm-2">
                          <div class="form-group">
                              <label>Berat</label>
                              <input type="text" class="form-control" placeholder="Total Berat" id="berat" name="berat" required>
                            </div>
                        </div>

                        <div class="col-sm-2">
                            <div class="form-group">
                                <label>Amount</label>
                                <input type="text" class="form-control" placeholder="Sub Total" name="subtotal" id="subtotal" value="0" readonly>
                              </div>
                          </div>

                          <div class="col-sm-2">
                              <div class="form-group">
                                  <label>Kurs</label>
                                  <input type="text" class="form-control" placeholder="Kurs" id="kurs" name="kurs" required>
                                </div>
                            </div>
                      </div>



                    <hr / style="border-color: #3598DC;">
                    <table  id="datatable" class="table striped hovered cell-hovered">
            						<thead>
            							<tr>
                            <td width="5%">ID</td>
            								<td width="20%">Item</td>
            								<td width="15%">Qty</td>
            								<td width="15%">Price</td>
            								<td width="15%">Amount</td>
                            <td width="5%"></td>
            							</tr>
            						 </thead>
                         <tbody id="container">

            						</tbody>
            				</table>

        <div class="row">
          <div class="col-sm-4"></div>
          <div class="col-sm-4"></div>
          <div class="col-sm-4">
            <input type="hidden" class="form-control" placeholder="Sub Total" name="subtotal_backup" id="subtotal_backup" value="0" readonly>
          </div>
        </div>


              </div>
              <div class="form-actions">
                <a class="btn blue" data-toggle="modal" href="#responsive"> Tambah Data </a>
                <button type="submit" class="btn red" name="submit_import">Submit</button>
              </div>
          </form>
  </div>
</div>


<!--------------------------- MODAL ADD ITEM--------------------------------------------->
<div id="responsive" class="modal fade" tabindex="-1" data-width="760">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">Detail Item</h4>
    </div>
    <div class="modal-body">
        <div class="row">
            <div class="col-md-2"></div>
            <div class="col-md-8">
            <div class="form-group">
                <label>Item</label>
                <select id='item_modal' class='form-control input-sm select2-multiple' name='item_modal'>
                  <option value=0><?php echo " - Pilih Item - ";?></option>
                  <?php  foreach($list_item->result_array() as $row){ echo '<option value='.$row['item_id'].'>'.$row['nama']." (".$row['item_code'].")".'</option>';} ?>
                </select>
            </div>
            </div>
            <div class="col-md-2"></div>
        </div>

        <div class="row">
            <div class="col-md-2"></div>
            <div class="col-md-2">
                <div class="form-group">
                <label>Quantity</label>
                <input type='text' class='form-control' placeholder="Quantity" name='qty_modal' id='qty_modal' onchange="autoAmount()" required>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                <label>Price</label>
                <input type='text' class='form-control' placeholder="Price" name='price_modal' id='price_modal' onchange="autoAmount()" required>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                <label>Amount</label>
                <input type='text' class='form-control' placeholder="Amount" name='amount_modal' id='amount_modal' required>
                </div>
            </div>
            <div class="col-md-2"></div>
        </div>

    </div>
    <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn btn-outline dark">Close</button>
        <button type="button" class="btn green" name="add_btn" id="add_btn">Insert</button>
    </div>
</div>


<script>
$(document).ready(function() {
    		var count = 0;

    		$("#add_btn").click(function(){
					count += 1;

          var item = document.getElementById('item_modal');
          var qty = document.getElementById('qty_modal');
          var price = document.getElementById('price_modal');
          var amount = document.getElementById('amount_modal');
          var namaItem = $('#item_modal option:selected').text();

		   		$('#container').append(
							 '<tr class="records" id="'+count+'">'
						 + '<td><input class="form-control" id="item_id_' + count + '" name="item_id_'+count+'" type="text" value="'+item.value+'" readonly></td>'
						 + '<td><input class="form-control" id="item_' + count + '" name="item_'+count+'" type="text" value="'+namaItem+'" readonly></td>'
						 + '<td><input class="form-control" id="qty_' + count + '" name="qty_'+count+'" type="text" value="'+qty.value+'" readonly></td>'
						 + '<td><input class="form-control" id="price_' + count + '" name="price_'+count+'" type="text" value="'+price.value+'" readonly></td>'
             + '<td><input class="form-control" id="amount_' + count + '" name="amount_'+count+'" type="text" value="'+amount.value+'" readonly></td>'
             + '<td><input id="rows_' + count + '" name="rows[]" value="'+ count +'" type="hidden"></td>'
						 + '<td><button type="button" class="btn red" onclick="hapus('+count+')">X</button></td></tr>'
					);

          autoSubtotal();
          eraseText();
          $('#responsive').modal('hide');
				});

				$(".remove_item").live('click', function (ev) {
    			if (ev.type == 'click') {
	        	$(this).parents(".records").fadeOut();
	        	$(this).parents(".records").remove();
        	}
     		});
		});

    /*function hapus(x){
            alert(x);
            document.getElementById('datatable').deleteRow(x);
    }*/


    function eraseText() {
     document.getElementById("item_modal").value = "";
     document.getElementById("qty_modal").value = "";
     document.getElementById("price_modal").value = "";
     document.getElementById("amount_modal").value = "";
    }


    function hapus(rowid)
    {
        autominSubtotal("amount_"+rowid);
        var row = document.getElementById(rowid);
        row.parentNode.removeChild(row);

        cekppn1();
        cekGrandTotal();
        //var amnt = document.getElementById("amount_"+rowid).value;
    }
</script>

<script type="text/javascript">
var berat = document.getElementById('berat');
  berat.addEventListener('keyup', function(e){
  berat.value = formatRupiah(this.value, 'Rp. ');
});

var kurs = document.getElementById('kurs');
  kurs.addEventListener('keyup', function(e){
  kurs.value = formatRupiah(this.value, 'Rp. ');
});

var qty_modal = document.getElementById('qty_modal');
  qty_modal.addEventListener('keyup', function(e){
  qty_modal.value = formatRupiah(this.value, 'Rp. ');
});

var price_modal = document.getElementById('price_modal');
  price_modal.addEventListener('keyup', function(e){
  price_modal.value = formatRupiah(this.value, 'Rp. ');
});

var amount_modal = document.getElementById('amount_modal');
  amount_modal.addEventListener('keyup', function(e){
  amount_modal.value = formatRupiah(this.value, 'Rp. ');
});

var subtotal = document.getElementById('subtotal');
  subtotal.addEventListener('keyup', function(e){
  subtotal.value = formatRupiah(this.value, 'Rp. ');
});

var grandtotal = document.getElementById('grandtotal');
  grandtotal.addEventListener('keyup', function(e){
  grandtotal.value = formatRupiah(this.value, 'Rp. ');
});



/* Fungsi formatRupiah */
function formatRupiah(angka, prefix){
  var number_string = angka.replace(/[^,\d]/g, '').toString(),
  split   		= number_string.split(','),
  sisa     		= split[0].length % 3,
  rupiah     		= split[0].substr(0, sisa),
  ribuan     		= split[0].substr(sisa).match(/\d{3}/gi);

  // tambahkan titik jika yang di input sudah menjadi angka ribuan
  if(ribuan){
    separator = sisa ? '.' : '';
    rupiah += separator + ribuan.join('.');
  }

  rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
  return prefix == undefined ? rupiah : (rupiah ? rupiah : '');
}
</script>

<script>
    function autoAmount(){
      var a = document.getElementById('qty_modal').value;
      var b = document.getElementById('price_modal').value;

      var c = a.split('.').join('');
      var d = b.split('.').join('');
      document.getElementById('amount_modal').value = c * d;
    }
</script>


<script>
    function autoSubtotal(){
      var a = document.getElementById('subtotal').value;
      var b = document.getElementById('amount_modal').value;

      var c = a.split('.').join('');
      var d = b.split('.').join('');

      document.getElementById('subtotal').value = (c*1) + (d*1);
      document.getElementById('subtotal_backup').value = (c*1) + (d*1);
    }

    function autominSubtotal(amnt){
      var a = document.getElementById('subtotal').value;
      var b = document.getElementById(amnt).value;

      var c = a.split('.').join('');
      var d = b.split('.').join('');
      document.getElementById('subtotal').value = (c*1) - (d*1);
      document.getElementById('subtotal_backup').value = (c*1) - (d*1);
    }
</script>
