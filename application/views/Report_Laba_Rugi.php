<?php
  $tgl_awal = $_GET['tglawal'];
  $tgl_akhir = $_GET['tglakhir'];

  $thn_awal = substr($tgl_awal,6,4);
  $bln_awal = substr($tgl_awal,0,2);
  $day_awal = substr($tgl_awal,3,2);
  $tgl_awal_formated = $thn_awal."-".$bln_awal."-".$day_awal;

  $thn_akhir = substr($tgl_akhir,6,4);
  $bln_akhir = substr($tgl_akhir,0,2);
  $day_akhir = substr($tgl_akhir,3,2);
  $tgl_akhir_formated = $thn_akhir."-".$bln_akhir."-".$day_akhir;
?>

          <h2>
              <center><b>Report Laba Rugi</b></center>
          </h2>
          <h4>
              <center>Periode <?php echo $day_awal."/".$bln_awal."/".$thn_awal;?> sampai <?php echo $day_akhir."/".$bln_akhir."/".$thn_akhir;?></center>
          </h4>
          <hr />

              <div class="row">
                  <div class="col-xs-12">
                      <table class="table table-striped table-hover">
                          <thead>
                              <tr>
                                  <th><b>KETERANGAN</b></th>
                                  <th><b>PERIODE</b></th>
                              </tr>
                          </thead>

                          <tbody>
                                  <td><b>PENJUALAN</b></td>
                                  <td></td>
                            <?php
/************************************* PENJUALAN ********************************************************/
                            $total_penjualan = 0;
                            foreach($list_coa_penjualan as $row){

                                //mencari rumus posisi debet kredit dari tipe coa
                                $sql_posisi_dk = $this->db->query("SELECT * FROM public.beone_coa WHERE coa_id =".intval($row['coa_id']));
                                $hasil_posisi_dk = $sql_posisi_dk->row_array();

                                //mengambil nilai total debit kredit Akun PENJUALAN
                                $sql_total_mutasi_dk = $this->db->query("SELECT SUM(debet) as totmdebet, SUM(kredit) as totmkredit FROM public.beone_gl WHERE gl_date BETWEEN '$tgl_awal_formated' AND '$tgl_akhir_formated' AND coa_id =".intval($row['coa_id']));
                                $hasil_total_mutasi_dk = $sql_total_mutasi_dk->row_array();

                                if ($hasil_posisi_dk['dk'] == "D"){
                                      $saldo = $hasil_total_mutasi_dk['totmdebet'] - $hasil_total_mutasi_dk['totmkredit'];
                                }elseif($hasil_posisi_dk['dk'] == "K"){
                                    $saldo = $hasil_total_mutasi_dk['totmkredit'] - $hasil_total_mutasi_dk['totmdebet'];
                                }
                            ?>
                              <tr>
                                  <td> <?php echo $row['nama'];?> </td>
                                  <td> <?php echo number_format($saldo,2);?> </td>
                              </tr>
                            <?php
                                $total_penjualan = $total_penjualan + $saldo;
                              }
                              /************************************* END PENJUALAN ********************************************************/
                            ?>
                              <tr>
                                  <td><b>TOTAL PENJUALAN</b></td>
                                  <td><b><?php echo number_format($total_penjualan,2);?></b></td>
                              </tr>

                              <?php
/***************************************************** MENGAMBIL DEBET KREDIT AKUN HPP *********************************************************/
                              //$sql_dk_HPP = $this->db->query("SELECT SUM(debet) as tothdebet, SUM(kredit) as tothkredit FROM public.beone_gl WHERE coa_no = '500-00'");
                              //$hasil_dk_HPP = $sql_dk_HPP->row_array();
                              $sql_posisi_dk = $this->db->query("SELECT * FROM public.beone_coa WHERE nomor = '510-00'");
                              $hasil_posisi_dk = $sql_posisi_dk->row_array();

                              //mengambil nilai total debit kredit Akun PENJUALAN
                              $sql_total_mutasi_dk = $this->db->query("SELECT SUM(debet) as totmdebet, SUM(kredit) as totmkredit FROM public.beone_gl WHERE gl_date BETWEEN '$tgl_awal_formated' AND '$tgl_akhir_formated' AND coa_no = '510-00'");
                              $hasil_total_mutasi_dk = $sql_total_mutasi_dk->row_array();

                              if ($hasil_posisi_dk['dk'] == "D"){
                                    $hp_produksi = $hasil_total_mutasi_dk['totmdebet']-$hasil_total_mutasi_dk['totmkredit'];
                              }elseif($hasil_posisi_dk['dk'] == "K"){
                                    $hp_produksi = $hasil_total_mutasi_dk['totmkredit']-$hasil_total_mutasi_dk['totmdebet'];
                              }

                              // MENCARI NILAI HPP ---------------------------------------------------------------------------------------

                              $coa_sawal_wip = $this->db->query("SELECT * FROM public.beone_coa WHERE nomor = '116-07'");
                              $hasil_coa_sawal_wip = $coa_sawal_wip->row_array();

                              $sql_mutasi_wip = $this->db->query("SELECT SUM(debet) as saldo_debet, SUM(kredit) as saldo_kredit FROM public.beone_gl WHERE coa_no = '116-07' AND gl_date BETWEEN '$tgl_awal_formated' AND '$tgl_akhir_formated'");
                              $hasil_mutasi_wip = $sql_mutasi_wip->row_array();

                              $sawal_wip = $hasil_coa_sawal_wip['debet_idr'] - $hasil_coa_sawal_wip['kredit_idr'];
                              $wip_akhir = ($sawal_wip + $hasil_mutasi_wip['saldo_debet']) -  $hasil_mutasi_wip['saldo_kredit'];

                              $barang_tersedia_untuk_produksi = $hp_produksi - $wip_akhir;

                              $sql_sawal = $this->db->query("SELECT * FROM public.beone_coa WHERE nomor = '116-08'");
                              $hasil_sawal = $sql_sawal->row_array();

                              $sawal_final = $hasil_sawal['debet_idr'] - $hasil_sawal['kredit_idr'];


                              $barang_tersedia_untuk_dijual = $barang_tersedia_untuk_produksi + $sawal_final;


                              $sql_gl_debet = $this->db->query("SELECT SUM(debet) as saldo_debet FROM public.beone_gl WHERE coa_no = '116-08' AND gl_date BETWEEN '$tgl_awal_formated' AND '$tgl_akhir_formated'");
                              $hasil_debet = $sql_gl_debet->row_array();

                              $sql_gl_kredit = $this->db->query("SELECT SUM(kredit) as saldo_kredit FROM public.beone_gl WHERE coa_no = '116-08' AND gl_date BETWEEN '$tgl_awal_formated' AND '$tgl_akhir_formated'");
                              $hasil_kredit = $sql_gl_kredit->row_array();

                              $sawal_final_dk = $hasil_sawal['debet_idr'] - $hasil_sawal['kredit_idr'];
                              $barang_jadi_akhir = ($sawal_final_dk + $hasil_debet['saldo_debet']) -  $hasil_kredit['saldo_kredit'];

                              $saldo_HPP = $barang_tersedia_untuk_dijual - $barang_jadi_akhir;

                              //END MENCARI NILAI HPP ---------------------------------------------------------------------------------------


                              $rugi_laba_kotor = $total_penjualan - $saldo_HPP;
                              ?>

                              <tr>
                                  <td><b>HARGA POKOK PENJUALAN</b></td>
                                  <td><?php echo number_format($saldo_HPP,2);?></td>
                              </tr>
                              <tr>
                                  <td><b>RUGI / LABA KOTOR</b></td>
                                  <td><b><?php echo number_format($rugi_laba_kotor,2);?></b></td>
                              </tr>
                              <tr>
                                  <td><b>BIAYA ADMINISTRASI & UMUM</b></td>
                                  <td></td>
                              </tr>

                              <?php
        /****************************************** ADMIN BIAYA UMUM ************************************/
                                $total_biaya_admin_umum = 0;
                                foreach($list_coa_biaya_admin_umum as $row){
                                  //mengambil nilai total debit kredit Akun BIAYA ADMIN UMUM
                                  $sql_dk_admin_umum = $this->db->query("SELECT SUM(debet) as totdebet, SUM(kredit) as totkredit FROM public.beone_gl WHERE gl_date BETWEEN '$tgl_awal_formated' AND '$tgl_akhir_formated' AND coa_id =".intval($row['coa_id']));
                                  $hasil_dk_admin_umum = $sql_dk_admin_umum->row_array();

                                  //mencari rumus posisi debet kredit dari tipe coa
                                  $sql_posisi_dk = $this->db->query("SELECT * FROM public.beone_coa WHERE coa_id =".intval($row['coa_id']));
                                  $hasil_posisi_dk = $sql_posisi_dk->row_array();

                                  if ($hasil_posisi_dk['dk'] == "D"){
                                          $saldo_admin = $hasil_dk_admin_umum['totdebet'] - $hasil_dk_admin_umum['totkredit'];
                                  }elseif($hasil_posisi_dk['dk'] == "K"){
                                          $saldo_admin = $hasil_dk_admin_umum['totkredit'] - $hasil_dk_admin_umum['totdebet'];
                                  }

                              ?>
                              <tr>
                                  <td> <?php echo $row['nama'];?> </td>
                                  <td> <?php echo number_format($saldo_admin,2);?> </td>
                              </tr>
                              <?php
                                $total_biaya_admin_umum = $total_biaya_admin_umum + $saldo_admin;
                              }

                                $rugi_laba_usaha = $rugi_laba_kotor - $total_biaya_admin_umum;
                              ?>
                              <tr>
                                  <td><b>TOTAL BIAYA ADMIN & UMUM</b></td>
                                  <td><b><?php echo number_format($total_biaya_admin_umum,2);?></b></td>
                              </tr>
                              <tr>
                                  <td><b>RUGI / LABA USAHA</b></td>
                                  <td><b><?php echo number_format($rugi_laba_usaha,2);?></b></td>
                              </tr>
                              <tr>
                                  <td><b>PENDAPATAN / BIAYA LAIN</b></td>
                                  <td></td>
                              </tr>

                              <?php
                              //************************* PENDAPATAN BIAYA LAIN ***************************
                              $total_pendapatan_biaya_lain = 0;
                              foreach($list_coa_pendapatan_lain as $row){
                                //mengambil nilai total debit kredit Akun BIAYA ADMIN UMUM
                                $sql_dk_pendapatan = $this->db->query("SELECT SUM(debet) as totpdebet, SUM(kredit) as totpkredit FROM public.beone_gl WHERE gl_date BETWEEN '$tgl_awal_formated' AND '$tgl_akhir_formated' AND coa_id =".intval($row['coa_id']));
                                $hasil_dk_pendapatan = $sql_dk_pendapatan->row_array();

                                //mencari rumus posisi debet kredit dari tipe coa
                                $sql_posisi_dk = $this->db->query("SELECT * FROM public.beone_coa WHERE coa_id =".intval($row['coa_id']));
                                $hasil_posisi_dk = $sql_posisi_dk->row_array();

                                if ($hasil_posisi_dk['dk'] == "D"){
                                      $saldo_pendapatan = $hasil_dk_pendapatan['totpdebet'] - $hasil_dk_pendapatan['totpkredit'];
                                }elseif($hasil_posisi_dk['dk'] == "K"){
                                    $saldo_pendapatan = $hasil_dk_pendapatan['totpkredit'] - $hasil_dk_pendapatan['totpdebet'];
                                }
                              ?>
                              <tr>
                                  <td><?php echo $row['nama']?></td>
                                  <td><?php echo number_format($saldo_pendapatan,2);?></td>
                              </tr>
                              <?php
                                $total_pendapatan_biaya_lain = $total_pendapatan_biaya_lain + $saldo_pendapatan;
                              }
                                $laba_rugi_bersih_usaha = $rugi_laba_usaha + $total_pendapatan_biaya_lain;
                                //************************* END PENDAPATAN BIAYA LAIN ***************************
                              ?>

                              <tr>
                                  <td><b> TOTAL PENDAPATAN / BIAYA LAIN LAIN </b></td>
                                  <td><b><?php echo number_format($total_pendapatan_biaya_lain,2);?></b></td>
                              </tr>
                              <tr>
                                  <td><h3><b>LABA / RUGI BERSIH USAHA</b></h3></td>
                                  <td><h3><b><?php echo number_format($laba_rugi_bersih_usaha,2);?></b></h3></td>
                              </tr>
                          </tbody>
                      </table>

                  </div>
              </div>
