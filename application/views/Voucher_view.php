<h3>List Voucher</h3>
<div class="portlet light bordered">
  <div class="portlet-title">
      <div class="tools"> </div>
  </div>

<table class="table table-striped table-bordered table-hover" id="sample_1">
        <thead>
          <tr>
              <th><center>Voucher</center></th>
              <th><center>Tanggal</center></th>
              <th><center>Keterangan</center></th>
              <th><center>COA</center></th>
              <th><center>Debet</center></th>
              <th><center>Kredit</center></th>
              <th><center>Action</center></th>
          </tr>
        </thead>
        <tbody>
          <?php 	foreach($list_voucher as $row){ ?>
            <tr>
                <td><?php echo $row['voucher_number'];?></td>
                <td><?php echo $row['voucher_date'];?></td>
                <td><?php echo $row['keterangan'];?></td>
                <td><?php echo $row['coa_no'];?></td>

                <?php if ($row['tipe'] == 1){?>
                  <td><?php echo number_format($row['jidr'],2);?></td>
                  <td>0</td>
                <?php }else{?>
                  <td>0</td>
                  <td><?php echo number_format($row['jidr'],2);?></td>
                <?php }
                // konversi voucher number dari / ke -
                $vnumber = str_replace("/", "-", $row['voucher_number']);
                ?>
                <td>
                    <?php if(helper_security("kas_bank_edit") == 1){?>
                    <a href='<?php echo base_url('Cashbank_controller/edit/'.$row['voucher_header_id'].'');?>' class='btn blue'><i class="fa fa-pencil"></i> </a>
                    <?php }?>
                    <?php if(helper_security("kas_bank_delete") == 1){?>
                    <a href="javascript:dialogHapus('<?php echo base_url('Cashbank_controller/delete/'.$row['voucher_header_id'].'/'.$vnumber.'');?>')" class='btn red'><i class="fa fa-trash-o"></i> </a>
                    <?php }?>
                    <a href='<?php echo base_url('Cashbank_controller/voucher_print/'.$row['voucher_header_id'].'');?>' class='btn yellow'><i class="fa fa-print"></i> </a>
                </td>
            </tr>
            <?php
              }
            ?>
        </tbody>
    </table>
</div>

<script>
	function dialogHapus(urlHapus) {
	  if (confirm("Apakah anda yakin ingin menghapus ini ?")) {
		document.location = urlHapus;
	  }
	}
</script>
