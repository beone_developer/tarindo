<div class="container">
    <div class="row">
        <p class="text-center" style="font-size: 17px"><b>PEMBERITAHUAN IMPOR BARANG UNTUK DITIMBUN <br>DI TEMPAT PENIMBUNAN BERIKAT</b></p>
    </div>

    <form action="preview_BC23/edit" method = "POST">
        <div class="form-group row">
            <label for="status" class="col-sm-2 col-form-label">Status</label>
            <input type="hidden" class="form-control" name="id" name ="id" Placeholder="id" value = "<?php
            if ($ptb_edit) {
                foreach ($ptb_edit as $data) {
                    echo $data->id;
                }
            }
            ?>" >
            <div class="col-sm-2">
                <input style="border: none;" type="text" readonly class="form-control" name="status" value="EDIT">
            </div>
        </div>

        <div class="form-group row">
            <label for="status_perbaikan" class="col-sm-2 col-form-label">Status Perbaikan</label>
            <div class="col-sm-2">
                <input style="border: none;" type="text" readonly class="form-control" name="status_perbaikan" value="-">
            </div>
        </div>
        <div class="form-group row">
            <div class="col-sm-2">
                <a href="#modal" class="btn btn-primary btn-sm" role="button" data-target="#modal" data-toggle="modal">DAFTAR RESPON</a>
            </div>
        </div>

        <!-- FORM NOMOR PENGAJUAN -->
        <div style="margin-top: 30px;">
            <div class="form-group row">
                <label for="nomor_pengajuan" class="col-sm-2 col-form-label">Nomor Pengajuan</label>
                <div class="col-sm-4">
                    <input type="text" readonly class="form-control" name="nomor_pengajuan" value = "<?php
                    $n = 0;
                    if ($ptb_edit) {
                        foreach ($ptb_edit as $data) {
                            $n = $n + 1;
                            echo $data->nomor_aju;
                        }
                    }
                    ?>">
                </div>
            </div>

            <div class="form-group row">
                <label for="nomor_pendaftaran" class="col-sm-2 col-form-label">Nomor Pendaftaran</label>
                <div class="col-sm-4">
                    <input type="text" class="form-control" name="nomor_pendaftaran" value="<?php
                    $n = 0;
                    if ($ptb_edit) {
                        foreach ($ptb_edit as $data) {
                            $n = $n + 1;
                            echo $data->nomor_daftar;
                        }
                    }
                    ?>">
                </div>
            </div>

            <div class="form-group row">
                <label for="tanggal_pendaftaran" class="col-sm-2 col-form-label">Tanggal Pendaftaran</label>
                <div class="col-sm-4">
                    <input type="text" class="form-control" name="tanggal_pendaftaran" value="<?php
                    $n = 0;
                    if ($ptb_edit) {
                        foreach ($ptb_edit as $data) {
                            $n = $n + 1;
                            echo $data->tanggal_daftar;
                        }
                    }
                    ?>" placeholder="DDMMYY">
                </div>
            </div>		
        </div>
        <!-- TUTUP FORM NOMOR PENGAJUAN -->

        <!-- FORM KPPBC BONGKAR -->
        <div style="margin-top: 30px;">
            <div class="form-group row">
                <label for="kppbc_bongkar" class="col-sm-2 col-form-label">KPPBC Bongkar</label>
                <div class="col-sm-4">
                    <input type="text" class="form-control" name="kppbc_bongkar" value="<?php
                    $n = 0;
                    if ($ptb_edit) {
                        foreach ($ptb_edit as $data) {
                            $n = $n + 1;
                            echo $data->kode_kppbc_bongkar . " - " . $data->kppbc_bongkar;
                        }
                    }
                    ?>">
                </div>
            </div>

            <div class="form-group row">
                <label for="kppbc_pengawas" class="col-sm-2 col-form-label">KPPBC Pengawas</label>
                <div class="col-sm-4">
                    <input type="text" class="form-control" name="kppbc_pengawas" value="<?php
                    $n = 0;
                    if ($ptb_edit) {
                        foreach ($ptb_edit as $data) {
                            $n = $n + 1;
                            echo $data->kode_kppbc_awas . " - " . $data->kppbc_awas;
                        }
                    }
                    ?>">
                </div>
            </div>

            <div class="form-group row">
                <label for="tujuan" class="col-sm-2 col-form-label">Tujuan</label>
                <div class="col-sm-4">
                    <select name="tujuan" class="form-control">
                        <option selected>Choose...</option>
                        <option>...</option>
                    </select>
                </div>
            </div>		
        </div>
        <!-- TUTUP FORM KPPBC BONGKAR -->


        <div class="row">
            <div class="col-sm-12">

                <!-- FORM LEFT SIDE -->
                <ol>
                    <div class="col-sm-6">
                        <p><b>PEMASOK</b></p>
                        <!-- FORM PEMASOK -->
                        <div>
                            <div class="form-group row">
                                <li>
                                    <label for="nama" class="col-sm-3 col-form-label">Nama</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" name="nama_pemasok" value = "<?php
                                        $n = 0;
                                        if ($ptb_edit) {
                                            foreach ($ptb_edit as $data) {
                                                $n = $n + 1;
                                                echo $data->nama_pemasok;
                                            }
                                        }
                                        ?>">
                                    </div>
                                </li>
                            </div>

                            <div class="form-group row">
                                <label for="alamat" class="col-sm-3 col-form-label">Alamat</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="alamat_pemasok" value = "<?php
                                    $n = 0;
                                    if ($ptb_edit) {
                                        foreach ($ptb_edit as $data) {
                                            $n = $n + 1;
                                            echo $data->alamat_pemasok;
                                        }
                                    }
                                    ?>">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="Negara" class="col-sm-3 col-form-label">Negara</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="negara_pemasok" value = "<?php
                                    $n = 0;
                                    if ($ptb_edit) {
                                        foreach ($ptb_edit as $data) {
                                            $n = $n + 1;
                                            echo $data->kode_negara_pemasok;
                                        }
                                    }
                                    ?>">
                                </div>
                            </div>
                        </div>
                        <!-- TUTUP FORM PEMASOK -->		

                        <!-- FORM IMPORTIR -->
                        <div>
                            <p style="margin-left: -38px"><b>IMPORTIR</b></p>

                            <div class="form-group row">
                                <li>
                                    <label for="nama" class="col-sm-3 col-form-label">Identitas</label>
                                    <div class="col-sm-4">
                                        <select name="jenis_identitas_importir" class="form-control">
                                            <option selected>NPWP</option>
                                            <option>...</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control" name="nomor_identitas_importir" value = "<?php
                                        if ($ptb_edit) {
                                            foreach ($ptb_edit as $data) {
                                                echo $data->kode_identitas_importir;
                                            }
                                        }
                                        ?>">
                                    </div>
                                </li>
                            </div>	

                            <div class="form-group row">
                                <li>
                                    <label for="alamat" class="col-sm-3 col-form-label">Nama</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" name="nama_importir" value ="<?php
                                        if ($ptb_edit) {
                                            foreach ($ptb_edit as $data) {
                                                echo $data->nama_importir;
                                            }
                                        }
                                        ?>" >
                                    </div>
                                </li>
                            </div>

                            <div class="form-group row">
                                <label for="nomor_ijin_tpb" class="col-sm-3 col-form-label">No Izin</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="nomor_ijin_tpb" value = "<?php
                                    if ($ptb_edit) {
                                        foreach ($ptb_edit as $data) {
                                            echo $data->nomor_ijin_tpb;
                                        }
                                    }
                                    ?>">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="alamat" class="col-sm-3 col-form-label">Alamat</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="alamat_importir" value ="<?php
                                    if ($ptb_edit) {
                                        foreach ($ptb_edit as $data) {
                                            echo $data->alamat_importir;
                                        }
                                    }
                                    ?>" >
                                </div>
                            </div>


                            <div class="form-group row">
                                <li>
                                    <label for="Negara" class="col-sm-3 col-form-label">API</label>
                                    <div class="col-sm-4">
                                        <select name="jenis_api_importir" class="form-control">
                                            <option selected>APIP</option>
                                            <option>...</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control" name="nomor_api_importir" value ="<?php
                                        if ($ptb_edit) {
                                            foreach ($ptb_edit as $data) {
                                                echo $data->api_pengusaha;
                                            }
                                        }
                                        ?>" >
                                    </div>
                                </li>
                            </div>
                        </div>
                        <!-- TUTUP FORM IMPORTIR -->

                        <!-- FORM PEMILIK -->
                        <div>
                            <p style="margin-left: -38px"><b>PEMILIK</b></p>

                            <div class="form-group row">
                                <li>
                                    <label for="nama" class="col-sm-3 col-form-label">Identitas</label>
                                    <div class="col-sm-4">
                                        <select name="jenis_identitas_pemilik" class="form-control">
                                            <option selected>NPWP</option>
                                            <option>...</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control" name="nomor_identitas_pemilik" value ="<?php
                                        if ($ptb_edit) {
                                            foreach ($ptb_edit as $data) {
                                                echo $data->jenis_identitas_pemilik;
                                            }
                                        }
                                        ?>" >
                                    </div>
                                </li>
                            </div>	

                            <div class="form-group row">
                                <li>
                                    <label for="alamat" class="col-sm-3 col-form-label">Nama</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" name="nama_pemilik" value ="<?php
                                        if ($ptb_edit) {
                                            foreach ($ptb_edit as $data) {
                                                echo $data->nama_pemilik;
                                            }
                                        }
                                        ?>" >
                                    </div>
                                </li>
                            </div>

                            <div class="form-group row">
                                <label for="alamat" class="col-sm-3 col-form-label">Alamat</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="alamat_pemilik" value="<?php
                                    if ($ptb_edit) {
                                        foreach ($ptb_edit as $data) {
                                            echo $data->alamat_pemilik;
                                        }
                                    }
                                    ?>" >
                                </div>
                            </div>


                            <div class="form-group row">	
                                <label for="Negara" class="col-sm-3 col-form-label">API</label>
                                <div class="col-sm-4">
                                    <select name="jenis_api_pemilik" class="form-control">
                                        <option selected>APIP</option>
                                        <option>...</option>
                                    </select>
                                </div>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" name="nomor_api_pemilik" value="<?php
                                    if ($ptb_edit) {
                                        foreach ($ptb_edit as $data) {
                                            echo $data->API_PEMILIK;
                                        }
                                    }
                                    ?>">
                                </div>

                            </div>
                        </div>

                        <!-- FORM PPJK -->
                        <div>
                            <p style="margin-left: -38px"><b>PPJK</b></p>

                            <div class="form-group row">
                                <li>
                                    <label for="nama" class="col-sm-3 col-form-label">Identitas</label>
                                    <div class="col-sm-4">
                                        <select name="jenis_identitas_pemilik" class="form-control">
                                            <option selected>NPWP</option>
                                            <option>...</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control" name="nomor_identitas_pemilik" value="<?php
                                        if ($ptb_edit) {
                                            foreach ($ptb_edit as $data) {
                                                echo $data->rp_npwp;
                                            }
                                        }
                                        ?>" >
                                    </div>
                                </li>
                            </div>	

                            <div class="form-group row">
                                <li>
                                    <label for="alamat" class="col-sm-3 col-form-label">Nama</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" name="nama_pemilik" value ="<?php
                                        if ($ptb_edit) {
                                            foreach ($ptb_edit as $data) {
                                                echo $data->rp_nama;
                                            }
                                        }
                                        ?>" >
                                    </div>
                                </li>
                            </div>

                            <div class="form-group row">
                                <label for="alamat" class="col-sm-3 col-form-label">Alamat</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="alamat_pemilik" value = "<?php
                                    if ($ptb_edit) {
                                        foreach ($ptb_edit as $data) {
                                            echo $data->rp_alamat;
                                        }
                                    }
                                    ?>">
                                </div>
                            </div>


                            <div class="form-group row">
                                <li>
                                    <label for="Negara" class="col-sm-3 col-form-label">NP-PPJK</label>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control" name="nomor_npppjk" placeholder="" value="<?php
                                        if ($ptb_edit) {
                                            foreach ($ptb_edit as $data) {
                                                echo $data->nomor_npppjk;
                                            }
                                        }
                                        ?>">
                                    </div>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control" name="tanggal_npppjk" placeholder="DDMMYY" value="<?php
                                        if ($ptb_edit) {
                                            foreach ($ptb_edit as $data) {
                                                echo $data->tanggal_npppjk;
                                            }
                                        }
                                        ?>">
                                    </div>
                                </li>
                            </div>
                        </div>
                        <!-- TUTUP FORM PPJK -->

                        <!-- FORM PENGANGKUTAN -->
                        <div>
                            <p style="margin-left: -38px"><b>PENGANGKUTAN</b></p>

                            <div class="form-group row">
                                <li>
                                    <label for="nama" class="col-sm-3 col-form-label">Cara Pengangkutan</label>
                                    <div class="col-sm-6">
                                        <select name="cara_angkut" class="form-control">
                                            <option selected>NPWP</option>
                                            <option>...</option>
                                        </select>
                                    </div>
                                </li>
                            </div>	

                            <div class="form-group row">
                                <li>
                                    <label for="alamat" class="col-sm-3 col-form-label">Nama Sarana Pengangkut</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" name="nama_sarana_angkut" value="<?php
                                        if ($ptb_edit) {
                                            foreach ($ptb_edit as $data) {
                                                echo $data->NAMA_PENGANGKUT;
                                            }
                                        }
                                        ?>" >
                                    </div>
                                </li>
                            </div>

                            <div class="form-group row">
                                <label for="alamat" class="col-sm-3 col-form-label">Voy/Flight & Negara</label>
                                <div class="col-sm-2">
                                    <input type="text" class="form-control" name="nomor_vf" value="<?php
                                    if ($ptb_edit) {
                                        foreach ($ptb_edit as $data) {
                                            echo $data->NOMOR_VOY_FLIGHT;
                                        }
                                    }
                                    ?>" >
                                </div>
                                <div class="col-sm-2">
                                    <input type="text" class="form-control" name="kode_bendera" value="<?php
                                    if ($ptb_edit) {
                                        foreach ($ptb_edit as $data) {
                                            echo $data->KODE_BENDERA;
                                        }
                                    }
                                    ?>" >
                                </div>
                                <!--									<div class="col-sm-4">
                                                                                                                <input type="text" class="form-control" name="alamat" >
                                                                                                        </div>-->
                            </div>

                            <div class="form-group row">
                                <li>
                                    <label for="Negara" class="col-sm-3 col-form-label">Pelabuhan Muat</label>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control" name="kode_pel_muat" value="<?php
                                        if ($ptb_edit) {
                                            foreach ($ptb_edit as $data) {
                                                echo $data->KODE_PEL_MUAT;
                                            }
                                        }
                                        ?>">
                                    </div>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control" name="pel_muat" value="<?php
                                        if ($ptb_edit) {
                                            foreach ($ptb_edit as $data) {
                                                echo $data->pel_muat;
                                            }
                                        }
                                        ?>">
                                    </div>
                                </li>
                            </div>

                            <div class="form-group row">
                                <li>
                                    <label for="Negara" class="col-sm-3 col-form-label">Pelabuhan Transit</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" name="kode_pel_transit" value="<?php
                                        if ($ptb_edit) {
                                            foreach ($ptb_edit as $data) {
                                                echo $data->KODE_PEL_TRANSIT . " - " . $data->pel_transit;
                                            }
                                        }
                                        ?>">
                                    </div>
                                </li>
                            </div>

                            <div class="form-group row">
                                <li>
                                    <label for="Negara" class="col-sm-3 col-form-label">Pelabuhan Bongkar</label>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control" name="kode_pel_bongkar" value="<?php
                                        if ($ptb_edit) {
                                            foreach ($ptb_edit as $data) {
                                                echo $data->KODE_PEL_BONGKAR;
                                            }
                                        }
                                        ?>" >
                                    </div>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control" name="pel_bongkar" value="<?php
                                        if ($ptb_edit) {
                                            foreach ($ptb_edit as $data) {
                                                echo $data->pel_bongkar;
                                            }
                                        }
                                        ?>">
                                    </div>
                                </li>
                            </div>
                        </div>
                        <!-- TUTUP FORM PENGANGKUTAN -->	
                    </div>
                    <!-- TUTUP FORM LEFT SIDE -->


                    <!-- FORM RIGHT SIDE -->
                    <div class="col-sm-6" style="padding-right: 50px;">
                        <li>
                            <a href="#modal4" class="btn btn-primary btn-sm" role="button" data-target="#modal4" data-toggle="modal">DOKUMEN</a>
                        </li>
                        <div class="form-group row">
                            <li>
                                <label for="nama" class="col-sm-3 col-form-label">Invoice</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" name="nomor_dokumen" value="<?php
                                    if ($ptb_inv) {
                                        foreach ($ptb_inv as $data) {
                                            echo $data->NOMOR_DOKUMEN;
                                        }
                                    }
                                    ?>">
                                </div>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" name="tgl_dokumen" value="<?php
                                    if ($ptb_inv) {
                                        foreach ($ptb_inv as $data) {
                                            echo $data->TANGGAL_DOKUMEN;
                                        }
                                    }
                                    ?>">
                                </div>
                            </li>
                        </div>

                        <div class="form-group row">
                            <li>
                                <label for="kppbc_pengawas" class="col-sm-3 col-form-label">Fasilitas Impor</label>
                                <div class="col-sm-3">
                                    <input type="text" class="form-control" name="fasim_1" value="">
                                </div>
                                <div class="col-sm-3">
                                    <input type="text" class="form-control" name="fasim_2" placeholder="DDMMYY">
                                </div>
                                <div class="col-sm-2">
                                    <input type="text" class="form-control" name="fasim_3" value="">
                                </div>
                            </li>
                        </div>

                        <div class="form-group row">
                            <li>
                                <label for="tujuan" class="col-sm-10 col-form-label">Surat Keputusan / Dokumen Lainnya</label>
                            </li>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-10">
                                <table class="table table-sm">
                                    <thead>
                                        <tr class="bg-success">
                                            <th scope="col">No.</th>
                                            <th scope="col">Jenis Dokumen</th>
                                            <th scope="col">Nomor Dokumen</th>
                                            <th scope="col">Tanggal</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                        <?php
                                        $n = 0;
                                        if ($ptb_other) {
                                            foreach ($ptb_other as $data) {
                                                $n = $n + 1;
                                                ?>
                                                <tr><td width="30%"><?php echo $n; ?></td>
                                                    <td width="30%"><?php echo $data->URAIAN_DOKUMEN; ?></td>
                                                    <td width="30%"><?php echo $data->NOMOR_DOKUMEN; ?></td>
                                                    <td width="30%"><?php echo $data->TANGGAL_DOKUMEN; ?></td></tr>
                                                <?php
                                            }
                                        }
                                        ?>						    

                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="form-group row">
                            <li>
                                <label for="Negara" class="col-sm-3 col-form-label">LC</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" name="LC" placeholder="" value="<?php
                                    if ($ptb_lc) {
                                        foreach ($ptb_lc as $data) {
                                            echo $data->NOMOR_DOKUMEN;
                                        }
                                    }
                                    ?>">
                                </div>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" name="tgl_lc" placeholder="DDMMYY" value="<?php
                                    if ($ptb_lc) {
                                        foreach ($ptb_lc as $data) {
                                            echo $data->TANGGAL_DOKUMEN;
                                        }
                                    }
                                    ?>">
                                </div>
                            </li>
                        </div>
                        <div class="form-group row">
                            <li>

                                <div class="col-sm-4">
                                    <select name="BL" class="form-control">
                                        <option selected>B/L</option>
                                        <option>...</option>
                                    </select>
                                </div>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" name="BL_1" value ="" >
                                </div>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" name="BL_2" value ="" >
                                </div>
                            </li>
                        </div>
                        <div class="form-group row">
                            <li>

                                <div class="col-sm-4">
                                    <button type="submit" class="btn blue" name="submit_user">BC 1.1</button>
                                </div>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" name="bc11_1" value ="" >
                                </div>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" name="bc11_2" value ="" >
                                </div>
                            </li>
                        </div>
                        <!-- FORM PENIMBUNAN -->
                        <div>
                            <p style="margin-left: -38px"><b>PENIMBUNAN</b></p>

                            <div class="form-group row">
                                <li>
                                    <label class="col-sm-3 col-form-label">Tempat Penimbunan</label>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control" name="kode_tps" value ="<?php
                                        if ($ptb_edit) {
                                            foreach ($ptb_edit as $data) {
                                                echo $data->kode_tps;
                                            }
                                        }
                                        ?>" >
                                    </div>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control" name="uraian_tps" value ="<?php
                                        if ($ptb_edit) {
                                            foreach ($ptb_edit as $data) {
                                                echo $data->uraian_tps;
                                            }
                                        }
                                        ?>" >
                                    </div>

                                </li>
                            </div>	
                        </div>
                        <!-- FORM Harga -->
                        <div>
                            <li>
                                <a href="#modal6" class="btn btn-primary btn-sm" role="button" data-target="#modal6" data-toggle="modal">HARGA</a>
                            </li>
                            <div class="form-group row">
                                <li>
                                    <label class="col-sm-3 col-form-label">Valuta</label>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control" name="kode_valuta" value ="<?php
                                        if ($ptb_edit) {
                                            foreach ($ptb_edit as $data) {
                                                echo $data->kode_valuta . " - " . $data->uraian_valuta;
                                            }
                                        }
                                        ?>" >
                                    </div>
                                </li>
                            </div>
                            <div class="form-group row">
                                <li>
                                    <label class="col-sm-3 col-form-label">NDPBM</label>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control" name="ndpbm" value ="<?php
                                        if ($ptb_edit) {
                                            foreach ($ptb_edit as $data) {
                                                echo $data->NDPBM;
                                            }
                                        }
                                        ?>" >
                                    </div>
                                </li>
                            </div>
                            <div class="form-group row">
                                <li>
                                    <label class="col-sm-3 col-form-label">FOB</label>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control" name="fob" value ="<?php
                                        if ($ptb_edit) {
                                            foreach ($ptb_edit as $data) {
                                                echo $data->FOB;
                                            }
                                        }
                                        ?>" >
                                    </div>


                                </li>
                            </div>	
                            <div class="form-group row">
                                <li>
                                    <label class="col-sm-3 col-form-label">Freight</label>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control" name="freight" value ="<?php
                                        if ($ptb_edit) {
                                            foreach ($ptb_edit as $data) {
                                                echo $data->freight;
                                            }
                                        }
                                        ?>" >
                                    </div>


                                </li>
                            </div>	
                            <div class="form-group row">
                                <li>
                                    <label class="col-sm-3 col-form-label">Asuransi LN/DN</label>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control" name="asuransi" value ="<?php
                                        if ($ptb_edit) {
                                            foreach ($ptb_edit as $data) {
                                                echo $data->asuransi;
                                            }
                                        }
                                        ?>" >
                                    </div>


                                </li>
                            </div>	
                            <div class="form-group row">
                                <li>
                                    <label class="col-sm-3 col-form-label">Nilai CIF</label>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control" name="cif" value ="<?php
                                        if ($ptb_edit) {
                                            foreach ($ptb_edit as $data) {
                                                echo $data->cif;
                                            }
                                        }
                                        ?>" >
                                    </div>


                                </li>
                            </div>
                            <div class="form-group row">
                                <li>
                                    <label class="col-sm-3 col-form-label">Nilai CIF Rupiah</label>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control" name="cif_rp" value ="<?php
                                        if ($ptb_edit) {
                                            foreach ($ptb_edit as $data) {
                                                echo $data->cif_rupiah;
                                            }
                                        }
                                        ?>" >
                                    </div>


                                </li>
                            </div>
                        </div>
                        <!-- FORM KONTAINER-->
                        <div class="form-group row">
                            <li>
                                <a href="#modal2" class="btn btn-primary btn-sm" role="button" data-target="#modal2" data-toggle="modal">KONTAINER</a>
                            </li>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-10">
                                <table class="table table-sm">
                                    <thead>
                                        <tr class="bg-success">
                                            <th scope="col">No.</th>
                                            <th scope="col">Nomor Kontainer</th>
                                            <th scope="col">Ukuran</th>
                                            <th scope="col">Tipe</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                        <?php
                                        $n = 0;
                                        if ($ptb_kontainer) {
                                            foreach ($ptb_kontainer as $data) {
                                                $n = $n + 1;
                                                ?>
                                                <tr><td width="30%"><?php echo $n; ?></td>
                                                    <td width="30%"><?php echo $data->NOMOR_KONTAINER; ?></td>
                                                    <td width="30%"><?php echo $data->URAIAN_UKURAN_KONTAINER; ?></td>
                                                    <td width="30%"><?php echo $data->URAIAN_TIPE_KONTAINER; ?></td></tr>
                                                <?php
                                            }
                                        }
                                        ?>						    

                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- FORM KEMASAN-->
                        <div class="form-group row">
                            <li>
                                <a href="#modal3" class="btn btn-primary btn-sm" role="button" data-target="#modal3" data-toggle="modal">KEMASAN</a>
                            </li>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-10">
                                <table class="table table-sm">
                                    <thead>
                                        <tr class="bg-success">
                                            <th scope="col">No.</th>
                                            <th scope="col">Jumlah</th>
                                            <th scope="col">Kode Jenis</th>
                                            <th scope="col">Jenis</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                        <?php
                                        $n = 0;
                                        if ($ptb_kemasan) {
                                            foreach ($ptb_kemasan as $data) {
                                                $n = $n + 1;
                                                ?>
                                                <tr><td width="30%"><?php echo $n; ?></td>
                                                    <td width="30%"><?php echo $data->JUMLAH_KEMASAN; ?></td>
                                                    <td width="30%"><?php echo $data->KODE_JENIS_KEMASAN; ?></td>
                                                    <td width="30%"><?php echo $data->URAIAN_KEMASAN; ?></td></tr>
                                                <?php
                                            }
                                        }
                                        ?>						    

                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- FORM BARANG -->
                        <div>
                            <a href="./preview_BC23/form_detail_barang?id=<?php
                            if ($ptb_edit) {
                                foreach ($ptb_edit as $data) {
                                    echo $data->id;
                                }
                            }
                            ?>"><p style="margin-left: -38px"><b>BARANG</b></p></a>

                            <div class="form-group row">
                                <li>
                                    <label class="col-sm-3 col-form-label">Bruto (Kg)</label>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control" name="bruto" value ="<?php
                                        if ($ptb_edit) {
                                            foreach ($ptb_edit as $data) {
                                                echo $data->bruto;
                                            }
                                        }
                                        ?>" >
                                    </div>
                                </li>
                            </div>
                            <div class="form-group row">
                                <li>
                                    <label class="col-sm-3 col-form-label">Netto (Kg)</label>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control" name="netto" value ="<?php
                                        if ($ptb_edit) {
                                            foreach ($ptb_edit as $data) {
                                                echo $data->netto;
                                            }
                                        }
                                        ?>" >
                                    </div>

                                </li>
                            </div>	
                        </div>
                        <!-- FORM PUNGUTAN-->
                        <!--							<div class="form-group row">
                                                                                <li>
                                                                                        <label for="tujuan" class="col-sm-10 col-form-label">Pngutan</label>
                                                                                </li>
                                                                        </div>-->







                </ol>	
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                        <tr class="bg-success">
                            <th scope="col">Jenis Pungutan</th>
                            <th scope="col">Ditangguhkan (Rp)</th>
                            <th scope="col">Dibebaskan (Rp)</th>
                            <th scope="col">Tidak Dipungut (Rp)</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <?php
                            $n = 0;
                            if ($ptb_kemasan) {
                                foreach ($ptb_kemasan as $data) {
                                    $n = $n + 1;
                                    ?>
                                    <td width="30%"><?php echo $data->JUMLAH_KEMASAN; ?></td>
                                    <td width="30%"><?php echo $data->JUMLAH_KEMASAN; ?></td>
                                    <td width="30%"><?php echo $data->KODE_JENIS_KEMASAN; ?></td>
                                    <td width="30%"><?php echo $data->URAIAN_KEMASAN; ?></td>
                                    <?php
                                }
                            }
                            ?>						    
                        </tr>
                    </tbody>
                </table>
                <p>Dengan saya menyatakan bertanggung jawab atas kebenaran
                    <br></br>hal hal yang diberitahukan pada dokumen ini. </p>
                <div class="form-group row">
                    <!--<li>-->
                    <!--<label for="Negara" class="col-sm-3 col-form-label">Pelabuhan Bongkar</label>-->
                    <div class="col-sm-2">
                        <input type="text" class="form-control" name="kota_ttd" Placeholder="kota" value = "<?php
                        if ($ptb_edit) {
                            foreach ($ptb_edit as $data) {
                                echo $data->KOTA_TTD;
                            }
                        }
                        ?>">
                    </div>
                    <div class="col-sm-2">
                        <input type="text" class="form-control" name="tgl_ttd" Placeholder="Tanggal" value = "<?php
                        if ($ptb_edit) {
                            foreach ($ptb_edit as $data) {
                                echo $data->TANGGAL_TTD;
                            }
                        }
                        ?>">
                    </div>
                    <!--</li>-->
                </div>
                <div class="form-group row">
                    <!--<li>-->
                    <label for="Negara" class="col-sm-2 col-form-label">Pemberitahu</label>
                    <div class="col-sm-2">
                        <input type="text" class="form-control" name="nama_ttd" Placeholder="nama" value = "<?php
                        if ($ptb_edit) {
                            foreach ($ptb_edit as $data) {
                                echo $data->NAMA_TTD;
                            }
                        }
                        ?>">
                    </div>
                    <!--</li>-->
                </div>
                <div class="form-group row">
                    <!--<li>-->
                    <label for="Negara" class="col-sm-2 col-form-label">Jabatan</label>
                    <div class="col-sm-2">
                        <input type="text" class="form-control" name="jabatan_ttd" Placeholder="jabatan" value = "<?php
                        if ($ptb_edit) {
                            foreach ($ptb_edit as $data) {
                                echo $data->JABATAN_TTD;
                            }
                        }
                        ?>" >
                    </div>
                    <!--</li>-->
                </div>
            </div>

            <div class="form-actions">
                <a href='<?php echo base_url('Bc_controller'); ?>' class='btn default'> Cancel</a>
                <button type="submit" class="btn blue" name="edit_bc23">Simpan Perubahan</button>
            </div>
        </div>		
        <!-- TUTUP FORM RIGHT SIDE -->

</div>
</div>
</form>

<div id="modal" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog">
    <div class="modal-content">
        <div class="modal-header">
            <h3 class="modal-title"><b>RESPON</b></h3>
        </div>
        <div class="modal-body">

            <div class="row">
                <div class="table-wrapper-scroll-y my-custom-scrollbar">
                    <table class="table table-striped table-bordered table-hover">

                        <thead>
                            <tr class="bg-success">
                                <th scope="col">Kode</th>
                                <th scope="col">Uraian</th>
                                <th scope="col">Waktu</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <?php
                                if ($ptb_respon) {
                                    foreach ($ptb_respon as $data) {
                                        ?>
                                        <td width="30%"><?php echo $data->KODE_RESPON; ?></td>
                                        <td width="30%"><?php echo $data->NOMOR_RESPON; ?></td>
                                        <td width="30%"><?php echo $data->TANGGAL_RESPON; ?></td></tr>
                                    <?php
                                }
                            }
                            ?>						    
                            </tr>
                        </tbody>
                    </table>



                </div>
            </div>	
        </div>

        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
    </div>

</div>

<div id="modal4" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog">
    <div class="modal-content">
        <div class="modal-header">
            <h3 class="modal-title"><b>DOKUMEN</b></h3>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-sm-12">
                    <form action="preview_BC23/update_modal_dokumen" method = "POST">
                        <div class="form-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <input type="hidden" class="form-control" id="id" name="id" value="">
                                </div>
                            </div>
                        </div>
                        <div class="form-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <label>Jenis Dokumen</label>
                                    <input type="text" class="form-control" id="dokumen" name="dokumen" value="">
                                </div>
                            </div>
                        </div>

                        <div class="form-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <label>Nomor Dokumen</label>
                                    <input type="text" class="form-control" id="nomor" name="nomor" value="">
                                </div>
                            </div>
                        </div>
                        <div class="form-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <label>Tanggal Dokumen</label>
                                    <input type="text" class="form-control" id="tanggal" name="tanggal" value="">
                                </div>
                            </div>
                        </div>

                        <div class="modal-footer">
                            <button type="submit" class="btn blue" name="edit_bc261">Save</button>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        </div>
                    </form>
                </div>	
            </div>
            <br></br>
            <div class="row">
                <div class="table-wrapper-scroll-y my-custom-scrollbar">
                    <table class="table table-striped table-bordered table-hover">

                        <thead>
                            <tr class="bg-success">
                                <th scope="col">No.</th>
                                <th scope="col">Jenis Dokumen</th>
                                <th scope="col">Nomor Dokumen</th>
                                <th scope="col">Tanggal</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <?php
                                $n = 0;
                                if ($ptb_other) {
                                    foreach ($ptb_other as $data) {
                                        $n = $n + 1;
                                        ?>
                                    <tr><td width="30%"><?php echo $n; ?></td>
                                        <td width="30%"><?php echo $data->URAIAN_DOKUMEN; ?></td>
                                        <td width="30%"><?php echo $data->NOMOR_DOKUMEN; ?></td>
                                        <td width="30%"><?php echo $data->TANGGAL_DOKUMEN; ?></td>
                                        <td><button type="button" class="btn btn-dark" onclick="tampilkanEditDokumen(<?php echo $data->id_dokumen; ?>)"><i class="fa fa-info"></i></button>
                                            <a href="javascript:dialogHapus('<?php echo base_url('preview_BC23/delete_dokumen/' . $data->id_dokumen . ''); ?>')" class='btn red'><i class="fa fa-trash-o"></i></a></td>
                                    </tr>
                                    <?php
                                }
                            }
                            ?>
                            </tr>	    

                        </tbody>
                    </table>



                </div>
            </div>	
        </div>

        <div class="modal2-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
    </div>
    <script>
        function dialogHapus(urlHapus) {
            if (confirm("Apakah anda yakin ingin menghapus ini ?")) {
                document.location = urlHapus;
            }
        }
    </script>
</div>
<div id="modal6" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog">
    <div class="modal-content">
        <div class="modal-header">
            <h3 class="modal-title"><b>HARGA</b></h3>
        </div>
        <div class="modal-body">
            <form action="preview_BC23/update_modal_harga" method = "POST">
                <div class="row">
                    <div class="table-wrapper-scroll-y my-custom-scrollbar">
                        <table class="table table-striped table-bordered table-hover">
                            <div class="form-group row">
                                <li>
                                    <div class="col-sm-4">
                                        Harga :<input type="text" class="form-control" name="harga" value ="<?php
                                        if ($ptb_edit) {
                                            foreach ($ptb_edit as $data) {
                                                echo $data->kode_harga;
                                            }
                                        }
                                        ?>" >
                                        <input type="hidden" class="form-control" name="id_header" value ="<?php
                                        if ($ptb_edit) {
                                            foreach ($ptb_edit as $data) {
                                                echo $data->id;
                                            }
                                        }
                                        ?>" >
                                    </div>
                                    <div class="col-sm-4">
                                        Valuta :<input type="text" class="form-control" name="valuta" value ="<?php
                                        if ($ptb_edit) {
                                            foreach ($ptb_edit as $data) {
                                                echo $data->kode_valuta . " - " . $data->uraian_valuta;
                                            }
                                        }
                                        ?>" >
                                    </div>
                                    <div class="col-sm-4">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Get Kurs</button>
                                    </div>

                                </li>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-4">
                                    NDPBM :<input type="text" class="form-control" name="modal_ndpbm" value ="<?php
                                    if ($ptb_edit) {
                                        foreach ($ptb_edit as $data) {
                                            echo $data->NDPBM;
                                        }
                                    }
                                    ?>" >
                                </div>
                                <div class="col-sm-4">
                                    Harga FOB :<input type="text" class="form-control" name="harga_fob" value ="<?php
                                    if ($ptb_edit) {
                                        foreach ($ptb_edit as $data) {
                                            echo $data->FOB;
                                        }
                                    }
                                    ?>" >
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-4">
                                    Biaya Tambahan :<input type="text" class="form-control" name="biaya_tambahan" value ="<?php
                                    if ($ptb_edit) {
                                        foreach ($ptb_edit as $data) {
                                            echo $data->biaya_tambahan;
                                        }
                                    }
                                    ?>" >
                                </div>
                                <div class="col-sm-4">
                                    Diskon :<input type="text" class="form-control" name="diskon" value ="<?php
                                    if ($ptb_edit) {
                                        foreach ($ptb_edit as $data) {
                                            echo $data->diskon;
                                        }
                                    }
                                    ?>" >
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-4">
                                    Asuransi Bayar di :<input type="text" class="form-control" name="asuransi_bayar_di" value ="<?php
                                    if ($ptb_edit) {
                                        foreach ($ptb_edit as $data) {
                                            echo $data->asuransi;
                                        }
                                    }
                                    ?>" >
                                </div>
                                <div class="col-sm-4">
                                    Nilai Asuransi :<input type="text" class="form-control" name="nilai_asuransi" value ="" >
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-4">
                                    Freight :<input type="text" class="form-control" name="freight" value ="<?php
                                    if ($ptb_edit) {
                                        foreach ($ptb_edit as $data) {
                                            echo $data->freight;
                                        }
                                    }
                                    ?>" >
                                </div>
                                <div class="col-sm-4">
                                    FOB :<input type="text" class="form-control" name="fob" value ="<?php
                                    if ($ptb_edit) {
                                        foreach ($ptb_edit as $data) {
                                            echo $data->FOB;
                                        }
                                    }
                                    ?>" >
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-4">
                                    CIF :<input type="text" class="form-control" name="cif" value ="<?php
                                    if ($ptb_edit) {
                                        foreach ($ptb_edit as $data) {
                                            echo $data->cif;
                                        }
                                    }
                                    ?>
                                                " >
                                </div>
                                <div class="col-sm-4">
                                    CIF Rp :<input type="text" class="form-control" name="cif_rp" value ="<?php
                                    if ($ptb_edit) {
                                        foreach ($ptb_edit as $data) {
                                            echo $data->cif_rupiah;
                                        }
                                    }
                                    ?>" >
                                </div>
                            </div>


                        </table>



                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn blue" name="edit_bc261">Save</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>


    </div>

</div>
<div id="modal2" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog">
    <div class="modal-content">
        <div class="modal-header">
            <h3 class="modal-title"><b>KONTAINER</b></h3>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-sm-12">
                    <form action="preview_BC23/update_popup_kontainer" method = "POST">
                        <div class="form-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <input type="hidden" class="form-control" id="id_kontainer" name="id_kontainer" value="">
                                </div>
                            </div>
                        </div>
                        <div class="form-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <label>Nomor Kontainer</label>
                                    <input type="text" class="form-control" id="nomor_kontainer" name="nomor_kontainer" value="">
                                </div>
                            </div>
                        </div>

                        <div class="form-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <label>Ukuran</label>
                                    <input type="text" class="form-control" id="ukuran_kontainer" name="ukuran_kontainer" value="">
                                </div>
                            </div>
                        </div>
                        <div class="form-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <label>Tipe Kontainer</label>
                                    <input type="text" class="form-control" id="tipe_kontainer" name="tipe_kontainer" value="">
                                </div>
                            </div>
                        </div>



                        <div class="modal-footer">
                            <button type="submit" class="btn blue" name="edit_bc261">Save</button>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        </div>
                    </form>
                </div>	
            </div>
            <br></br>
            <div class="row">
                <div class="table-wrapper-scroll-y my-custom-scrollbar">
                    <table class="table table-striped table-bordered table-hover">

                        <thead>
                            <tr class="bg-success">
                                <th scope="col">Nomor Kontainer</th>
                                <th scope="col">Ukuran</th>
                                <th scope="col">Tipe</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <?php
                                $n = 0;
                                if ($ptb_kontainer) {
                                    foreach ($ptb_kontainer as $data) {
                                        $n = $n + 1;
                                        ?>
                                        <td width="30%"><?php echo $data->NOMOR_KONTAINER; ?></td>
                                        <td width="30%"><?php echo $data->URAIAN_UKURAN_KONTAINER; ?></td>
                                        <td width="30%"><?php echo $data->URAIAN_TIPE_KONTAINER; ?></td>
                                        <td><button type="button" class="btn btn-dark" onclick="tampilkanEditKontainer(<?php echo $data->id_kontainer; ?>)"><i class="fa fa-info"></i></button>
                                            <a href="javascript:dialogHapus('<?php echo base_url('preview_BC23/delete_kontainer/' . $data->id_kontainer . ''); ?>')" class='btn red'><i class="fa fa-trash-o"></i></a></td>
                                    </tr>
                                    <?php
                                }
                            }
                            ?>
                            </tr>	    

                        </tbody>
                    </table>



                </div>
            </div>	
        </div>

        <div class="modal2-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
    </div>
    <script>
        function dialogHapus(urlHapus) {
            if (confirm("Apakah anda yakin ingin menghapus ini ?")) {
                document.location = urlHapus;
            }
        }
    </script>
</div>

<div id="modal3" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog">
    <div class="modal-content">
        <div class="modal-header">
            <h3 class="modal-title"><b>KEMASAN</b></h3>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-sm-12">
                    <form action="preview_BC23/update_popup_kemasan" method = "POST">
                        <div class="form-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <input type="hidden" class="form-control" id="id_kemasan" name="id_kemasan" value="">
                                </div>
                            </div>
                        </div>
                        <div class="form-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <label>Jumlah Kemasan</label>
                                    <input type="text" class="form-control" id="jml_kemasan" name="jml_kemasan" value="">
                                </div>
                            </div>
                        </div>

                        <div class="form-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <label>Kode Jenis Kemasan</label>
                                    <input type="text" class="form-control" id="kode_kemasan" name="kode_kemasan" value="">
                                </div>
                            </div>
                        </div>
                        <div class="form-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <label>Uraian Kemasan</label>
                                    <input type="text" class="form-control" id="uraian_kemasan" name="uraian_kemasan" value="">
                                </div>
                            </div>
                        </div>
                        <div class="form-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <label>Merk Kemasan</label>
                                    <input type="text" class="form-control" id="merk_kemasan" name="merk_kemasan" value="">
                                </div>
                            </div>
                        </div>


                        <div class="modal-footer">
                            <button type="submit" class="btn blue" name="edit_bc261">Save</button>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        </div>
                    </form>
                </div>	
            </div>
            <br></br>
            <div class="row">
                <div class="table-wrapper-scroll-y my-custom-scrollbar">
                    <table class="table table-striped table-bordered table-hover">

                        <thead>
                            <tr class="bg-success">
                                <th scope="col">Jumlah</th>
                                <th scope="col">Kode Jenis</th>
                                <th scope="col">Uraian</th>
                                <th scope="col">Merk Kemasan</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <?php
                                $n = 0;
                                if ($ptb_kemasan) {
                                    foreach ($ptb_kemasan as $data) {
                                        $n = $n + 1;
                                        ?>
                                        <td width="30%"><?php echo $data->JUMLAH_KEMASAN; ?></td>
                                        <td width="30%"><?php echo $data->KODE_JENIS_KEMASAN; ?></td>
                                        <td width="30%"><?php echo $data->URAIAN_KEMASAN; ?></td>
                                        <td width="30%"><?php echo $data->MERK_KEMASAN; ?></td>
                                        <td><button type="button" class="btn btn-dark" onclick="tampilkanEditKemasan(<?php echo $data->id_kemasan; ?>)"><i class="fa fa-info"></i></button>
                                            <a href="javascript:dialogHapus('<?php echo base_url('preview_BC23/delete_kemasan/' . $data->id_kemasan . ''); ?>')" class='btn red'><i class="fa fa-trash-o"></i></a></td>

                                        <?php
                                    }
                                }
                                ?>
                            </tr>	    

                        </tbody>
                    </table>



                </div>
            </div>	
        </div>

        <div class="modal2-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
    </div>
    <script>
        function dialogHapus(urlHapus) {
            if (confirm("Apakah anda yakin ingin menghapus ini ?")) {
                document.location = urlHapus;
            }
        }
    </script>
</div>

</div>
<!-- TUTUP CONTAINER -->
<script type="text/javascript">
    function tampilkanEditDokumen(id) {
// alert(id);
        $.ajax({
            url: "<?php echo base_url('preview_BC23/editmodal/') ?>" + id,
            type: "GET",
            dataType: "JSON",
            success: function (data)
            {

                $("#dokumen").empty();
                $("#dokumen").val(data.dokumenedit.URAIAN_DOKUMEN);
                $("#nomor").empty();
                $("#nomor").val(data.dokumenedit.NOMOR_DOKUMEN);
                $("#tanggal").empty();
                $("#tanggal").val(data.dokumenedit.TANGGAL_DOKUMEN);
                $("#id").empty();
                $("#id").val(data.dokumenedit.ID);
                $("#modal4").modal('show');

            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error get data from ajax');
            }
        });
// $('#modal').modal();

// var url= '<?php echo base_url(); ?>Bc25_controller/editmodal/'+id;
// $.get( url, function( data ) {
// // console.log(data);
// $("#dokumen").empty();
// $("#dokumen").val(data.KODE_JENIS_DOKUMEN);
// $("#nomor").empty();
// $("#nomor").val(data.NOMOR_DOKUMEN);
// $("#tgl").empty();
// $("#tgl").val(data.TANGGAL_DOKUMEN);
// });
    }
</script>
<script type="text/javascript">
    function tampilkanEditKemasan(id_kemasan) {
// alert(id_kemasan);
        $.ajax({
            url: "<?php echo base_url('preview_BC23/editmodal_kemasan/') ?>" + id_kemasan,
            type: "GET",
            dataType: "JSON",
            success: function (data)
            {

                $("#jml_kemasan").empty();
                $("#jml_kemasan").val(data.dokumenedit.JUMLAH_KEMASAN);
                $("#kode_kemasan").empty();
                $("#kode_kemasan").val(data.dokumenedit.KODE_JENIS_KEMASAN);
                $("#uraian_kemasan").empty();
                $("#uraian_kemasan").val(data.dokumenedit.URAIAN_KEMASAN);
                $("#merk_kemasan").empty();
                $("#merk_kemasan").val(data.dokumenedit.MERK_KEMASAN);
                $("#id_kemasan").empty();
                $("#id_kemasan").val(data.dokumenedit.ID);
                $("#modal3").modal('show');

            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error get data from ajax');
            }
        });
// $('#modal').modal();

// var url= '<?php echo base_url(); ?>Bc25_controller/editmodal/'+id;
// $.get( url, function( data ) {
// // console.log(data);
// $("#dokumen").empty();
// $("#dokumen").val(data.KODE_JENIS_DOKUMEN);
// $("#nomor").empty();
// $("#nomor").val(data.NOMOR_DOKUMEN);
// $("#tgl").empty();
// $("#tgl").val(data.TANGGAL_DOKUMEN);
// });
    }
</script>
<script type="text/javascript">
    function tampilkanEditKontainer(id_kontainer) {
// alert(id_kontainer);
        $.ajax({
            url: "<?php echo base_url('preview_BC23/editmodal_kontainer/') ?>" + id_kontainer,
            type: "GET",
            dataType: "JSON",
            success: function (data)
            {

                $("#nomor_kontainer").empty();
                $("#nomor_kontainer").val(data.dokumenedit.NOMOR_KONTAINER);
                $("#ukuran_kontainer").empty();
                $("#ukuran_kontainer").val(data.dokumenedit.URAIAN_UKURAN_KONTAINER);
                $("#tipe_kontainer").empty();
                $("#tipe_kontainer").val(data.dokumenedit.URAIAN_TIPE_KONTAINER);
                $("#id_kontainer").empty();
                $("#id_kontainer").val(data.dokumenedit.ID);
                $("#modal2").modal('show');

            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error get data from ajax');
            }
        });
// $('#modal').modal();

// var url= '<?php echo base_url(); ?>Bc25_controller/editmodal/'+id;
// $.get( url, function( data ) {
// // console.log(data);
// $("#dokumen").empty();
// $("#dokumen").val(data.KODE_JENIS_DOKUMEN);
// $("#nomor").empty();
// $("#nomor").val(data.NOMOR_DOKUMEN);
// $("#tgl").empty();
// $("#tgl").val(data.TANGGAL_DOKUMEN);
// });
    }
</script>
