<div class="container">
	<div class="row">
		<p class="text-center" style="font-size: 17px"><b>PEMBERITAHUAN PENGELUARAN UNTUK DIANGKUT DARI TEMPAT PENIMBUNAN<br>BERIKAT KE TEMPAT PENIMBUNAN BERIKAT LAINNYA</b></p>
	</div>
	
	<form role="form" method="post">
		<div class="form-group row">
			<label for="status" class="col-sm-2 col-form-label">Status</label>
			<div class="col-sm-2">
					<input style="border: none;" type="text" readonly class="form-control" id="uraian_status" name="uraian_status" value="<?=isset($default['URAIAN_STATUS'])? $default['URAIAN_STATUS'] : ""?>">
				</div>
			<div class="col-sm-2">
				<input style="border: none;" type="hidden" readonly class="form-control" id="status" name="status" value="<?=isset($default['KODE_STATUS'])? $default['KODE_STATUS'] : ""?>">
			</div>
		</div>

		<div class="form-group row">
			<label for="status_perbaikan" class="col-sm-2 col-form-label">Status Perbaikan</label>
			<div class="col-sm-2">
				<input style="border: none;" type="hidden" readonly class="form-control" id="status_perbaikan" name="status_perbaikan" value="<?=isset($default['KODE_STATUS_PERBAIKAN'])? $default['KODE_STATUS_PERBAIKAN'] : ""?>">
				<input style="border: none;" type="text" readonly class="form-control" name="uraian_status_perbaikan" value="<?=isset($default['uraian_status_perbaikan'])? $default['uraian_status_perbaikan'] : ""?>">
			</div>
			<div style="margin-top: -10px;" class="col-sm-2 col-sm-offset-6">
                <a href="#modal-daftar-respon" data-target="#modal-daftar-respon" data-toggle="modal">
                    <p><b>DAFTAR RESPON</b></p>
                </a>
            </div>
		</div>
		
		<!-- FORM NOMOR PENGAJUAN -->
		<div style="margin-top: 30px;">
			<div class="form-group row">
				<label for="nomor_pengajuan" class="col-sm-2 col-form-label">Nomor Pengajuan</label>
				<div class="col-sm-4">
					<input type="text" class="form-control" id="nomor_pengajuan" name="nomor_pengajuan" value="<?=isset($default['NOMOR_AJU'])? $default['NOMOR_AJU'] : ""?>" minlength="26" required>
				</div>
			</div>

			<div class="form-group row">
				<label for="nomor_pendaftaran" class="col-sm-2 col-form-label">Nomor Pendaftaran</label>
				<div class="col-sm-4">
					<input type="text" class="form-control" id="nomor_pendaftaran" name="nomor_pendaftaran" value="<?=isset($default['NOMOR_DAFTAR'])? $default['NOMOR_DAFTAR'] : ""?>">
				</div>
			</div>

			<div class="form-group row">
				<label for="tanggal_pendaftaran" class="col-sm-2 col-form-label">Tanggal Pendaftaran</label>
				<div class="col-sm-4">
					<input type="text" class="form-control" id="tanggal_pendaftaran" name="tanggal_pendaftaran" value="<?=isset($default['TANGGAL_DAFTAR'])? $default['TANGGAL_DAFTAR'] : ""?>" placeholder="DDMMYY">
				</div>
			</div>		
		</div>
		<!-- TUTUP FORM NOMOR PENGAJUAN -->
		
		<!-- FORM KANTOR PABEAN -->
		<div style="margin-top: 30px;">
			<div class="form-group row">
				<label for="kppbc_bongkar" class="col-sm-2 col-form-label">Kantor Asal </label>
				<div class="col-sm-4">
					<input type="text" class="form-control" id="kantor_asal" name="kantor_asal" value="<?=isset($default['KODE_KANTOR'])? $default['KODE_KANTOR'] : ""?>">
				</div>
			</div>

			<div class="form-group row">
				<label for="kppbc_bongkar" class="col-sm-2 col-form-label">Kantor Tujuan </label>
				<div class="col-sm-4">
					<input type="text" class="form-control" id="kantor_tujuan" name="kantor_tujuan" value="<?=isset($default['KODE_KANTOR_TUJUAN'])? $default['KODE_KANTOR_TUJUAN'] : ""?>">
				</div>
			</div>

			<div class="form-group row">
				<label for="tujuan" class="col-sm-2 col-form-label">Jenis TPB Asal</label>
				<div class="col-sm-4">
					<select id="jenis_tpb_asal" name="jenis_tpb_asal" class="form-control">
				        <option value="<?=isset($default['KODE_JENIS_TPB'])? $default['KODE_JENIS_TPB'] : ""?>"><?=isset($default['URAIAN_JENIS_TPB'])? $default['URAIAN_JENIS_TPB'] : ""?></option>
                         <?php foreach($jenis_tpb as $row){ ?>
                           <option value="<?php echo $row['KODE_JENIS_TPB'];?>"><?php echo $row['URAIAN_JENIS_TPB'];?></option>
                         <?php } ?>
			        </select>
				</div>
			</div>	

			<div class="form-group row">
				<label for="tujuan" class="col-sm-2 col-form-label">Jenis TPB Tujuan</label>
				<div class="col-sm-4">
					<select id="jenis_tpb_tujuan" name="jenis_tpb_tujuan" class="form-control">
				        <option value="<?=isset($default['KODE_TUJUAN_TPB'])? $default['KODE_TUJUAN_TPB'] : ""?>"><?=isset($default['URAIAN_TUJUAN_TPB'])? $default['URAIAN_TUJUAN_TPB'] : ""?></option>
                         <?php foreach($tujuan_tpb as $row){ ?>
                           <option value="<?php echo $row['KODE_TUJUAN_TPB'];?>"><?php echo $row['URAIAN_TUJUAN_TPB'];?></option>
                         <?php } ?>
			        </select>
				</div>
			</div>	

			<div class="form-group row">
				<label for="tujuan" class="col-sm-2 col-form-label">Tujuan Pengiriman</label>
				<div class="col-sm-4">
					<select id="tujuan_pengiriman" name="tujuan_pengiriman" class="form-control">
				        <option value="<?=isset($default['KODE_TUJUAN_PENGIRIMAN'])? $default['KODE_TUJUAN_PENGIRIMAN'] : ""?>"><?=isset($default['URAIAN_TUJUAN_PENGIRIMAN'])? $default['URAIAN_TUJUAN_PENGIRIMAN'] : ""?></option>
                         <?php foreach($tujuan_pengiriman as $row){ ?>
                           <option value="<?php echo $row['KODE_TUJUAN_PENGIRIMAN'];?>"><?php echo $row['URAIAN_TUJUAN_PENGIRIMAN'];?></option>
                         <?php } ?>
			        </select>
				</div>
			</div>
		</div>
		<!-- TUTUP FORM KANTOR PABEAN -->
		

		<div class="row">
			<div class="col-sm-12">

				<!-- FORM LEFT SIDE -->
				<ol>
					<div class="col-sm-6">
						<p><b>TPB ASAL BARANG</b></p>
							<!-- FORM TPB ASAL BARANG -->
							<div>
								<div class="form-group row">
									<li>
										<label for="nama" class="col-sm-3 col-form-label">NPWP</label>
										<div class="col-sm-4">
											<select id="kode_id_pengusaha" name="kode_id_pengusaha" class="form-control">
										        <option value="<?=isset($default['KODE_ID_PENGUSAHA'])? $default['KODE_ID_PENGUSAHA'] : $default_pengusaha['KODE_ID']?>"><?=isset($default['uraian_id_pengusaha'])? $default['uraian_id_pengusaha'] : $default_pengusaha['uraian_id_pengusaha']?></option>
                         							<?php foreach($jenis_id as $row){ ?>
                           						<option value="<?php echo $row['KODE_ID'];?>"><?php echo $row['URAIAN_KODE_ID'];?></option>
                        							<?php } ?>
									        </select>
										</div>
										<div class="col-sm-4">
											<input type="text" class="form-control" id="npwp_pengusaha" name="npwp_pengusaha" value="<?=isset($default['ID_PENGUSAHA'])? $default['ID_PENGUSAHA'] : $default_pengusaha['NPWP']?>">
										</div>
									</li>
								</div>
							
								<div class="form-group row">
									<li>
										<label for="nama" class="col-sm-3 col-form-label">Nama</label>
										<div class="col-sm-8">
											<input type="text" class="form-control" id="nama_pengusaha" name="nama_pengusaha" value="<?=isset($default['NAMA_PENGUSAHA'])? $default['NAMA_PENGUSAHA'] : $default_pengusaha['NAMA']?>">
										</div>
									</li>
								</div>

								<div class="form-group row">
									<label for="alamat" class="col-sm-3 col-form-label">Alamat</label>
									<div class="col-sm-8">
										<input type="text" class="form-control" id="alamat_pengusaha" name="alamat_pengusaha" value="<?=isset($default['ALAMAT_PENGUSAHA'])? $default['ALAMAT_PENGUSAHA'] : $default_pengusaha['ALAMAT']?>">
									</div>
								</div>

								<div class="form-group row">
									<label for="alamat" class="col-sm-3 col-form-label">No. Izin</label>
									<div class="col-sm-8">
										<input type="text" class="form-control" id="no_izin" name="no_izin" value="<?=isset($default['NOMOR_IJIN_TPB'])? $default['NOMOR_IJIN_TPB'] : $default_pengusaha['NOMOR_SKEP']?>">
									</div>
								</div>

								

							</div>
							<!-- TUTUP FORM TPB ASAL BARANG -->		
						

							<!-- FORM FORM TUJUAN BARANG -->
							<div>
								<p style="margin-left: -38px"><b>TPB TUJUAN BARANG</b></p>
									
								<div class="form-group row">
									<li>
										<label for="nama" class="col-sm-3 col-form-label">NPWP</label>
										<div class="col-sm-4">
											<select id="kode_id_penerima" name="kode_id_penerima" class="form-control">
										        <option value="<?=isset($default['KODE_ID_PENERIMA'])? $default['KODE_ID_PENERIMA'] : ""?>"><?=isset($default['uraian_id_penerima'])? $default['uraian_id_penerima'] : ""?></option>
                         							<?php foreach($jenis_id as $row){ ?>
                           						<option value="<?php echo $row['KODE_ID'];?>"><?php echo $row['URAIAN_KODE_ID'];?></option>
                        							<?php } ?>
									        </select>
										</div>
										<div class="col-sm-4">
											<input type="text" class="form-control" id="npwp_penerima" name="npwp_penerima" value="<?=isset($default['ID_PENERIMA_BARANG'])? $default['ID_PENERIMA_BARANG'] : ""?>">
										</div>
									</li>
								</div>	
								
								<div class="form-group row">
									<li>
										<label for="nama" class="col-sm-3 col-form-label">Nama</label>
										<div class="col-sm-8">
											<input type="text" class="form-control" id="nama_penerima" name="nama_penerima" value="<?=isset($default['NAMA_PENERIMA_BARANG'])? $default['NAMA_PENERIMA_BARANG'] : ""?>">
										</div>
									</li>
								</div>

								<div class="form-group row">
									<label for="alamat" class="col-sm-3 col-form-label">Alamat</label>
									<div class="col-sm-8">
										<input type="text" class="form-control" id="alamat_penerima" name="alamat_penerima" value="<?=isset($default['ALAMAT_PENERIMA_BARANG'])? $default['ALAMAT_PENERIMA_BARANG'] : ""?>">
									</div>
								</div>

								<div class="form-group row">
									<label for="alamat" class="col-sm-3 col-form-label">No. Izin</label>
									<div class="col-sm-8">
										<input type="text" class="form-control" id="no_izin_penerima" name="no_izin_penerima" value="<?=isset($default['NOMOR_IJIN_TPB_PENERIMA'])? $default['NOMOR_IJIN_TPB_PENERIMA'] : ""?>">
									</div>
								</div>

								
							</div>
							<!-- TUTUP FORM TPB TUJUAN BARANG-->
							
							
						
						<a href="#modal" data-target="#modal" data-toggle="modal"><p style="margin-left: -38px"><b>DOKUMEN</b></p></a>
						<div class="form-group row">
							<li>
								<label for="nama" class="col-sm-3 col-form-label">Invoice</label>
								<div class="col-sm-4">
									<input type="text" <?=isset($default['ID'])? "" : "readonly=true"?> class="form-control" id="invoice" name="invoice" value="<?=isset($default_dok_inv['NOMOR_DOKUMEN'])? $default_dok_inv['NOMOR_DOKUMEN'] : ""?>">
								</div>
								<div class="col-sm-4">
									<input <?=isset($default['ID'])? "" : "readonly=true"?> placeholder="YYYY-MM-DD" type="text" class="form-control" id="tgl_invoice" name="tgl_invoice" value="<?=isset($default_dok_inv['TANGGAL_DOKUMEN'])? $default_dok_inv['TANGGAL_DOKUMEN'] : ""?>">
								</div>
							</li>
						</div>

						<div class="form-group row">
							<li>
								<label for="kppbc_pengawas" class="col-sm-3 col-form-label">Packing List</label>
								<div class="col-sm-4">
									<input <?=isset($default['ID'])? "" : "readonly=true"?> type="text" class="form-control" id="packing_list" name="packing_list" value="<?=isset($default_dok_pl['NOMOR_DOKUMEN'])? $default_dok_pl['NOMOR_DOKUMEN'] : ""?>">
								</div>
								<div class="col-sm-4">
									<input placeholder="YYYY-MM-DD" type="text" <?=isset($default['ID'])? "" : "readonly=true"?> class="form-control" id="tgl_packing_list" name="tgl_packing_list" value="<?=isset($default_dok_pl['TANGGAL_DOKUMEN'])? $default_dok_pl['TANGGAL_DOKUMEN'] : ""?>">
								</div>
							</li>
						</div>

						<div class="form-group row">
							<li>
								<label for="kppbc_pengawas" class="col-sm-3 col-form-label">Kontrak</label>
								<div class="col-sm-4">
									<input <?=isset($default['ID'])? "" : "readonly=true"?> type="text" class="form-control" id="kontrak" name="kontrak" value="<?=isset($default_dok_ktr['NOMOR_DOKUMEN'])? $default_dok_ktr['NOMOR_DOKUMEN'] : ""?>">
								</div>
								<div class="col-sm-4">
									<input <?=isset($default['ID'])? "" : "readonly=true"?> placeholder="YYYY-MM-DD" type="text" class="form-control" id="tgl_kontrak" name="tgl_kontrak" value="<?=isset($default_dok_ktr['TANGGAL_DOKUMEN'])? $default_dok_ktr['TANGGAL_DOKUMEN'] : ""?>">
								</div>
							</li>
						</div>
						<div class="form-group row">
							<li>
								<label for="kppbc_pengawas" class="col-sm-3 col-form-label">Surat Jalan</label>
								<div class="col-sm-4">
									<input <?=isset($default['ID'])? "" : "readonly=true"?> type="text" class="form-control" id="surat_jalan" name="surat_jalan" value="<?=isset($default_dok_sj['NOMOR_DOKUMEN'])? $default_dok_sj['NOMOR_DOKUMEN'] : ""?>">
								</div>
								<div class="col-sm-4">
									<input <?=isset($default['ID'])? "" : "readonly=true"?> placeholder="YYYY-MM-DD" type="text" class="form-control" id="tgl_surat_jalan" name="tgl_surat_jalan" value="<?=isset($default_dok_sj['TANGGAL_DOKUMEN'])? $default_dok_sj['TANGGAL_DOKUMEN'] : ""?>">
								</div>
							</li>
						</div>

						<div class="form-group row">
							<li>
								<label for="kppbc_pengawas" class="col-sm-3 col-form-label">SKEP</label>
								<div class="col-sm-4">
									<input <?=isset($default['ID'])? "" : "readonly=true"?> type="text" class="form-control" id="skep" name="skep" value="<?=isset($default_dok_skep['NOMOR_DOKUMEN'])? $default_dok_skep['NOMOR_DOKUMEN'] : ""?>">
								</div>
								<div class="col-sm-4">
									<input <?=isset($default['ID'])? "" : "readonly=true"?> placeholder="YYYY-MM-DD" type="text" class="form-control" id="tgl_skep" name="tgl_skep" value="<?=isset($default_dok_skep['TANGGAL_DOKUMEN'])? $default_dok_skep['TANGGAL_DOKUMEN'] : ""?>">
								</div>
							</li>
						</div>

						<div class="form-group row">
							<li>
								<label for="tujuan" class="col-sm-10 col-form-label">Dokumen Lainnya</label>
							</li>
						</div>

						<div class="form-group row">
							<div class="col-sm-10">
								<div class="table-wrapper-scroll-y my-custom-scrollbar">
								<table class="table table-sm">
								  <thead>
								    <tr class="bg-success">
								      <th scope="col">Jenis Dokumen</th>
								      <th scope="col">Nomor Dokumen</th>
								      <th scope="col">Tanggal</th>
								    </tr>
								  </thead>
								  <tbody>
								    <?php   foreach($dokumen_luar as $row){ ?>
								    <tr>
								      <th><?php echo $row['URAIAN_DOKUMEN'];?></th>
								      <td><?php echo $row['NOMOR_DOKUMEN'];?></td>
								      <td><?php echo $row['TANGGAL_DOKUMEN'];?></td>
								    </tr>
								    <?php
                    				}
                  					?>
								  </tbody>
								</table>
							</div>
							</div>
						</div>

						<p style="margin-left: -38px"><b>HARGA</b></p>
						<div class="form-group row">
							<li>
								<label for="nama" class="col-sm-3 col-form-label">Valuta</label>
								<div class="col-sm-4">
									<input type="text" class="form-control" id="valuta" name="valuta" value="<?=isset($default['KODE_VALUTA'])? $default['KODE_VALUTA'] : ""?>">
								</div>
							</li>
						</div>

						<div class="form-group row">
							<li>
								<label for="kppbc_pengawas" class="col-sm-3 col-form-label">CIF</label>
								<div class="col-sm-3">
									<input type="text" class="form-control" id="nilai_cif" name="nilai_cif" value="<?=isset($default['CIF'])? $default['CIF'] : ""?>">
								</div>
							</li>
						</div>

						<div class="form-group row">
							<li>
								<label for="kppbc_pengawas" class="col-sm-3 col-form-label">Harga Penyerahan</label>
								<div class="col-sm-3">
									<input type="text" class="form-control" id="harga_penyerahan" name="harga_penyerahan" value="<?=isset($default['HARGA_PENYERAHAN'])? $default['HARGA_PENYERAHAN'] : ""?>">
								</div>
							</li>
						</div>
						<div class="form-group row">						
						<label for="kppbc_pengawas" class="col-sm-8 col-form-label">Dengan ini saya menyatakan bertanggung jawab atas kebenaran hal-hal yang diberitahukan dalam dokumen ini</label>
						</div>
						<div class="form-group row">	
							<li>
								<div class="col-sm-4">
									<input type="text" class="form-control" id="kota_ttd" name="kota_ttd" value="<?=isset($default['KOTA_TTD'])? $default['KOTA_TTD'] : ""?>">
								</div>
								<div class="col-sm-4">
									<input placeholder="YYYY-MM-DD" type="text" class="form-control" id="tgl_ttd" name="tgl_ttd" value="<?=isset($default['TANGGAL_TTD'])? $default['TANGGAL_TTD'] : date("Y-m-d")?>">
								</div>
							</li>
						</div>
						<div class="form-group row">	
							<label for="kppbc_pengawas" class="col-sm-3 col-form-label">Pemberitahu</label>
								<div class="col-sm-5">
									<input type="text" class="form-control" id="nama_ttd" name="nama_ttd" value="<?=isset($default['NAMA_TTD'])? $default['NAMA_TTD'] : ""?>">
								</div>
						</div>
						<div class="form-group row">	
							<label for="kppbc_pengawas" class="col-sm-3 col-form-label">Jabatan</label>
								<div class="col-sm-5">
									<input type="text" class="form-control" id="jabatan_ttd" name="jabatan_ttd" value="<?=isset($default['JABATAN_TTD'])? $default['JABATAN_TTD'] : ""?>">
								</div>
						</div>

					</div>

					<!-- TUTUP FORM LEFT SIDE -->


					<!-- FORM RIGHT SIDE -->
					<div class="col-sm-6" style="padding-right: 50px;">

						<!-- FORM FORM PENGANGKUTAN -->
							<div>
								<p style="margin-left: -38px"><b>PENGANGKUTAN</b></p>
									
									
								
								<div class="form-group row">
									<li>
										<label for="nama" class="col-sm-3 col-form-label">Jenis Sarana Pengangkut Darat</label>
										<div class="col-sm-8">
											<input type="text" class="form-control" id="pengangkut_darat" name="pengangkut_darat" value="<?=isset($default['NAMA_PENGANGKUT'])? $default['NAMA_PENGANGKUT'] : ""?>">
										</div>
									</li>
								</div>

								<div class="form-group row">
									<label for="alamat" class="col-sm-3 col-form-label">No. Polisi</label>
									<div class="col-sm-8">
										<input type="text" class="form-control" id="nopol" name="nopol" value="<?=isset($default['NOMOR_POLISI'])? $default['NOMOR_POLISI'] : ""?>">
									</div>
								</div>
								
							</div>
							<!-- TUTUP FORM PENGANGKUTAN-->


						<!-- FORM FORM SEGEL BC ASAL -->
							<div>
								<p style="margin-left: -38px"><b>SEGEL BC ASAL</b></p>
									
									
								
								<div class="form-group row">
									<li>
										<label for="nama" class="col-sm-3 col-form-label">No. Segel</label>
										<div class="col-sm-8">
											<input type="text" class="form-control" id="nosegel" name="nosegel" value="<?=isset($default[''])? $default[''] : ""?>">
										</div>
									</li>
								</div>

								<div class="form-group row">
									<label for="alamat" class="col-sm-3 col-form-label">Jenis</label>
									<div class="col-sm-8">
										<input type="text" class="form-control" id="jenis_segel" name="jenis_segel" value="<?=isset($default[''])? $default[''] : ""?>">
									</div>
								</div>
								
								<div class="form-group row">
									<label for="alamat" class="col-sm-3 col-form-label">Catatan BC Tujuan</label>
									<div class="col-sm-8">
										<input type="text" class="form-control" id="catatan_bc" name="catatan_bc" value="<?=isset($default[''])? $default[''] : ""?>">
									</div>
								</div>
							</div>
							<!-- TUTUP FORM SEGEL BC ASAL-->

						<!-- FORM KONTAINER -->
							<div>
								<a href="#modal2" data-target="#modal2" data-toggle="modal"><p style="margin-left: -38px"><b>KONTAINER</b></p></a>
							<div class="form-group row">
							<div class="col-sm-10">
								<div class="table-wrapper-scroll-y my-custom-scrollbar">
								<table class="table table-sm">
								  <thead>
								    <tr class="bg-success">
								      <th scope="col">Nomor Cont</th>
								      <th scope="col">Ukuran</th>
								      <th scope="col">Tipe</th>
								    </tr>
								  </thead>
								  <tbody>
								    <?php   foreach($kontainer as $row){ ?>
								    <tr>
								      <th><?php echo $row['NOMOR_KONTAINER'];?></th>
								      <td><?php echo $row['KODE_UKURAN_KONTAINER'];?></td>
								      <td><?php echo $row['URAIAN_UKURAN_KONTAINER'];?></td>
								    </tr>
								    <?php
                    				}
                  					?>
						  		</tbody>
				 				</table>
				 			</div>
							</div>
							</div>	

							</div>
							<!-- TUTUP FORM KONTAINER -->
			<a href="#modalkemasan" data-target="#modalkemasan" data-toggle="modal"><p style="margin-left: -38px"><b>KEMASAN</b></p></a>
			<div class="form-group row">
							<div class="col-sm-10">
								<div class="table-wrapper-scroll-y my-custom-scrollbar">
								<table class="table table-sm">
								  <thead>
								    <tr class="bg-success">
								      <th scope="col">Jumlah</th>
								      <th scope="col">Kode Jenis</th>
								      <th scope="col">Jenis</th>
								    </tr>
								  </thead>
								  <tbody>
								  	 <?php   foreach($kemasan as $row){ ?>
								    <tr>
								      <th><?php echo $row['JUMLAH_KEMASAN'];?></th>
								      <td><?php echo $row['KODE_JENIS_KEMASAN'];?></td>
								      <td><?php echo $row['URAIAN_KEMASAN'];?></td>
								    </tr>
								    <?php
                    				}
                  					?>
						  </tbody>
				 	</table>
				 </div>
				</div>
			</div>

			<!-- FORM BARANG -->
							<div>
								<a href="<?php if(isset($default['ID'])){
										echo base_url('Bc27_detail_controller/form/'.$default['ID'].'');
									}else{echo "#";} ?>"><p style="margin-left: -38px"><b>BARANG</b></p></a>
								<!-- <a href="<?php echo base_url('Bc27_detail_controller/form/'.$default['ID'].'');?>"><p style="margin-left: -38px"><b>BARANG</b></p></a> -->
							<div class="form-group row">
							<li>
								<label for="kppbc_pengawas" class="col-sm-3 col-form-label">Volume (m 3)</label>
								<div class="col-sm-3">
									<input type="text" class="form-control" id="volume" name="volume" value="<?=isset($default['VOLUME'])? $default['VOLUME'] : ""?>">
								</div>
							</li>
							</div>

							<div class="form-group row">
							<li>
								<label for="kppbc_pengawas" class="col-sm-3 col-form-label">Berat Kotor (Kg)</label>
								<div class="col-sm-3">
									<input type="text" class="form-control" id="bruto" name="bruto" value="<?=isset($default['BRUTO'])? $default['BRUTO'] : ""?>">
								</div>
							</li>
							</div>

							<div class="form-group row">
							<li>
								<label for="kppbc_pengawas" class="col-sm-3 col-form-label">Berat Bersih (Kg)</label>
								<div class="col-sm-3">
									<input type="text" class="form-control" id="netto" name="netto" value="<?=isset($default['NETTO'])? $default['NETTO'] : ""?>">
								</div>
							</li>
							</div>

							<div class="form-group row">
							<li>
								<label for="kppbc_pengawas" class="col-sm-3 col-form-label">Jumlah Barang</label>
								<div class="col-sm-3">
									<input type="text" class="form-control" id="jumlah_barang" name="jumlah_barang" value="<?=isset($default['JUMLAH_BARANG'])? $default['JUMLAH_BARANG'] : ""?>">
								</div>
							</li>
							</div>	
								
							</div>
							<!-- TUTUP FORM BARAMG -->
													

					</ol>			
				</div>	

				<!-- TUTUP FORM RIGHT SIDE -->
				
			</div>
		</div>
		<div class="form-actions">
            <a href='<?php echo base_url('Bc27_controller');?>' class='btn default'> Cancel</a>
            <button type="submit" class="btn blue" name="submit_bc27">Simpan</button>
        </div>
	</form>

	<!-- MODAL DOKUMEN -->
		
		<div id="modal" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog">
		    <div class="modal-content">
		      <div class="modal-header">
		        <h3 class="modal-title"><b>Dokumen</b></h3>
		      </div>
		      <div class="modal-body">
				<div class="row">
					<div class="col-sm-12">
						<form action="<?php echo base_url('Bc27_controller/update_dokumen/'.$default['ID'].'');?>" method="post">
							<div class="form-body">
								<div class="row">
									<div class="col-sm-12">
										<label>Dokumen</label>
										<input type="hidden" class="form-control" id="id_dokumen" name="id_dokumen" value="">
										<input type="text" class="form-control" id="jenis_dokumen" name="jenis_dokumen">
									</div>
								</div>
							</div>

							<div class="form-body">
								<div class="row">
									<div class="col-sm-12">
										<label>Nomor</label>
										<input type="text" class="form-control" id="nomor_dokumen" name="nomor_dokumen" >
									</div>
								</div>
							</div>

							<div class="form-body">
								<div class="row">
									<div class="col-sm-12">
										<label>Tanggal</label>
										<input type="text" class="form-control" id="tgl_dokumen" name="tgl_dokumen" placeholder="DDMMYY" >
									</div>
								</div>
							</div>
						
					</div>	
				</div>
				
				<div class="row">
					<div class="table-wrapper-scroll-y my-custom-scrollbar">
						<table class="table table-striped table-bordered table-sm">
						    <thead>
						      <tr>
						          <th scope="col"><center><p style="font-size: 12px;">Seri</p></center></th>
							      <th scope="col"><center><p style="font-size: 12px;">Kode Dokumen</p></center></th>
							      <th scope="col"><center><p style="font-size: 12px;">Jenis Dokumen</p></center></th>
							      <th scope="col"><center><p style="font-size: 12px;">Nomor</p></center></th>
							      <th scope="col"><center><p style="font-size: 12px;">Tanggal</p></center></th>
						      </tr>
						    </thead>
						    <tbody>
						      <?php foreach ($dokumen_modal_luar as $row) { ?>
						        <tr>
						            <td><p style="font-size: 12px"><?php echo $row['SERI_DOKUMEN'];?></p></td>
						            <td><p style="font-size: 12px"><?php echo $row['KODE_JENIS_DOKUMEN'];?></p></td>
						            <td><p style="font-size: 12px"><?php echo $row['TIPE_DOKUMEN'];?></p></td>
						            <td><p style="font-size: 12px"><?php echo $row['NOMOR_DOKUMEN'];?></p></td>
						            <td><p style="font-size: 12px"><?php echo $row['TANGGAL_DOKUMEN'];?></p></td>
						            
						            <td>
						            	<button type="button" class="btn blue" onclick="tampilkanEditDokumen(<?php echo $row['ID'];?>)"><i class="fa fa-pencil"></i></button>
					                    <!-- <a href='<?php echo base_url('Bc25_controller/edit_dokumen_modal/'.$row['ID'].'');?>' class='btn blue'><i class="fa fa-pencil"></i></a> -->
					                    <a href="javascript:dialogHapus('<?php echo base_url('Bc27_controller/delete_dokumen/' . $row['ID'] . '/'.$default['ID'].''); ?>')" class='btn red'><i class="fa fa-trash-o"></i></a>
						            </td>
						        </tr>
						       <?php } ?>
						    </tbody>
						</table>
					</div>
			      </div>	
				</div>

		      <div class="modal-footer">
		        <button type="submit" class="btn blue" name="submit_modaldokumen">Simpan</button>
		        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
		      </div>
		      </form>
		    </div>
		  <script>
        function dialogHapus(urlHapus) {
            if (confirm("Apakah anda yakin ingin menghapus ini ?")) {
                document.location = urlHapus;
            }
        }
    </script>
		</div>

		<!-- MODAL KEMASAN -->
		
		<div id="modalkemasan" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog">
		    <div class="modal-content">
		      <div class="modal-header">
		        <h3 class="modal-title"><b>Kemasan</b></h3>
		      </div>
		      <div class="modal-body">
				<div class="row">
					<div class="col-sm-12">
						<form action="<?php echo base_url('Bc27_controller/update_kemasan/'.$default['ID'].'');?>" method="post">
							<div class="form-body">
								<div class="row">
									<div class="col-sm-12">
										<label>Jumlah</label>
										<input type="hidden" class="form-control" id="id_kemasan" name="id_kemasan" value="">
										<input type="text" class="form-control" id="jumlah_kemasan" name="jumlah_kemasan" value="">
									</div>
								</div>
							</div>

							<div class="form-body">
								<div class="row">
									<div class="col-sm-12">
										<label>Jenis</label>
										<input type="text" class="form-control" id="jenis_kemasan" name="jenis_kemasan" value="">
									</div>
								</div>
							</div>

							<div class="form-body">
								<div class="row">
									<div class="col-sm-12">
										<label>Merk</label>
										<input type="text" class="form-control" id="merk_kemasan" name="merk_kemasan" value="">
									</div>
								</div>
							</div>
					</div>	
				</div>
				
				<div class="row">
					<div class="table-wrapper-scroll-y my-custom-scrollbar">
						<table class="table table-striped table-bordered table-sm">
						    <thead>
						      <tr>
						          <th scope="col"><center><p style="font-size: 12px;">No</p></center></th>
							      <th scope="col"><center><p style="font-size: 12px;">Jumlah</p></center></th>
							      <th scope="col"><center><p style="font-size: 12px;">Kode</p></center></th>
							      <th scope="col"><center><p style="font-size: 12px;">Uraian</p></center></th>
							      <th scope="col"><center><p style="font-size: 12px;">Merk Kemasan</p></center></th>
						      </tr>
						    </thead>
						    <tbody>
						      <?php $a=0; foreach ($kemasan_modal_luar as $row) { $a++; ?>
						        <tr>
						            <td><p style="font-size: 12px"><?php echo $a;?></p></td>
						            <td><p style="font-size: 12px"><?php echo $row['JUMLAH_KEMASAN'];?></p></td>
						            <td><p style="font-size: 12px"><?php echo $row['KODE_JENIS_KEMASAN'];?></p></td>
						            <td><p style="font-size: 12px"><?php echo $row['URAIAN_KEMASAN'];?></p></td>
						            <td><p style="font-size: 12px"><?php echo $row['MERK_KEMASAN'];?></p></td>
						            
						            <td>
					                    <button type="button" class="btn blue" onclick="tampilkanEditKemasan(<?php echo $row['ID'];?>)"><i class="fa fa-pencil"></i></button>
					                    <a href="javascript:dialogHapus('<?php echo base_url('Bc27_controller/delete_kemasan/' . $row['ID'] . '/'.$default['ID'].''); ?>')" class='btn red'><i class="fa fa-trash-o"></i></a>
						            </td>
						        </tr>
						       <?php } ?>
						    </tbody>
						</table>
					</div>
			      </div>	
				</div>

		      <div class="modal-footer">
		        <button type="submit" class="btn blue" name="submit_modalkemasan">Simpan</button>
		        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
		      </div>
		      </form>
		    </div>
		    <script>
        function dialogHapus(urlHapus) {
            if (confirm("Apakah anda yakin ingin menghapus ini ?")) {
                document.location = urlHapus;
            }
        }
    </script>
		  
		</div>

		<!-- MODAL DAFTAR RESPON -->
        
        <div id="modal-daftar-respon" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog">
            <div class="modal-content">
              <div class="modal-header">
                <h3 class="modal-title"><b>Respon</b></h3>

                <div class="row">
                    <div class="col-sm-6">
                        <a href="#" class="btn btn-primary btn-sm" role="button" aria-pressed="true">Respon</a>
                       <!--  <a href="#" class="btn btn-primary btn-sm" role="button" aria-pressed="true">Status</a> -->
                    </div>
                    
                </div>
              </div>
              <div class="modal-body">
                
                <div class="row">
                    <div class="table-wrapper-scroll-y my-custom-scrollbar">
                        <table class="table table-striped table-bordered table-sm">
                            <thead>
                              <tr>
                                  <th scope="col"><center><p style="font-size: 12px;">Kode</p></center></th>
                                  <th scope="col"><center><p style="font-size: 12px;">Uraian</p></center></th>
                                  <th scope="col"><center><p style="font-size: 12px;">Waktu</p></center></th>
                              </tr>
                            </thead>
                            <tbody>
                              <?php foreach ($modal_daftar_respon as $data) { ?>
                                <tr>
                                    <td><p style="font-size: 12px"><?php echo $data['KODE_RESPON'];?></p></td>
                                    <td><p style="font-size: 12px"><?php echo $data['URAIAN_RESPON'];?></p></td>
                                    <td><p style="font-size: 12px"><?php echo $data['WAKTU_RESPON'];?></p></td>
                                </tr>
                               <?php } ?>
                            </tbody>
                        </table>
                    </div>
                  </div>    
                </div>

              <!-- <div class="modal-footer">
                <button type="button" class="btn btn-primary">Save changes</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              </div> -->
            </div>
        </div>
        <!-- TUTUP MODAL DAFTAR RESPON -->

        <!-- MODAL KONTAINER -->

<div id="modal2" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog">
    <div class="modal-content">
        <div class="modal-header">
            <h3 class="modal-title"><b>KONTAINER</b></h3>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-sm-12">
                    <form action="preview_BC261/update_popup_kontainer" method = "POST">
                        <div class="form-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <input type="hidden" class="form-control" id="id_kontainer" name="id_kontainer" value="">
                                </div>
                            </div>
                        </div>
                        <div class="form-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <label>Nomor Kontainer</label>
                                    <input type="text" class="form-control" id="nomor_kontainer" name="nomor_kontainer" value="">
                                </div>
                            </div>
                        </div>

                        <div class="form-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <label>Ukuran</label>
                                    <input type="text" class="form-control" id="ukuran_kontainer" name="ukuran_kontainer" value="">
                                </div>
                            </div>
                        </div>
                        <div class="form-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <label>Tipe Kontainer</label>
                                    <input type="text" class="form-control" id="tipe_kontainer" name="tipe_kontainer" value="">
                                </div>
                            </div>
                        </div>



                        <div class="modal-footer">
                            <button type="submit" class="btn blue" name="edit_bc261">Save</button>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        </div>
                    </form>
                </div>	
            </div>
            <br></br>
            <div class="row">
                <div class="table-wrapper-scroll-y my-custom-scrollbar">
                    <table class="table table-striped table-bordered table-hover">

                        <thead>
                            <tr class="bg-success">
                                <th scope="col">Nomor Kontainer</th>
                                <th scope="col">Ukuran</th>
                                <th scope="col">Tipe</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <?php
                                $n = 0;
                                if ($ptb_kontainer) {
                                    foreach ($ptb_kontainer as $data) {
                                        $n = $n + 1;
                                        ?>

                                        <td width="30%"><?php echo $data->NOMOR_KONTAINER; ?></td>
                                        <td width="30%"><?php echo $data->URAIAN_UKURAN_KONTAINER; ?></td>
                                        <td width="30%"><?php echo $data->URAIAN_TIPE_KONTAINER; ?></td>
                                        <td><button type="button" class="btn btn-dark" onclick="tampilkanEditKontainer(<?php echo $data->id_kontainer; ?>)"><i class="fa fa-info"></i></button>
                                            <a href="javascript:dialogHapus('<?php echo base_url('preview_BC261/delete_kontainer/' . $data->id_kontainer . ''); ?>')" class='btn red'><i class="fa fa-trash-o"></i></a></td>


                                        <?php
                                    }
                                }
                                ?>
                            </tr>	    

                        </tbody>
                    </table>



                </div>
            </div>	
        </div>

</div>

<!-- TUTUP CONTAINER -->
<script type="text/javascript">
  function tampilkanEditDokumen(id){
// alert(id);
$.ajax({
        url : "<?php echo base_url('Bc27_controller/editmodal/')?>" + id,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
 			$("#id_dokumen").empty();
			$("#id_dokumen").val(data.dokumenedit.ID);
            $("#jenis_dokumen").empty();
			$("#jenis_dokumen").val(data.dokumenedit.KODE_JENIS_DOKUMEN);
			$("#nomor_dokumen").empty();
			$("#nomor_dokumen").val(data.dokumenedit.NOMOR_DOKUMEN);
			$("#tgl_dokumen").empty();
			$("#tgl_dokumen").val(data.dokumenedit.TANGGAL_DOKUMEN);
			$("#modal").modal('show');
 
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
}

function tampilkanEditKemasan(id){
// alert(id);
$.ajax({
        url : "<?php echo base_url('Bc27_controller/editkemasan/')?>" + id,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
 			$("#id_kemasan").empty();
			$("#id_kemasan").val(data.kemasanedit.ID);
            $("#jenis_kemasan").empty();
			$("#jenis_kemasan").val(data.kemasanedit.KODE_JENIS_KEMASAN);
			$("#jumlah_kemasan").empty();
			$("#jumlah_kemasan").val(data.kemasanedit.JUMLAH_KEMASAN);
			$("#merk_kemasan").empty();
			$("#merk_kemasan").val(data.kemasanedit.MERK_KEMASAN);
			$("#modalkemasan").modal('show');
 
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
}
</script>