<!-- BEGIN PAGE TITLE-->
<!-- END PAGE TITLE-->
<!-- END PAGE HEADER-->
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js"></script>
<?php $list_item = $this->db->query("SELECT * FROM public.beone_item");?>

<div class="portlet box blue ">
      <div class="portlet-title">
          <div class="caption">
              <i class="fa fa-gift"></i> Bill Of Material</div>
          <div class="tools">
              <a href="" class="collapse"> </a>
              <a href="#portlet-config" data-toggle="modal" class="config"> </a>
              <a href="" class="reload"> </a>
              <a href="" class="remove"> </a>
          </div>
      </div>
      <div class="portlet-body form">




          <form role="form" method="post">
              <div class="form-body">

                <div class="row">
                  <div class="col-sm-4"><h3><b><?php echo $default['transfer_no'];?></b></h3></div>
                  <!--<div class="col-sm-8">
                    <div class="form-group">
                          <label>Tanggal</label>
                          <div class="input-group">
                            <span class="input-group-addon input-circle-left">
                                <i class="fa fa-calendar"></i>
                            </span>
                            <input class="form-control form-control-inline input-medium date-picker input-circle-right" size="16" type="text" name="tanggal" value="<?php echo date('m/d/Y');?>" readonly required/>
                            <span class="help-block"></span>
                          </div>
                    </div>
                  </div>-->
                  <div class="col-sm-8"><h3><?php echo $default['transfer_date'];?></h3></div>
                  </div>

                  <div class="row">
                    <!--<div class="col-sm-4">
                      <div class="form-group">
                            <label>Kode Biaya</label>
                            <div class="input-group">
                               <select id="select2-single-input-sm" class="form-control input-sm select2-multiple" name="kode_biaya" required>
                                 <option value=""></option>
                                 <option value=1>PRODUKSI WIP</option>
                                 <option value=2>PRODUKSI BARANG JADI</option>
                               </select>
                         </div>
                       </div>
                    </div>-->
                      <div class="col-sm-4"><h3><i><?php if($default['coa_kode_biaya'] == 1){echo "PRODUKSI WIP";}else{echo "PRODUKSI BARANG JADI";}?></i></h3></div>

                      <!--<div class="col-sm-8">
                        <div class="form-group">
                            <label>Keterangan</label>
                            <input type="text" class="form-control" placeholder="Keterangan" id="keterangan" name="keterangan" required>
                            <input type="hidden" class="form-control" name="transfer_no" value='<?php echo $nomor_bom;?>'>
                          </div>
                      </div>-->
                      <div class="col-sm-8"><h3><?php echo $default['keterangan'];?></h3></div>
                    </div>

                    <div class="row">
                      <div class="col-sm-4"><input type="hidden" class="form-control" id="cocokan_dari_item" name="cocokan_dari_item" value=0 required></div>
                      <div class="col-sm-4"><input type="hidden" class="form-control" id="cocokan_hasil_item" name="cocokan_hasil_item" value=0 required></div>
                      <div class="col-sm-4"></div>
                    </div>

                    <hr / style="border-color: #3598DC;">
                    <table  id="datatable" class="table striped hovered cell-hovered">
                        <h4><b>DARI ITEM</b></h4>
                        <a class="btn blue" data-toggle="modal" href="#responsive" id="tambahdata"><i class="fa fa-plus"></i> Tambah Data </a>
            						<thead>
            							<tr>
                            <td width="40%">Item</td>
            								<td width="20%">Qty</td>
            								<td width="20%">Gudang</td>
                            <td width="10%">Action</td>
                            <td width="5%"></td>
                            <td width="5%"></td>
            							</tr>

                          </thead>
                          <tbody id="container">

                            <?php
                            $ctr = 0;
                             foreach($default_detail as $row){
                             $ctr = $ctr + 1;
                             $nama_ctr = "x".$ctr;
                             $nama_item = "item_".$nama_ctr;
                             $nama_qty = "qty_".$nama_ctr;
                             $nama_gudang = "gudang_".$nama_ctr;
                             $nama_item_id = "item_id_".$nama_ctr;
                             $nama_gudang_id = "gudang_id_".$nama_ctr;
                             $rowss = "rows_".$nama_ctr;

                             $sql_cari_nama_item = $this->db->query("SELECT * FROM public.beone_item WHERE item_id = ".intval($row['item_id']));
                 					 	 $hasil_cari_nama_item = $sql_cari_nama_item->row_array();
                 					 	 $nitem = $hasil_cari_nama_item['nama'];

                             $sql_cari_nama_gudang = $this->db->query("SELECT * FROM public.beone_gudang WHERE gudang_id = ".intval($row['gudang_id']));
                 					 	 $hasil_cari_nama_gudang = $sql_cari_nama_gudang->row_array();
                 					 	 $ngudang = $hasil_cari_nama_gudang['nama'];

                           ?>

                           <tr class='records' id='<?php echo $nama_ctr;?>'>
              						 <td><input class="form-control" id='<?php echo $nama_item;?>' name='<?php echo $nama_item;?>' type="text" value='<?php echo $nitem;?>' readonly></td>
              						 <td><input class="form-control" id='<?php echo $nama_qty;?>' name='<?php echo $nama_qty;?>' type="text" value='<?php echo $row['qty'];?>' readonly></td>
                           <td><input class="form-control" id='<?php echo $nama_gudang;?>' name='<?php echo $nama_gudang;?>' type="text" value='<?php echo $ngudang;?>' readonly></td>
                           <td><button type="button" class="btn red" onclick="hapus('<?php echo $nama_ctr;?>')">X</button></td>
                           <td><input class="form-control" id='<?php echo $nama_item_id;?>' name='<?php echo $nama_item_id;?>' type="hidden" value='<?php echo $row['item_id'];?>' readonly></td>
                           <td><input class="form-control" id='<?php echo $nama_gudang_id;?>' name='<?php echo $nama_gudang_id;?>' type="hidden" value='<?php echo $row['gudang_id'];?>' readonly></td>
                           <td><input id='<?php echo $rowss;?>' name="rows[]" value='<?php echo $nama_ctr;?>' type="hidden"></td></td></tr>

                           <?php
                             }
                            ?>

             						  </tbody>


            				</table>


                    <hr / style="border-color: #3598DC;">
                    <table  id="datatable_hasil" class="table striped hovered cell-hovered">
                        <h4><b>MENJADI ITEM</b></h4>
                        <a class="btn blue" data-toggle="modal" href="#responsive_hasil" id="tambahdata_hasil"><i class="fa fa-plus"></i> Tambah Data </a>
            						<thead>
            							<tr>
                            <td width="20%">Item</td>
            								<td width="15%">Qty</td>
            								<td width="15%">Biaya</td>
                            <td width="10%">Gudang</td>
            								<td width="20%">% Hasil</td>
                            <td width="10%">Action</td>
                            <td width="5%"></td>
                            <td width="5%"></td>

            							</tr>
                         <tbody id="container_hasil">

                           <?php
                           $ctr = 0;
                            foreach($default_detail_hasil as $row){
                            $ctr = $ctr + 1;
                            $nama_ctr = "xh".$ctr;
                            $nama_item = "item_hasil_".$nama_ctr;
                            $nama_qty = "qty_hasil_".$nama_ctr;
                            $nama_biaya = "biaya_hasil_".$nama_ctr;
                            $nama_persen = "persen_hasil_".$nama_ctr;
                            $nama_gudang = "gudang_hasil_".$nama_ctr;
                            $nama_item_id = "item_hasil_id_".$nama_ctr;
                            $nama_gudang_id = "gudang_hasil_id_".$nama_ctr;
                            $rowss = "rows_hasil".$nama_ctr;

                            $sql_cari_nama_item = $this->db->query("SELECT * FROM public.beone_item WHERE item_id = ".intval($row['item_id']));
                            $hasil_cari_nama_item = $sql_cari_nama_item->row_array();
                            $nitem = $hasil_cari_nama_item['nama'];

                            $sql_cari_nama_gudang = $this->db->query("SELECT * FROM public.beone_gudang WHERE gudang_id = ".intval($row['gudang_id']));
                            $hasil_cari_nama_gudang = $sql_cari_nama_gudang->row_array();
                            $ngudang = $hasil_cari_nama_gudang['nama'];

                          ?>

                          <tr class='records' id='<?php echo $nama_ctr;?>'>
                          <td><input class="form-control" id='<?php echo $nama_item;?>' name='<?php echo $nama_item;?>' type="text" value='<?php echo $nitem;?>' readonly></td>
                          <td><input class="form-control" id='<?php echo $nama_qty;?>' name='<?php echo $nama_qty;?>' type="text" value='<?php echo $row['qty'];?>' readonly></td>
                          <td><input class="form-control" id='<?php echo $nama_biaya;?>' name='<?php echo $nama_biaya;?>' type="text" value='<?php echo $row['biaya'];?>' readonly></td>
                          <td><input class="form-control" id='<?php echo $nama_gudang;?>' name='<?php echo $nama_gudang;?>' type="text" value='<?php echo $ngudang;?>' readonly></td>
                          <td><input class="form-control" id='<?php echo $nama_persen;?>' name='<?php echo $nama_persen;?>' type="text" value='<?php echo $row['persen_produksi'];?>' readonly></td>
                          <td><button type="button" class="btn red" onclick="hapus_hasil('<?php echo $nama_ctr;?>')">X</button></td>
                          <td><input class="form-control" id='<?php echo $nama_item_id;?>' name='<?php echo $nama_item_id;?>' type="hidden" value='<?php echo $row['item_id'];?>' readonly></td>
                          <td><input class="form-control" id='<?php echo $nama_gudang_id;?>' name='<?php echo $nama_gudang_id;?>' type="hidden" value='<?php echo $row['gudang_id'];?>' readonly></td>
                          <td><input id='<?php echo $rowss;?>' name="rows_hasil[]" value='<?php echo $nama_ctr;?>' type="hidden"></td></td></tr>

                          <?php
                            }
                           ?>

                         </tbody>
            				</table>
              </div>
              <div class="form-actions">
                  <button type="submit" class="btn red" id="submit_transfer" name="submit_transfer">Submit</button>
                  <!--<a class="btn green" id="cekdata" onclick="validasi_produksi();"><i class="fa fa-plus"></i> Validasi </a>-->
              </div>
          </form>
  </div>
</div>

<?php
if ($default['coa_kode_biaya'] == 1){//produksi wip
  $list_item2 = $this->db->query("SELECT d.gudang_id, d.item_id, i.nama as nitem, SUM(qty_in) - SUM(qty_out) as jml_qty, d.nomor_transaksi
                                      FROM public.beone_gudang_detail d INNER JOIN public.beone_item i ON d.item_id = i.item_id
                                      WHERE d.flag = 1 AND d.gudang_id = 1 GROUP BY i.nama, d.item_id, d.gudang_id, d.nomor_transaksi");

  $gudang = 1;
}else{//produksi barang jadi
  $list_item2 = $this->db->query("SELECT d.gudang_id, d.item_id, i.nama as nitem, SUM(qty_in) - SUM(qty_out) as jml_qty, d.nomor_transaksi
                                      FROM public.beone_gudang_detail d INNER JOIN public.beone_item i ON d.item_id = i.item_id
                                      WHERE d.flag = 1 AND d.gudang_id = 3 GROUP BY i.nama, d.item_id, d.gudang_id, d.nomor_transaksi");

  $gudang = 3;
}

$gudang_asal = $this->db->query("SELECT * FROM public.beone_gudang WHERE gudang_id = ".intval($gudang));
$hasil_gudang_asal = $gudang_asal->row_array();
$gudang_id = $hasil_gudang_asal['gudang_id'];
$nama = $hasil_gudang_asal['nama'];
?>


<!--------------------------- MODAL ITEM ASAL--------------------------------------------->
<div id="responsive" class="modal fade" tabindex="-1" data-width="760">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">Item Asal</h4>
    </div>
    <div class="modal-body">
        <div class="row">
          <div class="col-sm-12">
              <!--<div name="txtItemAsal" id="txtItemAsal"><b>Item</b></div>-->
            <div class="form-group">
                <label>Item</label>
                <input type="hidden" class="form-control" name="nodoc" id="nodoc" required>
                <select id='item_modal' class='form-control input-sm select2-multiple' name='item_modal' onchange="copydoc();">
                  <option value=""></option>
                  <?php  foreach($list_item2->result_array() as $row){
                      if ($row['jml_qty'] <> 0){
                  ?>
                      <option value="<?php echo $row['item_id'];?>"><?php echo $row['nitem']." | Ready Stock = ".number_format($row['jml_qty'],2)." | Doc :".$row['nomor_transaksi'];?></option>
                  <?php
                      }
                    }
                  ?>
                </select>
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-sm-4">
            <div class="form-group">
                <label>Qty</label>
                <input type='text' class='form-control' placeholder="Qty" name='qty_modal' id='qty_modal'>
            </div>
          </div>

          <div class="col-sm-8">
            <div class="form-group">
            <label>Gudang</label>
            <select id='gudang_modal' class='form-control input-sm select2-multiple' name='gudang_modal'>
              <option value="<?php echo $gudang_id;?>"><?php echo $nama;?></option>
            </select>
            </div>
          </div>
        </div>

    </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-outline dark">Close</button>
            <button type="button" class="btn green" name="add_btn" id="add_btn">Insert</button>
        </div>
</div>
<!----------------------------------------------------------------------------->


<!--------------------------- MODAL ITEM HASIL --------------------------------------------->
<div id="responsive_hasil" class="modal fade" tabindex="-1" data-width="760">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">Item Hasil</h4>
    </div>
    <div class="modal-body">
        <div class="row">
          <div class="col-sm-4">
            <!--<div name="txtItemHasil" id="txtItemHasil"><b>Item Hasil</b></div>-->
            <div class="form-group">
              <label>Item</label>
              <select id='item_modal_hasil' class='form-control input-sm select2-multiple' name='item_modal_hasil'>
                <option value=""><?php //echo "- Pilih Item -";?></option>
                <?php
                if ($default['coa_kode_biaya'] == 1){//produksi wip
                    $list_item3 = $this->db->query("SELECT * FROM public.beone_item WHERE item_type_id = 2");
                }else{
                    $list_item3 = $this->db->query("SELECT * FROM public.beone_item WHERE item_type_id = 3");
                }

                foreach($list_item3->result_array() as $row){
                      echo '<option value='.$row['item_id'].'>'.$row['nama'].'</option>';
                }
                ?>
              </select>
            </div>
          </div>

          <div class="col-sm-4">
            <div class="form-group">
                <label>Qty</label>
                <input type='text' class='form-control' placeholder="Qty" name='qty_modal_hasil' id='qty_modal_hasil'>
            </div>
          </div>

          <div class="col-sm-4">
            <div class="form-group">
              <label>Gudang</label>
              <select id='gudang_modal_hasil' class='form-control input-sm select2-multiple' name='gudang_modal_hasil'>
                <option value="">- Pilih Gudang -</option>
                <?php 	foreach($list_gudang as $row){ ?>
                  <option value="<?php echo $row['gudang_id'];?>"><?php echo $row['nama'];?></option>
                <?php } ?>
              </select>
            </div>
          </div>
          </div>


          <div class ="row">
            <div class="col-sm-6">
                <div class="form-group">
                <label>Biaya</label>
                <input type='text' class='form-control' placeholder="Biaya" name='biaya_modal_hasil' id='biaya_modal_hasil' required>
                </div>
            </div>

            <div class="col-sm-6">
              <div class="form-group">
                <label>% Prosentase</label>
              <input type='text' class='form-control' placeholder="% Hasil" name='persen_modal_hasil' id='persen_modal_hasil'>
              </div>
            </div>
          </div>

    </div>
    <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn btn-outline dark">Close</button>
        <button type="button" class="btn green" name="add_btn_hasil" id="add_btn_hasil">Insert</button>
    </div>
</div>
<!----------------------------------------------------------------------------->

<!-----------------------------ITEM ASAL----------------------------------------->
    <script>
    /*var yy = document.getElementById("submit_transfer");
    yy.style.display = 'none';*/

    $(document).ready(function() {
        		var count = 0;

        		$("#add_btn").click(function(){
    					count += 1;

              var item = document.getElementById('item_modal');
              var qty = document.getElementById('qty_modal');
              var nodoc = document.getElementById('nodoc');
              var gudang = document.getElementById('gudang_modal');
              var namaItem = $('#item_modal option:selected').text();
              var namaGudang = $('#gudang_modal option:selected').text();

              if (item.value == ""){
                alert("Silahkan isi item..!!!");
                item.focus();
                return false;
              }else if(qty.value == ""){
                alert("Silahkan isi qty..!!!");
                qty.focus();
                return false;
              }else if(gudang.value == ""){
                alert("Silahkan isi gudang..!!!");
                gudang.focus();
                return false;
              }else{

        		   		$('#container').append(
        							 '<tr class="records" id="'+count+'">'

        						 + '<td><input class="form-control" id="item_' + count + '" name="item_'+count+'" type="text" value="'+namaItem+'" readonly></td>'
        						 + '<td><input class="form-control" id="qty_' + count + '" name="qty_'+count+'" type="text" value="'+qty.value+'" readonly></td>'
                     + '<td><input class="form-control" id="gudang_' + count + '" name="gudang_'+count+'" type="text" value="'+namaGudang+'" readonly></td>'
                     + '<td><button type="button" class="btn red" onclick="hapus('+count+')">X</button></td>'
                     + '<td><input class="form-control" id="item_id_' + count + '" name="item_id_'+count+'" type="hidden" value="'+item.value+'" readonly></td>'
                     + '<td><input class="form-control" id="nodoc_' + count + '" name="nodoc_'+count+'" type="hidden" value="'+nodoc.value+'" readonly></td>'
                     + '<td><input class="form-control" id="gudang_id_' + count + '" name="gudang_id_'+count+'" type="hidden" value="'+gudang.value+'" readonly></td>'
                     + '<td><input id="rows_' + count + '" name="rows[]" value="'+ count +'" type="hidden"></td></tr>'
        					);
                  autoCocokan_dari_item(qty.value);
                  eraseText();
                  $('#responsive').modal('hide');
            }
    				});


            var count_hasil = 0;

        		$("#add_btn_hasil").click(function(){
    					count_hasil += 1;

              var item_hasil = document.getElementById('item_modal_hasil');
              var qty_hasil = document.getElementById('qty_modal_hasil');
              var gudang_hasil = document.getElementById('gudang_modal_hasil');
              var biaya_hasil = document.getElementById('biaya_modal_hasil');
              var persen_hasil = document.getElementById('persen_modal_hasil');
              var namaItem_hasil = $('#item_modal_hasil option:selected').text();
              var namaGudang_hasil = $('#gudang_modal_hasil option:selected').text();

              if (item_hasil.value == ""){
                alert("Silahkan isi item..!!!");
                item_hasil.focus();
                return false;
              }else if(qty_hasil.value == ""){
                alert("Silahkan isi qty..!!!");
                qty_hasil.focus();
                return false;
              }else if(gudang_hasil.value == ""){
                alert("Silahkan isi gudang..!!!");
                gudang_hasil.focus();
                return false;
              }else if(biaya_hasil.value == ""){
                alert("Silahkan isi biaya..!!!");
                biaya_hasil.focus();
                return false;
              }else if(persen_hasil.value == ""){
                alert("Silahkan isi % hasil..!!!");
                persen_hasil.focus();
                return false;
              }else{

        		   		$('#container_hasil').append(
        							 '<tr class="records" id="'+count_hasil+'">'

        						 + '<td><input class="form-control" id="item_hasil_' + count_hasil + '" name="item_hasil_'+count_hasil+'" type="text" value="'+namaItem_hasil+'" readonly></td>'
        						 + '<td><input class="form-control" id="qty_hasil_' + count_hasil + '" name="qty_hasil_'+count_hasil+'" type="text" value="'+qty_hasil.value+'" readonly></td>'
                     + '<td><input class="form-control" id="biaya_hasil_' + count_hasil + '" name="biaya_hasil_'+count_hasil+'" type="text" value="'+biaya_hasil.value+'" readonly></td>'
                     + '<td><input class="form-control" id="gudang_hasil_' + count_hasil + '" name="gudang_hasil_'+count_hasil+'" type="text" value="'+namaGudang_hasil+'" readonly></td>'
                     + '<td><input class="form-control" id="persen_hasil_' + count_hasil + '" name="persen_hasil_'+count_hasil+'" type="text" value="'+persen_hasil.value+'" readonly></td>'
                     + '<td><button type="button" class="btn red" onclick="hapus_hasil('+count_hasil+')">X</button></td>'
                     + '<td><input class="form-control" id="item_hasil_id_' + count_hasil + '" name="item_hasil_id_'+count_hasil+'" type="hidden" value="'+item_hasil.value+'" readonly></td>'
                     + '<td><input class="form-control" id="gudang_hasil_id_' + count_hasil + '" name="gudang_hasil_id_'+count_hasil+'" type="hidden" value="'+gudang_hasil.value+'" readonly></td>'
                     + '<td><input id="rows_hasil_' + count_hasil + '" name="rows_hasil[]" value="'+ count_hasil +'" type="hidden"></td></tr>'
        					);

                  autoCocokan_hasil_item(qty_hasil.value);
                  eraseText_hasil();
                  $('#responsive_hasil').modal('hide');
            }
    				});


    		});

        function eraseText() {
         document.getElementById("item_modal").value = "";
         document.getElementById("qty_modal").value = "";
         document.getElementById("gudang_modal").value = "";
        }


        function hapus(rowid)
        {
            autoCocokan_dari_item_min("qty_"+rowid);

            var row = document.getElementById(rowid);
            row.parentNode.removeChild(row);
        }

        function eraseText_hasil() {
         document.getElementById("item_modal_hasil").value = "";
         document.getElementById("qty_modal_hasil").value = "";
         document.getElementById("gudang_modal_hasil").value = "";
         document.getElementById("biaya_modal_hasil").value = "";
         document.getElementById("persen_modal_hasil").value = "";
        }


        function hapus_hasil(rowid)
        {
            autoCocokan_hasil_item_min("qty_hasil_"+rowid);

            var row = document.getElementById(rowid);
            row.parentNode.removeChild(row);

            var totalQty = document.getElementById('cocokan_hasil_item').value;
            if (totalQty == 0){
              var yy = document.getElementById("submit_transfer");
              yy.style.display = 'none';

              var zz = document.getElementById("cekdata");
              zz.style.display = 'block';
            }
        }

        function autoCocokan_dari_item(xqty){
          var totalQty = document.getElementById('cocokan_dari_item').value;
          var qty = xqty;

          var tb_ex = totalQty.split('.').join('');
          var b_ex = qty.split('.').join('');

          var tb = tb_ex.split(',').join('.');
          var b = b_ex.split(',').join('.');

          document.getElementById('cocokan_dari_item').value = (tb*1) + (b*1);
        }


        function autoCocokan_dari_item_min(xqty){
          var totalQty = document.getElementById('cocokan_dari_item').value;
          var qty = document.getElementById(xqty).value;

          var tb_ex = totalQty.split('.').join('');
          var b_ex = qty.split('.').join('');

          var tb = tb_ex.split(',').join('.');
          var b = b_ex.split(',').join('.');

          document.getElementById('cocokan_dari_item').value = (tb*1) - (b*1);
        }


        function autoCocokan_hasil_item(xqty){
          var totalQty = document.getElementById('cocokan_hasil_item').value;
          var qty = xqty;

          var tb_ex = totalQty.split('.').join('');
          var b_ex = qty.split('.').join('');

          var tb = tb_ex.split(',').join('.');
          var b = b_ex.split(',').join('.');

          document.getElementById('cocokan_hasil_item').value = (tb*1) + (b*1);
        }

        function autoCocokan_hasil_item_min(xqty){
          var totalQty = document.getElementById('cocokan_hasil_item').value;
          var qty = document.getElementById(xqty).value;

          var tb_ex = totalQty.split('.').join('');
          var b_ex = qty.split('.').join('');

          var tb = tb_ex.split(',').join('.');
          var b = b_ex.split(',').join('.');

          document.getElementById('cocokan_hasil_item').value = (tb*1) - (b*1);
        }


    </script>
    <!--------------------------------- END ITEM ASAL --------------------------------------->

    <script type="text/javascript">

    function validasi_produksi(){
      var aa = document.getElementById('cocokan_dari_item').value;
      var bb = document.getElementById('cocokan_hasil_item').value;

      if (aa == 0 || bb == 0){
        alert("Isian data belum lengkap...!!!");
      }else{
        var yy = document.getElementById("submit_transfer");
        yy.style.display = 'block';

        var zz = document.getElementById("cekdata");
        zz.style.display = 'none';
      }
    }

    var qty_modal = document.getElementById('qty_modal');
      qty_modal.addEventListener('keyup', function(e){
      qty_modal.value = formatRupiah(this.value, 'Rp. ');
    });

    var qty_modal_hasil = document.getElementById('qty_modal_hasil');
      qty_modal_hasil.addEventListener('keyup', function(e){
      qty_modal_hasil.value = formatRupiah(this.value, 'Rp. ');
    });

    var biaya_modal_hasil = document.getElementById('biaya_modal_hasil');
      biaya_modal_hasil.addEventListener('keyup', function(e){
      biaya_modal_hasil.value = formatRupiah(this.value, 'Rp. ');
    });

    var persen_modal_hasil = document.getElementById('persen_modal_hasil');
      persen_modal_hasil.addEventListener('keyup', function(e){
        if (persen_modal_hasil.value >= 101){
          alert("Tidak bisa lebih dari 100%...!");
          persen_modal_hasil.value = 0;
        }
      persen_modal_hasil.value = formatRupiah(this.value, 'Rp. ');

    });

    /* Fungsi formatRupiah */
    function formatRupiah(angka, prefix){
      var number_string = angka.replace(/[^,\d]/g, '').toString(),
      split   		= number_string.split(','),
      sisa     		= split[0].length % 3,
      rupiah     		= split[0].substr(0, sisa),
      ribuan     		= split[0].substr(sisa).match(/\d{3}/gi);

      // tambahkan titik jika yang di input sudah menjadi angka ribuan
      if(ribuan){
        separator = sisa ? '.' : '';
        rupiah += separator + ribuan.join('.');
      }

      rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
      return prefix == undefined ? rupiah : (rupiah ? rupiah : '');
    }
    </script>

    <script>
				function tampilItemAsal(str)
				{
				if (str=="")
				  {
				  document.getElementById("txtItemAsal").innerHTML="";
				  return;
				  }
				if (window.XMLHttpRequest)
				  {// code for IE7+, Firefox, Chrome, Opera, Safari
				  xmlhttp=new XMLHttpRequest();
				  }
				else
				  {// code for IE6, IE5
				  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
				  }
				xmlhttp.onreadystatechange=function()
				  {
				  if (xmlhttp.readyState==4 && xmlhttp.status==200)
					{
					document.getElementById("txtItemAsal").innerHTML=xmlhttp.responseText;
					}
				  }
				xmlhttp.open("GET","Transfer_controller/get_item_asal?q="+str,true);
				xmlhttp.send();
				}
    </script>

    <script>
        function tampilItemHasil(str)
				{
				if (str=="")
				  {
				  document.getElementById("txtItemHasil").innerHTML="";
				  return;
				  }
				if (window.XMLHttpRequest)
				  {// code for IE7+, Firefox, Chrome, Opera, Safari
				  xmlhttp=new XMLHttpRequest();
				  }
				else
				  {// code for IE6, IE5
				  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
				  }
				xmlhttp.onreadystatechange=function()
				  {
				  if (xmlhttp.readyState==4 && xmlhttp.status==200)
					{
					document.getElementById("txtItemHasil").innerHTML=xmlhttp.responseText;
					}
				  }
				xmlhttp.open("GET","Transfer_controller/get_item_hasil?z="+str,true);
				xmlhttp.send();
				}
		</script>

    <script>
    function copydoc(){
      var namaItem = $('#item_modal option:selected').text();
      var nodoc = document.getElementById('nodoc').value = namaItem;
    }
    </script>
