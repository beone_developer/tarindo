<div class="container">
    <div class="row">
        <p class="text-center" style="font-size: 17px"><b>PEMBERITAHUAN PEMASUKAN KEMBALI BARANG YANG DIKELUARKAN DARI <br> TEMPAT PENIMBUNAN BERIKAT DENGAN JAMINAN</b></p>
    </div>

    <form action="preview_BC262/edit" method = "POST">
        <div class="form-group row">
            <label for="status" class="col-sm-2 col-form-label">Status</label>
            <div class="col-sm-2">
                <input type="hidden" class="form-control" name="id" name ="id" Placeholder="id" value = "<?php
                if ($ptb_edit) {
                    foreach ($ptb_edit as $data) {
                        echo $data->id;
                    }
                }
                ?>" >
                <input style="border: none;" type="text" readonly class="form-control" name="status" value = "<?php
                $n = 0;
                if ($ptb_edit) {
                    foreach ($ptb_edit as $data) {
                        $n = $n + 1;
                        echo $data->URAIAN_STATUS;
                    }
                }
                ?>">
            </div>
        </div>

        <div class="form-group row">
            <label for="status_perbaikan" class="col-sm-2 col-form-label">Status Perbaikan</label>
            <div class="col-sm-2">
                <input style="border: none;" type="text" readonly class="form-control" name="status_perbaikan" value="-">
            </div>
        </div>
        <div class="form-group row">
            <div class="col-sm-2">
                <a href="#modal" class="btn btn-primary btn-sm" role="button" data-target="#modal" data-toggle="modal">DAFTAR RESPON</a>
            </div>
        </div>

        <!-- FORM NOMOR PENGAJUAN -->
        <div style="margin-top: 30px;">
            <div class="form-group row">
                <label for="nomor_pengajuan" class="col-sm-2 col-form-label">Nomor Pengajuan</label>
                <div class="col-sm-4">
                    <input type="text" readonly class="form-control" name="nomor_pengajuan" value = "<?php
                    $n = 0;
                    if ($ptb_edit) {
                        foreach ($ptb_edit as $data) {
                            $n = $n + 1;
                            echo $data->nomor_aju;
                        }
                    }
                    ?>">
                </div>
            </div>

            <div class="form-group row">
                <label for="nomor_pendaftaran" class="col-sm-2 col-form-label">Nomor Pendaftaran</label>
                <div class="col-sm-4">
                    <input type="text" class="form-control" name="nomor_pendaftaran" value="<?php
                    $n = 0;
                    if ($ptb_edit) {
                        foreach ($ptb_edit as $data) {
                            $n = $n + 1;
                            echo $data->nomor_daftar;
                        }
                    }
                    ?>">
                </div>
            </div>

            <div class="form-group row">
                <label for="tanggal_pendaftaran" class="col-sm-2 col-form-label">Tanggal Pendaftaran</label>
                <div class="col-sm-4">
                    <input type="text" class="form-control" name="tanggal_pendaftaran" value="<?php
                    $n = 0;
                    if ($ptb_edit) {
                        foreach ($ptb_edit as $data) {
                            $n = $n + 1;
                            echo $data->tanggal_daftar;
                        }
                    }
                    ?>" placeholder="DDMMYY">
                </div>
            </div>		
        </div>
        <!-- TUTUP FORM NOMOR PENGAJUAN -->

        <!-- FORM KPPBC BONGKAR -->
        <div style="margin-top: 30px;">


            <div class="form-group row">
                <label for="kppbc_pengawas" class="col-sm-2 col-form-label">Kantor Pabean</label>
                <div class="col-sm-4">
                    <input type="text" class="form-control" name="kppbc_pengawas" value="<?php
                    $n = 0;
                    if ($ptb_edit) {
                        foreach ($ptb_edit as $data) {
                            $n = $n + 1;
                            echo $data->kode_kantor_pabean . " - " . $data->kantor_pabean;
                        }
                    }
                    ?>">
                </div>
                <label for="tujuan" class="col-sm-2 col-form-label">Kode Gudang PLB </label>
                <div class="col-sm-2">
                    <input type="text" class="form-control" name="kode_gudang_plb" value ="<?php
                    $n = 0;
                    if ($ptb_edit) {
                        foreach ($ptb_edit as $data) {
                            $n = $n + 1;
                            echo $data->KODE_TPS;
                        }
                    }
                    ?>" >
                </div>
            </div>

            <div class="form-group row">
                <label for="tujuan" class="col-sm-2 col-form-label">Tujuan Pemasukan </label>
                <div class="col-sm-4">
                    <select name="tujuan_pemasukan" class="form-control">
                        <option selected>Choose...</option>
                        <option>...</option>
                    </select>
                </div>
            </div>		
        </div>
        <!-- TUTUP FORM KPPBC BONGKAR -->


        <div class="row">
            <div class="col-sm-12">

                <!-- FORM LEFT SIDE -->
                <ol>
                    <div class="col-sm-6">
                        <p><b>PENGUSAHA TPB</b></p>
                        <!-- FORM PENGUSAHA -->
                        <div>
                            <div class="form-group row">
                                <li>
                                    <label for="nama" class="col-sm-3 col-form-label">NPWP</label>
                                    <div class="col-sm-4">
                                        <select name="kode_id_pengusaha" class="form-control">
                                            <option selected>NPWP</option>
                                            <option>...</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control" name="id_pengusaha" value = "<?php
                                        if ($ptb_edit) {
                                            foreach ($ptb_edit as $data) {
                                                echo $data->id_pengusaha;
                                            }
                                        }
                                        ?>">
                                    </div>
                                </li>
                            </div>	
                            <div class="form-group row">
                                <li>
                                    <label for="nama" class="col-sm-3 col-form-label">Nama</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" name="nama_pengusaha" value = "<?php
                                        $n = 0;
                                        if ($ptb_edit) {
                                            foreach ($ptb_edit as $data) {
                                                $n = $n + 1;
                                                echo $data->NAMA_PENGUSAHA;
                                            }
                                        }
                                        ?>">
                                    </div>
                                </li>
                            </div>

                            <div class="form-group row">
                                <label for="alamat" class="col-sm-3 col-form-label">Alamat</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="alamat_pengusaha" value = "<?php
                                    $n = 0;
                                    if ($ptb_edit) {
                                        foreach ($ptb_edit as $data) {
                                            $n = $n + 1;
                                            echo $data->ALAMAT_PENGUSAHA;
                                        }
                                    }
                                    ?>">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="Negara" class="col-sm-3 col-form-label">IJIN PTB</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" name="ptb_pengusaha" value = "<?php
                                    $n = 0;
                                    if ($ptb_edit) {
                                        foreach ($ptb_edit as $data) {
                                            $n = $n + 1;
                                            echo $data->NOMOR_IJIN_TPB;
                                        }
                                    }
                                    ?>">
                                </div>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" name="tgl_ijinptb_pengusaha" value = "<?php
                                    $n = 0;
                                    if ($ptb_edit) {
                                        foreach ($ptb_edit as $data) {
                                            $n = $n + 1;
                                            echo $data->TANGGAL_IJIN_TPB;
                                        }
                                    }
                                    ?>">
                                </div>
                            </div>
                            <div class="form-group row">
                                <li>
                                    <label for="nama" class="col-sm-3 col-form-label">API</label>
                                    <div class="col-sm-4">
                                        <select name="jenis_api_pengusaha" class="form-control">
                                            <option selected>API</option>
                                            <option>...</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control" name="nomor_api_pengusaha" value = "<?php
                                        if ($ptb_edit) {
                                            foreach ($ptb_edit as $data) {
                                                echo $data->api_pengusaha;
                                            }
                                        }
                                        ?>">
                                    </div>
                                </li>
                            </div>

                        </div>
                        <!-- TUTUP FORM PEMASOK -->		

                        <!-- FORM Penerima barang -->
                        <div>
                            <p style="margin-left: -38px"><b>PENGIRIM BARANG</b></p>

                            <div class="form-group row">
                                <li>
                                    <label for="nama" class="col-sm-3 col-form-label">NPWP</label>
                                    <div class="col-sm-4">
                                        <select name="jenis_npwp_pengirim" class="form-control">
                                            <option selected>NPWP</option>
                                            <option>...</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control" name="nomor_npwp_pengirim" value = "<?php
                                        if ($ptb_edit) {
                                            foreach ($ptb_edit as $data) {
                                                echo $data->ID_PENGIRIM;
                                            }
                                        }
                                        ?>">
                                    </div>
                                </li>
                            </div>	

                            <div class="form-group row">
                                <li>
                                    <label for="alamat" class="col-sm-3 col-form-label">Nama</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" name="nama_pengirim" value ="<?php
                                        if ($ptb_edit) {
                                            foreach ($ptb_edit as $data) {
                                                echo $data->NAMA_PENGIRIM;
                                            }
                                        }
                                        ?>" >
                                    </div>
                                </li>

                            </div>
                            <div class="form-group row">
                                <li>
                                    <label for="alamat" class="col-sm-3 col-form-label">Alamat</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" name="alamat_pengirim" value ="<?php
                                        if ($ptb_edit) {
                                            foreach ($ptb_edit as $data) {
                                                echo $data->ALAMAT_PENGIRIM;
                                            }
                                        }
                                        ?>" >
                                    </div>
                                </li>

                            </div>
                        </div>
                        <!-- TUTUP FORM IMPORTIR -->

                        <!-- FORM PEMILIK -->
                        <div>
                            <li>
                                <a href="#modal4" class="btn btn-primary btn-sm" role="button" data-target="#modal4" data-toggle="modal">DOKUMEN</a>
                            </li>
                            <div class="form-group row">
                                <li>
                                    <label for="nama" class="col-sm-3 col-form-label">Packing List</label>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control" name="nomor_dokumen" value ="<?php
                                        if ($ptb_packing) {
                                            foreach ($ptb_packing as $data) {
                                                echo $data->nomor_dokumen;
                                            }
                                        }
                                        ?>" >
                                    </div>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control" name="tanggal_dokumen" value ="<?php
                                        if ($ptb_packing) {
                                            foreach ($ptb_packing as $data) {
                                                echo $data->tanggal_dokumen;
                                            }
                                        }
                                        ?>" >
                                    </div>
                                </li>
                            </div>	

                            <div class="form-group row">
                                <li>
                                    <label for="nama" class="col-sm-3 col-form-label">Surat Keputusan</label>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control" name="sk_1" value ="" >
                                    </div>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control" name="sk_2" value ="" >
                                    </div>
                                </li>
                            </div>	
                            <div class="form-group row">
                                <li>
                                    <label for="nama" class="col-sm-3 col-form-label">Dok. BC 2.6.1</label>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control" name="261_1" value ="" >
                                    </div>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control" name="261_2" value ="" >
                                    </div>
                                </li>
                            </div>	

                            <div class="form-group row">
                                <li>
                                    <div class="col-sm-10">
                                        <table class="table table-sm">
                                            <thead>
                                                <tr class="bg-success">
                                                    <th scope="col">Jenis Dokumen</th>
                                                    <th scope="col">Nomor Dokumen</th>
                                                    <th scope="col">Tanggal</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                                <?php
                                                $n = 0;
                                                if ($ptb_other) {
                                                    foreach ($ptb_other as $data) {
                                                        $n = $n + 1;
                                                        ?>
                                                    <td width="30%"><?php echo $data->uraian_dokumen; ?></td>
                                                    <td width="30%"><?php echo $data->nomor_dokumen; ?></td>
                                                    <td width="30%"><?php echo $data->tanggal_dokumen; ?></td></tr>
                                                    <?php
                                                }
                                            }
                                            ?>						    

                                            </tbody>
                                        </table>
                                    </div>
                                </li>
                            </div>
                        </div>


                        <!-- FORM HARGA -->
                        <div>
                            <p style="margin-left: -38px"><b>HARGA</b></p>


                            <div class="form-group row">
                                <li>
                                    <label for="alamat" class="col-sm-3 col-form-label">Valuta</label>
                                    <div class="col-sm-3">
                                        <input type="text" class="form-control" name="kode_valuta" value="<?php
                                        if ($ptb_edit) {
                                            foreach ($ptb_edit as $data) {
                                                echo $data->KODE_VALUTA;
                                            }
                                        }
                                        ?>" >
                                    </div>
                                </li>
                            </div>

                            <div class="form-group row">
                                <li>
                                    <div class="col-sm-3">
                                        <button type="submit" class="btn blue" name="submit_user">NDPBM</button>
                                    </div>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control" name="ndpbm" value ="<?php
                                        if ($ptb_edit) {
                                            foreach ($ptb_edit as $data) {
                                                echo $data->NDPBM;
                                            }
                                        }
                                        ?>" >
                                    </div>
                                </li>
                                <!--									<div class="col-sm-4">
                                                                                                                <input type="text" class="form-control" name="alamat" >
                                                                                                        </div>-->
                            </div>

                            <div class="form-group row">
                                <li>
                                    <label for="Negara" class="col-sm-3 col-form-label">Nilai CIF</label>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control" name="cif" value="<?php
                                        if ($ptb_edit) {
                                            foreach ($ptb_edit as $data) {
                                                echo $data->CIF;
                                            }
                                        }
                                        ?>">
                                    </div>
                                </li>
                            </div>

                            <div class="form-group row">

                                <label for="Negara" class="col-sm-3 col-form-label">Nilai CIF (Rp)</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" name="cif_rp" value="<?php
                                    if ($ptb_edit) {
                                        foreach ($ptb_edit as $data) {
                                            echo $data->CIF_RUPIAH;
                                        }
                                    }
                                    ?>">
                                </div>

                            </div>


                        </div>
                        <!-- TUTUP FORM HARGA -->	
                    </div>
                    <!-- TUTUP FORM LEFT SIDE -->


                    <!-- FORM RIGHT SIDE -->
                    <div class="col-sm-6" style="padding-right: 50px;">
                        <p><b>PENGANGKUTAN</b></p>
                        <div class="form-group row">
                            <li>
                                <label for="nama" class="col-sm-4 col-form-label">Jenis Sarana Pengangkut</label>
                                <div class="col-sm-6">
                                    <select name="sarana_angkut" class="form-control">
                                        <option selected></option>
                                        <option>...</option>
                                    </select>
                                </div>
                            </li>
                        </div>


                        <div class="form-group row">
                            <li>
                                <a href="#modal2" class="btn btn-primary btn-sm" role="button" data-target="#modal2" data-toggle="modal">KONTAINER</a>
                            </li>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-10">
                                <table class="table table-sm">
                                    <thead>
                                        <tr class="bg-success">
                                            <th scope="col">No Cont</th>
                                            <th scope="col">Ukuran</th>
                                            <th scope="col">Tipe</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $n = 0;
                                        if ($ptb_kontainer) {
                                            foreach ($ptb_kontainer as $data) {
                                                $n = $n + 1;
                                                ?>
                                            <td width="30%"><?php echo $data->NOMOR_KONTAINER; ?></td>
                                            <td width="30%"><?php echo $data->URAIAN_UKURAN_KONTAINER; ?></td>
                                            <td width="30%"><?php echo $data->URAIAN_TIPE_KONTAINER; ?></td></tr>
                                            <?php
                                        }
                                    }
                                    ?>


                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="form-group row">
                            <li>
                                <a href="#modal3" class="btn btn-primary btn-sm" role="button" data-target="#modal3" data-toggle="modal">KEMASAN</a>
                            </li>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-10">
                                <table class="table table-sm">
                                    <thead>
                                        <tr class="bg-success">
                                            <th scope="col">Jumlah</th>
                                            <th scope="col">Kode Jenis</th>
                                            <th scope="col">Jenis</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $n = 0;
                                        if ($ptb_kemasan) {
                                            foreach ($ptb_kemasan as $data) {
                                                $n = $n + 1;
                                                ?>
                                            <td width="30%"><?php echo $data->JUMLAH_KEMASAN; ?></td>
                                            <td width="30%"><?php echo $data->KODE_JENIS_KEMASAN; ?></td>
                                            <td width="30%"><?php echo $data->URAIAN_KEMASAN; ?></td></tr>
                                            <?php
                                        }
                                    }
                                    ?>


                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- FORM BARANG -->
                        <div>
                            <a href="./preview_BC262/form_detail_barang?id=<?php
                            if ($ptb_edit) {
                                foreach ($ptb_edit as $data) {
                                    echo $data->id;
                                }
                            }
                            ?>"><p style="margin-left: -38px"><b>BARANG</b></p></a>
                            <div class="form-group row">
                                <li>
                                    <label class="col-sm-3 col-form-label">Bruto (Kg)</label>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control" name="bruto" value ="<?php
                                        if ($ptb_edit) {
                                            foreach ($ptb_edit as $data) {
                                                echo $data->bruto;
                                            }
                                        }
                                        ?>" >
                                    </div>
                                </li>
                            </div>
                            <div class="form-group row">
                                <li>
                                    <label class="col-sm-3 col-form-label">Netto (Kg)</label>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control" name="netto" value ="<?php
                                        if ($ptb_edit) {
                                            foreach ($ptb_edit as $data) {
                                                echo $data->NETTO;
                                            }
                                        }
                                        ?>" >
                                    </div>

                                </li>

                            </div>	
                            <div class="form-group row">

                                <label class="col-sm-3 col-form-label">Jumlah Barang</label>
                                <div class="col-sm-2">
                                    <input type="text" class="form-control" name="jml_barang" value ="<?php
                                    if ($ptb_edit) {
                                        foreach ($ptb_edit as $data) {
                                            echo $data->JUMLAH_BARANG;
                                        }
                                    }
                                    ?>" >
                                </div>



                            </div>	
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-10">
                                <table class="table table-sm">
                                    <thead>
                                        <tr class="bg-success">
                                            <th scope="col">Jenis Pungutan</th>
                                            <th scope="col">Ditangguhkan (Rp)</th>

                                        </tr>
                                    </thead>
                                    <tbody>



                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- FORM PUNGUTAN-->
                        <!--							<div class="form-group row">
                                                                                <li>
                                                                                        <label for="tujuan" class="col-sm-10 col-form-label">Pngutan</label>
                                                                                </li>
                                                                        </div>-->







                </ol>
                <div class="form-group row">
                    <li>
                        <a href="#modal5" class="btn btn-primary btn-sm" role="button" data-target="#modal5" data-toggle="modal">JAMINAN</a>
                    </li>
                </div>
                <table class="table table-striped table-bordered table-hover">

                    <thead>
                        <tr class="bg-success">
                            <th scope="col">Jenis</th>
                            <th scope="col">Nomor</th>
                            <th scope="col">Tanggal</th>
                            <th scope="col">Nilai</th>
                            <th scope="col">Jatuh Tempo</th>
                            <th scope="col">Penjamin</th>
                            <th scope="col">Nomor BPJ</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <?php
                            $n = 0;
                            if ($ptb_jaminan) {
                                foreach ($ptb_jaminan as $data) {
                                    $n = $n + 1;
                                    ?>
                                <tr>
                                    <td width="30%"><?php echo $data->URAIAN_JENIS_JAMINAN; ?></td>
                                    <td width="30%"><?php echo $data->NOMOR_JAMINAN; ?></td>
                                    <td width="30%"><?php echo $data->TANGGAL_JAMINAN; ?></td>
                            <td width="30%"><?php echo $data->NILAI_JAMINAN; ?></td>
                            <td width="30%"><?php echo $data->TANGGAL_JATUH_TEMPO; ?></td>
                            <td width="30%"><?php echo $data->PENJAMIN; ?></td>
                            <td width="30%"><?php echo $data->NOMOR_BPJ; ?></td></tr>
                            <?php
                        }
                    }
                    ?>						    
                    </tr>
                    </tbody>
                </table>
                <p>Dengan saya menyatakan bertanggung jawab atas kebenaran
                    <br></br>hal hal yang diberitahukan dalam pemberitahuan pabean ini. </p>
                <div class="form-group row">
                    <!--<li>-->
                    <!--<label for="Negara" class="col-sm-3 col-form-label">Pelabuhan Bongkar</label>-->
                    <div class="col-sm-2">
                        <input type="text" class="form-control" name="kota_ttd" Placeholder="kota" value = "<?php
                        if ($ptb_edit) {
                            foreach ($ptb_edit as $data) {
                                echo $data->KOTA_TTD;
                            }
                        }
                        ?>">
                    </div>
                    <div class="col-sm-2">
                        <input type="text" class="form-control" name="tgl_ttd" Placeholder="Tanggal" value = "<?php
                        if ($ptb_edit) {
                            foreach ($ptb_edit as $data) {
                                echo $data->TANGGAL_TTD;
                            }
                        }
                        ?>">
                    </div>
                    <!--</li>-->
                </div>
                <div class="form-group row">
                    <!--<li>-->
                    <label for="Negara" class="col-sm-2 col-form-label">Pemberitahu</label>
                    <div class="col-sm-2">
                        <input type="text" class="form-control" name="pemberitahu" Placeholder="nama" value = "<?php
                        if ($ptb_edit) {
                            foreach ($ptb_edit as $data) {
                                echo $data->NAMA_TTD;
                            }
                        }
                        ?>">
                    </div>
                    <!--</li>-->
                </div>
                <div class="form-group row">
                    <!--<li>-->
                    <label for="Negara" class="col-sm-2 col-form-label">Jabatan</label>
                    <div class="col-sm-2">
                        <input type="text" class="form-control" name="jabatan" Placeholder="jabatan" value = "<?php
                        if ($ptb_edit) {
                            foreach ($ptb_edit as $data) {
                                echo $data->JABATAN_TTD;
                            }
                        }
                        ?>" >
                    </div>
                    <!--</li>-->
                </div>
            </div>

            <div class="form-actions">
                <a href='<?php echo base_url('BC_262'); ?>' class='btn default'> Cancel</a>
                <button type="submit" class="btn blue" name="edit_bc262">Simpan Perubahan</button>
            </div>
        </div>		
        <!-- TUTUP FORM RIGHT SIDE -->

</div>
</div>
</form>

<div id="modal" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog">
    <div class="modal-content">
        <div class="modal-header">
            <a href="" class="btn btn-primary btn-sm" role="button" aria-pressed="true">Respon</a>
            <!--<a href="" class="btn btn-primary btn-sm" role="button" aria-pressed="true">Status</a>-->
        </div>
        <div class="modal-body">

            <div class="row">
                <div class="table-wrapper-scroll-y my-custom-scrollbar">
                    <table class="table table-striped table-bordered table-hover">

                        <thead>
                            <tr class="bg-success">
                                <th scope="col">Kode</th>
                                <th scope="col">Uraian</th>
                                <th scope="col">Waktu</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <?php
                                if ($ptb_respon) {
                                    foreach ($ptb_respon as $data) {
                                        ?>
                                        <td width="30%"><?php echo $data->KODE_RESPON; ?></td>
                                        <td width="30%"><?php echo $data->NOMOR_RESPON; ?></td>
                                        <td width="30%"><?php echo $data->TANGGAL_RESPON; ?></td>
                                        <td>
                                            <!--<a href='<?php echo base_url('preview_BC262/edit_respon/' . $data->ID . ''); ?>' class='btn blue'><i class="fa fa-pencil"></i></a>-->
                                            <a href="javascript:dialogHapus('<?php echo base_url('preview_BC262/delete_respon/' . $data->ID . ''); ?>')" class='btn red'><i class="fa fa-trash-o"></i></a>
                                        </td>
                                    </tr>
                                    <?php
                                }
                            }
                            ?>						    
                            </tr>
                        </tbody>
                    </table>



                </div>
            </div>	
        </div>

        <div class="modal-footer">
            <button type="button" class="btn btn-primary">Save changes</button>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
    </div>

</div>
<!-- MODAL KONTAINER -->

<div id="modal2" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog">
    <form action="preview_BC262/update_modal_kontainer" method = "POST">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title"><b>KONTAINER</b></h3>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12">
                        <form action="preview_BC261/update_popup_kontainer" method = "POST">
                            <div class="form-body">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <input type="hidden" class="form-control" id="id_kontainer" name="id_kontainer" value="">
                                    </div>
                                </div>
                            </div>
                            <div class="form-body">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <label>Nomor Kontainer</label>
                                        <input type="text" class="form-control" id="nomor_kontainer" name="nomor_kontainer" value="">
                                    </div>
                                </div>
                            </div>

                            <div class="form-body">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <label>Ukuran</label>
                                        <input type="text" class="form-control" id="ukuran_kontainer" name="ukuran_kontainer" value="">
                                    </div>
                                </div>
                            </div>
                            <div class="form-body">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <label>Tipe Kontainer</label>
                                        <input type="text" class="form-control" id="tipe_kontainer" name="tipe_kontainer" value="">
                                    </div>
                                </div>
                            </div>



                            <div class="modal-footer">
                                <button type="submit" class="btn blue" name="edit_bc261">Save</button>
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            </div>
                        </form>
                    </div>	
                </div>
                <br></br>
                <div class="row">
                    <div class="table-wrapper-scroll-y my-custom-scrollbar">

                        <table class="table table-striped table-bordered table-hover">

                            <thead>
                                <tr class="bg-success">
                                    <th scope="col">Nomor Kontainer</th>
                                    <th scope="col">Ukuran</th>
                                    <th scope="col">Tipe</th>
                                    <th scope="col">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <?php
                                    $n = 0;
                                    if ($ptb_kontainer) {
                                        foreach ($ptb_kontainer as $data) {
                                            $n = $n + 1;
                                            ?>
                                            <td width="30%"><input name ="nomor_kontainer" value = "<?php echo $data->NOMOR_KONTAINER; ?>"></input></td>
                                            <td width="30%"><input name ="ukuran_kontainer" value="<?php echo $data->URAIAN_UKURAN_KONTAINER; ?>"></input></td>
                                            <td width="30%"><input name ="tipe_kontainer" value ="<?php echo $data->URAIAN_TIPE_KONTAINER; ?>"></input></td>
                                    <input type="hidden" name ="id_kontainer" value ="<?php echo $data->id_kontainer; ?>">
                                    <td><button type="button" class="btn btn-dark" onclick="tampilkanEditKontainer(<?php echo $data->id_kontainer; ?>)"><i class="fa fa-info"></i></button>
                                        <a href="javascript:dialogHapus('<?php echo base_url('preview_BC262/delete_kontainer/' . $data->id_kontainer . ''); ?>')" class='btn red'><i class="fa fa-trash-o"></i></a></td>

                                    <?php
                                }
                            }
                            ?>
                            </tr>	    

                            </tbody>
                        </table>



                    </div>
                </div>	
            </div>

            <div class="modal2-footer">
                <button type="submit" class="btn btn-secondary">Save</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </form>
    <script>
        function dialogHapus(urlHapus) {
            if (confirm("Apakah anda yakin ingin menghapus ini ?")) {
                document.location = urlHapus;
            }
        }
    </script>
</div>

<!-- MODAL KEMASAN -->

<div id="modal3" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog">
    <!--<form action="preview_BC262/update_modal_kemasan " method = "POST">-->
    <div class="modal-content">
        <div class="modal-header">
            <h3 class="modal-title"><b>KEMASAN</b></h3>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-sm-12">
                    <form action="preview_BC262/update_popup_kemasan" method = "POST">
                        <div class="form-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <input type="hidden" class="form-control" id="id_kemasan" name="id_kemasan" value="">
                                </div>
                            </div>
                        </div>
                        <div class="form-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <label>Jumlah Kemasan</label>
                                    <input type="text" class="form-control" id="jml_kemasan" name="jml_kemasan" value="">
                                </div>
                            </div>
                        </div>

                        <div class="form-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <label>Kode Jenis Kemasan</label>
                                    <input type="text" class="form-control" id="kode_kemasan" name="kode_kemasan" value="">
                                </div>
                            </div>
                        </div>
                        <div class="form-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <label>Uraian Kemasan</label>
                                    <input type="text" class="form-control" id="uraian_kemasan" name="uraian_kemasan" value="">
                                </div>
                            </div>
                        </div>
                        <div class="form-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <label>Merk Kemasan</label>
                                    <input type="text" class="form-control" id="merk_kemasan" name="merk_kemasan" value="">
                                </div>
                            </div>
                        </div>


                        <div class="modal-footer">
                            <button type="submit" class="btn blue" name="edit_bc261">Save</button>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        </div>
                    </form>
                </div>	
            </div>
            <br></br>
            <div class="row">
                <div class="table-wrapper-scroll-y my-custom-scrollbar">
                    <table class="table table-striped table-bordered table-hover">

                        <thead>
                            <tr class="bg-success">
                                <th scope="col">Jumlah</th>
                                <th scope="col">Kode Jenis</th>
                                <th scope="col">Uraian</th>
                                <th scope="col">Merk Kemasan</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <?php
                                $n = 0;
                                if ($ptb_kemasan) {
                                    foreach ($ptb_kemasan as $data) {
                                        $n = $n + 1;
                                        ?>

                                        <td width="30%"><?php echo $data->JUMLAH_KEMASAN; ?></td>
                                        <td width="30%"><?php echo $data->KODE_JENIS_KEMASAN; ?></td>
                                        <td width="30%"><?php echo $data->URAIAN_KEMASAN; ?></td>
                                        <td width="30%"><?php echo $data->MERK_KEMASAN; ?></td>
                                <input type="hidden" name ="id_kemasan" value ="<?php echo $data->id_kemasan; ?>">      
                                <td><button type="button" class="btn btn-dark" onclick="tampilkanEditKemasan(<?php echo $data->id_kemasan; ?>)"><i class="fa fa-info"></i></button>
                                    <a href="javascript:dialogHapus('<?php echo base_url('preview_BC262/delete_kemasan/' . $data->id_kemasan . ''); ?>')" class='btn red'><i class="fa fa-trash-o"></i></a></td>


                                <?php
                            }
                        }
                        ?>
                        </tr>	    

                        </tbody>
                    </table>



                </div>
            </div>	
        </div>

        <!--            <div class="modal2-footer">
                        <button type="submit" class="btn btn-secondary">Save</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>-->
    </div>
</form>
<script>
    function dialogHapus(urlHapus) {
        if (confirm("Apakah anda yakin ingin menghapus ini ?")) {
            document.location = urlHapus;
        }
    }
</script>
</div>

<!-- MODAL DOKUMEN -->

<!-- MODAL DOKUMEN -->

<div id="modal4" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog">

    <div class="modal-content">
        <div class="modal-header">
            <h3 class="modal-title"><b>DOKUMEN</b></h3>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-sm-12">
                    <form action="preview_BC262/update_modal_dokumen" method = "POST">
                        <div class="form-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <input type="hidden" class="form-control" id="id" name="id" value="">
                                </div>
                            </div>
                        </div>
                        <div class="form-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <label>Jenis Dokumen</label>
                                    <input type="text" class="form-control" id="dokumen" name="dokumen" value="">
                                </div>
                            </div>
                        </div>

                        <div class="form-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <label>Nomor Dokumen</label>
                                    <input type="text" class="form-control" id="nomor" name="nomor" value="">
                                </div>
                            </div>
                        </div>
                        <div class="form-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <label>Tanggal Dokumen</label>
                                    <input type="text" class="form-control" id="tanggal" name="tanggal" value="">
                                </div>
                            </div>
                        </div>

                        <div class="modal-footer">
                            <button type="submit" class="btn blue" name="edit_bc261">Save</button>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        </div>
                    </form>
                </div>	
            </div>
            <br></br>
            <div class="row">
                <div class="table-wrapper-scroll-y my-custom-scrollbar">
                    <table class="table table-striped table-bordered table-hover">

                        <thead>
                            <tr class="bg-success">
                                <th scope="col">No.</th>
                                <th scope="col">Jenis Dokumen</th>
                                <th scope="col">Nomor Dokumen</th>
                                <th scope="col">Tanggal</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <?php
                                $n = 0;
                                if ($ptb_other) {
                                    foreach ($ptb_other as $data) {
                                        $n = $n + 1;
                                        ?>
                                    <tr><td width="30%"><?php echo $n; ?></td>
                                        <td width="30%"><?php echo $data->uraian_dokumen; ?></td>
                                        <td width="30%"><?php echo $data->nomor_dokumen; ?></td>
                                        <td width="30%"><?php echo $data->tanggal_dokumen; ?></td>
                                        <td><button type="button" class="btn btn-dark" onclick="tampilkanEditDokumen(<?php echo $data->id; ?>)"><i class="fa fa-info"></i></button>
                                            <a href="javascript:dialogHapus('<?php echo base_url('preview_BC262/delete_dokumen/' . $data->id . ''); ?>')" class='btn red'><i class="fa fa-trash-o"></i></a></td>
                                    </tr>
                                    <?php
                                }
                            }
                            ?>
                            </tr>		    

                        </tbody>
                    </table>



                </div>
            </div>	
        </div>

    </div>
</form>
<script>
    function dialogHapus(urlHapus) {
        if (confirm("Apakah anda yakin ingin menghapus ini ?")) {
            document.location = urlHapus;
        }
    }
</script>
</div>
<!-- MODAL JAMINAN -->

<div id="modal5" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog">
    <div class="modal-content">
        <div class="modal-header">
            <h3 class="modal-title"><b>JAMINAN</b></h3>
        </div>
        <div class="modal-body">
<div class="row">
                <div class="col-sm-12">
                    <form action="preview_BC262/update_popup_jaminan" method = "POST">
                        <div class="form-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <input type="hidden" class="form-control" id="id_jaminan" name="id_jaminan" value="">
                                </div>
                            </div>
                        </div>
                        <div class="form-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <label>Jenis Jaminan</label>
                                    <input type="text" class="form-control" id="jenis_jaminan" name="jenis_jaminan" value="">
                                </div>
                            </div>
                        </div>

                        <div class="form-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <label>Nomor Jaminan</label>
                                    <input type="text" class="form-control" id="nomor_jaminan" name="nomor_jaminan" value="">
                                </div>
                            </div>
                        </div>
                        <div class="form-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <label>Tanggal Jaminan</label>
                                    <input type="text" class="form-control" id="tgl_jaminan" name="tgl_jaminan" value="">
                                </div>
                            </div>
                        </div>
                        <div class="form-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <label>Nilai Jaminan</label>
                                    <input type="text" class="form-control" id="nilai_jaminan" name="nilai_jaminan" value="">
                                </div>
                            </div>
                        </div>
                        <div class="form-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <label>Jatuh Tempo</label>
                                    <input type="text" class="form-control" id="jatuh_tempo" name="jatuh_tempo" value="">
                                </div>
                            </div>
                        </div>
                        <div class="form-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <label>Penjamin</label>
                                    <input type="text" class="form-control" id="penjamin" name="penjamin" value="">
                                </div>
                            </div>
                        </div>
                        <div class="form-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <label>Nomor BPJ</label>
                                    <input type="text" class="form-control" id="nomor_bpj" name="nomor_bpj" value="">
                                </div>
                            </div>
                        </div>


                        <div class="modal-footer">
                            <button type="submit" class="btn blue" name="edit_bc261">Save</button>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        </div>
                    </form>
                </div>	
            </div>
            <br></br>
            <div class="row">
                <div class="table-wrapper-scroll-y my-custom-scrollbar">
                    <table class="table table-striped table-bordered table-hover">

                        <thead>
                            <tr class="bg-success">
                                <th scope="col">Jenis</th>
                                <th scope="col">Nomor</th>
                                <th scope="col">Tanggal</th>
                                <th scope="col">Nilai</th>
                                <th scope="col">Jatuh Tempo</th>
                                <th scope="col">Penjamin</th>
                                <th scope="col">Nomor BPJ</th>
                                <th scope="col">Tanggal BPJ</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                            <tr> <?php
                                $n = 0;
                                if ($ptb_jaminan) {
                                    foreach ($ptb_jaminan as $data) {
                                        $n = $n + 1;
                                        ?>
                            <tr>
                                        <td width="30%"><?php echo $data->URAIAN_JENIS_JAMINAN; ?></td>
                                        <td width="30%"><?php echo $data->NOMOR_JAMINAN; ?></td>
                                        <td width="30%"><?php echo $data->TANGGAL_JAMINAN; ?></td>
                                        <td width="30%"><?php echo $data->NILAI_JAMINAN; ?></td>
                                        <td width="30%"><?php echo $data->TANGGAL_JATUH_TEMPO; ?></td>
                                        <td width="30%"><?php echo $data->PENJAMIN; ?></td>
                                        <td width="30%"><?php echo $data->NOMOR_BPJ; ?></td>
                                        <td width="30%"><?php echo $data->TANGGAL_BPJ; ?></td>
                                        <td><button type="button" class="btn btn-dark" onclick="tampilkanEditJaminan(<?php echo $data->id_jaminan; ?>)"><i class="fa fa-info"></i></button>

                                            <a href="javascript:dialogHapus('<?php echo base_url('preview_BC262/delete_jaminan/' . $data->id_jaminan . ''); ?>')" class='btn red'><i class="fa fa-trash-o"></i></a></td>

                                        <?php
                                    }
                                }
                                ?>		
                            </tr>	    

                        </tbody>
                    </table>



                </div>
            </div>	
        </div>

        <div class="modal2-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
    </div>
    <script>
        function dialogHapus(urlHapus) {
            if (confirm("Apakah anda yakin ingin menghapus ini ?")) {
                document.location = urlHapus;
            }
        }
    </script>
</div>
</div>
<!-- TUTUP CONTAINER -->
<script type="text/javascript">
    function tampilkanEditDokumen(id) {
// alert(id);
        $.ajax({
            url: "<?php echo base_url('preview_BC261/editmodal/') ?>" + id,
            type: "GET",
            dataType: "JSON",
            success: function (data)
            {

                $("#dokumen").empty();
                $("#dokumen").val(data.dokumenedit.URAIAN_DOKUMEN);
                $("#nomor").empty();
                $("#nomor").val(data.dokumenedit.NOMOR_DOKUMEN);
                $("#tanggal").empty();
                $("#tanggal").val(data.dokumenedit.TANGGAL_DOKUMEN);
                $("#id").empty();
                $("#id").val(data.dokumenedit.ID);
                $("#modal4").modal('show');

            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error get data from ajax');
            }
        });
// $('#modal').modal();

// var url= '<?php echo base_url(); ?>Bc25_controller/editmodal/'+id;
// $.get( url, function( data ) {
// // console.log(data);
// $("#dokumen").empty();
// $("#dokumen").val(data.KODE_JENIS_DOKUMEN);
// $("#nomor").empty();
// $("#nomor").val(data.NOMOR_DOKUMEN);
// $("#tgl").empty();
// $("#tgl").val(data.TANGGAL_DOKUMEN);
// });
    }
</script>
<script type="text/javascript">
    function tampilkanEditKemasan(id_kemasan) {
// alert(id_kemasan);
        $.ajax({
            url: "<?php echo base_url('preview_BC262/editmodal_kemasan/') ?>" + id_kemasan,
            type: "GET",
            dataType: "JSON",
            success: function (data)
            {

                $("#jml_kemasan").empty();
                $("#jml_kemasan").val(data.dokumenedit.JUMLAH_KEMASAN);
                $("#kode_kemasan").empty();
                $("#kode_kemasan").val(data.dokumenedit.KODE_JENIS_KEMASAN);
                $("#uraian_kemasan").empty();
                $("#uraian_kemasan").val(data.dokumenedit.URAIAN_KEMASAN);
                $("#merk_kemasan").empty();
                $("#merk_kemasan").val(data.dokumenedit.MERK_KEMASAN);
                $("#id_kemasan").empty();
                $("#id_kemasan").val(data.dokumenedit.ID);
                $("#modal3").modal('show');

            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error get data from ajax');
            }
        });
// $('#modal').modal();

// var url= '<?php echo base_url(); ?>Bc25_controller/editmodal/'+id;
// $.get( url, function( data ) {
// // console.log(data);
// $("#dokumen").empty();
// $("#dokumen").val(data.KODE_JENIS_DOKUMEN);
// $("#nomor").empty();
// $("#nomor").val(data.NOMOR_DOKUMEN);
// $("#tgl").empty();
// $("#tgl").val(data.TANGGAL_DOKUMEN);
// });
    }
</script>
<script type="text/javascript">
    function tampilkanEditKontainer(id_kontainer) {
// alert(id_kontainer);
        $.ajax({
            url: "<?php echo base_url('preview_BC262/editmodal_kontainer/') ?>" + id_kontainer,
            type: "GET",
            dataType: "JSON",
            success: function (data)
            {

                $("#nomor_kontainer").empty();
                $("#nomor_kontainer").val(data.dokumenedit.NOMOR_KONTAINER);
                $("#ukuran_kontainer").empty();
                $("#ukuran_kontainer").val(data.dokumenedit.URAIAN_UKURAN_KONTAINER);
                $("#tipe_kontainer").empty();
                $("#tipe_kontainer").val(data.dokumenedit.URAIAN_TIPE_KONTAINER);
                $("#id_kontainer").empty();
                $("#id_kontainer").val(data.dokumenedit.ID);
                $("#modal2").modal('show');

            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error get data from ajax');
            }
        });
// $('#modal').modal();

// var url= '<?php echo base_url(); ?>Bc25_controller/editmodal/'+id;
// $.get( url, function( data ) {
// // console.log(data);
// $("#dokumen").empty();
// $("#dokumen").val(data.KODE_JENIS_DOKUMEN);
// $("#nomor").empty();
// $("#nomor").val(data.NOMOR_DOKUMEN);
// $("#tgl").empty();
// $("#tgl").val(data.TANGGAL_DOKUMEN);
// });
    }
</script>
<script type="text/javascript">
    function tampilkanEditJaminan(id_jaminan) {
// alert(id_kontainer);
        $.ajax({
            url: "<?php echo base_url('preview_BC262/editmodal_jaminan/') ?>" + id_jaminan,
            type: "GET",
            dataType: "JSON",
            success: function (data)
            {

                $("#jenis_jaminan").empty();
                $("#jenis_jaminan").val(data.dokumenedit.URAIAN_JENIS_JAMINAN);
                $("#nomor_jaminan").empty();
                $("#nomor_jaminan").val(data.dokumenedit.NOMOR_JAMINAN);
                $("#tgl_jaminan").empty();
                $("#tgl_jaminan").val(data.dokumenedit.TANGGAL_JAMINAN);
                $("#nilai_jaminan").empty();
                $("#nilai_jaminan").val(data.dokumenedit.NILAI_JAMINAN);
                $("#jatuh_tempo").empty();
                $("#jatuh_tempo").val(data.dokumenedit.TANGGAL_JATUH_TEMPO);
                $("#penjamin").empty();
                $("#penjamin").val(data.dokumenedit.PENJAMIN);
                $("#nomor_bpj").empty();
                $("#nomor_bpj").val(data.dokumenedit.NOMOR_BPJ);
                $("#id_jaminan").empty();
                $("#id_jaminan").val(data.dokumenedit.ID);
                $("#modal5").modal('show');

            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error get data from ajax');
            }
        });
// $('#modal').modal();

// var url= '<?php echo base_url(); ?>Bc25_controller/editmodal/'+id;
// $.get( url, function( data ) {
// // console.log(data);
// $("#dokumen").empty();
// $("#dokumen").val(data.KODE_JENIS_DOKUMEN);
// $("#nomor").empty();
// $("#nomor").val(data.NOMOR_DOKUMEN);
// $("#tgl").empty();
// $("#tgl").val(data.TANGGAL_DOKUMEN);
// });
    }
</script>