<?php
//Model_data.php
defined('BASEPATH') OR exit('No direct script access allowed');

Class Recal_model extends CI_Model{

	public function recal_inventory($post){
		$tgl = $this->db->escape($post['tanggal_awal']);
		$tgl_akhir = $this->db->escape($post['tanggal_akhir']);

		$tgl_bulan = substr($tgl, 1, 2);
		$tgl_hari = substr($tgl, 4, 2);
		$tgl_tahun = substr($tgl, 7, 4);

		$tgl_akhir_bulan = substr($tgl_akhir, 1, 2);
		$tgl_akhir_hari = substr($tgl_akhir, 4, 2);
		$tgl_akhir_tahun = substr($tgl_akhir, 7, 4);

		$tanggal_awal = $tgl_akhir_tahun."-01-01";
		$tanggal = $tgl_tahun."-".$tgl_bulan."-".$tgl_hari;
		$tanggal_akhir = $tgl_akhir_tahun."-".$tgl_akhir_bulan."-".$tgl_akhir_hari;

		$sql_item_inventory = $this->db->query("SELECT distinct(item_id) FROM public.beone_inventory WHERE flag = 1");

for ($x = 0; $x <= 10; $x++) {

		foreach($sql_item_inventory->result_array() as $row){
						$sql_list_item = $this->db->query("SELECT * FROM public.beone_inventory WHERE item_id = ".intval($row['item_id'])." AND flag = 1 AND trans_date BETWEEN '$tanggal' AND '$tanggal_akhir' ORDER BY trans_date ASC, intvent_trans_id ASC");

						//SALDO AWAL ITEM
						$sai = $this->db->query("SELECT * FROM public.beone_item WHERE item_id =".intval($row['item_id']));
						$saldo_awal_item = $sai->row_array();

						//SALDO MUTASI ITEM DARI AWAL TAHUN SAMPAI SEKARANG
						$inv = $this->db->query("SELECT qty_in, value_in, qty_out, value_out, sa_qty, sa_unit_price, sa_amount FROM public.beone_inventory WHERE trans_date BETWEEN '$tanggal_awal' AND '$tanggal' AND item_id =".intval($row['item_id'])." ORDER BY trans_date DESC, intvent_trans_id DESC LIMIT 1");
						$hasil_inv = $inv->row_array();

						//cek saldo awal
						$cek = $this->db->query("SELECT count(item_id) as jml FROM public.beone_inventory WHERE flag = 1 AND item_id = ".intval($row['item_id'])); //tambah filteran range tanggal sampai tgl produksi (edit lukman)
						$cek_sa = $cek->row_array();


						$urutan_transaksi = 0;
						$saldo_awal_qty = 0;
						$saldo_awal_unit_price = 0;
						$saldo_awal_amount = 0;

						foreach($sql_list_item->result_array() as $row2){//mutasi per item

//******************************************************************************************************

							$urutan_transaksi = $urutan_transaksi + 1;

							if ($urutan_transaksi == 1){
								$saldo_awal_qty = $row2['sa_qty'];
								$saldo_awal_unit_price = $row2['sa_unit_price'];
								$saldo_awal_amount = $row2['sa_amount'];
							}else{

								if ($row2['qty_in'] == 0){//transaksi keluar

									$hasil_sa_qty = $saldo_awal_qty - $row2['qty_out'];

									if($saldo_awal_qty == $row2['qty_out']){
										$hasil_sa_amount = 0;
										$hasil_sa_unit_price = 0;
									}else{
										$hasil_sa_amount = $saldo_awal_amount-($row2['qty_out'] * $row2['value_out']);
										//$hasil_sa_unit_price = ($saldo_awal_amount - ($row2['sa_qty'] * $row2['value_out'])) / ($saldo_awal_qty - $row2['sa_qty']);
										$hasil_sa_unit_price = $row2['sa_amount'] / $row2['sa_qty'];
									}

									$sql_produksi = $this->db->query("UPDATE public.beone_inventory
																										SET value_out = $saldo_awal_unit_price ,sa_qty = $hasil_sa_qty, sa_unit_price=$hasil_sa_unit_price, sa_amount=$hasil_sa_amount
																										WHERE intvent_trans_id = $row2[intvent_trans_id]");


									$saldo_awal_qty = $row2['sa_qty'];
									$saldo_awal_amount = $row2['sa_amount'];
									if ($row2['sa_amount'] == 0 OR $row2['sa_qty'] == 0){
											$saldo_awal_unit_price = 0;
									}else{
											$saldo_awal_unit_price = $row2['sa_amount'] / $row2['sa_qty'];
									}

								}else{//transaksi masuk

										if($saldo_awal_qty == 0){
											$hasil_sa_qty = $row2['qty_in'];
											$hasil_sa_amount = $row2['qty_in']*$row2['value_in'];
											$hasil_sa_unit_price = $row2['value_in'];
										}else{
											$hasil_sa_qty = $saldo_awal_qty + $row2['qty_in'];
											$hasil_sa_amount = $saldo_awal_amount+($row2['qty_in'] * $row2['value_in']);
											$hasil_sa_unit_price = $row2['sa_amount'] / $row2['sa_qty'];
										}

									$sql_produksi = $this->db->query("UPDATE public.beone_inventory
																										SET sa_qty = $hasil_sa_qty, sa_unit_price=$hasil_sa_unit_price, sa_amount=$hasil_sa_amount
																										WHERE intvent_trans_id = $row2[intvent_trans_id]");

										$saldo_awal_qty = $row2['sa_qty'];
										$saldo_awal_amount = $row2['sa_amount'];
										if ($row2['sa_amount'] == 0 OR $row2['sa_qty'] == 0){
												$saldo_awal_unit_price = 0;
										}else{
												$saldo_awal_unit_price = $row2['sa_amount'] / $row2['sa_qty'];
										}
								}

							}

//******************************************************************************************************

						}

				}
			}//for 300
}
}
?>
