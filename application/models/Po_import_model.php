<?php
//Model_data.php
defined('BASEPATH') OR exit('No direct script access allowed');

Class Po_import_model extends CI_Model{


	public function simpan($post){
		$session_id = $this->session->userdata('user_id');
		$tanggal_awal = $this->db->escape($post['tanggal']);
		$supplier = $this->db->escape($post['supplier']);
		$keterangan = $this->db->escape($post['keterangan_header']);
		$purch_no = $this->db->escape($post['nomor_po']);
		$subtotal_ = $this->db->escape($post['subtotal']);
		$update_date = date('Y-m-d');

		$subtotal_ex = str_replace(".", "", $subtotal_);
		$subtotal = str_replace(",", ".", $subtotal_ex);

		$tgl_bulan = substr($tanggal_awal, 1, 2);
		$tgl_hari = substr($tanggal_awal, 4, 2);
		$tgl_tahun = substr($tanggal_awal, 7, 4);

		$tanggal = $tgl_tahun."-".$tgl_bulan."-".$tgl_hari;
		$tanggal_awal_tahun = $tgl_tahun."-01-01";

		$sql_header = $this->db->query("INSERT INTO public.beone_po_import_header(
																		purchase_header_id, purchase_no, trans_date, supplier_id, keterangan, grandtotal, flag, update_by, update_date)
																		VALUES (DEFAULT, $purch_no, '$tanggal', $supplier, $keterangan, $subtotal, 1, $session_id, '$tanggal')");

		helper_log($tipe = "add", $str = "Tambah PO Import ".$post['nomor_po']);

		$header_id = $this->db->query("SELECT * FROM public.beone_po_import_header ORDER BY purchase_header_id DESC LIMIT 1");
		$hasil_header_id = $header_id->row_array();
		$hid = $hasil_header_id['purchase_header_id'];

		foreach ($_POST['rows'] as $key => $count ){
							 $item_id = $_POST['item_id_'.$count];
							 $qty_ = $_POST['qty_'.$count];
							 $price_ = $_POST['price_'.$count];
							 $amount_ = $_POST['amount_'.$count];

					 		 $qty_ex = str_replace(".", "", $qty_);
							 $price_ex = str_replace(".", "", $price_);
							 $amount_ex = str_replace(".", "", $amount_);

							 $qty = str_replace(",", ".", $qty_ex);
							 $price = str_replace(",", ".", $price_ex);
							 $amount = str_replace(",", ".", $amount_ex);

							 $sql_detail = $this->db->query("INSERT INTO public.beone_po_import_detail(
																								purchase_detail_id, purchase_header_id, item_id, qty, price, amount, flag)
																								VALUES (DEFAULT, $hid, $item_id, $qty, $price, $amount, 1)");

					 }

		if($sql_header && $sql_detail)
			return true;
		return false;
	}

	public function delete($purchase_header_id){
		$sql_po = $this->db->query("SELECT purchase_no FROM public.beone_po_import_header WHERE purchase_header_id = ".intval($purchase_header_id));
		$hasil_po = $sql_po->row_array();
		helper_log($tipe = "delete", $str = "Hapus PO Import ".$hasil_po['purchase_no']);

		$sql = $this->db->query("DELETE FROM public.beone_po_import_header WHERE purchase_header_id = ".intval($purchase_header_id));
		$sql_detail = $this->db->query("DELETE FROM public.beone_po_import_detail WHERE purchase_header_id = ".intval($purchase_header_id));
	}


	public function load_po_import(){
		$sql = $this->db->query("SELECT * FROM public.beone_po_import_header");
		return $sql->result_array();
	}

	public function load_data_po_import($header_id){
		$sql = $this->db->query("SELECT h.purchase_header_id, d.purchase_detail_id, h.purchase_no, h.trans_date, c.nama as nsupplier, i.nama as nitem, d.price, d.amount, d.qty, c.alamat
														FROM public.beone_po_import_header h INNER JOIN public.beone_po_import_detail d ON h.purchase_header_id = d.purchase_header_id INNER JOIN public.beone_custsup c ON c.custsup_id = h.supplier_id INNER JOIN public.beone_item i ON i.item_id = d.item_id WHERE h.purchase_header_id = ".intval($header_id));
		return $sql->result_array();
	}


}
?>
