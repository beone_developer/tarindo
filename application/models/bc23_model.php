<?php

class bc23_model extends CI_Model {

//    var $tabel = "usr";
    //   private $another;
    function __construct() {
        parent::__construct();
        $this->db = $this->load->database('default', TRUE);
        $this->mysql = $this->load->database('mysql', TRUE);
    }

    public function select() {

    //$select = "select * from tpb_header where kode_dokumen_pabean = 23";
        $select = "select h.id as ID, h.nomor_aju as NOMOR_AJU, h.nama_pemasok as NAMA_PEMASOK, h.tanggal_aju as TANGGAL_AJU, h.kode_status, s.uraian_status as URAIAN_STATUS from tpb_header h INNER JOIN referensi_status s ON h.kode_status = s.kode_status where h.kode_dokumen_pabean = 23 AND s.kode_dokumen = 23 order by h.tanggal_aju desc";
        $data = $this->mysql->query($select);
        return $data->result();
	}

    public function delete($id) {

        $delete_pungutan = "delete from tpb_pungutan where ID_HEADER = '" . $id . "'";
        $data1 = $this->mysql->query($delete_pungutan);
        $delete_kontainer = "delete from tpb_kontainer where ID_HEADER = '" . $id . "'";
        $data2 = $this->mysql->query($delete_kontainer);
        $delete_kemasan = "delete from tpb_kemasan where ID_HEADER = '" . $id . "'";
        $data3 = $this->mysql->query($delete_kemasan);
        $delete_dokumen = "delete from tpb_dokumen where ID_HEADER = '" . $id . "'";
        $data4 = $this->mysql->query($delete_dokumen);
        $delete_brgtarif = "delete from tpb_barang_tarif where ID_HEADER = '" . $id . "'";
        $data5 = $this->mysql->query($delete_brgtarif);
        $delete_brg= "delete from tpb_barang where ID_HEADER = '" . $id . "'";
        $data6 = $this->mysql->query($delete_brg);
        $delete_dtl= "delete from tpb_detil_status where ID_HEADER = '" . $id . "'";
        $data7 = $this->mysql->query($delete_dtl);
        $delete_val= "delete from hasil_validasi_header where ID_HEADER = '" . $id . "'";
        $data8 = $this->mysql->query($delete_val);
        $delete_bb= "delete from tpb_bahan_baku where ID_HEADER = '" . $id . "'";
        $data9 = $this->mysql->query($delete_bb);
        $delete_bbdok= "delete from tpb_bahan_baku_dokumen where ID_HEADER = '" . $id . "'";
        $data10 = $this->mysql->query($delete_bbdok);
        $delete_bbtar= "delete from tpb_bahan_baku_tarif where ID_HEADER = '" . $id . "'";
        $data11 = $this->mysql->query($delete_bbtar);
        $delete_brgdok= "delete from tpb_barang_dokumen where ID_HEADER = '" . $id . "'";
        $data12 = $this->mysql->query($delete_brgdok);
        $delete_brgter= "delete from tpb_barang_penerima where ID_HEADER = '" . $id . "'";
        $data13 = $this->mysql->query($delete_brgter);
        $delete_jamin= "delete from tpb_jaminan where ID_HEADER = '" . $id . "'";
        $data14 = $this->mysql->query($delete_jamin);
        $delete_billing= "delete from tpb_npwp_billing where ID_HEADER = '" . $id . "'";
        $data15 = $this->mysql->query($delete_billing);
        $delete_penerima= "delete from tpb_penerima where ID_HEADER = '" . $id . "'";
        $data16 = $this->mysql->query($delete_penerima);
        $delete_respon = "delete from tpb_respon where ID_HEADER = '" . $id . "'";
        $data17 = $this->mysql->query($delete_respon);

        $delete = "delete from tpb_header where ID = '" . $id . "'";
        $data = $this->mysql->query($delete);
        if ($data)
            return true;
        return false;
    }
    public function insert($post) {

        $KODE_STATUS = $this->mysql->escape($post['status']);
        $KODE_STATUS_PERBAIKAN = $this->mysql->escape($post['status_perbaikan']);
        $NOMOR_AJU = $this->mysql->escape($post['nomor_pengajuan']);
        $NOMOR_DAFTAR = $this->mysql->escape($post['nomor_pendaftaran']);
        $TANGGAL_DAFTAR = $this->mysql->escape($post['tanggal_pendaftaran']);
        $kppbc_bongkar = $this->mysql->escape($post['kppbc_bongkar']);
        $kppbc_pengawas = $this->mysql->escape($post['kppbc_pengawas']);
        $tujuan = $this->mysql->escape($post['tujuan']);
        $nama_pemasok = $this->mysql->escape($post['nama_pemasok']);
        $alamat_pemasok = $this->mysql->escape($post['alamat_pemasok']);
        $negara_pemasok = $this->mysql->escape($post['negara_pemasok']);
        $jenis_npwp = $this->mysql->escape($post['jenis_npwp']);
        $no_npwp = $this->mysql->escape($post['no_npwp']);
        $nama_importir = $this->mysql->escape($post['nama_importir']);
        $no_izin_importir = $this->mysql->escape($post['no_izin_importir']);
        $alamat_importir = $this->mysql->escape($post['alamat_importir']);
        $kode_api_importir = $this->mysql->escape($post['kode_api_importir']);
        $nomor_api_importir = $this->mysql->escape($post['nomor_api_importir']);
        $jenis_npwp_pemilik = $this->mysql->escape($post['jenis_npwp_pemilik']);
        $nomor_npwp_pemilik = $this->mysql->escape($post['nomor_npwp_pemilik']);
        $nama_pemilik = $this->mysql->escape($post['nama_pemilik']);
        $alamat_pemilik = $this->mysql->escape($post['alamat_pemilik']);

        $NAMA_PENERIMA_BARANG = $this->mysql->escape($post['nama_penerima']);
        $ALAMAT_PENERIMA_BARANG = $this->mysql->escape($post['alamat_penerima']);
        $NIPER_PENERIMA = $this->mysql->escape($post['niper_penerima']);
        $KODE_JENIS_API_PENERIMA = $this->mysql->escape($post['kode_api_penerima']);
        $API_PENERIMA = $this->mysql->escape($post['api_penerima']);
        $BRUTO = $this->mysql->escape($post['bruto']);
        $NETTO = $this->mysql->escape($post['netto']);
        $JUMLAH_BARANG = $this->mysql->escape($post['jumlah_barang']);
        $KODE_VALUTA = $this->mysql->escape($post['valuta']);
        $NDPBM = $this->mysql->escape($post['ndpbm']);
        $CIF = $this->mysql->escape($post['nilai_cif']);
        $HARGA_PENYERAHAN = $this->mysql->escape($post['harga_penyerahan']);
        $KODE_CARA_ANGKUT = $this->mysql->escape($post['kode_sarana_pengangkut']);
        $KODE_PEMBAYAR = $this->mysql->escape($post['jenis_pembayar']);
        $KODE_LOKASI_BAYAR = $this->mysql->escape($post['kode_lokasi_bayar']);
        // $KODE_LOKASI_BAYAR = $this->mysql->escape($post['kode_lokasi_bayar']);
        // $KODE_LOKASI_BAYAR = $this->mysql->escape($post['kode_lokasi_bayar']);
        //insert akun
        $sql = $this->mysql->query("INSERT INTO tpb_header(KODE_STATUS, KODE_STATUS_PERBAIKAN, NOMOR_AJU, NOMOR_DAFTAR, TANGGAL_DAFTAR, KODE_KANTOR, KODE_JENIS_TPB, KODE_ID_PENGUSAHA, ID_PENGUSAHA, NAMA_PENGUSAHA, ALAMAT_PENGUSAHA, NOMOR_IJIN_TPB, KODE_JENIS_API_PENGUSAHA, API_PENGUSAHA, KODE_ID_PEMILIK, ID_PEMILIK, NAMA_PEMILIK, ALAMAT_PEMILIK, KODE_JENIS_API_PEMILIK, API_PEMILIK, KODE_ID_PENERIMA_BARANG, ID_PENERIMA_BARANG, NAMA_PENERIMA_BARANG, ALAMAT_PENERIMA_BARANG, NIPER_PENERIMA, KODE_JENIS_API_PENERIMA, API_PENERIMA, BRUTO, NETTO, JUMLAH_BARANG, KODE_VALUTA, NDPBM, CIF, HARGA_PENYERAHAN, KODE_CARA_ANGKUT, KODE_PEMBAYAR, KODE_LOKASI_BAYAR, KODE_DOKUMEN_PABEAN, VERSI_MODUL, ID_MODUL) VALUES ($KODE_STATUS, $KODE_STATUS_PERBAIKAN, $NOMOR_AJU, $NOMOR_DAFTAR, $TANGGAL_DAFTAR, $KODE_KANTOR, $KODE_JENIS_TPB, $KODE_ID_PENGUSAHA, $ID_PENGUSAHA, $NAMA_PENGUSAHA, $ALAMAT_PENGUSAHA, $NOMOR_IJIN_TPB, $KODE_JENIS_API_PENGUSAHA, $API_PENGUSAHA, $KODE_ID_PEMILIK, $ID_PEMILIK, $NAMA_PEMILIK, $ALAMAT_PEMILIK, $KODE_JENIS_API_PEMILIK, $API_PEMILIK, $KODE_ID_PENERIMA_BARANG, $ID_PENERIMA_BARANG, $NAMA_PENERIMA_BARANG, $ALAMAT_PENERIMA_BARANG, $NIPER_PENERIMA, $KODE_JENIS_API_PENERIMA, $API_PENERIMA, $BRUTO, $NETTO, $JUMLAH_BARANG, $KODE_VALUTA, $NDPBM, $CIF, $HARGA_PENYERAHAN, $KODE_CARA_ANGKUT, $KODE_PEMBAYAR, $KODE_LOKASI_BAYAR, '25', '3.1.8', '15346')");

        if ($sql)
            return true;
        return false;
    }
    public function daftar_respon() {
        $select = "select * from tpb_header th "
                . "left join tpb_respon tr on th.id = tr.ID_header";
        $data = $this->mysql->query($select);
        return $data->result();
    }
}

?>
