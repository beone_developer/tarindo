<?php
//Model_data.php
defined('BASEPATH') OR exit('No direct script access allowed');

Class Kredit_note_model extends CI_Model{

	public function simpan_kredit_note($post){
		$session_id = $this->session->userdata('user_id');
		$nomor_transaksi = $this->db->escape($post['nomor_kd_note']);
		$tanggal_awal = $this->db->escape($post['tanggal_dk_note']);
		$coa_id = $this->db->escape($post['coa_id']);
		$supplier = $this->db->escape($post['supplier']);
		$keterangan = $this->db->escape($post['keterangan_kredit_note']);
		$saldo_idr_ = $this->db->escape($post['jumlah_idr']);
		$saldo_valas_ = $this->db->escape($post['jumlah_valas']);
		$posisi = $this->db->escape($post['posisi']);//0=minus, 1=plus

		$tgl_bulan = substr($tanggal_awal, 1, 2);
		$tgl_hari = substr($tanggal_awal, 4, 2);
		$tgl_tahun = substr($tanggal_awal, 7, 4);

		$tanggal = $tgl_tahun."-".$tgl_bulan."-".$tgl_hari;

		$update_date = date('Y-m-d');

		$saldo_idr_ex = str_replace(".", "", $saldo_idr_);
		$saldo_idr = str_replace(",", ".", $saldo_idr_ex);

		$saldo_valas_ex = str_replace(".", "", $saldo_valas_);
		$saldo_valas = str_replace(",", ".", $saldo_valas_ex);

			$cek_supplier = $this->db->query("SELECT * FROM public.beone_custsup WHERE custsup_id = $supplier");
			$hasil_cek_supplier = $cek_supplier->row_array();

			$pelunsan_idr = $hasil_cek_supplier['pelunasan_hutang_idr'];
			$pelunasan_valas = $hasil_cek_supplier['pelunasan_hutang_valas'];

			$pelunasan_hutang_idr_update = $pelunsan_idr + $saldo_idr;
			$pelunasan_hutang_valas_update = $pelunasan_valas + $saldo_valas;

			$sql_update_custsup = $this->db->query("UPDATE public.beone_custsup
																							SET pelunasan_hutang_idr=$pelunasan_hutang_idr_update, pelunasan_hutang_valas=$pelunasan_hutang_valas_update
																							WHERE custsup_id = ".intval($supplier));

		if ($hasil_cek_supplier['negara'] == 1){//supplier import
						$coa_jurnal_hutang_usaha_import = $this->db->query("SELECT * FROM public.beone_coa_jurnal WHERE coa_jurnal_id = 8"); //coa jurnal hutang usaha import
						$coa_hutang_import = $coa_jurnal_hutang_usaha_import->row_array();
						$chi_id = $coa_hutang_import['coa_id'];
						$chi_no = $coa_hutang_import['coa_no'];
		}else{
						$coa_jurnal_hutang_usaha_lokal = $this->db->query("SELECT * FROM public.beone_coa_jurnal WHERE coa_jurnal_id = 3"); //coa jurnal hutang usaha
						$coa_hutang_lokal = $coa_jurnal_hutang_usaha_lokal->row_array();
						$chi_id = $coa_hutang_lokal['coa_id'];
						$chi_no = $coa_hutang_lokal['coa_no'];

		}

		$coa_kredit_note = $this->db->query("SELECT * FROM public.beone_coa WHERE coa_id = $coa_id");
		$hasil_coa_kredit_note = $coa_kredit_note->row_array();
		$coa_no = $hasil_coa_kredit_note['nomor'];


		if ($post['posisi'] == 1){//posisi plus
				$sql = $this->db->query("INSERT INTO public.beone_hutang_piutang(
																hutang_piutang_id, custsup_id, trans_date, nomor, keterangan, valas_trans, idr_trans, valas_pelunasan, idr_pelunasan, tipe_trans, update_by, update_date, flag, status_lunas)
																VALUES (DEFAULT, $supplier, '$tanggal', $nomor_transaksi, $keterangan, $saldo_valas, $saldo_idr, 0, 0, 1, $session_id, '$update_date', 1, 1)");


				$sql_ledger_debet = $this->db->query("INSERT INTO public.beone_gl(
																				 gl_id, gl_date, coa_id, coa_no, coa_id_lawan, coa_no_lawan, keterangan, debet, kredit, pasangan_no, gl_number, update_by, update_date)
																				 VALUES (DEFAULT, '$tanggal', $coa_id, '$coa_no', $chi_id, '$chi_no', $keterangan, $saldo_idr, 0, $nomor_transaksi, $nomor_transaksi, $session_id, '$update_date')");

				$sql_ledger_kredit = $this->db->query("INSERT INTO public.beone_gl(
																				gl_id, gl_date, coa_id, coa_no, coa_id_lawan, coa_no_lawan, keterangan, debet, kredit, pasangan_no, gl_number, update_by, update_date)
																				VALUES (DEFAULT, '$tanggal', $chi_id, '$chi_no', $coa_id, '$coa_no', $keterangan, 0, $saldo_idr, $nomor_transaksi, $nomor_transaksi, $session_id, '$update_date')");
		}else{//posisi minus
					$sql = $this->db->query("INSERT INTO public.beone_hutang_piutang(
																	hutang_piutang_id, custsup_id, trans_date, nomor, keterangan, valas_trans, idr_trans, valas_pelunasan, idr_pelunasan, tipe_trans, update_by, update_date, flag, status_lunas)
																	VALUES (DEFAULT, $supplier, '$tanggal', $nomor_transaksi, $keterangan, 0, 0, $saldo_valas, $saldo_idr, 1, $session_id, '$update_date', 1, 1)");

					$sql_ledger_debet = $this->db->query("INSERT INTO public.beone_gl(
																					 gl_id, gl_date, coa_id, coa_no, coa_id_lawan, coa_no_lawan, keterangan, debet, kredit, pasangan_no, gl_number, update_by, update_date)
																					 VALUES (DEFAULT, '$tanggal', $chi_id, '$chi_no', $coa_id, '$coa_no', $keterangan, $saldo_idr, 0, $nomor_transaksi, $nomor_transaksi, $session_id, '$update_date')");

					$sql_ledger_kredit = $this->db->query("INSERT INTO public.beone_gl(
																					gl_id, gl_date, coa_id, coa_no, coa_id_lawan, coa_no_lawan, keterangan, debet, kredit, pasangan_no, gl_number, update_by, update_date)
																					VALUES (DEFAULT, '$tanggal', $coa_id, '$coa_no', $chi_id, '$chi_no', $keterangan, 0, $saldo_idr, $nomor_transaksi, $nomor_transaksi, $session_id, '$update_date')");
		}

		helper_log($tipe = "add", $str = "Kredit Note, No ".$post['nomor_kd_note']);

		if($sql)
			return true;
		return false;
	}


	public function update($post, $kdn_no){
		$session_id = $this->session->userdata('user_id');
		$id = $this->db->escape($post['hp_id']);
		$coa_id = $this->db->escape($post['coa_id']);
		$supplier = $this->db->escape($post['supplier']);
		$keterangan = $this->db->escape($post['keterangan_kredit_note']);
		$saldo_idr_ = $this->db->escape($post['jumlah_idr']);
		$saldo_valas_ = $this->db->escape($post['jumlah_valas']);
		$posisi = $this->db->escape($post['posisi']);//0=minus, 1=plus

		$update_date = date('Y-m-d');

		$saldo_idr_ex = str_replace(".", "", $saldo_idr_);
		$saldo_idr = str_replace(",", ".", $saldo_idr_ex);

		$saldo_valas_ex = str_replace(".", "", $saldo_valas_);
		$saldo_valas = str_replace(",", ".", $saldo_valas_ex);

		$cek_supplier = $this->db->query("SELECT * FROM public.beone_custsup WHERE custsup_id = $supplier");
		$hasil_cek_supplier = $cek_supplier->row_array();

		$pelunsan_idr = $hasil_cek_supplier['pelunasan_hutang_idr'];
		$pelunasan_valas = $hasil_cek_supplier['pelunasan_hutang_valas'];

		$pelunasan_hutang_idr_update = $pelunsan_idr + $saldo_idr;
		$pelunasan_hutang_valas_update = $pelunasan_valas + $saldo_valas;

		$sql_update_custsup = $this->db->query("UPDATE public.beone_custsup
																						SET pelunasan_hutang_idr=$pelunasan_hutang_idr_update, pelunasan_hutang_valas=$pelunasan_hutang_valas_update
																						WHERE custsup_id = ".intval($supplier));

		if ($hasil_cek_supplier['negara'] == 1){//supplier import
						$coa_jurnal_hutang_usaha_import = $this->db->query("SELECT * FROM public.beone_coa_jurnal WHERE coa_jurnal_id = 8"); //coa jurnal hutang usaha import
						$coa_hutang_import = $coa_jurnal_hutang_usaha_import->row_array();
						$chi_id = $coa_hutang_import['coa_id'];
						$chi_no = $coa_hutang_import['coa_no'];
		}else{
						$coa_jurnal_hutang_usaha_lokal = $this->db->query("SELECT * FROM public.beone_coa_jurnal WHERE coa_jurnal_id = 3"); //coa jurnal hutang usaha
						$coa_hutang_lokal = $coa_jurnal_hutang_usaha_lokal->row_array();
						$chi_id = $coa_hutang_lokal['coa_id'];
						$chi_no = $coa_hutang_lokal['coa_no'];

		}

		$coa_kredit_note = $this->db->query("SELECT * FROM public.beone_coa WHERE coa_id = $coa_id");
		$hasil_coa_kredit_note = $coa_kredit_note->row_array();
		$coa_no = $hasil_coa_kredit_note['nomor'];

		$sql_dk = $this->db->query("SELECT * FROM public.beone_gl WHERE gl_number = '$kdn_no'");


		if ($post['posisi'] == 1){//posisi plus (beli)
						$sql = $this->db->query("UPDATE public.beone_hutang_piutang
																			SET  custsup_id=$supplier, keterangan=$keterangan, valas_trans=$saldo_valas, idr_trans=$saldo_idr, valas_pelunasan=0, idr_pelunasan=0, update_by=$session_id, update_date='$update_date'
																			WHERE hutang_piutang_id = $id");

						foreach($sql_dk->result_array() as $row){
									$id_gl = $row['gl_id'];
									if ($row['kredit'] == 0){ //posisi debit
													$sql_ledger_debet = $this->db->query("UPDATE public.beone_gl
																														SET coa_id=$chi_id, coa_no='$chi_no', coa_id_lawan=$coa_id, coa_no_lawan='$coa_no', keterangan=$keterangan, debet=$saldo_idr, update_by=$session_id, update_date='$update_date'
																														WHERE gl_id = $id_gl");
									}else{
													$sql_ledger_kredit = $this->db->query("UPDATE public.beone_gl
																														SET coa_id=$coa_id, coa_no='$coa_no', coa_id_lawan=$chi_id, coa_no_lawan='$chi_no', keterangan=$keterangan, kredit=$saldo_idr, update_by=$session_id, update_date='$update_date'
																														WHERE gl_id = $id_gl");
									}
						}

		}else{//posisi minus (pelunasan)
						$sql = $this->db->query("UPDATE public.beone_hutang_piutang
																			SET  custsup_id=$supplier, keterangan=$keterangan, valas_trans=0, idr_trans=0, valas_pelunasan=$saldo_valas, idr_pelunasan=$saldo_idr, update_by=$session_id, update_date='$update_date'
																			WHERE hutang_piutang_id = $id");

							foreach($sql_dk->result_array() as $row){
										$id_gl = $row['gl_id'];
										if ($row['kredit'] == 0){ //posisi debit
														$sql_ledger_debet = $this->db->query("UPDATE public.beone_gl
																															SET coa_id=$coa_id, coa_no='$coa_no', coa_id_lawan=$chi_id, coa_no_lawan='$chi_no', keterangan=$keterangan, debet=$saldo_idr, update_by=$session_id, update_date='$update_date'
																															WHERE gl_id = $id_gl");
										}else{
														$sql_ledger_kredit = $this->db->query("UPDATE public.beone_gl
																															SET coa_id=$chi_id, coa_no='$chi_no', coa_id_lawan=$coa_id, coa_no_lawan='$coa_no', keterangan=$keterangan, kredit=$saldo_idr, update_by=$session_id, update_date='$update_date'
																															WHERE gl_id = $id_gl");
										}
							}
		}

		helper_log($tipe = "edit", $str = "Ubah Kredit Note, No ".$kdn_no);


		if($sql AND $sql_ledger_debet AND $sql_ledger_kredit)
			return true;
		return false;
	}

	public function delete($hp_id, $kdn_no){

		helper_log($tipe = "delete", $str = "Hapus Kredit Note, No ".$kdn_no);

		$sql_hp = $this->db->query("DELETE FROM public.beone_hutang_piutang WHERE hutang_piutang_id = $hp_id");
		$sql_gl = $this->db->query("DELETE FROM public.beone_gl WHERE gl_number = '$kdn_no'");

		if($sql_hp AND $sql_gl)
			return true;
		return false;
	}


	public function load_kredit_note(){
		$sql = $this->db->query("SELECT * FROM public.beone_hutang_piutang WHERE nomor LIKE 'KDN%' ORDER BY nomor ASC");
		return $sql->result_array();
	}

	public function get_default($hp_id){
		$sql = $this->db->query("SELECT * FROM public.beone_hutang_piutang WHERE hutang_piutang_id = ".intval($hp_id));
		if($sql->num_rows() > 0)
			return $sql->row_array();
		return false;
	}

}
?>
