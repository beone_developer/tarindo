<?php
//Model_data.php
defined('BASEPATH') OR exit('No direct script access allowed');

Class Inventin_model extends CI_Model{

	public function received($post, $import_header_id){
		$session_id = $this->session->userdata('user_id');
		$received_date = $this->db->escape($post['received_date']);
		$received_no = $this->db->escape($post['received_no']);

		$tgl_bulan = substr($received_date, 1, 2);
		$tgl_hari = substr($received_date, 4, 2);
		$tgl_tahun = substr($received_date, 7, 4);


		$tanggal = $tgl_tahun."-".$tgl_bulan."-".$tgl_hari;

		//insert table export detail
		$sql = $this->db->query("UPDATE public.beone_import_header SET status=1, receive_no=$received_no, receive_date='$tanggal' WHERE import_header_id = ".intval($import_header_id));

		$id = intval($import_header_id);


		$ctr = 0;
		$detail = $this->db->query("SELECT  h.bc_no, h.invoice_no, h.bc_date, h.receive_date, d.item_id, d.qty, d.price, h.valas_value, h.amount_value, h.supplier_id
																FROM public.beone_import_header h INNER JOIN public.beone_import_detail d ON h.import_header_id = d.import_header_id
																WHERE h.import_header_id = $id");
		//$hasil_detail = $detail->row_array();
		foreach($detail->result_array() as $row){
				$item = $row['item_id'];
				$qty = $row['qty'];
				$price = $row['price'];
				$bc = $row['bc_no'];
				$invoice = $row['invoice_no'];
				$date = $row['receive_date'];
				$valas = $row['valas_value'];
				$amount = $row['amount_value'];
				$supplier = $row['supplier_id'];
				$update_date = date('Y-m-d');
				$ctr = $ctr + 1; //counter supaya hutang hanya diinsert sekali

				//insert mutasi gudang detail
				$sql_gudang_detail = $this->db->query("INSERT INTO public.beone_gudang_detail(
																							gudang_detail_id, gudang_id, trans_date, item_id, qty_in, qty_out, nomor_transaksi, update_by, update_date, flag, keterangan)
																							VALUES (DEFAULT, 4, '$tanggal', $item, $qty, 0, '$bc', $session_id, '$update_date', 1, 'IMPORT')");

				//cek saldo awal
				$inv = $this->db->query("SELECT * FROM public.beone_inventory WHERE flag = 1 AND item_id = $item ORDER BY trans_date DESC, intvent_trans_id DESC LIMIT 1");
				$hasil_inv = $inv->row_array();
				//end cek saldo awal

				//SALDO AWAL ITEM
				$sai = $this->db->query("SELECT * FROM public.beone_item WHERE item_id =".intval($item));
				$saldo_awal_item = $sai->row_array();

				if ($hasil_inv['qty_in'] == NULL){
						if ($saldo_awal_item['saldo_idr'] == 0 OR $saldo_awal_item['saldo_qty'] == 0){
								$unit_price = $row['amount_value'] / $row['qty'];
								//$unit_price = 80000;
						}else{
								$unit_price = $saldo_awal_item['saldo_idr'] / $saldo_awal_item['saldo_qty'];
						}

					$sa_qty= $saldo_awal_item['saldo_qty'];
					$s_akhir_qty = $sa_qty+$qty;
					$s_akhir_amount = ($sa_qty+$qty)*$unit_price;

					$jml_hutang = $qty * $price;

					$sql = $this->db->query("INSERT INTO public.beone_inventory(intvent_trans_id, intvent_trans_no, item_id, trans_date, keterangan, qty_in, value_in, qty_out, value_out, sa_qty, sa_unit_price, sa_amount, flag, update_by, update_date)
					VALUES (DEFAULT, '$bc', $item, '$date', 'IMPOR', $qty, $unit_price, 0, 0, $s_akhir_qty, $unit_price, $s_akhir_amount,1, $session_id, '$update_date')");

				}else{
					$sa_qty = $hasil_inv['sa_qty'];
					$s_akhir_qty = $sa_qty+$qty;
					$s_akhir_amount = ($sa_qty+$qty)*$hasil_inv['sa_unit_price'];

					$jml_hutang = $qty * $price;

					//$unit_price = $hasil_inv['sa_unit_price'];
					$sa_amount = ($qty * $price) + $hasil_inv['sa_amount'];
					$unit_price = $sa_amount / $s_akhir_qty;

					$sql = $this->db->query("INSERT INTO public.beone_inventory(intvent_trans_id, intvent_trans_no, item_id, trans_date, keterangan, qty_in, value_in, qty_out, value_out, sa_qty, sa_unit_price, sa_amount, flag, update_by, update_date)
					VALUES (DEFAULT, '$bc', $item, '$date', 'IMPOR', $qty, $amount, 0, 0, $s_akhir_qty, $unit_price, $s_akhir_amount,1, $session_id, '$update_date')");
				}


				$sql_hp = $this->db->query("INSERT INTO public.beone_hutang_piutang(hutang_piutang_id, custsup_id, trans_date, nomor, keterangan, valas_trans, idr_trans, valas_pelunasan, idr_pelunasan, tipe_trans, update_by, update_date, flag, status_lunas)
				VALUES (DEFAULT, $supplier, '$date','$bc', 'IMPOR', $valas, $amount, 0, 0, 1, $session_id,'$update_date', 1, 0)");

				$coa_jurnal_persediaan = $this->db->query("SELECT * FROM public.beone_coa_jurnal WHERE coa_jurnal_id = 1"); //coa jurnal persediaan
				$coa_persediaan = $coa_jurnal_persediaan->row_array();
				$cj_id = $coa_persediaan['coa_id'];
				$cj_no = $coa_persediaan['coa_no'];

				$coa_jurnal_hutang_usaha_import = $this->db->query("SELECT * FROM public.beone_coa_jurnal WHERE coa_jurnal_id = 8"); //coa jurnal hutang usaha import
				$coa_hutang_usaha_import = $coa_jurnal_hutang_usaha_import->row_array();
				$chui_id = $coa_hutang_usaha_import['coa_id'];
				$chui_no = $coa_hutang_usaha_import['coa_no'];

				//insert akun ke general ledger (buku besar)
				$sql_debet = $this->db->query("INSERT INTO public.beone_gl VALUES (DEFAULT, '$date', $cj_id, '$cj_no', $chui_id, '$chui_no', '$bc', $amount ,0, '$bc', '$bc' ,$session_id, '$update_date')");

				//insert lawan akun ke general ledger (buku besar)
				$sql_kredit = $this->db->query("INSERT INTO public.beone_gl VALUES (DEFAULT, '$date', $chui_id, '$chui_no', $cj_id, '$cj_no', '$bc', 0 ,$amount, '$bc', '$bc' ,$session_id, '$update_date')");


		}

		if($sql)
			return true;
		return false;
	}


	public function deliverd($post, $export_header_id){
		$session_id = $this->session->userdata('user_id');
		$deliverd_date = $this->db->escape($post['deliverd_date']);
		$deliverd_no = $this->db->escape($post['deliverd_no']);

		$tgl_bulan = substr($deliverd_date, 1, 2);
		$tgl_hari = substr($deliverd_date, 4, 2);
		$tgl_tahun = substr($deliverd_date, 7, 4);


		$tanggal = $tgl_tahun."-".$tgl_bulan."-".$tgl_hari;

		//insert table export detail
		$sql = $this->db->query("UPDATE public.beone_export_header SET status=1, delivery_no=$deliverd_no, delivery_date='$tanggal' WHERE export_header_id = ".intval($export_header_id));

		$id = intval($export_header_id);
		$detail = $this->db->query("SELECT  h.bc_no, h.bc_date, h.delivery_date, d.item_id, d.qty, d.price, h.valas_value, h.amount_value, h.receiver_id
																FROM public.beone_export_header h INNER JOIN public.beone_export_detail d ON h.export_header_id = d.export_header_id
																WHERE h.export_header_id = $id");

		$total_piutang_eksport = 0;
		$cust = 0;
		$amount_idr = 0;
		foreach($detail->result_array() as $row){
				$item = $row['item_id'];
				$qty = $row['qty'];
				$price = $row['price'];
				$bc = $row['bc_no'];
				$date = $row['delivery_date'];
				$valas = $row['valas_value'];
				$amount = $row['amount_value'];
				$customer = $row['receiver_id'];
				$update_date = date('Y-m-d');
				$ctr = $ctr + 1; //counter supaya hutang hanya diinsert sekali
				$cust = $customer;
				$amount_idr = $amount;

				//cek saldo awal
				$inv = $this->db->query("SELECT * FROM public.beone_inventory WHERE flag = 1 AND item_id = $item ORDER BY trans_date DESC, intvent_trans_id DESC LIMIT 1");
				$hasil_inv = $inv->row_array();
				//end cek saldo awal

				//SALDO AWAL ITEM
				$sai = $this->db->query("SELECT * FROM public.beone_item WHERE item_id =".intval($item));
				$saldo_awal_item = $sai->row_array();

				if ($hasil_inv['qty_in'] == NULL){
						if ($saldo_awal_item['saldo_idr'] == 0 OR $saldo_awal_item['saldo_qty'] == 0){
								$unit_price_awal = 0;
						}else{
								$unit_price_awal = $saldo_awal_item['saldo_idr'] / $saldo_awal_item['saldo_qty'];
						}

						$sa_akhir_qty = $saldo_awal_item['saldo_qty'] - $qty;
						$sa_akhir_amount = $saldo_awal_item['saldo_idr'] - ($qty * $unit_price_awal);
						$unit_price_akhir = $sa_akhir_amount / $sa_akhir_qty;

						$sql_inventory = $this->db->query("INSERT INTO public.beone_inventory(intvent_trans_id, intvent_trans_no, item_id, trans_date, keterangan, qty_in, value_in, qty_out, value_out, sa_qty, sa_unit_price, sa_amount, flag, update_by, update_date) VALUES (DEFAULT, $deliverd_no, $item, '$tanggal', 'KETERANGAN EXPORT', 0, 0, $qty, $unit_price_awal, $sa_akhir_qty, $unit_price_akhir, $sa_akhir_amount, 1, $session_id, '$update_date')");
						$total_piutang_eksport = $total_piutang_eksport + ($qty * $unit_price_awal);
				}else{
						$sa_qty = $hasil_inv['sa_qty'];
						$sa_akhir_qty = $sa_qty-$qty;

						$sa_amount = $hasil_inv['sa_amount'];
						$sa_akhir_amount = $hasil_inv['sa_amount'] - ($qty * $hasil_inv['sa_unit_price']);

						$unit_price_awal = $hasil_inv['sa_unit_price'];
						$unit_price_akhir = $sa_akhir_amount / $sa_akhir_qty;
						$sql_inventory = $this->db->query("INSERT INTO public.beone_inventory(intvent_trans_id, intvent_trans_no, item_id, trans_date, keterangan, qty_in, value_in, qty_out, value_out, sa_qty, sa_unit_price, sa_amount, flag, update_by, update_date) VALUES (DEFAULT, $deliverd_no, $item, '$tanggal', 'KETERANGAN EXPORT', 0, 0, $qty, $unit_price_awal, $sa_akhir_qty, $unit_price_akhir, $sa_akhir_amount, 1, $session_id, '$update_date')");
						$total_piutang_eksport = $total_piutang_eksport + ($qty * $unit_price_awal);
				}
					/*$sa_qty= $saldo_awal_item['saldo_qty'];
					$s_akhir_qty = $sa_qty+$qty;
					$s_akhir_amount = ($sa_qty+$qty)*$unit_price;

					$jml_hutang = $qty * $price;*/
		}

		$sql = $this->db->query("INSERT INTO public.beone_hutang_piutang(hutang_piutang_id, custsup_id, trans_date, nomor, keterangan, valas_trans, idr_trans, valas_pelunasan, idr_pelunasan, tipe_trans, update_by, update_date, flag, status_lunas)
		VALUES (DEFAULT, $cust, '$date','$bc', 'EKSPOR', $valas, 0, 0, 0, 0, $session_id,'$update_date', 1, 0)");

		$coa_jurnal_hpp = $this->db->query("SELECT * FROM public.beone_coa_jurnal WHERE coa_jurnal_id = 4"); //coa HPP
		$coa_hpp = $coa_jurnal_hpp->row_array();
		$chpp_id = $coa_hpp['coa_id'];
		$chpp_no = $coa_hpp['coa_no'];

		$coa_jurnal_persediaan_barang_jadi = $this->db->query("SELECT * FROM public.beone_coa_jurnal WHERE coa_jurnal_id = 5"); //coa Persediaan Barang Jadi
		$coa_persediaan_barang_jadi = $coa_jurnal_persediaan_barang_jadi->row_array();
		$cpbj_id = $coa_persediaan_barang_jadi['coa_id'];
		$cpbj_no = $coa_persediaan_barang_jadi['coa_no'];


		$coa_jurnal_piutang_usaha = $this->db->query("SELECT * FROM public.beone_coa_jurnal WHERE coa_jurnal_id = 6"); //coa Piutang Usaha
		$coa_piutang_usaha = $coa_jurnal_piutang_usaha->row_array();
		$cpu_id = $coa_piutang_usaha['coa_id'];
		$cpu_no = $coa_piutang_usaha['coa_no'];

		$coa_jurnal_penjualan_ekspor = $this->db->query("SELECT * FROM public.beone_coa_jurnal WHERE coa_jurnal_id = 12"); //coa Penjualan Ekspor
		$coa_penjualan_ekspor = $coa_jurnal_penjualan_ekspor->row_array();
		$cpe_id = $coa_penjualan_ekspor['coa_id'];
		$cpe_no = $coa_penjualan_ekspor['coa_no'];


		$sql_debet1 = $this->db->query("INSERT INTO public.beone_gl VALUES (DEFAULT, '$date', $chpp_id, '$chpp_no', $cpbj_id, '$cpbj_no', '$bc', 0 ,$amount_idr, '$bc', '$bc' ,$session_id, '$update_date')");
		$sql_kredit1 = $this->db->query("INSERT INTO public.beone_gl VALUES (DEFAULT, '$date', $cpbj_id, '$cpbj_no', $chpp_id, '$chpp_no', '$bc', $amount_idr ,0, '$bc', '$bc' ,$session_id, '$update_date')");

		$sql_debet2 = $this->db->query("INSERT INTO public.beone_gl VALUES (DEFAULT, '$date', $cpu_id, '$cpu_no', $cpe_id, '$cpe_no', '$bc', 0 ,$amount_idr, '$bc', '$bc' ,$session_id, '$update_date')");
		$sql_kredit2 = $this->db->query("INSERT INTO public.beone_gl VALUES (DEFAULT, '$date', $cpe_id, '$cpe_no', $cpu_id, '$cpu_no', '$bc', $amount_idr ,0, '$bc', '$bc' ,$session_id, '$update_date')");

		if($sql)
			return true;
		return false;
	}

	public function load_inventory(){
		$sql = $this->db->query("SELECT intvent_trans_id, intvent_trans_no, item_id, trans_date, keterangan, qty_in, value_in, qty_out, value_out, sa_qty, sa_unit_price, sa_amount, flag, update_by, update_date
															FROM public.beone_inventory WHERE flag = 1 ORDER BY trans_date ASC");
		return $sql->result_array();
	}

}
?>
