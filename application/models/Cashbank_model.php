<?php
//Model_data.php
defined('BASEPATH') OR exit('No direct script access allowed');

Class Cashbank_model extends CI_Model{

	public function simpan($post){
		$session_id = $this->session->userdata('user_id');

		$tanggal_awal = $this->db->escape($post['tanggal_voucher']);
		$tgl_bulan = substr($tanggal_awal, 1, 2);
		$tgl_hari = substr($tanggal_awal, 4, 2);
		$tgl_tahun = substr($tanggal_awal, 7, 4);

		$tanggal = $tgl_tahun."-".$tgl_bulan."-".$tgl_hari;
		//Tipe 1 Debit, 0 Kredit
		$tipe = $this->db->escape($post['tipe_voucher']);
		$voucher = $this->db->escape($post['nomor_voucher']);
		$coa_cb = $this->db->escape($post['coa_id_cash_bank']);
		$keterangan_header = $this->db->escape($post['keterangan_header']);

		$update_date = date('Y-m-d');

		$sql_coa_no = $this->db->query("SELECT * FROM public.beone_coa WHERE coa_id = $coa_cb");
		$hasil_coa_no = $sql_coa_no->row_array();
		$coa_no = $hasil_coa_no['nomor'];

		//insert table voucher
		$sql = $this->db->query("INSERT INTO public.beone_voucher_header VALUES (DEFAULT, $voucher, '$tanggal', $tipe, $keterangan_header, $coa_cb, '$coa_no', $session_id, '$tanggal')");

		$header_id = $this->db->query("SELECT * FROM public.beone_voucher_header ORDER BY voucher_header_id DESC LIMIT 1");
		$hasil_header_id = $header_id->row_array();
		$hid = $hasil_header_id['voucher_header_id'];

		helper_log($tipe = "add", $str = "Voucher no : ".$voucher);

		foreach ($_POST['rows'] as $key => $count ){
							 $coa_lawan = $_POST['coa_id_lawan_'.$count];
							 $keterangan = $_POST['keterangan_voucher_'.$count];
							 $valas_ = $_POST['valas_'.$count];
							 $kurs_ = $_POST['kurs_'.$count];
							 $idr_ = $_POST['saldo_'.$count];

					 		 $valas_ex = str_replace(".", "", $valas_);
							 $valas_round = str_replace(",", ".", $valas_ex);
							 $valas = round($valas_round, 2);

							 $kurs_ex = str_replace(".", "", $kurs_);
							 $kurs_round = str_replace(",", ".", $kurs_ex);
							 $kurs = round($kurs_round, 2);

							 $idr_ex = str_replace(".", "", $idr_);
							 $idr_round = str_replace(",", ".", $idr_ex);
							 $idr = round($idr_round, 2);

							$sql_coa_lawan_no = $this->db->query("SELECT * FROM public.beone_coa WHERE coa_id = $coa_lawan");
					 		$hasil_coa_lawan_no = $sql_coa_lawan_no->row_array();
					 		$coa_no_lawan = $hasil_coa_lawan_no['nomor'];

							 $sql_detail = $this->db->query("INSERT INTO public.beone_voucher_detail(voucher_detail_id, voucher_header_id, coa_id_lawan, coa_no_lawan, jumlah_valas, kurs, jumlah_idr, keterangan_detail) VALUES (DEFAULT, $hid, $coa_lawan, '$coa_no_lawan', $valas, $kurs, $idr, '$keterangan');");
					 }


		//menambahkn nomor voucher untuk keterangan apabila journal dari voucher
		$ket_voucher = $post['keterangan_header']." (".$post['nomor_voucher'].")";

		if ($post['tipe_voucher'] == 1){
			foreach ($_POST['rows'] as $key => $count ){
								 $coa_lawan = $_POST['coa_id_lawan_'.$count];
								 $keterangan = $_POST['keterangan_voucher_'.$count];
								 $valas_ = $_POST['valas_'.$count];
								 $kurs_ = $_POST['kurs_'.$count];
								 $idr_ = $_POST['saldo_'.$count];

								 $valas_ex = str_replace(".", "", $valas_);
								 $valas_round = str_replace(",", ".", $valas_ex);
								 $valas = round($valas_round, 2);

								 $kurs_ex = str_replace(".", "", $kurs_);
								 $kurs_round = str_replace(",", ".", $kurs_ex);
								 $kurs = round($kurs_round, 2);

								 $idr_ex = str_replace(".", "", $idr_);
								 $idr_round = str_replace(",", ".", $idr_ex);
								 $idr = round($idr_round, 2);

								$sql_coa_lawan_no = $this->db->query("SELECT * FROM public.beone_coa WHERE coa_id = $coa_lawan");
						 		$hasil_coa_lawan_no = $sql_coa_lawan_no->row_array();
						 		$coa_no_lawan = $hasil_coa_lawan_no['nomor'];


								$sql_gl_1 = $this->db->query("INSERT INTO public.beone_gl VALUES (DEFAULT, '$tanggal', $coa_cb, '$coa_no', $coa_lawan, '$coa_no_lawan', '$ket_voucher', $idr ,0, $voucher, $voucher ,1, '$update_date')");
								$sql_gl_2 = $this->db->query("INSERT INTO public.beone_gl VALUES (DEFAULT, '$tanggal', $coa_lawan, '$coa_no_lawan', $coa_cb, '$coa_no', '$ket_voucher', 0 ,$idr, $voucher, $voucher ,1, '$update_date')");
						 }
		}else{
			foreach ($_POST['rows'] as $key => $count ){
								 $coa_lawan = $_POST['coa_id_lawan_'.$count];
								 $keterangan = $_POST['keterangan_voucher_'.$count];
								 $valas_ = $_POST['valas_'.$count];
								 $kurs_ = $_POST['kurs_'.$count];
								 $idr_ = $_POST['saldo_'.$count];

								 $valas_ex = str_replace(".", "", $valas_);
								 $valas_round = str_replace(",", ".", $valas_ex);
								 $valas = round($valas_round, 2);

								 $kurs_ex = str_replace(".", "", $kurs_);
								 $kurs_round = str_replace(",", ".", $kurs_ex);
								 $kurs = round($kurs_round, 2);

								 $idr_ex = str_replace(".", "", $idr_);
								 $idr_round = str_replace(",", ".", $idr_ex);
								 $idr = round($idr_round, 2);

								$sql_coa_lawan_no = $this->db->query("SELECT * FROM public.beone_coa WHERE coa_id = $coa_lawan");
						 		$hasil_coa_lawan_no = $sql_coa_lawan_no->row_array();
						 		$coa_no_lawan = $hasil_coa_lawan_no['nomor'];


								$sql_gl_1 = $this->db->query("INSERT INTO public.beone_gl VALUES (DEFAULT, '$tanggal', $coa_cb, '$coa_no', $coa_lawan, '$coa_no_lawan', '$ket_voucher', 0 ,$idr, $voucher, $voucher ,1, '$update_date')");
								$sql_gl_2 = $this->db->query("INSERT INTO public.beone_gl VALUES (DEFAULT, '$tanggal', $coa_lawan, '$coa_no_lawan', $coa_cb, '$coa_no', '$ket_voucher', $idr ,0, $voucher, $voucher ,1, '$update_date')");
						 }
		}


						foreach ($_POST['rows_pelunasan'] as $key => $count ){
								 $invoice = $_POST['hp_id_'.$count];
								 $pembayaran_ = $_POST['pembayaran_'.$count];
								 $pembayaran_valas_ = $_POST['pembayaran_valas_'.$count];

								 $pembayaran_ex = str_replace(".", "", $pembayaran_);
								 $pembayaran_idr = str_replace(",", ".", $pembayaran_ex);

								 $pembayaran_valas_ex = str_replace(".", "", $pembayaran_valas_);
								 $pembayaran_valas = str_replace(",", ".", $pembayaran_valas_ex);

								 $sa = substr($invoice, 0, 2);
								 if ($sa == "c_"){//PELUNASAN DARI SALDO AWAL CUSTOMER SUPPLIER
											 $digit_akhir = strlen($invoice);
											 $custsup_id = substr($invoice, 2, $digit_akhir);
											 $nomor_hutang_piutang = 'saldo_awal_'.$custsup_id;
											 $keterangan_hp = $post['nomor_voucher'];


											 $sql_sa_hutang_piutang = $this->db->query("SELECT * FROM public.beone_custsup WHERE custsup_id =".intval($custsup_id));
											 $hasil_sa_hutang_piutang = $sql_sa_hutang_piutang->row_array();

											 $count_pembayaran_saldo_awal = $this->db->query("SELECT COUNT(custsup_id) as jml FROM public.beone_hutang_piutang WHERE nomor = '$nomor_hutang_piutang' AND custsup_id =".intval($custsup_id));
											 $hasil_count = $count_pembayaran_saldo_awal->row_array();
											 $hasil_count_pembayaran = $hasil_count['jml'];

											 if ($hasil_count['jml'] > 0){
													 	$sql_total_pembayaran_saldo_awal = $this->db->query("SELECT SUM(valas_pelunasan) as jml_valas, SUM(idr_pelunasan) as jml_idr FROM public.beone_hutang_piutang WHERE nomor = '$nomor_hutang_piutang' AND custsup_id =".intval($custsup_id));
														$total_pembayaran_saldo_awal = $sql_total_pembayaran_saldo_awal->row_array();
														$total_pembayaran_valas = $total_pembayaran_saldo_awal['jml_valas'];
														$total_pembayaran_idr = $total_pembayaran_saldo_awal['jml_idr'];
											 }else{
												 		$total_pembayaran_valas = 0;
														$total_pembayaran_idr = 0;
											 }
											 /*$sql_total_pembayaran_saldo_awal = $this->db->query("SELECT SUM(valas_pelunasan) as jml_valas, SUM(idr_pelunasan) as jml_idr FROM public.beone_hutang_piutang WHERE nomor LIKE 'saldo_awal_%' AND custsup_id =".intval($custsup_id));
											 $total_pembayaran_saldo_awal = $sql_total_pembayaran_saldo_awal->row_array();
											 $total_pembayaran_valas = $total_pembayaran_saldo_awal['jml_valas'];
											 $total_pembayaran_idr = $total_pembayaran_saldo_awal['jml_idr'];*/


											 if ($post['tipe_voucher'] == 1){//pelunasan piutang
															 if ($hasil_sa_hutang_piutang['negara'] == 0){ //transaksi menggunakan idr sebagai pelunasan
																 		//Pelunasan berdasarkan jumlah piutang idr = jumlah pembayaran idr
																 		if ($hasil_sa_hutang_piutang['saldo_piutang_idr'] == $total_pembayaran_idr + $pembayaran_idr){ //mutasi + pembayaran baru

																			$sql_pembayaran = $this->db->query("INSERT INTO public.beone_hutang_piutang(
																															hutang_piutang_id, custsup_id, trans_date, nomor, keterangan, valas_trans, idr_trans, valas_pelunasan, idr_pelunasan, tipe_trans, update_by, update_date, flag, status_lunas)
																															VALUES (DEFAULT, $custsup_id, '$tanggal', '$nomor_hutang_piutang', '$post[nomor_voucher]', 0, 0, $pembayaran_valas, $pembayaran_idr, 0, $session_id, '$update_date', 1, 1)");

																			//note lukman
																			$sql_cek_pelunasan = $this->db->query("SELECT pelunasan_piutang_valas, pelunasan_piutang_idr FROM public.beone_custsup WHERE custsup_id = ".intval($custsup_id));
																			$hasil_cek_pelunasan = $sql_cek_pelunasan->row_array();
							 											 	$pelunasan_valas = $hasil_cek_pelunasan['pelunasan_piutang_valas'];
																			$pelunasan_idr = $hasil_cek_pelunasan['pelunasan_piutang_idr'];

																			$total_pelunasan_valas = $pelunasan_valas + $pembayaran_valas;
																			$total_pelunasan_idr = $pelunasan_idr + $pembayaran_idr;

																			$sql_update_saldo_awal_lunas = $this->db->query("UPDATE public.beone_custsup
																																												SET status_lunas_piutang=1, pelunasan_piutang_valas = $total_pelunasan_valas, pelunasan_piutang_idr = $total_pelunasan_idr
																																												WHERE custsup_id = ".intval($custsup_id));
																		}else{

																				$sql_pembayaran = $this->db->query("INSERT INTO public.beone_hutang_piutang(
																																hutang_piutang_id, custsup_id, trans_date, nomor, keterangan, valas_trans, idr_trans, valas_pelunasan, idr_pelunasan, tipe_trans, update_by, update_date, flag, status_lunas)
																																VALUES (DEFAULT, $custsup_id, '$tanggal', '$nomor_hutang_piutang', '$post[nomor_voucher]', 0, 0, $pembayaran_valas, $pembayaran_idr, 0, $session_id, '$update_date', 1, 1)");

																				//note lukman
																				$sql_cek_pelunasan = $this->db->query("SELECT pelunasan_piutang_valas, pelunasan_piutang_idr FROM public.beone_custsup WHERE custsup_id = ".intval($custsup_id));
																				$hasil_cek_pelunasan = $sql_cek_pelunasan->row_array();
								 											 	$pelunasan_valas = $hasil_cek_pelunasan['pelunasan_piutang_valas'];
																				$pelunasan_idr = $hasil_cek_pelunasan['pelunasan_piutang_idr'];

																				$total_pelunasan_valas = $pelunasan_valas + $pembayaran_valas;
																				$total_pelunasan_idr = $pelunasan_idr + $pembayaran_idr;

																				$sql_update_saldo_awal_lunas = $this->db->query("UPDATE public.beone_custsup
																																													SET pelunasan_piutang_valas = $total_pelunasan_valas, pelunasan_piutang_idr = $total_pelunasan_idr
																																													WHERE custsup_id = ".intval($custsup_id));
																		}
															 }else{
																 		//Pelunasan menggunakan usd atau valas sebagai pelunasan
																	 if ($hasil_sa_hutang_piutang['saldo_piutang_valas'] == $total_pembayaran_valas + $pembayaran_valas){

																			 $sql_pembayaran = $this->db->query("INSERT INTO public.beone_hutang_piutang(
			 																													hutang_piutang_id, custsup_id, trans_date, nomor, keterangan, valas_trans, idr_trans, valas_pelunasan, idr_pelunasan, tipe_trans, update_by, update_date, flag, status_lunas)
			 																													VALUES (DEFAULT, $custsup_id, '$tanggal', '$nomor_hutang_piutang', '$post[nomor_voucher]', 0, 0, $pembayaran_valas, $pembayaran_idr, 0, $session_id, '$update_date', 1, 1)");

																				//note lukman
																				$sql_cek_pelunasan = $this->db->query("SELECT pelunasan_piutang_valas, pelunasan_piutang_idr FROM public.beone_custsup WHERE custsup_id = ".intval($custsup_id));
																				$hasil_cek_pelunasan = $sql_cek_pelunasan->row_array();
								 											 	$pelunasan_valas = $hasil_cek_pelunasan['pelunasan_piutang_valas'];
																				$pelunasan_idr = $hasil_cek_pelunasan['pelunasan_piutang_idr'];

																				$total_pelunasan_valas = $pelunasan_valas + $pembayaran_valas;
																				$total_pelunasan_idr = $pelunasan_idr + $pembayaran_idr;

																				$sql_update_saldo_awal_lunas = $this->db->query("UPDATE public.beone_custsup
																																													SET status_lunas_piutang=1, pelunasan_piutang_valas = $total_pelunasan_valas, pelunasan_piutang_idr = $total_pelunasan_idr
																																													WHERE custsup_id = ".intval($custsup_id));
																	 }else{

																			 $sql_pembayaran = $this->db->query("INSERT INTO public.beone_hutang_piutang(
	   																													hutang_piutang_id, custsup_id, trans_date, nomor, keterangan, valas_trans, idr_trans, valas_pelunasan, idr_pelunasan, tipe_trans, update_by, update_date, flag, status_lunas)
	   																													VALUES (DEFAULT, $custsup_id, '$tanggal', '$nomor_hutang_piutang', '$post[nomor_voucher]', 0, 0, $pembayaran_valas, $pembayaran_idr, 0, $session_id, '$update_date', 1, 1)");

																				//note lukman
																				$sql_cek_pelunasan = $this->db->query("SELECT pelunasan_piutang_valas, pelunasan_piutang_idr FROM public.beone_custsup WHERE custsup_id = ".intval($custsup_id));
																				$hasil_cek_pelunasan = $sql_cek_pelunasan->row_array();
								 											 	$pelunasan_valas = $hasil_cek_pelunasan['pelunasan_piutang_valas'];
																				$pelunasan_idr = $hasil_cek_pelunasan['pelunasan_piutang_idr'];

																				$total_pelunasan_valas = $pelunasan_valas + $pembayaran_valas;
																				$total_pelunasan_idr = $pelunasan_idr + $pembayaran_idr;

																				$sql_update_saldo_awal_lunas = $this->db->query("UPDATE public.beone_custsup
																																													SET pelunasan_piutang_valas = $total_pelunasan_valas, pelunasan_piutang_idr = $total_pelunasan_idr
																																													WHERE custsup_id = ".intval($custsup_id));
																	 }
															 }

											 }else{//pelunasan hutang
													 if ($hasil_sa_hutang_piutang['negara'] == 0){ //tidak ada valas hanya menggunakan idr
																//Pelunasan berdasarkan jumlah hutang idr
																if ($hasil_sa_hutang_piutang['saldo_hutang_idr'] == $total_pembayaran_idr + $pembayaran_idr){

																	$sql_pembayaran = $this->db->query("INSERT INTO public.beone_hutang_piutang(
																													hutang_piutang_id, custsup_id, trans_date, nomor, keterangan, valas_trans, idr_trans, valas_pelunasan, idr_pelunasan, tipe_trans, update_by, update_date, flag, status_lunas)
																													VALUES (DEFAULT, $custsup_id, '$tanggal', '$nomor_hutang_piutang', '$post[nomor_voucher]', 0, 0, $pembayaran_valas, $pembayaran_idr, 1, $session_id, '$update_date', 1, 1)");

																	//note lukman
																	$sql_cek_pelunasan = $this->db->query("SELECT pelunasan_hutang_valas, pelunasan_hutang_idr FROM public.beone_custsup  WHERE custsup_id = ".intval($custsup_id));
																	$hasil_cek_pelunasan = $sql_cek_pelunasan->row_array();
																	$pelunasan_valas = $hasil_cek_pelunasan['pelunasan_hutang_valas'];
																	$pelunasan_idr = $hasil_cek_pelunasan['pelunasan_hutang_idr'];

																	$total_pelunasan_valas = $pelunasan_valas + $pembayaran_valas;
																	$total_pelunasan_idr = $pelunasan_idr + $pembayaran_idr;

																	$sql_update_saldo_awal_lunas = $this->db->query("UPDATE public.beone_custsup
																																										SET status_lunas_hutang=1, pelunasan_hutang_valas = $total_pelunasan_valas, pelunasan_hutang_idr = $total_pelunasan_idr
																																										WHERE custsup_id = ".intval($custsup_id));
																}else{

																	$sql_pembayaran = $this->db->query("INSERT INTO public.beone_hutang_piutang(
																													hutang_piutang_id, custsup_id, trans_date, nomor, keterangan, valas_trans, idr_trans, valas_pelunasan, idr_pelunasan, tipe_trans, update_by, update_date, flag, status_lunas)
																													VALUES (DEFAULT, $custsup_id, '$tanggal', '$nomor_hutang_piutang', '$post[nomor_voucher]', 0, 0, $pembayaran_valas, $pembayaran_idr, 1, $session_id, '$update_date', 1, 1)");

																	//note lukman
																	$sql_cek_pelunasan = $this->db->query("SELECT pelunasan_hutang_valas, pelunasan_hutang_idr FROM public.beone_custsup WHERE custsup_id = ".intval($custsup_id));
																	$hasil_cek_pelunasan = $sql_cek_pelunasan->row_array();
																	$pelunasan_valas = $hasil_cek_pelunasan['pelunasan_hutang_valas'];
																	$pelunasan_idr = $hasil_cek_pelunasan['pelunasan_hutang_idr'];

																	$total_pelunasan_valas = $pelunasan_valas + $pembayaran_valas;
																	$total_pelunasan_idr = $pelunasan_idr + $pembayaran_idr;

																	$sql_update_saldo_awal_lunas = $this->db->query("UPDATE public.beone_custsup
																																										SET pelunasan_hutang_valas = $total_pelunasan_valas, pelunasan_hutang_idr = $total_pelunasan_idr
																																										WHERE custsup_id = ".intval($custsup_id));
																}
													 }else{
																//Pelunasan berdasarkan jumlah hutang valas = jumlah pembayaran valas
															 if ($hasil_sa_hutang_piutang['saldo_hutang_valas'] == $total_pembayaran_valas + $pembayaran_valas){

																 $sql_pembayaran = $this->db->query("INSERT INTO public.beone_hutang_piutang(
																													hutang_piutang_id, custsup_id, trans_date, nomor, keterangan, valas_trans, idr_trans, valas_pelunasan, idr_pelunasan, tipe_trans, update_by, update_date, flag, status_lunas)
																													VALUES (DEFAULT, $custsup_id, '$tanggal', '$nomor_hutang_piutang', '$post[nomor_voucher]', 0, 0, $pembayaran_valas, $pembayaran_idr, 1, $session_id, '$update_date', 1, 1)");

																	//note lukman
																	$sql_cek_pelunasan = $this->db->query("SELECT pelunasan_hutang_valas, pelunasan_hutang_idr FROM public.beone_custsup WHERE custsup_id = ".intval($custsup_id));
																	$hasil_cek_pelunasan = $sql_cek_pelunasan->row_array();
																	$pelunasan_valas = $hasil_cek_pelunasan['pelunasan_hutang_valas'];
																	$pelunasan_idr = $hasil_cek_pelunasan['pelunasan_hutang_idr'];

																	$total_pelunasan_valas = $pelunasan_valas + $pembayaran_valas;
																	$total_pelunasan_idr = $pelunasan_idr + $pembayaran_idr;

																	$sql_update_saldo_awal_lunas = $this->db->query("UPDATE public.beone_custsup
																																										SET status_lunas_hutang=1, pelunasan_hutang_valas = $total_pelunasan_valas, pelunasan_hutang_idr = $total_pelunasan_idr
																																										WHERE custsup_id = ".intval($custsup_id));

															 }else{
																	 $sql_pembayaran = $this->db->query("INSERT INTO public.beone_hutang_piutang(
 																													hutang_piutang_id, custsup_id, trans_date, nomor, keterangan, valas_trans, idr_trans, valas_pelunasan, idr_pelunasan, tipe_trans, update_by, update_date, flag, status_lunas)
 																													VALUES (DEFAULT, $custsup_id, '$tanggal', '$nomor_hutang_piutang', '$post[nomor_voucher]', 0, 0, $pembayaran_valas, $pembayaran_idr, 1, $session_id, '$update_date', 1, 1)");

																		//note lukman
																		$sql_cek_pelunasan = $this->db->query("SELECT pelunasan_hutang_valas, pelunasan_hutang_idr FROM public.beone_custsup  WHERE custsup_id = ".intval($custsup_id));
																		$hasil_cek_pelunasan = $sql_cek_pelunasan->row_array();
																		$pelunasan_valas = $hasil_cek_pelunasan['pelunasan_hutang_valas'];
																		$pelunasan_idr = $hasil_cek_pelunasan['pelunasan_hutang_idr'];

																		$total_pelunasan_valas = $pelunasan_valas + $pembayaran_valas;
																		$total_pelunasan_idr = $pelunasan_idr + $pembayaran_idr;

																		$sql_update_saldo_awal_lunas = $this->db->query("UPDATE public.beone_custsup
																																											SET pelunasan_hutang_valas = $total_pelunasan_valas, pelunasan_hutang_idr = $total_pelunasan_idr
																																											WHERE custsup_id = ".intval($custsup_id));
															 }
													 }
											 }

								 }else{//PELUNASAN DARI INVOICE
									 			$sql_hutang_piutang = $this->db->query("SELECT * FROM public.beone_hutang_piutang WHERE hutang_piutang_id =".intval($invoice));
												$hasil_hutang_piutang = $sql_hutang_piutang->row_array();
												$no_invoice = $hasil_hutang_piutang['nomor'];
												$custsup_id = $hasil_hutang_piutang['custsup_id'];
												$nomor_hutang_piutang = $keterangan.' ('.$post['nomor_voucher'].')';

												 $count_pembayaran_invoice = $this->db->query("SELECT COUNT(custsup_id) as jml FROM public.beone_hutang_piutang WHERE nomor LIKE '$no_invoice%'");
	 											 $hasil_count_invoice = $count_pembayaran_invoice->row_array();
	 											 $hasil_count_invoice_pembayaran = $hasil_count_invoice['jml'];

	 											 if ($hasil_count_invoice_pembayaran > 0){
	 													 	$sql_total_pembayaran_invoice = $this->db->query("SELECT SUM(valas_pelunasan) as jml_valas, SUM(idr_pelunasan) as jml_idr FROM public.beone_hutang_piutang WHERE nomor LIKE '$no_invoice%'");
	 														$total_pembayaran_invoice = $sql_total_pembayaran_invoice->row_array();
	 														$total_pembayaran_valas = $total_pembayaran_invoice['jml_valas'];
	 														$total_pembayaran_idr = $total_pembayaran_invoice['jml_idr'];
	 											 }else{
	 												 		$total_pembayaran_valas = 0;
	 														$total_pembayaran_idr = 0;
	 											 }


												if ($post['tipe_voucher'] == 1){//pelunasan piutang

																	if ($hasil_hutang_piutang['valas_trans'] == 0){//transaksi hanya menggunakan idr maka pelunasan mengacu pada jumlah bayar = idr

																						if ($hasil_hutang_piutang['idr_trans'] <= $total_pembayaran_idr + $pembayaran_idr){//piutang idr = pembayaran piutang idr
																										$sql_update_invoice = $this->db->query("UPDATE public.beone_hutang_piutang
																																																			SET status_lunas=1
																																																			WHERE nomor = '$no_invoice'");

																											$sql_pembayaran = $this->db->query("INSERT INTO public.beone_hutang_piutang(
																																							hutang_piutang_id, custsup_id, trans_date, nomor, keterangan, valas_trans, idr_trans, valas_pelunasan, idr_pelunasan, tipe_trans, update_by, update_date, flag, status_lunas)
																																							VALUES (DEFAULT, $custsup_id, '$tanggal', '$no_invoice', '$post[nomor_voucher]', 0, 0, $pembayaran_valas, $pembayaran_idr, 0, $session_id, '$update_date', 1, 1)");

																						}else{
																											$sql_pembayaran = $this->db->query("INSERT INTO public.beone_hutang_piutang(
																																							hutang_piutang_id, custsup_id, trans_date, nomor, keterangan, valas_trans, idr_trans, valas_pelunasan, idr_pelunasan, tipe_trans, update_by, update_date, flag, status_lunas)
																																							VALUES (DEFAULT, $custsup_id, '$tanggal', '$no_invoice', '$post[nomor_voucher]', 0, 0, $pembayaran_valas, $pembayaran_idr, 0, $session_id, '$update_date', 1, 1)");
																						}

																	}else{//transaksi utama menggunakan valas
																						if ($hasil_hutang_piutang['valas_trans'] <= $total_pembayaran_valas + $pembayaran_valas){//piutang valas = pembayaran piutang valas
																											$sql_update_invoice = $this->db->query("UPDATE public.beone_hutang_piutang
																																																				SET status_lunas=1
																																																				WHERE nomor = '$no_invoice'");

																												$sql_pembayaran = $this->db->query("INSERT INTO public.beone_hutang_piutang(
																																								hutang_piutang_id, custsup_id, trans_date, nomor, keterangan, valas_trans, idr_trans, valas_pelunasan, idr_pelunasan, tipe_trans, update_by, update_date, flag, status_lunas)
																																								VALUES (DEFAULT, $custsup_id, '$tanggal', '$no_invoice', '$post[nomor_voucher]', 0, 0, $pembayaran_valas, $pembayaran_idr, 0, $session_id, '$update_date', 1, 1)");
																						}else{
																											$sql_pembayaran = $this->db->query("INSERT INTO public.beone_hutang_piutang(
																																							hutang_piutang_id, custsup_id, trans_date, nomor, keterangan, valas_trans, idr_trans, valas_pelunasan, idr_pelunasan, tipe_trans, update_by, update_date, flag, status_lunas)
																																							VALUES (DEFAULT, $custsup_id, '$tanggal', '$no_invoice', '$post[nomor_voucher]', 0, 0, $pembayaran_valas, $pembayaran_idr, 0, $session_id, '$update_date', 1, 1)");
																						}
																	}

												}else{//pelunasan hutang


																	if ($hasil_hutang_piutang['valas_trans'] == 0){//transaksi hanya menggunakan idr maka pelunasan mengacu pada jumlah bayar = idr
																							if ($hasil_hutang_piutang['idr_trans'] <= $total_pembayaran_idr + $pembayaran_idr){//hutang idr = pembayaran hutang idr
																											$sql_update_invoice = $this->db->query("UPDATE public.beone_hutang_piutang
																																																				SET status_lunas=1
																																																				WHERE nomor = '$no_invoice'");

																												$sql_pembayaran = $this->db->query("INSERT INTO public.beone_hutang_piutang(
																																								hutang_piutang_id, custsup_id, trans_date, nomor, keterangan, valas_trans, idr_trans, valas_pelunasan, idr_pelunasan, tipe_trans, update_by, update_date, flag, status_lunas)
																																								VALUES (DEFAULT, $custsup_id, '$tanggal', '$no_invoice', '$post[nomor_voucher]', 0, 0, $pembayaran_valas, $pembayaran_idr, 1, $session_id, '$update_date', 1, 1)");

																							}else{
																												$sql_pembayaran = $this->db->query("INSERT INTO public.beone_hutang_piutang(
																																								hutang_piutang_id, custsup_id, trans_date, nomor, keterangan, valas_trans, idr_trans, valas_pelunasan, idr_pelunasan, tipe_trans, update_by, update_date, flag, status_lunas)
																																								VALUES (DEFAULT, $custsup_id, '$tanggal', '$no_invoice', '$post[nomor_voucher]', 0, 0, $pembayaran_valas, $pembayaran_idr, 1, $session_id, '$update_date', 1, 1)");
																							}
																	}else{//transaksi utama menggunakan valas
																							if ($hasil_hutang_piutang['valas_trans'] <= $total_pembayaran_valas + $pembayaran_valas){//hutang valas = pembayaran hutang valas
																												$sql_update_invoice = $this->db->query("UPDATE public.beone_hutang_piutang
																																																					SET status_lunas=1
																																																					WHERE nomor = '$no_invoice'");

																													$sql_pembayaran = $this->db->query("INSERT INTO public.beone_hutang_piutang(
																																									hutang_piutang_id, custsup_id, trans_date, nomor, keterangan, valas_trans, idr_trans, valas_pelunasan, idr_pelunasan, tipe_trans, update_by, update_date, flag, status_lunas)
																																									VALUES (DEFAULT, $custsup_id, '$tanggal', '$no_invoice', '$post[nomor_voucher]', 0, 0, $pembayaran_valas, $pembayaran_idr, 1, $session_id, '$update_date', 1, 1)");
																							}else{
																												$sql_pembayaran = $this->db->query("INSERT INTO public.beone_hutang_piutang(
																																								hutang_piutang_id, custsup_id, trans_date, nomor, keterangan, valas_trans, idr_trans, valas_pelunasan, idr_pelunasan, tipe_trans, update_by, update_date, flag, status_lunas)
																																								VALUES (DEFAULT, $custsup_id, '$tanggal', '$no_invoice', '$post[nomor_voucher]', 0, 0, $pembayaran_valas, $pembayaran_idr, 1, $session_id, '$update_date', 1, 1)");
																							}
																	}

												}
								 }
					 }

		if($sql AND $sql_detail AND $sql_pembayaran AND $sql_gl_1 AND $sql_gl_2)
			return true;
		return false;
	}

	public function update($post, $voucher_id){
		$session_id = $this->session->userdata('user_id');
		$update_date = date('Y-m-d');
		$sql_detail_del = $this->db->query("DELETE FROM public.beone_voucher_detail WHERE voucher_header_id = $voucher_id");
		$sql_gl_del = $this->db->query("DELETE FROM public.beone_gl WHERE gl_number = '$post[no_voucher]'");

		foreach ($_POST['rows'] as $key => $count ){
							 $detail_id = $_POST['detail_id_'.$count];
							 $coa_lawan = $_POST['coa_id_lawan_'.$count];
							 $keterangan = $_POST['keterangan_voucher_'.$count];
							 $valas_ = $_POST['valas_'.$count];
							 $kurs_ = $_POST['kurs_'.$count];
							 $idr_ = $_POST['saldo_'.$count];

					 		 $valas_ex = str_replace(".", "", $valas_);
							 $valas_round = str_replace(",", ".", $valas_ex);
							 $valas = round($valas_round, 2);

							 $kurs_ex = str_replace(".", "", $kurs_);
							 $kurs_round = str_replace(",", ".", $kurs_ex);
							 $kurs = round($kurs_round, 2);

							 $idr_ex = str_replace(".", "", $idr_);
							 $idr_round = str_replace(",", ".", $idr_ex);
							 $idr = round($idr_round, 2);

							$sql_coa_lawan_no = $this->db->query("SELECT * FROM public.beone_coa WHERE coa_id = $coa_lawan");
					 		$hasil_coa_lawan_no = $sql_coa_lawan_no->row_array();
					 		$coa_no_lawan = $hasil_coa_lawan_no['nomor'];

							$sql_header = $this->db->query("UPDATE public.beone_voucher_header SET update_by=$session_id, update_date='$update_date' WHERE voucher_header_id = $voucher_id");
							$sql_detail = $this->db->query("INSERT INTO public.beone_voucher_detail(voucher_detail_id, voucher_header_id, coa_id_lawan, coa_no_lawan, jumlah_valas, kurs, jumlah_idr, keterangan_detail) VALUES (DEFAULT, $voucher_id, $coa_lawan, '$coa_no_lawan', $valas, $kurs, $idr, '$keterangan');");

							if ($post['tipe_voucher'] == 1){
									$sql_gl_1 = $this->db->query("INSERT INTO public.beone_gl VALUES (DEFAULT, '$post[tgl_voucher]', $post[coa_id_voucher], '$post[coa_no_voucher]', $coa_lawan, '$coa_no_lawan', '$post[keterangan_voucher]', $idr ,0, '$post[no_voucher]', '$post[no_voucher]' , $session_id, '$update_date')");
									$sql_gl_2 = $this->db->query("INSERT INTO public.beone_gl VALUES (DEFAULT, '$post[tgl_voucher]', $coa_lawan, '$coa_no_lawan', $post[coa_id_voucher], '$post[coa_no_voucher]', '$post[keterangan_voucher]', 0 ,$idr, '$post[no_voucher]', '$post[no_voucher]' , $session_id, '$update_date')");
							}else{
								$sql_gl_1 = $this->db->query("INSERT INTO public.beone_gl VALUES (DEFAULT, '$post[tgl_voucher]', $post[coa_id_voucher], '$post[coa_no_voucher]', $coa_lawan, '$coa_no_lawan', '$post[keterangan_voucher]', 0, $idr, '$post[no_voucher]', '$post[no_voucher]' , $session_id, '$update_date')");
								$sql_gl_2 = $this->db->query("INSERT INTO public.beone_gl VALUES (DEFAULT, '$post[tgl_voucher]', $coa_lawan, '$coa_no_lawan', $post[coa_id_voucher], '$post[coa_no_voucher]', '$post[keterangan_voucher]', $idr, 0, '$post[no_voucher]', '$post[no_voucher]' , $session_id, '$update_date')");
							}


					 }


		return true;

	}


	public function simpan_kode_cashbank($post){
		$nama = $this->db->escape($post['nama_item']);
		$kode = $this->db->escape($post['kode']);
		$akun = $this->db->escape($post['akun']);
		$inout = $this->db->escape($post['inout']);

		//insert table voucher
		$sql = $this->db->query("INSERT INTO public.beone_kode_trans(kode_trans_id, nama, coa_id, kode_bank, in_out, flag) VALUES (DEFAULT, $nama, $akun, $kode, $inout, 1)");

		if($sql)
			return true;
		return false;
	}

	public function update_kode_cashbank($post, $kode_trans_id){
		$nama = $this->db->escape($post['nama_item']);
		$kode = $this->db->escape($post['kode']);
		$akun = $this->db->escape($post['akun']);
		$inout = $this->db->escape($post['inout']);

		$sql = $this->db->query("UPDATE public.beone_kode_trans	SET nama=$nama, coa_id=$akun, kode_bank=$kode, in_out=$inout WHERE kode_trans_id = ".intval($kode_trans_id));

		if($sql)
			return true;
		return false;
	}

	public function delete_kode_cashbank($kode_trans_id){
		$sql = $this->db->query("DELETE FROM public.beone_kode_trans WHERE kode_trans_id = ".intval($kode_trans_id));
	}

	public function get_default_header($voucher_id){
		$sql = $this->db->query("SELECT * FROM public.beone_voucher_header WHERE voucher_header_id = ".intval($voucher_id));
		if($sql->num_rows() > 0)
			return $sql->row_array();
		return false;
	}

	public function get_default_detail($voucher_id){
		$sql = $this->db->query("SELECT * FROM public.beone_voucher_detail WHERE voucher_header_id = ".intval($voucher_id));
		return $sql->result_array();
	}

	public function load_voucher_print($voucher_id){
		$sql = $this->db->query("SELECT h.voucher_header_id, h.voucher_number, h.voucher_date, h.tipe, h.keterangan, h.coa_id_cash_bank, h.coa_no, h.update_by, h.update_date, d.coa_no_lawan, d.keterangan_detail, d.jumlah_valas, d.jumlah_idr
															FROM public.beone_voucher_header h INNER JOIN public.beone_voucher_detail d ON h.voucher_header_id = d.voucher_header_id WHERE h.voucher_header_id = $voucher_id");
		return $sql->result_array();
	}

	public function load_voucher(){
		$sql = $this->db->query("SELECT h.voucher_header_id, h.voucher_number, h.voucher_date, h.tipe, h.keterangan, h.coa_id_cash_bank, h.coa_no, h.update_by, h.update_date, SUM(d.jumlah_valas) as jvalas, SUM(d.jumlah_idr) as jidr
															FROM public.beone_voucher_header h INNER JOIN public.beone_voucher_detail d ON h.voucher_header_id = d.voucher_header_id GROUP BY h.voucher_header_id ORDER BY h.voucher_date ASC");
		return $sql->result_array();
	}

	public function delete($voucher_id, $voucher_number){

		$vnumber = str_replace("-", "/", $voucher_number);

		$sql_hp_custsup = $this->db->query("SELECT * FROM public.beone_hutang_piutang WHERE keterangan = '$vnumber'");
		$hasil_hp_custsup = $sql_hp_custsup->row_array();
		$nomor_hp = $hasil_hp_custsup['nomor'];
		$custsup_id = $hasil_hp_custsup['custsup_id'];
		$tp_trans = $hasil_hp_custsup['tipe_trans'];
		$pelunasan_valas_delete = $hasil_hp_custsup['valas_pelunasan'];
		$pelunasan_idr_delete = $hasil_hp_custsup['idr_pelunasan'];

		//update pelunasan pada tabel custsup
		if ($nomor_hp == "saldo_awal_".$custsup_id){
				$sql_custsup = $this->db->query("SELECT * FROM public.beone_custsup WHERE custsup_id = ".intval($custsup_id));
				$hasil_custsup = $sql_custsup->row_array();

				if ($tp_trans == 1){//pelunasan hutang
							$pelunasan_hutang_idr_sebelumnya = $hasil_custsup['pelunasan_hutang_idr'];
							$pelunasan_hutang_valas_sebelumnya = $hasil_custsup['pelunasan_hutang_valas'];

							$pelunasan_hutang_idr_update = $pelunasan_hutang_idr_sebelumnya - $pelunasan_idr_delete;
							$pelunasan_hutang_valas_update = $pelunasan_hutang_valas_sebelumnya - $pelunasan_valas_delete;

							$sql_update_custsup = $this->db->query("UPDATE public.beone_custsup
																											SET pelunasan_hutang_idr=$pelunasan_hutang_idr_update, pelunasan_hutang_valas=$pelunasan_hutang_valas_update WHERE custsup_id = ".intval($custsup_id));

				}else{//pelunasan piutang
							$pelunasan_piutang_idr_sebelumnya = $hasil_custsup['pelunasan_piutang_idr'];
							$pelunasan_piutang_valas_sebelumnya = $hasil_custsup['pelunasan_piutang_valas'];

							$pelunasan_piutang_idr_update = $pelunasan_piutang_idr_sebelumnya - $pelunasan_idr_delete;
							$pelunasan_piutang_valas_update = $pelunasan_piutang_valas_sebelumnya - $pelunasan_valas_delete;

							$sql_update_custsup = $this->db->query("UPDATE public.beone_custsup
																											SET pelunasan_piutang_idr=$pelunasan_piutang_idr_update, pelunasan_piutang_valas=$pelunasan_piutang_valas_update WHERE custsup_id = ".intval($custsup_id));
				}
		}

		$sql = $this->db->query("DELETE FROM public.beone_voucher_header WHERE voucher_header_id = ".intval($voucher_id));
		$sql_detail = $this->db->query("DELETE FROM public.beone_voucher_detail WHERE voucher_header_id = ".intval($voucher_id));

		$sql_gl = $this->db->query("DELETE FROM public.beone_gl WHERE pasangan_no = '$vnumber'");
		$sql_hp = $this->db->query("DELETE FROM public.beone_hutang_piutang WHERE keterangan = '$vnumber'");


		/*$count_pelunasan = $this->db->query("SELECT COUNT(custsup_id) as jml, custsup_id FROM public.beone_hutang_piutang WHERE idr_pelunasan = 0 AND valas_pelunasan = 0 AND nomor LIKE '%$voucher_number' GROUP BY custsup_id");
		$hasil_count = $count_pelunasan->row_array();

		if ($hasil_count['jml'] > 0){//hutang piutang berasal dari transaksi
					$sql_update_lunas = $this->db->query("UPDATE public.beone_hutang_piutang SET status_lunas = 0 WHERE idr_pelunasan = 0 AND valas_pelunasan = 0 AND nomor LIKE '%$voucher_number'");
		}else{//hutang piutang berasal dari saldo awal
					$sql_update_lunas = $this->db->query("UPDATE public.beone_custsup SET status_lunas = 0 WHERE custsup_id = ".intval($hasil_count['custsup_id']));
		}
		$sql_pelunasan = $this->db->query("DELETE FROM public.beone_hutang_piutang WHERE idr_trans = 0 AND valas_trans = 0 AND nomor LIKE '%$voucher_number'");
		*/
	}


	public function load_kode_cashbank(){
		$sql = $this->db->query("SELECT k.kode_trans_id, k.nama, k.coa_id, k.kode_bank, k.in_out, k.flag, c.nama as ncoa FROM public.beone_kode_trans k INNER JOIN public.beone_coa c ON k.coa_id = c.coa_id WHERE k.flag=1");
		return $sql->result_array();
	}

	public function get_default_kode_cashbank($kode_trans_id){
		$sql = $this->db->query("SELECT k.kode_trans_id, k.nama, k.coa_id, k.kode_bank, k.in_out, k.flag, c.nama as ncoa FROM public.beone_kode_trans k INNER JOIN public.beone_coa c ON k.coa_id = c.coa_id WHERE k.kode_trans_id = ".intval($kode_trans_id));
		if($sql->num_rows() > 0)
			return $sql->row_array();
		return false;
	}

	public function load_hutang(){
		$sql = $this->db->query("SELECT h.hutang_piutang_id, h.custsup_id, c.nama as ncustsup, h.trans_date, h.nomor, h.keterangan, h.valas_trans, h.idr_trans, h.valas_pelunasan, h.idr_pelunasan, h.tipe_trans, h.update_by, h.update_date, h.flag
															FROM public.beone_hutang_piutang h INNER JOIN public.beone_custsup c ON h.custsup_id = c.custsup_id WHERE h.flag = 1 AND h.status_lunas = 0 AND h.tipe_trans = 1");
		return $sql->result_array();
	}

	public function load_saldo_awal_hutang(){
		$sql = $this->db->query("SELECT custsup_id, nama, alamat, tipe_custsup, negara, saldo_hutang_idr as saldo_idr, saldo_hutang_valas as saldo_valas, saldo_piutang_idr, saldo_piutang_valas, flag, saldo_hutang_valas - pelunasan_hutang_valas as sisavalas, saldo_hutang_idr - pelunasan_hutang_idr as sisaidr
															FROM public.beone_custsup WHERE saldo_hutang_idr > 0 OR saldo_hutang_valas > 0 AND flag = 1 AND status_lunas_hutang = 0");
		return $sql->result_array();
	}

	public function load_piutang(){
		$sql = $this->db->query("SELECT h.hutang_piutang_id, h.custsup_id, c.nama as ncustsup, h.trans_date, h.nomor, h.keterangan, h.valas_trans, h.idr_trans, h.valas_pelunasan, h.idr_pelunasan, h.idr_trans - h.idr_pelunasan as sisa, h.valas_pelunasan, h.idr_pelunasan, h.tipe_trans, h.update_by, h.update_date, h.flag
															FROM public.beone_hutang_piutang h INNER JOIN public.beone_custsup c ON h.custsup_id = c.custsup_id WHERE h.flag = 1 AND h.status_lunas = 0 AND h.tipe_trans = 0");
		return $sql->result_array();
	}

	public function load_saldo_awal_piutang(){
		$sql = $this->db->query("SELECT custsup_id, nama, alamat, tipe_custsup, negara, saldo_hutang_idr, saldo_hutang_valas, saldo_piutang_idr as saldo_idr, saldo_piutang_valas as saldo_valas, flag, saldo_piutang_valas - pelunasan_piutang_valas as sisavalas, saldo_piutang_idr - pelunasan_piutang_idr as sisaidr, status_lunas_piutang
															FROM public.beone_custsup WHERE saldo_piutang_idr > 0 AND saldo_piutang_valas > 0 AND flag = 1 AND status_lunas_piutang = 0");
		return $sql->result_array();
	}

}
?>
