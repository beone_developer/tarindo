<?php
//Model_data.php
defined('BASEPATH') OR exit('No direct script access allowed');

Class Gudang_model extends CI_Model{

	public function simpan($post){
		$nama = $this->db->escape($post['nama_gudang']);
		$keterangan = $this->db->escape($post['keterangan']);

		$sql = $this->db->query("INSERT INTO public.beone_gudang(gudang_id, nama, keterangan, flag) VALUES (DEFAULT, $nama, $keterangan, 1)");
		helper_log($tipe = "add", $str = "Tambah Gudang ".$post['nama_gudang']);

		if($sql)
			return true;
		return false;
	}

	public function update($post, $gudang_id){
		$nama = $this->db->escape($post['nama_gudang']);
		$keterangan = $this->db->escape($post['keterangan']);

		$sql = $this->db->query("UPDATE public.beone_gudang SET nama=$nama, keterangan=$keterangan WHERE gudang_id = ".intval($gudang_id));
		helper_log($tipe = "edit", $str = "Ubah Gudang ".$post['nama_gudang']);

		if($sql)
			return true;
		return false;
	}

	public function delete($gudang_id){
		$sql_gudang = $this->db->query("SELECT nama FROM public.beone_gudang WHERE gudang_id = ".intval($gudang_id));
		$hasil_gudang = $sql_gudang->row_array();
		helper_log($tipe = "delete", $str = "Hapus Gudang ".$hasil_gudang['nama']);

		$sql = $this->db->query("UPDATE public.beone_gudang SET flag=0 WHERE gudang_id =".intval($gudang_id));
	}

	public function pindah_gudang_delete($kode_tracing){
		$sql_gudang = $this->db->query("DELETE FROM public.beone_gudang_detail WHERE kode_tracing = '$kode_tracing'");
	}

	public function get_default($gudang_id){
		$sql = $this->db->query("SELECT gudang_id, nama, keterangan, flag	FROM public.beone_gudang WHERE flag = 1 AND gudang_id = ".intval($gudang_id));
		if($sql->num_rows() > 0)
			return $sql->row_array();
		return false;
	}

	public function load_gudang(){
		$sql = $this->db->query("SELECT * FROM public.beone_gudang WHERE flag = 1 ORDER BY gudang_id ASC");
		return $sql->result_array();
	}

	public function simpan_pindah_gudang($post){
		$session_id = $this->session->userdata('user_id');
		//$no_doc = $this->db->escape($post['nodoc']);
		$gudang_asal = $this->db->escape($post['gudang_asal']);
		$gudang_tujuan = $this->db->escape($post['gudang_tujuan']);
		$item = $this->db->escape($post['item']);
		$keterangan = $this->db->escape($post['keterangan']);
		$update_date = date('Y-m-d');

		$qty_= str_replace(".", "", $post['qty']);
		$qty = str_replace(",", ".", $qty_);

		$tanggal_awal = $this->db->escape($post['tanggal']);
		$tgl_bulan = substr($tanggal_awal, 1, 2);
		$tgl_hari = substr($tanggal_awal, 4, 2);
		$tgl_tahun = substr($tanggal_awal, 7, 4);

		$tanggal = $tgl_tahun."-".$tgl_bulan."-".$tgl_hari;

		$doc_ = explode(":", $post['nodoc']);
		$doc = $doc_[1];

		$kode_tracing = date('dmYhis');

		$sql_out = $this->db->query("INSERT INTO public.beone_gudang_detail(
																gudang_detail_id, gudang_id, trans_date, item_id, qty_in, qty_out, nomor_transaksi, update_by, update_date, flag, keterangan, kode_tracing)
																VALUES (DEFAULT, $gudang_asal, '$tanggal', $item, 0, $qty, '$doc', $session_id, '$update_date', 1, $keterangan, '$kode_tracing')");

		$sql_in = $this->db->query("INSERT INTO public.beone_gudang_detail(
																gudang_detail_id, gudang_id, trans_date, item_id, qty_in, qty_out, nomor_transaksi, update_by, update_date, flag, keterangan, kode_tracing)
																VALUES (DEFAULT, $gudang_tujuan, '$tanggal', $item, $qty, 0, '$doc', $session_id, '$update_date', 1, $keterangan, '$kode_tracing')");

		helper_log($tipe = "add", $str = "Pindah Gudang ".$keterangan);

	if($sql_out AND $sql_in)
			return true;
		return false;
	}

	public function load_pindah_gudang(){
		$sql = $this->db->query("SELECT * FROM public.beone_gudang_detail WHERE flag = 1 AND gudang_id = 4 AND keterangan != 'IMPORT' ORDER BY trans_date DESC");
		return $sql->result_array();
	}

	public function load_pindah_gudang_penerimaan(){
		//$sql = $this->db->query("SELECT * FROM public.beone_gudang_detail WHERE flag = 1 AND gudang_id = 4 AND keterangan = 'IMPORT' ORDER BY trans_date DESC");
		$sql = $this->db->query("SELECT * FROM public.beone_received_import ORDER BY tanggal_received DESC");
		return $sql->result_array();
	}

	public function load_print_pindah_gudang($pindah_id){
		$sql = $this->db->query("SELECT d.gudang_detail_id, d.gudang_id, d.trans_date, d.item_id, i.nama as nitem, i.item_code, d.qty_in, d.qty_out, d.nomor_transaksi, d.update_by, d.update_date, d.flag, d.keterangan
															FROM public.beone_gudang_detail d INNER JOIN public.beone_item i ON d.item_id = i.item_id
															WHERE d.flag = 1 AND d.gudang_detail_id =".intval($pindah_id));
		return $sql->result_array();
	}

	public function load_print_pindah_gudang_penerimaan($no_aju){
		$sql = $this->db->query("SELECT d.gudang_detail_id, d.gudang_id, d.trans_date, d.item_id, i.nama as nitem, i.item_code, d.qty_in, d.qty_out, d.nomor_transaksi, d.update_by, d.update_date, d.flag, d.keterangan
															FROM public.beone_gudang_detail d INNER JOIN public.beone_item i ON d.item_id = i.item_id
															WHERE d.flag = 1 AND d.nomor_transaksi = '$no_aju' AND d.keterangan = 'IMPORT'");
		return $sql->result_array();
	}
}
?>
