<?php
//Model_data.php
defined('BASEPATH') OR exit('No direct script access allowed');

Class Recal_model extends CI_Model{

	public function recal_inventory($post){
		$tgl = $this->db->escape($post['tanggal_awal']);
		$tgl_akhir = $this->db->escape($post['tanggal_akhir']);

		$tgl_bulan = substr($tgl, 1, 2);
		$tgl_hari = substr($tgl, 4, 2);
		$tgl_tahun = substr($tgl, 7, 4);

		$tgl_akhir_bulan = substr($tgl_akhir, 1, 2);
		$tgl_akhir_hari = substr($tgl_akhir, 4, 2);
		$tgl_akhir_tahun = substr($tgl_akhir, 7, 4);

		$tanggal_awal = $tgl_akhir_tahun."-01-01";
		$tanggal = $tgl_tahun."-".$tgl_bulan."-".$tgl_hari;
		$tanggal_akhir = $tgl_akhir_tahun."-".$tgl_akhir_bulan."-".$tgl_akhir_hari;

		$sql_item_inventory = $this->db->query("SELECT distinct(item_id) FROM public.beone_inventory WHERE flag = 1");

		foreach($sql_item_inventory->result_array() as $row){
						$sql_list_item = $this->db->query("SELECT * FROM public.beone_inventory WHERE item_id = ".intval($row['item_id'])." AND flag = 1 AND trans_date BETWEEN '$tanggal' AND '$tanggal_akhir' ORDER BY trans_date ASC, intvent_trans_id ASC");

						//SALDO AWAL ITEM
						$sai = $this->db->query("SELECT * FROM public.beone_item WHERE item_id =".intval($row['item_id']));
						$saldo_awal_item = $sai->row_array();

						//SALDO MUTASI ITEM DARI AWAL TAHUN SAMPAI SEKARANG
						$inv = $this->db->query("SELECT qty_in, value_in, qty_out, value_out, sa_qty, sa_unit_price, sa_amount FROM public.beone_inventory WHERE trans_date BETWEEN '$tanggal_awal' AND '$tanggal' AND item_id =".intval($row['item_id'])." ORDER BY trans_date DESC, intvent_trans_id DESC LIMIT 1");
						$hasil_inv = $inv->row_array();

						//cek saldo awal
						$cek = $this->db->query("SELECT count(item_id) as jml FROM public.beone_inventory WHERE flag = 1 AND item_id = ".intval($row['item_id'])); //tambah filteran range tanggal sampai tgl produksi (edit lukman)
						$cek_sa = $cek->row_array();


						$urutan_transaksi = 0;
						$saldo_awal_qty = 0;
						$saldo_awal_unit_price = 0;
						$saldo_awal_amount = 0;

						foreach($sql_list_item->result_array() as $row2){//mutasi per item

								$kode_transaksi = substr($row2['intvent_trans_no'], 0, 2);

								if ($kode_transaksi == "BO"){
//******************************************************************************************************
													$sql_produksi = $this->db->query("SELECT *
																														FROM public.beone_transfer_stock
																														WHERE transfer_no = '$row2[intvent_trans_no]'");
													$hasil_produksi = $sql_produksi->row_array();

													$sql_produksi_detail = $this->db->query("SELECT *
																														FROM public.beone_transfer_stock_detail
																														WHERE tipe_transfer_stock = 'BB' AND item_id = $row2[item_id] AND transfer_stock_header_id = ".intval($hasil_produksi['transfer_stock_id']));

													$amount_bahan_produksi = 0;
													foreach($sql_produksi_detail->result_array() as $row_produksi_detail){
														$harga_satuan_bahan_produksi = $this->db->query("SELECT item_id, trans_date, sa_unit_price
																															FROM public.beone_inventory
																															WHERE item_id = '$row_produksi_detail[item_id]' ORDER BY trans_date DESC LIMIT 1");
														$hasil_harga_satuan_bahan_produksi = $harga_satuan_bahan_produksi->row_array();

														$amount_bahan_produksi = $amount_bahan_produksi + ($row_produksi_detail['qty'] * $hasil_harga_satuan_bahan_produksi['sa_unit_price']);
													}

													$urutan_transaksi = $urutan_transaksi + 1;

													if ($urutan_transaksi == 1){//kalau transaksi pertama tidak memiliki saldo awal item maka menggunakan perhitungan transaksinya
															if ($cek_sa['jml'] == 0){ //jika tidak ada mutasi maka ambil dari saldo awal item
																	$sa_qty = $saldo_awal_item['saldo_qty'];
																	$sa_amount = $saldo_awal_item['saldo_idr'];



																	if ($row2['qty_in'] == 0){//transaksi keluar (BOM OUT)
																			$unit_price_awal = $row2['value_out'];
																	}else{//transaksi masuk (BOM IN)
																			//$unit_price_awal = $row2['value_in'];
																			if ($hasil_produksi['coa_kode_biaya'] == 1){//produksi wip
																				$total_biaya_produksi = $this->db->query("SELECT SUM(debet) - SUM(kredit) as hasil
																																					FROM public.beone_gl
																																					WHERE gl_date BETWEEN '$tanggal' AND '$tanggal_akhir' AND coa_id BETWEEN 64 AND 67 OR coa_id BETWEEN 69 AND 77");
																				$hasil_total_biaya_produksi = $total_biaya_produksi->row_array();
																			}else{//produksi barang jadi
																				$total_biaya_produksi = $this->db->query("SELECT SUM(debet) - SUM(kredit) as hasil
																																					FROM public.beone_gl
																																					WHERE gl_date BETWEEN '$tanggal' AND '$tanggal_akhir' AND coa_id BETWEEN 64 AND 77");
																				$hasil_total_biaya_produksi = $total_biaya_produksi->row_array();
																			}

																			$total_qty_produksi = $this->db->query("SELECT SUM(qty_in) as hasil
																																						FROM public.beone_inventory i INNER JOIN public.beone_item t ON i.item_id = t.item_id
																																						WHERE i.trans_date BETWEEN '$tanggal' AND '$tanggal_akhir' AND i.intvent_trans_no LIKE 'BO%' AND i.qty_in <> 0 AND t.item_type_id BETWEEN 2 AND 3");
																			$hasil_total_qty_produksi = $total_qty_produksi->row_array();

																			//kurang amount_bahan_produksi kali prosentase
																			$unit_price_awal = $amount_bahan_produksi + ($hasil_total_biaya_produksi['hasil']/$hasil_total_qty_produksi['hasil']);
																	}

//BELUM CUSTOM---------------------------------------------------------------
																	$sa_qty_akhir = ($saldo_awal_item['saldo_qty'] + $row2['qty_in']) - $row2['qty_out'];

																	if ($row2['qty_in'] == 0){//saldo akhir keluar
																			$sa_amount_akhir = $saldo_awal_item['saldo_idr'] - ($row2['qty_out'] * $row2['value_out']);
																			$unit_price_akhir = ($saldo_awal_item['saldo_idr'] - ($row2['qty_out'] * $row2['value_out'])) / ($saldo_awal_item['saldo_qty'] + $row2['qty_in']) - $row2['qty_out'];
																	}else{//saldo akhir masuk
																			$sa_amount_akhir = $saldo_awal_item['saldo_idr'] + ($row2['qty_in'] * $unit_price_awal);
																			$unit_price_akhir = ($saldo_awal_item['saldo_idr'] + ($row2['qty_in'] * $row2['value_in'])) / ($saldo_awal_item['saldo_qty'] + $row2['qty_in']) - $row2['qty_out'];
																	}

																	$unit_price_akhir = $sa_amount_akhir / $sa_qty_akhir;


																	if ($row2['qty_in'] == 0){//saldo akhir keluar
																	$sql_produksi = $this->db->query("UPDATE public.beone_inventory
																																		SET value_out = $unit_price_awal, sa_qty=$sa_qty_akhir, sa_unit_price=$unit_price_akhir, sa_amount=$sa_amount_akhir
																																		WHERE intvent_trans_id = $row2[intvent_trans_id]");
																	}else{//saldo akhir masuk
																		$sql_produksi = $this->db->query("UPDATE public.beone_inventory
																																			SET value_in = $unit_price_awal, sa_qty=$sa_qty_akhir, sa_unit_price=$unit_price_akhir, sa_amount=$sa_amount_akhir
																																			WHERE intvent_trans_id = $row2[intvent_trans_id]");
																	}

															}
													}else{//urutan lebih dari 1, jika urutan sudah lebih dari 1 maka ambil saldo dari transaksi sebelumnya


														$sa_qty_akhir = ($saldo_awal_qty + $row2['qty_in']) - $row2['qty_out'];

															if ($hasil_produksi['coa_kode_biaya'] == 1){//produksi wip
																$total_biaya_produksi = $this->db->query("SELECT SUM(debet) - SUM(kredit) as hasil
																																	FROM public.beone_gl
																																	WHERE gl_date BETWEEN '$tanggal' AND '$tanggal_akhir' AND coa_id BETWEEN 64 AND 67 OR coa_id BETWEEN 69 AND 77");
																$hasil_total_biaya_produksi = $total_biaya_produksi->row_array();
															}else{//produksi barang jadi
																$total_biaya_produksi = $this->db->query("SELECT SUM(debet) - SUM(kredit) as hasil
																																	FROM public.beone_gl
																																	WHERE gl_date BETWEEN '$tanggal' AND '$tanggal_akhir' AND coa_id BETWEEN 64 AND 77");
																$hasil_total_biaya_produksi = $total_biaya_produksi->row_array();
															}

															$total_qty_produksi = $this->db->query("SELECT SUM(qty_in) as hasil
																																		FROM public.beone_inventory i INNER JOIN public.beone_item t ON i.item_id = t.item_id
																																		WHERE i.trans_date BETWEEN '$tanggal' AND '$tanggal_akhir' AND i.intvent_trans_no LIKE 'BO%' AND i.qty_in <> 0 AND t.item_type_id BETWEEN 2 AND 3");
															$hasil_total_qty_produksi = $total_qty_produksi->row_array();

															//kurang amount_bahan_produksi kali prosentase
															$unit_price_awal = $amount_bahan_produksi + ($hasil_total_biaya_produksi['hasil'] / $hasil_total_qty_produksi['hasil']);


																$sql_produksi = $this->db->query("UPDATE public.beone_inventory
																																	SET sa_qty=$sa_qty_akhir, sa_unit_price=$unit_price_awal, sa_amount=$sa_amount_akhir
																																	WHERE intvent_trans_id = $row2[intvent_trans_id]");
													}
//******************************************************************************************************
								}

								$sak = $this->db->query("SELECT intvent_trans_id, sa_qty, sa_unit_price, sa_amount FROM public.beone_inventory WHERE intvent_trans_id =".intval($row2['intvent_trans_id']));
								$hasil_sak = $sak->row_array();

								$saldo_awal_qty = $hasil_sak['sa_qty'];
								$saldo_awal_unit_price = $hasil_sak['sa_unit_price'];
								$saldo_awal_amount = $hasil_sak['sa_amount'];
						}

				}
}
}
?>
