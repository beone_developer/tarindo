<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 
 */
class Bc41_detail_penggunaan_bb_impor_controller extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		if($this->session->userdata('status') != "login"){
			redirect(base_url("Login_controller"));
		}
		 else{
                    $this->load->model('bc25_detail_impor_model');
                }
	}

	public function form($ID_HEADER, $ID)
	{
		$data['judul'] = 'Bahan Baku Impor';
		$data['tipe'] = "New";
		$this->load->view('Header', $data);
		$data['idheader'] = $ID_HEADER;
		$data['default'] = $this->bc25_detail_impor_model->get_default_bahan($ID_HEADER, $ID);
		$data['default_tarifcukai'] = $this->bc25_detail_impor_model->get_default_bahan_tarifcukai($ID_HEADER, $ID);
		$data['default_tarifBM'] = $this->bc25_detail_impor_model->get_default_bahan_tarifBM($ID_HEADER, $ID);
		$data['default_tarifPPNBM'] = $this->bc25_detail_impor_model->get_default_bahan_tarifPPNBM($ID_HEADER, $ID);
		$data['default_tarifPPN'] = $this->bc25_detail_impor_model->get_default_bahan_tarifPPN($ID_HEADER, $ID);
		$data['default_tarifPPH'] = $this->bc25_detail_impor_model->get_default_bahan_tarifPPH($ID_HEADER, $ID);

		if(isset($_POST['submit_bbimpor'])){
			$this->bc25_detail_impor_model->update41($_POST);
			redirect("Bc41_detail_penggunaan_bb_impor_controller/form/$ID_HEADER/$ID");
		}

		$this->load->view('bc41_detail_penggunaan_bb_impor_view', $data);
		$this->load->view('Footer'); 
	}

	public function next($ID_HEADER, $ID, $ID_BARANG)
	{
		$data['judul'] = 'Detail Barang';
		$data['tipe'] = "New";
		$this->load->view('Header', $data);

		$data['default'] = $this->bc25_detail_impor_model->get_default_bahannext($ID_HEADER, $ID, $ID_BARANG);
		$data['default_tarifcukai'] = $this->bc25_detail_impor_model->get_default_bahan_tarifcukainext($ID_HEADER, $ID, $ID_BARANG);
		$data['default_tarifBM'] = $this->bc25_detail_impor_model->get_default_bahan_tarifBMnext($ID_HEADER, $ID, $ID_BARANG);
		$data['default_tarifPPNBM'] = $this->bc25_detail_impor_model->get_default_bahan_tarifPPNBMnext($ID_HEADER, $ID, $ID_BARANG);
		$data['default_tarifPPN'] = $this->bc25_detail_impor_model->get_default_bahan_tarifPPNnext($ID_HEADER, $ID, $ID_BARANG);
		$data['default_tarifPPH'] = $this->bc25_detail_impor_model->get_default_bahan_tarifPPHnext($ID_HEADER, $ID, $ID_BARANG);

		if(isset($_POST['submit_bbimpor'])){
			$this->bc25_detail_impor_model->update41($_POST);
			redirect("Bc41_detail_penggunaan_bb_impor_controller/next/$ID_HEADER/$ID/$ID_BARANG");
		}

		$this->load->view('bc41_detail_penggunaan_bb_impor_view', $data);
		$this->load->view('Footer'); 
	}
	public function prev($ID_HEADER, $ID, $ID_BARANG)
	{
		$data['judul'] = 'Detail Barang';
		$data['tipe'] = "New";
		$this->load->view('Header', $data);

		$data['default'] = $this->bc25_detail_impor_model->get_default_bahanprev($ID_HEADER, $ID, $ID_BARANG);
		$data['default_tarifcukai'] = $this->bc25_detail_impor_model->get_default_bahan_tarifcukaiprev($ID_HEADER, $ID, $ID_BARANG);
		$data['default_tarifBM'] = $this->bc25_detail_impor_model->get_default_bahan_tarifBMprev($ID_HEADER, $ID, $ID_BARANG);
		$data['default_tarifPPNBM'] = $this->bc25_detail_impor_model->get_default_bahan_tarifPPNBMprev($ID_HEADER, $ID, $ID_BARANG);
		$data['default_tarifPPN'] = $this->bc25_detail_impor_model->get_default_bahan_tarifPPNprev($ID_HEADER, $ID, $ID_BARANG);
		$data['default_tarifPPH'] = $this->bc25_detail_impor_model->get_default_bahan_tarifPPHprev($ID_HEADER, $ID, $ID_BARANG);
		if(isset($_POST['submit_bbimpor'])){
			$this->bc25_detail_impor_model->update41($_POST);
			redirect("Bc41_detail_penggunaan_bb_impor_controller/prev/$ID_HEADER/$ID/$ID_BARANG");
		}

		$this->load->view('bc41_detail_penggunaan_bb_impor_view', $data);
		$this->load->view('Footer'); 
	}

}


 ?>