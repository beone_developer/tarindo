<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Beranda_controller extends CI_Controller {

	public function index()
	{
		$this->load->model('Beranda_model');
		$this->load->view('Header');
		//$this->load->view('Dashboard');
		$this->load->view('Footer');
	}
}
