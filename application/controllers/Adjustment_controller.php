<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Adjustment_controller extends CI_Controller {

	function __construct(){
		parent::__construct();
		if($this->session->userdata('status') != "login"){
			redirect(base_url("Login_controller"));
		}
	}

	public function index()
	{
		$this->load->model('Adjustment_model');
		$this->load->model('Gudang_model');
		$this->load->view('Header');

		$data['list_item'] = $this->Adjustment_model->load_item_dari_stockopname();
		$data['list_gudang'] = $this->Gudang_model->load_gudang();

		if(isset($_POST['submit_adjustment'])){
			$this->Adjustment_model->simpan($_POST);
			redirect("Adjustment_controller");
		}

		$this->load->view('Adjustment_form_view', $data);
		$this->load->view('Footer');
	}

	public function List_adjustment()
	{
		$this->load->model('Adjustment_model');
		$this->load->view('Header');

		$data['List_adjustment'] = $this->Adjustment_model->load_adjustment();

		$this->load->view('Adjustment_view', $data);
		$this->load->view('Footer');
	}

	public function get_nomor_adjustment(){
		$this->load->view('get_nomor_adjustment');
	}

	public function delete($adjustment_id, $adjustment_no){
		$this->load->model("Adjustment_model");
		$this->Adjustment_model->delete($adjustment_id, $adjustment_no);
		redirect("Adjustment_controller/List_adjustment");
	}


}
