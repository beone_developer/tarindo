<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Gudang_controller extends CI_Controller {

	function __construct(){
		parent::__construct();
		if($this->session->userdata('status') != "login"){
			redirect(base_url("Login_controller"));
		}
	}

	public function index()
	{
		$this->load->model('Gudang_model');
		$this->load->view('Header');

		$data['list_gudang'] = $this->Gudang_model->load_gudang();
		$data['tipe'] = "Tambah";

		if(isset($_POST['submit_gudang'])){
			$this->Gudang_model->simpan($_POST);
			redirect("Gudang_controller");
		}

		$this->load->view('Gudang_view',$data);
		$this->load->view('Footer');
	}

	public function Edit($gudang_id)
	{
		$this->load->model('Gudang_model');
		$this->load->view('Header');

		$data['list_gudang'] = $this->Gudang_model->load_gudang();
		$data['default'] = $this->Gudang_model->get_default($gudang_id);
		$data['tipe'] = "Ubah";

		if(isset($_POST['submit_gudang'])){
			$this->Gudang_model->update($_POST, $gudang_id);
			redirect("Gudang_controller");
		}

		$this->load->view('Gudang_view',$data);
		$this->load->view('Footer');
	}

	public function delete($gudang_id){
		$this->load->model("Gudang_model");
		$this->Gudang_model->delete($gudang_id);
		redirect("Gudang_controller");
	}

}
