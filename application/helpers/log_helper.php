<?php
function helper_log($tipe = "", $str = ""){
    $CI =& get_instance();

    if (strtolower($tipe) == "login"){
        $log_tipe   = 0;
    }
    elseif(strtolower($tipe) == "logout")
    {
        $log_tipe   = 1;
    }
    elseif(strtolower($tipe) == "add"){
        $log_tipe   = 2;
    }
    elseif(strtolower($tipe) == "edit"){
        $log_tipe  = 3;
    }
    elseif(strtolower($tipe) == "delete"){
        $log_tipe  = 4;
    }
    elseif(strtolower($tipe) == "import"){
        $log_tipe  = 5;
    }
    elseif(strtolower($tipe) == "export"){
        $log_tipe  = 6;
    }

    date_default_timezone_set("Asia/Bangkok");
    $log_waktu = date('Y-m-d H:i:s');

    // paramter
    $param['log_user']      				= $CI->session->userdata('username');
    $param['log_tipe']      				= $log_tipe;
    $param['log_desc']      				= $str;
    $param['log_time']       	      = $log_waktu;

    //load model log
    $CI->load->model('Log_model');

    //save to database
    $CI->Log_model->save_log($param);

}
?>
