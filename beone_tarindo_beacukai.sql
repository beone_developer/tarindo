PGDMP     !    ;            
    w            beone_tarindo    10.6    10.6 &   �           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                       false            �           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                       false            �           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                       false            �           1262    173017    beone_tarindo    DATABASE     �   CREATE DATABASE beone_tarindo WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'English_Indonesia.1252' LC_CTYPE = 'English_Indonesia.1252';
    DROP DATABASE beone_tarindo;
             postgres    false                        2615    2200    public    SCHEMA        CREATE SCHEMA public;
    DROP SCHEMA public;
             postgres    false            �           0    0    SCHEMA public    COMMENT     6   COMMENT ON SCHEMA public IS 'standard public schema';
                  postgres    false    3                        3079    12924    plpgsql 	   EXTENSION     ?   CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;
    DROP EXTENSION plpgsql;
                  false            �           0    0    EXTENSION plpgsql    COMMENT     @   COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';
                       false    1            �            1259    173018    beone_adjustment    TABLE     =  CREATE TABLE public.beone_adjustment (
    adjustment_id bigint NOT NULL,
    adjustment_no character varying(20),
    adjustment_date date,
    keterangan character varying(100),
    item_id integer,
    qty_adjustment bigint,
    posisi_qty integer,
    update_by integer,
    update_date date,
    flag integer
);
 $   DROP TABLE public.beone_adjustment;
       public         postgres    false    3            �            1259    173021 "   beone_adjustment_adjustment_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_adjustment_adjustment_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 9   DROP SEQUENCE public.beone_adjustment_adjustment_id_seq;
       public       postgres    false    196    3            �           0    0 "   beone_adjustment_adjustment_id_seq    SEQUENCE OWNED BY     i   ALTER SEQUENCE public.beone_adjustment_adjustment_id_seq OWNED BY public.beone_adjustment.adjustment_id;
            public       postgres    false    197            �            1259    173023 	   beone_coa    TABLE     S  CREATE TABLE public.beone_coa (
    coa_id integer NOT NULL,
    nama character varying(50),
    nomor character varying(50),
    tipe_akun integer,
    debet_valas double precision,
    debet_idr double precision,
    kredit_valas double precision,
    kredit_idr double precision,
    dk character varying,
    tipe_transaksi integer
);
    DROP TABLE public.beone_coa;
       public         postgres    false    3            �            1259    173029    beone_coa_coa_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_coa_coa_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 +   DROP SEQUENCE public.beone_coa_coa_id_seq;
       public       postgres    false    198    3            �           0    0    beone_coa_coa_id_seq    SEQUENCE OWNED BY     M   ALTER SEQUENCE public.beone_coa_coa_id_seq OWNED BY public.beone_coa.coa_id;
            public       postgres    false    199            �            1259    173031    beone_coa_jurnal    TABLE     �   CREATE TABLE public.beone_coa_jurnal (
    coa_jurnal_id integer NOT NULL,
    nama_coa_jurnal character varying(100),
    keterangan_coa_jurnal character varying(1000),
    coa_id integer,
    coa_no character varying(50)
);
 $   DROP TABLE public.beone_coa_jurnal;
       public         postgres    false    3            �            1259    173037 "   beone_coa_jurnal_coa_jurnal_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_coa_jurnal_coa_jurnal_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 9   DROP SEQUENCE public.beone_coa_jurnal_coa_jurnal_id_seq;
       public       postgres    false    3    200            �           0    0 "   beone_coa_jurnal_coa_jurnal_id_seq    SEQUENCE OWNED BY     i   ALTER SEQUENCE public.beone_coa_jurnal_coa_jurnal_id_seq OWNED BY public.beone_coa_jurnal.coa_jurnal_id;
            public       postgres    false    201            �            1259    173039    beone_country    TABLE     �   CREATE TABLE public.beone_country (
    country_id integer NOT NULL,
    nama character varying(50),
    country_code character varying(10),
    flag integer
);
 !   DROP TABLE public.beone_country;
       public         postgres    false    3            �            1259    173042    beone_country_country_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_country_country_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 3   DROP SEQUENCE public.beone_country_country_id_seq;
       public       postgres    false    3    202            �           0    0    beone_country_country_id_seq    SEQUENCE OWNED BY     ]   ALTER SEQUENCE public.beone_country_country_id_seq OWNED BY public.beone_country.country_id;
            public       postgres    false    203            �            1259    173044    beone_custsup    TABLE     b  CREATE TABLE public.beone_custsup (
    custsup_id integer NOT NULL,
    nama character varying(100),
    alamat character varying(200),
    tipe_custsup integer,
    negara integer,
    saldo_hutang_idr double precision,
    saldo_hutang_valas double precision,
    saldo_piutang_idr double precision,
    saldo_piutang_valas double precision,
    flag integer,
    pelunasan_hutang_idr double precision,
    pelunasan_hutang_valas double precision,
    pelunasan_piutang_idr double precision,
    pelunasan_piutang_valas double precision,
    status_lunas_piutang integer,
    status_lunas_hutang integer
);
 !   DROP TABLE public.beone_custsup;
       public         postgres    false    3            �            1259    173047    beone_custsup_custsup_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_custsup_custsup_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 3   DROP SEQUENCE public.beone_custsup_custsup_id_seq;
       public       postgres    false    204    3            �           0    0    beone_custsup_custsup_id_seq    SEQUENCE OWNED BY     ]   ALTER SEQUENCE public.beone_custsup_custsup_id_seq OWNED BY public.beone_custsup.custsup_id;
            public       postgres    false    205            �            1259    173049    beone_export_detail    TABLE     �  CREATE TABLE public.beone_export_detail (
    export_detail_id integer NOT NULL,
    export_header_id integer,
    item_id integer,
    qty double precision,
    satuan_qty integer,
    price double precision,
    pack_qty double precision,
    satuan_pack integer,
    origin_country integer,
    doc character varying(50),
    volume double precision,
    netto double precision,
    brutto double precision,
    flag integer
);
 '   DROP TABLE public.beone_export_detail;
       public         postgres    false    3            �            1259    173052 (   beone_export_detail_export_detail_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_export_detail_export_detail_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ?   DROP SEQUENCE public.beone_export_detail_export_detail_id_seq;
       public       postgres    false    206    3            �           0    0 (   beone_export_detail_export_detail_id_seq    SEQUENCE OWNED BY     u   ALTER SEQUENCE public.beone_export_detail_export_detail_id_seq OWNED BY public.beone_export_detail.export_detail_id;
            public       postgres    false    207            �            1259    173054    beone_export_header    TABLE       CREATE TABLE public.beone_export_header (
    export_header_id integer NOT NULL,
    jenis_bc integer,
    car_no character varying(50),
    bc_no character varying(50),
    bc_date date,
    kontrak_no character varying(50),
    kontrak_date date,
    jenis_export integer,
    invoice_no character varying(50),
    invoice_date date,
    surat_jalan_no character varying(50),
    surat_jalan_date date,
    receiver_id integer,
    country_id integer,
    price_type character varying(10),
    amount_value double precision,
    valas_value double precision,
    insurance_type character varying(10),
    insurance_value double precision,
    freight double precision,
    flag integer,
    status integer,
    delivery_date date,
    update_by integer,
    update_date date,
    delivery_no character varying(50),
    vessel character varying(100),
    port_loading character varying(100),
    port_destination character varying(100),
    container character varying(10),
    no_container character varying(25),
    no_seal character varying(25)
);
 '   DROP TABLE public.beone_export_header;
       public         postgres    false    3            �            1259    173057 (   beone_export_header_export_header_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_export_header_export_header_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ?   DROP SEQUENCE public.beone_export_header_export_header_id_seq;
       public       postgres    false    3    208            �           0    0 (   beone_export_header_export_header_id_seq    SEQUENCE OWNED BY     u   ALTER SEQUENCE public.beone_export_header_export_header_id_seq OWNED BY public.beone_export_header.export_header_id;
            public       postgres    false    209                       1259    214301    beone_fix_asset    TABLE     �   CREATE TABLE public.beone_fix_asset (
    fix_asset_id integer NOT NULL,
    nama_asset character varying(100),
    tanggal_perolehan date,
    harga_perolehan double precision
);
 #   DROP TABLE public.beone_fix_asset;
       public         postgres    false    3                       1259    214309    beone_fix_asset_akm    TABLE     �  CREATE TABLE public.beone_fix_asset_akm (
    akm_id integer NOT NULL,
    periode_id integer,
    fix_asset_id integer,
    akm_penyusutan double precision,
    jan double precision,
    feb double precision,
    mar double precision,
    apr double precision,
    mei double precision,
    jun double precision,
    jul double precision,
    ags double precision,
    sep double precision,
    okt double precision,
    nov double precision,
    des double precision,
    nilai_buku double precision
);
 '   DROP TABLE public.beone_fix_asset_akm;
       public         postgres    false    3                       1259    214307    beone_fix_asset_akm_akm_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_fix_asset_akm_akm_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 5   DROP SEQUENCE public.beone_fix_asset_akm_akm_id_seq;
       public       postgres    false    276    3            �           0    0    beone_fix_asset_akm_akm_id_seq    SEQUENCE OWNED BY     a   ALTER SEQUENCE public.beone_fix_asset_akm_akm_id_seq OWNED BY public.beone_fix_asset_akm.akm_id;
            public       postgres    false    275                       1259    214299     beone_fix_asset_fix_asset_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_fix_asset_fix_asset_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 7   DROP SEQUENCE public.beone_fix_asset_fix_asset_id_seq;
       public       postgres    false    274    3            �           0    0     beone_fix_asset_fix_asset_id_seq    SEQUENCE OWNED BY     e   ALTER SEQUENCE public.beone_fix_asset_fix_asset_id_seq OWNED BY public.beone_fix_asset.fix_asset_id;
            public       postgres    false    273                       1259    214317    beone_fix_asset_periode    TABLE     z   CREATE TABLE public.beone_fix_asset_periode (
    periode_id integer NOT NULL,
    nama_periode character varying(100)
);
 +   DROP TABLE public.beone_fix_asset_periode;
       public         postgres    false    3                       1259    214315 &   beone_fix_asset_periode_periode_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_fix_asset_periode_periode_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 =   DROP SEQUENCE public.beone_fix_asset_periode_periode_id_seq;
       public       postgres    false    278    3            �           0    0 &   beone_fix_asset_periode_periode_id_seq    SEQUENCE OWNED BY     q   ALTER SEQUENCE public.beone_fix_asset_periode_periode_id_seq OWNED BY public.beone_fix_asset_periode.periode_id;
            public       postgres    false    277            �            1259    173059    beone_gl    TABLE     �  CREATE TABLE public.beone_gl (
    gl_id bigint NOT NULL,
    gl_date date,
    coa_id integer,
    coa_no character varying(20),
    coa_id_lawan integer,
    coa_no_lawan character varying(20),
    keterangan character varying(100),
    debet double precision,
    kredit double precision,
    pasangan_no character varying(50),
    gl_number character varying(50),
    update_by integer,
    update_date date
);
    DROP TABLE public.beone_gl;
       public         postgres    false    3            �            1259    173062    beone_gl_gl_id_seq    SEQUENCE     {   CREATE SEQUENCE public.beone_gl_gl_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 )   DROP SEQUENCE public.beone_gl_gl_id_seq;
       public       postgres    false    3    210            �           0    0    beone_gl_gl_id_seq    SEQUENCE OWNED BY     I   ALTER SEQUENCE public.beone_gl_gl_id_seq OWNED BY public.beone_gl.gl_id;
            public       postgres    false    211            �            1259    173064    beone_gudang    TABLE     �   CREATE TABLE public.beone_gudang (
    gudang_id integer NOT NULL,
    nama character varying(100),
    keterangan character varying(200),
    flag integer
);
     DROP TABLE public.beone_gudang;
       public         postgres    false    3            �            1259    173067    beone_gudang_detail    TABLE     �  CREATE TABLE public.beone_gudang_detail (
    gudang_detail_id bigint NOT NULL,
    gudang_id integer,
    trans_date date,
    item_id integer,
    qty_in double precision,
    qty_out double precision,
    nomor_transaksi character varying(50),
    update_by integer,
    update_date date,
    flag integer,
    keterangan character varying(100),
    kode_tracing character varying(50)
);
 '   DROP TABLE public.beone_gudang_detail;
       public         postgres    false    3            �            1259    173070 (   beone_gudang_detail_gudang_detail_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_gudang_detail_gudang_detail_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ?   DROP SEQUENCE public.beone_gudang_detail_gudang_detail_id_seq;
       public       postgres    false    213    3            �           0    0 (   beone_gudang_detail_gudang_detail_id_seq    SEQUENCE OWNED BY     u   ALTER SEQUENCE public.beone_gudang_detail_gudang_detail_id_seq OWNED BY public.beone_gudang_detail.gudang_detail_id;
            public       postgres    false    214            �            1259    173072    beone_gudang_gudang_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_gudang_gudang_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 1   DROP SEQUENCE public.beone_gudang_gudang_id_seq;
       public       postgres    false    3    212            �           0    0    beone_gudang_gudang_id_seq    SEQUENCE OWNED BY     Y   ALTER SEQUENCE public.beone_gudang_gudang_id_seq OWNED BY public.beone_gudang.gudang_id;
            public       postgres    false    215            �            1259    173074    beone_hutang_piutang    TABLE     �  CREATE TABLE public.beone_hutang_piutang (
    hutang_piutang_id bigint NOT NULL,
    custsup_id integer,
    trans_date date,
    nomor character varying(50),
    keterangan character varying(100),
    valas_trans double precision,
    idr_trans double precision,
    valas_pelunasan double precision,
    idr_pelunasan double precision,
    tipe_trans integer,
    update_by integer,
    update_date date,
    flag integer,
    status_lunas integer
);
 (   DROP TABLE public.beone_hutang_piutang;
       public         postgres    false    3            �            1259    173077 *   beone_hutang_piutang_hutang_piutang_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_hutang_piutang_hutang_piutang_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 A   DROP SEQUENCE public.beone_hutang_piutang_hutang_piutang_id_seq;
       public       postgres    false    216    3            �           0    0 *   beone_hutang_piutang_hutang_piutang_id_seq    SEQUENCE OWNED BY     y   ALTER SEQUENCE public.beone_hutang_piutang_hutang_piutang_id_seq OWNED BY public.beone_hutang_piutang.hutang_piutang_id;
            public       postgres    false    217            �            1259    173079    beone_import_detail    TABLE     �  CREATE TABLE public.beone_import_detail (
    import_detail_id integer NOT NULL,
    item_id integer,
    qty double precision,
    satuan_qty integer,
    price double precision,
    pack_qty double precision,
    satuan_pack integer,
    origin_country integer,
    volume double precision,
    netto double precision,
    brutto double precision,
    hscode character varying(50),
    tbm double precision,
    ppnn double precision,
    tpbm double precision,
    cukai integer,
    sat_cukai integer,
    cukai_value double precision,
    bea_masuk character varying(100),
    sat_bea_masuk integer,
    flag integer,
    item_type_id integer,
    import_header_id integer
);
 '   DROP TABLE public.beone_import_detail;
       public         postgres    false    3            �            1259    173082 (   beone_import_detail_import_detail_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_import_detail_import_detail_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ?   DROP SEQUENCE public.beone_import_detail_import_detail_id_seq;
       public       postgres    false    218    3            �           0    0 (   beone_import_detail_import_detail_id_seq    SEQUENCE OWNED BY     u   ALTER SEQUENCE public.beone_import_detail_import_detail_id_seq OWNED BY public.beone_import_detail.import_detail_id;
            public       postgres    false    219            �            1259    173084    beone_import_header    TABLE     C  CREATE TABLE public.beone_import_header (
    import_header_id integer NOT NULL,
    jenis_bc integer,
    car_no character varying(50),
    bc_no character varying(50),
    bc_date date,
    invoice_no character varying(50),
    invoice_date date,
    kontrak_no character varying(50),
    kontrak_date date,
    purpose_id character varying(100),
    supplier_id integer,
    price_type character varying(10),
    "from" character varying(10),
    amount_value double precision,
    valas_value double precision,
    value_added double precision,
    discount double precision,
    insurance_type character varying(10),
    insurace_value double precision,
    freight double precision,
    flag integer,
    update_by integer,
    update_date date,
    status integer,
    receive_date date,
    receive_no character varying(50)
);
 '   DROP TABLE public.beone_import_header;
       public         postgres    false    3            �            1259    173087 (   beone_import_header_import_header_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_import_header_import_header_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ?   DROP SEQUENCE public.beone_import_header_import_header_id_seq;
       public       postgres    false    3    220            �           0    0 (   beone_import_header_import_header_id_seq    SEQUENCE OWNED BY     u   ALTER SEQUENCE public.beone_import_header_import_header_id_seq OWNED BY public.beone_import_header.import_header_id;
            public       postgres    false    221            �            1259    173089    beone_inventory    TABLE     �  CREATE TABLE public.beone_inventory (
    intvent_trans_id bigint NOT NULL,
    intvent_trans_no character varying(50),
    item_id integer,
    trans_date date,
    keterangan character varying(100),
    qty_in double precision,
    value_in double precision,
    qty_out double precision,
    value_out double precision,
    sa_qty double precision,
    sa_unit_price double precision,
    sa_amount double precision,
    flag integer,
    update_by integer,
    update_date date
);
 #   DROP TABLE public.beone_inventory;
       public         postgres    false    3            �            1259    173092 $   beone_inventory_intvent_trans_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_inventory_intvent_trans_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ;   DROP SEQUENCE public.beone_inventory_intvent_trans_id_seq;
       public       postgres    false    3    222            �           0    0 $   beone_inventory_intvent_trans_id_seq    SEQUENCE OWNED BY     m   ALTER SEQUENCE public.beone_inventory_intvent_trans_id_seq OWNED BY public.beone_inventory.intvent_trans_id;
            public       postgres    false    223            �            1259    173094 
   beone_item    TABLE     S  CREATE TABLE public.beone_item (
    item_id integer NOT NULL,
    nama character varying(100),
    item_code character varying(20),
    saldo_qty double precision,
    saldo_idr double precision,
    keterangan character varying(200),
    flag integer,
    item_type_id integer,
    hscode character varying(50),
    satuan_id integer
);
    DROP TABLE public.beone_item;
       public         postgres    false    3            �            1259    173097    beone_item_item_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_item_item_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 -   DROP SEQUENCE public.beone_item_item_id_seq;
       public       postgres    false    3    224            �           0    0    beone_item_item_id_seq    SEQUENCE OWNED BY     Q   ALTER SEQUENCE public.beone_item_item_id_seq OWNED BY public.beone_item.item_id;
            public       postgres    false    225            �            1259    173099    beone_item_type    TABLE     �   CREATE TABLE public.beone_item_type (
    item_type_id integer NOT NULL,
    nama character varying(100),
    keterangan character varying(200),
    flag integer
);
 #   DROP TABLE public.beone_item_type;
       public         postgres    false    3            �            1259    173102     beone_item_type_item_type_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_item_type_item_type_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 7   DROP SEQUENCE public.beone_item_type_item_type_id_seq;
       public       postgres    false    226    3            �           0    0     beone_item_type_item_type_id_seq    SEQUENCE OWNED BY     e   ALTER SEQUENCE public.beone_item_type_item_type_id_seq OWNED BY public.beone_item_type.item_type_id;
            public       postgres    false    227            �            1259    173104    beone_kode_trans    TABLE     �   CREATE TABLE public.beone_kode_trans (
    kode_trans_id integer NOT NULL,
    nama character varying,
    coa_id integer,
    kode_bank character varying(20),
    in_out integer,
    flag integer
);
 $   DROP TABLE public.beone_kode_trans;
       public         postgres    false    3            �            1259    173110 "   beone_kode_trans_kode_trans_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_kode_trans_kode_trans_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 9   DROP SEQUENCE public.beone_kode_trans_kode_trans_id_seq;
       public       postgres    false    228    3            �           0    0 "   beone_kode_trans_kode_trans_id_seq    SEQUENCE OWNED BY     i   ALTER SEQUENCE public.beone_kode_trans_kode_trans_id_seq OWNED BY public.beone_kode_trans.kode_trans_id;
            public       postgres    false    229            �            1259    173112    beone_konfigurasi_perusahaan    TABLE     G  CREATE TABLE public.beone_konfigurasi_perusahaan (
    nama_perusahaan character varying(100),
    alamat_perusahaan character varying(500),
    kota_perusahaan character varying(100),
    provinsi_perusahaan character varying(100),
    kode_pos_perusahaan character varying(50),
    logo_perusahaan character varying(1000)
);
 0   DROP TABLE public.beone_konfigurasi_perusahaan;
       public         postgres    false    3                       1259    198056 	   beone_log    TABLE     �   CREATE TABLE public.beone_log (
    log_id bigint NOT NULL,
    log_user character varying(100),
    log_tipe integer,
    log_desc character varying(250),
    log_time timestamp without time zone
);
    DROP TABLE public.beone_log;
       public         postgres    false    3                       1259    198054    beone_log_log_id_seq    SEQUENCE     }   CREATE SEQUENCE public.beone_log_log_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 +   DROP SEQUENCE public.beone_log_log_id_seq;
       public       postgres    false    3    272            �           0    0    beone_log_log_id_seq    SEQUENCE OWNED BY     M   ALTER SEQUENCE public.beone_log_log_id_seq OWNED BY public.beone_log.log_id;
            public       postgres    false    271            �            1259    173118    beone_opname_detail    TABLE       CREATE TABLE public.beone_opname_detail (
    opname_detail_id integer NOT NULL,
    opname_header_id integer,
    item_id integer,
    qty_existing double precision,
    qty_opname double precision,
    qty_selisih double precision,
    status_opname integer,
    flag integer
);
 '   DROP TABLE public.beone_opname_detail;
       public         postgres    false    3            �            1259    173121 (   beone_opname_detail_opname_detail_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_opname_detail_opname_detail_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ?   DROP SEQUENCE public.beone_opname_detail_opname_detail_id_seq;
       public       postgres    false    3    231            �           0    0 (   beone_opname_detail_opname_detail_id_seq    SEQUENCE OWNED BY     u   ALTER SEQUENCE public.beone_opname_detail_opname_detail_id_seq OWNED BY public.beone_opname_detail.opname_detail_id;
            public       postgres    false    232            �            1259    173123    beone_opname_header    TABLE     �  CREATE TABLE public.beone_opname_header (
    opname_header_id integer NOT NULL,
    document_no character varying(50),
    opname_date date,
    total_item_opname integer,
    total_item_opname_match integer,
    total_item_opname_plus integer,
    total_item_opname_min integer,
    update_by integer,
    flag integer,
    keterangan character varying(100),
    update_date date
);
 '   DROP TABLE public.beone_opname_header;
       public         postgres    false    3            �            1259    173126 (   beone_opname_header_opname_header_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_opname_header_opname_header_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ?   DROP SEQUENCE public.beone_opname_header_opname_header_id_seq;
       public       postgres    false    3    233            �           0    0 (   beone_opname_header_opname_header_id_seq    SEQUENCE OWNED BY     u   ALTER SEQUENCE public.beone_opname_header_opname_header_id_seq OWNED BY public.beone_opname_header.opname_header_id;
            public       postgres    false    234            �            1259    173128    beone_peleburan    TABLE     !  CREATE TABLE public.beone_peleburan (
    peleburan_id integer NOT NULL,
    peleburan_no character varying(20),
    peleburan_date date,
    keterangan character varying(100),
    item_id integer,
    qty_peleburan bigint,
    update_by integer,
    update_date date,
    flag integer
);
 #   DROP TABLE public.beone_peleburan;
       public         postgres    false    3            �            1259    173131     beone_peleburan_peleburan_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_peleburan_peleburan_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 7   DROP SEQUENCE public.beone_peleburan_peleburan_id_seq;
       public       postgres    false    235    3            �           0    0     beone_peleburan_peleburan_id_seq    SEQUENCE OWNED BY     e   ALTER SEQUENCE public.beone_peleburan_peleburan_id_seq OWNED BY public.beone_peleburan.peleburan_id;
            public       postgres    false    236                       1259    197815    beone_po_import_detail    TABLE     �   CREATE TABLE public.beone_po_import_detail (
    purchase_detail_id bigint NOT NULL,
    purchase_header_id integer,
    item_id integer,
    qty double precision,
    price double precision,
    amount double precision,
    flag integer
);
 *   DROP TABLE public.beone_po_import_detail;
       public         postgres    false    3                       1259    197813 -   beone_po_import_detail_purchase_detail_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_po_import_detail_purchase_detail_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 D   DROP SEQUENCE public.beone_po_import_detail_purchase_detail_id_seq;
       public       postgres    false    3    268            �           0    0 -   beone_po_import_detail_purchase_detail_id_seq    SEQUENCE OWNED BY        ALTER SEQUENCE public.beone_po_import_detail_purchase_detail_id_seq OWNED BY public.beone_po_import_detail.purchase_detail_id;
            public       postgres    false    267            
           1259    197807    beone_po_import_header    TABLE     3  CREATE TABLE public.beone_po_import_header (
    purchase_header_id bigint NOT NULL,
    purchase_no character varying(50),
    trans_date date,
    supplier_id integer,
    keterangan character varying(250),
    grandtotal double precision,
    flag integer,
    update_by integer,
    update_date date
);
 *   DROP TABLE public.beone_po_import_header;
       public         postgres    false    3            	           1259    197805 -   beone_po_import_header_purchase_header_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_po_import_header_purchase_header_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 D   DROP SEQUENCE public.beone_po_import_header_purchase_header_id_seq;
       public       postgres    false    266    3            �           0    0 -   beone_po_import_header_purchase_header_id_seq    SEQUENCE OWNED BY        ALTER SEQUENCE public.beone_po_import_header_purchase_header_id_seq OWNED BY public.beone_po_import_header.purchase_header_id;
            public       postgres    false    265            �            1259    173133    beone_purchase_detail    TABLE     �   CREATE TABLE public.beone_purchase_detail (
    purchase_detail_id bigint NOT NULL,
    purchase_header_id integer,
    item_id integer,
    qty double precision,
    price double precision,
    amount double precision,
    flag integer
);
 )   DROP TABLE public.beone_purchase_detail;
       public         postgres    false    3            �            1259    173136 ,   beone_purchase_detail_purchase_detail_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_purchase_detail_purchase_detail_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 C   DROP SEQUENCE public.beone_purchase_detail_purchase_detail_id_seq;
       public       postgres    false    237    3            �           0    0 ,   beone_purchase_detail_purchase_detail_id_seq    SEQUENCE OWNED BY     }   ALTER SEQUENCE public.beone_purchase_detail_purchase_detail_id_seq OWNED BY public.beone_purchase_detail.purchase_detail_id;
            public       postgres    false    238            �            1259    173138    beone_purchase_header    TABLE     �  CREATE TABLE public.beone_purchase_header (
    purchase_header_id bigint NOT NULL,
    purchase_no character varying(50),
    trans_date date,
    supplier_id integer,
    keterangan character varying(200),
    update_by integer,
    update_date date,
    flag integer,
    ppn integer,
    subtotal double precision,
    ppn_value double precision,
    grandtotal double precision
);
 )   DROP TABLE public.beone_purchase_header;
       public         postgres    false    3            �            1259    173141 ,   beone_purchase_header_purchase_header_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_purchase_header_purchase_header_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 C   DROP SEQUENCE public.beone_purchase_header_purchase_header_id_seq;
       public       postgres    false    3    239            �           0    0 ,   beone_purchase_header_purchase_header_id_seq    SEQUENCE OWNED BY     }   ALTER SEQUENCE public.beone_purchase_header_purchase_header_id_seq OWNED BY public.beone_purchase_header.purchase_header_id;
            public       postgres    false    240            �            1259    173143    beone_received_import    TABLE     �  CREATE TABLE public.beone_received_import (
    received_id bigint NOT NULL,
    nomor_aju character varying(50),
    nomor_dokumen_pabean character varying(10),
    tanggal_received date,
    status_received integer,
    flag integer,
    update_by integer,
    update_date date,
    nomor_received character varying(50),
    tpb_header_id integer,
    kurs double precision,
    po_import_header_id integer
);
 )   DROP TABLE public.beone_received_import;
       public         postgres    false    3            �            1259    173146 %   beone_received_import_received_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_received_import_received_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 <   DROP SEQUENCE public.beone_received_import_received_id_seq;
       public       postgres    false    241    3            �           0    0 %   beone_received_import_received_id_seq    SEQUENCE OWNED BY     o   ALTER SEQUENCE public.beone_received_import_received_id_seq OWNED BY public.beone_received_import.received_id;
            public       postgres    false    242            �            1259    173148 
   beone_role    TABLE     �  CREATE TABLE public.beone_role (
    role_id integer NOT NULL,
    nama_role character varying(50),
    keterangan character varying(200),
    master_menu integer,
    pembelian_menu integer,
    penjualan_menu integer,
    inventory_menu integer,
    produksi_menu integer,
    asset_menu integer,
    jurnal_umum_menu integer,
    kasbank_menu integer,
    laporan_inventory integer,
    laporan_keuangan integer,
    konfigurasi integer,
    import_menu integer,
    eksport_menu integer
);
    DROP TABLE public.beone_role;
       public         postgres    false    3            �            1259    173151    beone_role_role_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_role_role_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 -   DROP SEQUENCE public.beone_role_role_id_seq;
       public       postgres    false    243    3            �           0    0    beone_role_role_id_seq    SEQUENCE OWNED BY     Q   ALTER SEQUENCE public.beone_role_role_id_seq OWNED BY public.beone_role.role_id;
            public       postgres    false    244                       1259    198003    beone_role_user    TABLE     �  CREATE TABLE public.beone_role_user (
    role_id integer NOT NULL,
    nama_role character varying(50),
    keterangan character varying(200),
    master_menu integer,
    user_add integer,
    user_edit integer,
    user_delete integer,
    role_add integer,
    role_edit integer,
    role_delete integer,
    customer_add integer,
    customer_edit integer,
    customer_delete integer,
    supplier_add integer,
    supplier_edit integer,
    supplier_delete integer,
    item_add integer,
    item_edit integer,
    item_delete integer,
    jenis_add integer,
    jenis_edit integer,
    jenis_delete integer,
    satuan_add integer,
    satuan_edit integer,
    satuan_delete integer,
    gudang_add integer,
    gudang_edit integer,
    gudang_delete integer,
    master_import integer,
    po_add integer,
    po_edit integer,
    po_delete integer,
    tracing integer,
    terima_barang integer,
    master_pembelian integer,
    pembelian_add integer,
    pembelian_edit integer,
    pembelian_delete integer,
    kredit_note_add integer,
    kredit_note_edit integer,
    kredit_note_delete integer,
    master_eksport integer,
    eksport_add integer,
    eksport_edit integer,
    eksport_delete integer,
    kirim_barang integer,
    menu_penjualan integer,
    penjualan_add integer,
    penjualan_edit integer,
    penjualan_delete integer,
    debit_note_add integer,
    debit_note_edit integer,
    debit_note_delete integer,
    menu_inventory integer,
    pindah_gudang integer,
    stockopname_add integer,
    stockopname_edit integer,
    stockopname_delete integer,
    stockopname_opname integer,
    adjustment_add integer,
    adjustment_edit integer,
    adjustment_delete integer,
    pemusnahan_add integer,
    pemusnahan_edit integer,
    pemusnahan_delete integer,
    recal_inventory integer,
    menu_produksi integer,
    produksi_add integer,
    produksi_edit integer,
    produksi_delete integer,
    menu_asset integer,
    menu_jurnal_umum integer,
    jurnal_umum_add integer,
    jurnal_umum_edit integer,
    jurnal_umum_delete integer,
    menu_kas_bank integer,
    kas_bank_add integer,
    kas_bank_edit integer,
    kas_bank_delete integer,
    menu_laporan_inventory integer,
    menu_laporan_keuangan integer,
    menu_konfigurasi integer
);
 #   DROP TABLE public.beone_role_user;
       public         postgres    false    3                       1259    198001    beone_role_user_role_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_role_user_role_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 2   DROP SEQUENCE public.beone_role_user_role_id_seq;
       public       postgres    false    3    270            �           0    0    beone_role_user_role_id_seq    SEQUENCE OWNED BY     [   ALTER SEQUENCE public.beone_role_user_role_id_seq OWNED BY public.beone_role_user.role_id;
            public       postgres    false    269            �            1259    173153    beone_sales_detail    TABLE     �   CREATE TABLE public.beone_sales_detail (
    sales_detail_id bigint NOT NULL,
    sales_header_id integer,
    item_id integer,
    qty double precision,
    price double precision,
    amount double precision,
    flag integer
);
 &   DROP TABLE public.beone_sales_detail;
       public         postgres    false    3            �            1259    173156 &   beone_sales_detail_sales_detail_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_sales_detail_sales_detail_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 =   DROP SEQUENCE public.beone_sales_detail_sales_detail_id_seq;
       public       postgres    false    3    245            �           0    0 &   beone_sales_detail_sales_detail_id_seq    SEQUENCE OWNED BY     q   ALTER SEQUENCE public.beone_sales_detail_sales_detail_id_seq OWNED BY public.beone_sales_detail.sales_detail_id;
            public       postgres    false    246            �            1259    173158    beone_sales_header    TABLE     �  CREATE TABLE public.beone_sales_header (
    sales_header_id bigint NOT NULL,
    sales_no character varying(50),
    trans_date date,
    customer_id integer,
    keterangan character varying(200),
    ppn double precision,
    subtotal double precision,
    ppn_value double precision,
    grandtotal double precision,
    update_by integer,
    update_date date,
    flag integer
);
 &   DROP TABLE public.beone_sales_header;
       public         postgres    false    3            �            1259    173161 &   beone_sales_header_sales_header_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_sales_header_sales_header_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 =   DROP SEQUENCE public.beone_sales_header_sales_header_id_seq;
       public       postgres    false    3    247            �           0    0 &   beone_sales_header_sales_header_id_seq    SEQUENCE OWNED BY     q   ALTER SEQUENCE public.beone_sales_header_sales_header_id_seq OWNED BY public.beone_sales_header.sales_header_id;
            public       postgres    false    248            �            1259    173163    beone_satuan_item    TABLE     �   CREATE TABLE public.beone_satuan_item (
    satuan_id integer NOT NULL,
    satuan_code character varying(5),
    keterangan character varying(100),
    flag integer
);
 %   DROP TABLE public.beone_satuan_item;
       public         postgres    false    3            �            1259    173166    beone_satuan_item_satuan_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_satuan_item_satuan_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 6   DROP SEQUENCE public.beone_satuan_item_satuan_id_seq;
       public       postgres    false    249    3            �           0    0    beone_satuan_item_satuan_id_seq    SEQUENCE OWNED BY     c   ALTER SEQUENCE public.beone_satuan_item_satuan_id_seq OWNED BY public.beone_satuan_item.satuan_id;
            public       postgres    false    250            �            1259    173168    beone_tipe_coa    TABLE     i   CREATE TABLE public.beone_tipe_coa (
    tipe_coa_id integer NOT NULL,
    nama character varying(50)
);
 "   DROP TABLE public.beone_tipe_coa;
       public         postgres    false    3            �            1259    173171    beone_tipe_coa_tipe_coa_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_tipe_coa_tipe_coa_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 5   DROP SEQUENCE public.beone_tipe_coa_tipe_coa_id_seq;
       public       postgres    false    251    3            �           0    0    beone_tipe_coa_tipe_coa_id_seq    SEQUENCE OWNED BY     a   ALTER SEQUENCE public.beone_tipe_coa_tipe_coa_id_seq OWNED BY public.beone_tipe_coa.tipe_coa_id;
            public       postgres    false    252            �            1259    173173    beone_tipe_coa_transaksi    TABLE     �   CREATE TABLE public.beone_tipe_coa_transaksi (
    tipe_coa_trans_id integer NOT NULL,
    nama character varying(50),
    flag integer
);
 ,   DROP TABLE public.beone_tipe_coa_transaksi;
       public         postgres    false    3            �            1259    173176 .   beone_tipe_coa_transaksi_tipe_coa_trans_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_tipe_coa_transaksi_tipe_coa_trans_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 E   DROP SEQUENCE public.beone_tipe_coa_transaksi_tipe_coa_trans_id_seq;
       public       postgres    false    3    253            �           0    0 .   beone_tipe_coa_transaksi_tipe_coa_trans_id_seq    SEQUENCE OWNED BY     �   ALTER SEQUENCE public.beone_tipe_coa_transaksi_tipe_coa_trans_id_seq OWNED BY public.beone_tipe_coa_transaksi.tipe_coa_trans_id;
            public       postgres    false    254            �            1259    173178    beone_transfer_stock    TABLE       CREATE TABLE public.beone_transfer_stock (
    transfer_stock_id bigint NOT NULL,
    transfer_no character varying(50),
    transfer_date date,
    coa_kode_biaya integer,
    keterangan character varying(200),
    update_by integer,
    update_date date,
    flag integer
);
 (   DROP TABLE public.beone_transfer_stock;
       public         postgres    false    3                        1259    173181    beone_transfer_stock_detail    TABLE     u  CREATE TABLE public.beone_transfer_stock_detail (
    transfer_stock_detail_id bigint NOT NULL,
    transfer_stock_header_id integer,
    tipe_transfer_stock character varying(10),
    item_id integer,
    qty double precision,
    gudang_id integer,
    biaya double precision,
    update_by integer,
    update_date date,
    flag integer,
    persen_produksi integer
);
 /   DROP TABLE public.beone_transfer_stock_detail;
       public         postgres    false    3                       1259    173184 8   beone_transfer_stock_detail_transfer_stock_detail_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_transfer_stock_detail_transfer_stock_detail_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 O   DROP SEQUENCE public.beone_transfer_stock_detail_transfer_stock_detail_id_seq;
       public       postgres    false    3    256            �           0    0 8   beone_transfer_stock_detail_transfer_stock_detail_id_seq    SEQUENCE OWNED BY     �   ALTER SEQUENCE public.beone_transfer_stock_detail_transfer_stock_detail_id_seq OWNED BY public.beone_transfer_stock_detail.transfer_stock_detail_id;
            public       postgres    false    257                       1259    173186 *   beone_transfer_stock_transfer_stock_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_transfer_stock_transfer_stock_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 A   DROP SEQUENCE public.beone_transfer_stock_transfer_stock_id_seq;
       public       postgres    false    255    3            �           0    0 *   beone_transfer_stock_transfer_stock_id_seq    SEQUENCE OWNED BY     y   ALTER SEQUENCE public.beone_transfer_stock_transfer_stock_id_seq OWNED BY public.beone_transfer_stock.transfer_stock_id;
            public       postgres    false    258                       1259    173188 
   beone_user    TABLE     �   CREATE TABLE public.beone_user (
    user_id integer NOT NULL,
    username character varying(50),
    password character varying(50),
    nama character varying(100),
    role_id integer,
    update_by integer,
    update_date date,
    flag integer
);
    DROP TABLE public.beone_user;
       public         postgres    false    3                       1259    173196    beone_user_user_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_user_user_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 -   DROP SEQUENCE public.beone_user_user_id_seq;
       public       postgres    false    3    259            �           0    0    beone_user_user_id_seq    SEQUENCE OWNED BY     Q   ALTER SEQUENCE public.beone_user_user_id_seq OWNED BY public.beone_user.user_id;
            public       postgres    false    260                       1259    173201    beone_voucher_detail    TABLE     B  CREATE TABLE public.beone_voucher_detail (
    voucher_detail_id bigint NOT NULL,
    voucher_header_id integer,
    coa_id_lawan integer,
    coa_no_lawan character varying(20),
    jumlah_valas double precision,
    kurs double precision,
    jumlah_idr double precision,
    keterangan_detail character varying(200)
);
 (   DROP TABLE public.beone_voucher_detail;
       public         postgres    false    3                       1259    173204 *   beone_voucher_detail_voucher_detail_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_voucher_detail_voucher_detail_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 A   DROP SEQUENCE public.beone_voucher_detail_voucher_detail_id_seq;
       public       postgres    false    3    261            �           0    0 *   beone_voucher_detail_voucher_detail_id_seq    SEQUENCE OWNED BY     y   ALTER SEQUENCE public.beone_voucher_detail_voucher_detail_id_seq OWNED BY public.beone_voucher_detail.voucher_detail_id;
            public       postgres    false    262                       1259    173206    beone_voucher_header    TABLE     ;  CREATE TABLE public.beone_voucher_header (
    voucher_header_id bigint NOT NULL,
    voucher_number character varying(20),
    voucher_date date,
    tipe integer,
    keterangan character varying(200),
    coa_id_cash_bank integer,
    coa_no character varying(20),
    update_by integer,
    update_date date
);
 (   DROP TABLE public.beone_voucher_header;
       public         postgres    false    3                       1259    173209 *   beone_voucher_header_voucher_header_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_voucher_header_voucher_header_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 A   DROP SEQUENCE public.beone_voucher_header_voucher_header_id_seq;
       public       postgres    false    263    3            �           0    0 *   beone_voucher_header_voucher_header_id_seq    SEQUENCE OWNED BY     y   ALTER SEQUENCE public.beone_voucher_header_voucher_header_id_seq OWNED BY public.beone_voucher_header.voucher_header_id;
            public       postgres    false    264            g           2604    173213    beone_adjustment adjustment_id    DEFAULT     �   ALTER TABLE ONLY public.beone_adjustment ALTER COLUMN adjustment_id SET DEFAULT nextval('public.beone_adjustment_adjustment_id_seq'::regclass);
 M   ALTER TABLE public.beone_adjustment ALTER COLUMN adjustment_id DROP DEFAULT;
       public       postgres    false    197    196            h           2604    173214    beone_coa coa_id    DEFAULT     t   ALTER TABLE ONLY public.beone_coa ALTER COLUMN coa_id SET DEFAULT nextval('public.beone_coa_coa_id_seq'::regclass);
 ?   ALTER TABLE public.beone_coa ALTER COLUMN coa_id DROP DEFAULT;
       public       postgres    false    199    198            i           2604    173215    beone_coa_jurnal coa_jurnal_id    DEFAULT     �   ALTER TABLE ONLY public.beone_coa_jurnal ALTER COLUMN coa_jurnal_id SET DEFAULT nextval('public.beone_coa_jurnal_coa_jurnal_id_seq'::regclass);
 M   ALTER TABLE public.beone_coa_jurnal ALTER COLUMN coa_jurnal_id DROP DEFAULT;
       public       postgres    false    201    200            j           2604    173216    beone_country country_id    DEFAULT     �   ALTER TABLE ONLY public.beone_country ALTER COLUMN country_id SET DEFAULT nextval('public.beone_country_country_id_seq'::regclass);
 G   ALTER TABLE public.beone_country ALTER COLUMN country_id DROP DEFAULT;
       public       postgres    false    203    202            k           2604    173217    beone_custsup custsup_id    DEFAULT     �   ALTER TABLE ONLY public.beone_custsup ALTER COLUMN custsup_id SET DEFAULT nextval('public.beone_custsup_custsup_id_seq'::regclass);
 G   ALTER TABLE public.beone_custsup ALTER COLUMN custsup_id DROP DEFAULT;
       public       postgres    false    205    204            l           2604    173218 $   beone_export_detail export_detail_id    DEFAULT     �   ALTER TABLE ONLY public.beone_export_detail ALTER COLUMN export_detail_id SET DEFAULT nextval('public.beone_export_detail_export_detail_id_seq'::regclass);
 S   ALTER TABLE public.beone_export_detail ALTER COLUMN export_detail_id DROP DEFAULT;
       public       postgres    false    207    206            m           2604    173219 $   beone_export_header export_header_id    DEFAULT     �   ALTER TABLE ONLY public.beone_export_header ALTER COLUMN export_header_id SET DEFAULT nextval('public.beone_export_header_export_header_id_seq'::regclass);
 S   ALTER TABLE public.beone_export_header ALTER COLUMN export_header_id DROP DEFAULT;
       public       postgres    false    209    208            �           2604    214304    beone_fix_asset fix_asset_id    DEFAULT     �   ALTER TABLE ONLY public.beone_fix_asset ALTER COLUMN fix_asset_id SET DEFAULT nextval('public.beone_fix_asset_fix_asset_id_seq'::regclass);
 K   ALTER TABLE public.beone_fix_asset ALTER COLUMN fix_asset_id DROP DEFAULT;
       public       postgres    false    274    273    274            �           2604    214312    beone_fix_asset_akm akm_id    DEFAULT     �   ALTER TABLE ONLY public.beone_fix_asset_akm ALTER COLUMN akm_id SET DEFAULT nextval('public.beone_fix_asset_akm_akm_id_seq'::regclass);
 I   ALTER TABLE public.beone_fix_asset_akm ALTER COLUMN akm_id DROP DEFAULT;
       public       postgres    false    275    276    276            �           2604    214320 "   beone_fix_asset_periode periode_id    DEFAULT     �   ALTER TABLE ONLY public.beone_fix_asset_periode ALTER COLUMN periode_id SET DEFAULT nextval('public.beone_fix_asset_periode_periode_id_seq'::regclass);
 Q   ALTER TABLE public.beone_fix_asset_periode ALTER COLUMN periode_id DROP DEFAULT;
       public       postgres    false    277    278    278            n           2604    173220    beone_gl gl_id    DEFAULT     p   ALTER TABLE ONLY public.beone_gl ALTER COLUMN gl_id SET DEFAULT nextval('public.beone_gl_gl_id_seq'::regclass);
 =   ALTER TABLE public.beone_gl ALTER COLUMN gl_id DROP DEFAULT;
       public       postgres    false    211    210            o           2604    173221    beone_gudang gudang_id    DEFAULT     �   ALTER TABLE ONLY public.beone_gudang ALTER COLUMN gudang_id SET DEFAULT nextval('public.beone_gudang_gudang_id_seq'::regclass);
 E   ALTER TABLE public.beone_gudang ALTER COLUMN gudang_id DROP DEFAULT;
       public       postgres    false    215    212            p           2604    173222 $   beone_gudang_detail gudang_detail_id    DEFAULT     �   ALTER TABLE ONLY public.beone_gudang_detail ALTER COLUMN gudang_detail_id SET DEFAULT nextval('public.beone_gudang_detail_gudang_detail_id_seq'::regclass);
 S   ALTER TABLE public.beone_gudang_detail ALTER COLUMN gudang_detail_id DROP DEFAULT;
       public       postgres    false    214    213            q           2604    173223 &   beone_hutang_piutang hutang_piutang_id    DEFAULT     �   ALTER TABLE ONLY public.beone_hutang_piutang ALTER COLUMN hutang_piutang_id SET DEFAULT nextval('public.beone_hutang_piutang_hutang_piutang_id_seq'::regclass);
 U   ALTER TABLE public.beone_hutang_piutang ALTER COLUMN hutang_piutang_id DROP DEFAULT;
       public       postgres    false    217    216            r           2604    173224 $   beone_import_detail import_detail_id    DEFAULT     �   ALTER TABLE ONLY public.beone_import_detail ALTER COLUMN import_detail_id SET DEFAULT nextval('public.beone_import_detail_import_detail_id_seq'::regclass);
 S   ALTER TABLE public.beone_import_detail ALTER COLUMN import_detail_id DROP DEFAULT;
       public       postgres    false    219    218            s           2604    173225 $   beone_import_header import_header_id    DEFAULT     �   ALTER TABLE ONLY public.beone_import_header ALTER COLUMN import_header_id SET DEFAULT nextval('public.beone_import_header_import_header_id_seq'::regclass);
 S   ALTER TABLE public.beone_import_header ALTER COLUMN import_header_id DROP DEFAULT;
       public       postgres    false    221    220            t           2604    173226     beone_inventory intvent_trans_id    DEFAULT     �   ALTER TABLE ONLY public.beone_inventory ALTER COLUMN intvent_trans_id SET DEFAULT nextval('public.beone_inventory_intvent_trans_id_seq'::regclass);
 O   ALTER TABLE public.beone_inventory ALTER COLUMN intvent_trans_id DROP DEFAULT;
       public       postgres    false    223    222            u           2604    173227    beone_item item_id    DEFAULT     x   ALTER TABLE ONLY public.beone_item ALTER COLUMN item_id SET DEFAULT nextval('public.beone_item_item_id_seq'::regclass);
 A   ALTER TABLE public.beone_item ALTER COLUMN item_id DROP DEFAULT;
       public       postgres    false    225    224            v           2604    173228    beone_item_type item_type_id    DEFAULT     �   ALTER TABLE ONLY public.beone_item_type ALTER COLUMN item_type_id SET DEFAULT nextval('public.beone_item_type_item_type_id_seq'::regclass);
 K   ALTER TABLE public.beone_item_type ALTER COLUMN item_type_id DROP DEFAULT;
       public       postgres    false    227    226            w           2604    173229    beone_kode_trans kode_trans_id    DEFAULT     �   ALTER TABLE ONLY public.beone_kode_trans ALTER COLUMN kode_trans_id SET DEFAULT nextval('public.beone_kode_trans_kode_trans_id_seq'::regclass);
 M   ALTER TABLE public.beone_kode_trans ALTER COLUMN kode_trans_id DROP DEFAULT;
       public       postgres    false    229    228            �           2604    198059    beone_log log_id    DEFAULT     t   ALTER TABLE ONLY public.beone_log ALTER COLUMN log_id SET DEFAULT nextval('public.beone_log_log_id_seq'::regclass);
 ?   ALTER TABLE public.beone_log ALTER COLUMN log_id DROP DEFAULT;
       public       postgres    false    272    271    272            x           2604    173230 $   beone_opname_detail opname_detail_id    DEFAULT     �   ALTER TABLE ONLY public.beone_opname_detail ALTER COLUMN opname_detail_id SET DEFAULT nextval('public.beone_opname_detail_opname_detail_id_seq'::regclass);
 S   ALTER TABLE public.beone_opname_detail ALTER COLUMN opname_detail_id DROP DEFAULT;
       public       postgres    false    232    231            y           2604    173231 $   beone_opname_header opname_header_id    DEFAULT     �   ALTER TABLE ONLY public.beone_opname_header ALTER COLUMN opname_header_id SET DEFAULT nextval('public.beone_opname_header_opname_header_id_seq'::regclass);
 S   ALTER TABLE public.beone_opname_header ALTER COLUMN opname_header_id DROP DEFAULT;
       public       postgres    false    234    233            z           2604    173232    beone_peleburan peleburan_id    DEFAULT     �   ALTER TABLE ONLY public.beone_peleburan ALTER COLUMN peleburan_id SET DEFAULT nextval('public.beone_peleburan_peleburan_id_seq'::regclass);
 K   ALTER TABLE public.beone_peleburan ALTER COLUMN peleburan_id DROP DEFAULT;
       public       postgres    false    236    235            �           2604    197818 )   beone_po_import_detail purchase_detail_id    DEFAULT     �   ALTER TABLE ONLY public.beone_po_import_detail ALTER COLUMN purchase_detail_id SET DEFAULT nextval('public.beone_po_import_detail_purchase_detail_id_seq'::regclass);
 X   ALTER TABLE public.beone_po_import_detail ALTER COLUMN purchase_detail_id DROP DEFAULT;
       public       postgres    false    267    268    268            �           2604    197810 )   beone_po_import_header purchase_header_id    DEFAULT     �   ALTER TABLE ONLY public.beone_po_import_header ALTER COLUMN purchase_header_id SET DEFAULT nextval('public.beone_po_import_header_purchase_header_id_seq'::regclass);
 X   ALTER TABLE public.beone_po_import_header ALTER COLUMN purchase_header_id DROP DEFAULT;
       public       postgres    false    265    266    266            {           2604    173233 (   beone_purchase_detail purchase_detail_id    DEFAULT     �   ALTER TABLE ONLY public.beone_purchase_detail ALTER COLUMN purchase_detail_id SET DEFAULT nextval('public.beone_purchase_detail_purchase_detail_id_seq'::regclass);
 W   ALTER TABLE public.beone_purchase_detail ALTER COLUMN purchase_detail_id DROP DEFAULT;
       public       postgres    false    238    237            |           2604    173234 (   beone_purchase_header purchase_header_id    DEFAULT     �   ALTER TABLE ONLY public.beone_purchase_header ALTER COLUMN purchase_header_id SET DEFAULT nextval('public.beone_purchase_header_purchase_header_id_seq'::regclass);
 W   ALTER TABLE public.beone_purchase_header ALTER COLUMN purchase_header_id DROP DEFAULT;
       public       postgres    false    240    239            }           2604    173235 !   beone_received_import received_id    DEFAULT     �   ALTER TABLE ONLY public.beone_received_import ALTER COLUMN received_id SET DEFAULT nextval('public.beone_received_import_received_id_seq'::regclass);
 P   ALTER TABLE public.beone_received_import ALTER COLUMN received_id DROP DEFAULT;
       public       postgres    false    242    241            ~           2604    173236    beone_role role_id    DEFAULT     x   ALTER TABLE ONLY public.beone_role ALTER COLUMN role_id SET DEFAULT nextval('public.beone_role_role_id_seq'::regclass);
 A   ALTER TABLE public.beone_role ALTER COLUMN role_id DROP DEFAULT;
       public       postgres    false    244    243            �           2604    198006    beone_role_user role_id    DEFAULT     �   ALTER TABLE ONLY public.beone_role_user ALTER COLUMN role_id SET DEFAULT nextval('public.beone_role_user_role_id_seq'::regclass);
 F   ALTER TABLE public.beone_role_user ALTER COLUMN role_id DROP DEFAULT;
       public       postgres    false    269    270    270                       2604    173237 "   beone_sales_detail sales_detail_id    DEFAULT     �   ALTER TABLE ONLY public.beone_sales_detail ALTER COLUMN sales_detail_id SET DEFAULT nextval('public.beone_sales_detail_sales_detail_id_seq'::regclass);
 Q   ALTER TABLE public.beone_sales_detail ALTER COLUMN sales_detail_id DROP DEFAULT;
       public       postgres    false    246    245            �           2604    173238 "   beone_sales_header sales_header_id    DEFAULT     �   ALTER TABLE ONLY public.beone_sales_header ALTER COLUMN sales_header_id SET DEFAULT nextval('public.beone_sales_header_sales_header_id_seq'::regclass);
 Q   ALTER TABLE public.beone_sales_header ALTER COLUMN sales_header_id DROP DEFAULT;
       public       postgres    false    248    247            �           2604    173239    beone_satuan_item satuan_id    DEFAULT     �   ALTER TABLE ONLY public.beone_satuan_item ALTER COLUMN satuan_id SET DEFAULT nextval('public.beone_satuan_item_satuan_id_seq'::regclass);
 J   ALTER TABLE public.beone_satuan_item ALTER COLUMN satuan_id DROP DEFAULT;
       public       postgres    false    250    249            �           2604    173240    beone_tipe_coa tipe_coa_id    DEFAULT     �   ALTER TABLE ONLY public.beone_tipe_coa ALTER COLUMN tipe_coa_id SET DEFAULT nextval('public.beone_tipe_coa_tipe_coa_id_seq'::regclass);
 I   ALTER TABLE public.beone_tipe_coa ALTER COLUMN tipe_coa_id DROP DEFAULT;
       public       postgres    false    252    251            �           2604    173241 *   beone_tipe_coa_transaksi tipe_coa_trans_id    DEFAULT     �   ALTER TABLE ONLY public.beone_tipe_coa_transaksi ALTER COLUMN tipe_coa_trans_id SET DEFAULT nextval('public.beone_tipe_coa_transaksi_tipe_coa_trans_id_seq'::regclass);
 Y   ALTER TABLE public.beone_tipe_coa_transaksi ALTER COLUMN tipe_coa_trans_id DROP DEFAULT;
       public       postgres    false    254    253            �           2604    173242 &   beone_transfer_stock transfer_stock_id    DEFAULT     �   ALTER TABLE ONLY public.beone_transfer_stock ALTER COLUMN transfer_stock_id SET DEFAULT nextval('public.beone_transfer_stock_transfer_stock_id_seq'::regclass);
 U   ALTER TABLE public.beone_transfer_stock ALTER COLUMN transfer_stock_id DROP DEFAULT;
       public       postgres    false    258    255            �           2604    173243 4   beone_transfer_stock_detail transfer_stock_detail_id    DEFAULT     �   ALTER TABLE ONLY public.beone_transfer_stock_detail ALTER COLUMN transfer_stock_detail_id SET DEFAULT nextval('public.beone_transfer_stock_detail_transfer_stock_detail_id_seq'::regclass);
 c   ALTER TABLE public.beone_transfer_stock_detail ALTER COLUMN transfer_stock_detail_id DROP DEFAULT;
       public       postgres    false    257    256            �           2604    173244    beone_user user_id    DEFAULT     x   ALTER TABLE ONLY public.beone_user ALTER COLUMN user_id SET DEFAULT nextval('public.beone_user_user_id_seq'::regclass);
 A   ALTER TABLE public.beone_user ALTER COLUMN user_id DROP DEFAULT;
       public       postgres    false    260    259            �           2604    173247 &   beone_voucher_detail voucher_detail_id    DEFAULT     �   ALTER TABLE ONLY public.beone_voucher_detail ALTER COLUMN voucher_detail_id SET DEFAULT nextval('public.beone_voucher_detail_voucher_detail_id_seq'::regclass);
 U   ALTER TABLE public.beone_voucher_detail ALTER COLUMN voucher_detail_id DROP DEFAULT;
       public       postgres    false    262    261            �           2604    173248 &   beone_voucher_header voucher_header_id    DEFAULT     �   ALTER TABLE ONLY public.beone_voucher_header ALTER COLUMN voucher_header_id SET DEFAULT nextval('public.beone_voucher_header_voucher_header_id_seq'::regclass);
 U   ALTER TABLE public.beone_voucher_header ALTER COLUMN voucher_header_id DROP DEFAULT;
       public       postgres    false    264    263            U          0    173018    beone_adjustment 
   TABLE DATA               �   COPY public.beone_adjustment (adjustment_id, adjustment_no, adjustment_date, keterangan, item_id, qty_adjustment, posisi_qty, update_by, update_date, flag) FROM stdin;
    public       postgres    false    196   ƞ      W          0    173023 	   beone_coa 
   TABLE DATA               �   COPY public.beone_coa (coa_id, nama, nomor, tipe_akun, debet_valas, debet_idr, kredit_valas, kredit_idr, dk, tipe_transaksi) FROM stdin;
    public       postgres    false    198   �      Y          0    173031    beone_coa_jurnal 
   TABLE DATA               q   COPY public.beone_coa_jurnal (coa_jurnal_id, nama_coa_jurnal, keterangan_coa_jurnal, coa_id, coa_no) FROM stdin;
    public       postgres    false    200   ��      [          0    173039    beone_country 
   TABLE DATA               M   COPY public.beone_country (country_id, nama, country_code, flag) FROM stdin;
    public       postgres    false    202   Ƨ      ]          0    173044    beone_custsup 
   TABLE DATA               4  COPY public.beone_custsup (custsup_id, nama, alamat, tipe_custsup, negara, saldo_hutang_idr, saldo_hutang_valas, saldo_piutang_idr, saldo_piutang_valas, flag, pelunasan_hutang_idr, pelunasan_hutang_valas, pelunasan_piutang_idr, pelunasan_piutang_valas, status_lunas_piutang, status_lunas_hutang) FROM stdin;
    public       postgres    false    204   �      _          0    173049    beone_export_detail 
   TABLE DATA               �   COPY public.beone_export_detail (export_detail_id, export_header_id, item_id, qty, satuan_qty, price, pack_qty, satuan_pack, origin_country, doc, volume, netto, brutto, flag) FROM stdin;
    public       postgres    false    206   ��      a          0    173054    beone_export_header 
   TABLE DATA               �  COPY public.beone_export_header (export_header_id, jenis_bc, car_no, bc_no, bc_date, kontrak_no, kontrak_date, jenis_export, invoice_no, invoice_date, surat_jalan_no, surat_jalan_date, receiver_id, country_id, price_type, amount_value, valas_value, insurance_type, insurance_value, freight, flag, status, delivery_date, update_by, update_date, delivery_no, vessel, port_loading, port_destination, container, no_container, no_seal) FROM stdin;
    public       postgres    false    208   ��      �          0    214301    beone_fix_asset 
   TABLE DATA               g   COPY public.beone_fix_asset (fix_asset_id, nama_asset, tanggal_perolehan, harga_perolehan) FROM stdin;
    public       postgres    false    274   Ҫ      �          0    214309    beone_fix_asset_akm 
   TABLE DATA               �   COPY public.beone_fix_asset_akm (akm_id, periode_id, fix_asset_id, akm_penyusutan, jan, feb, mar, apr, mei, jun, jul, ags, sep, okt, nov, des, nilai_buku) FROM stdin;
    public       postgres    false    276   E�      �          0    214317    beone_fix_asset_periode 
   TABLE DATA               K   COPY public.beone_fix_asset_periode (periode_id, nama_periode) FROM stdin;
    public       postgres    false    278   ��      c          0    173059    beone_gl 
   TABLE DATA               �   COPY public.beone_gl (gl_id, gl_date, coa_id, coa_no, coa_id_lawan, coa_no_lawan, keterangan, debet, kredit, pasangan_no, gl_number, update_by, update_date) FROM stdin;
    public       postgres    false    210   ϫ      e          0    173064    beone_gudang 
   TABLE DATA               I   COPY public.beone_gudang (gudang_id, nama, keterangan, flag) FROM stdin;
    public       postgres    false    212   �      f          0    173067    beone_gudang_detail 
   TABLE DATA               �   COPY public.beone_gudang_detail (gudang_detail_id, gudang_id, trans_date, item_id, qty_in, qty_out, nomor_transaksi, update_by, update_date, flag, keterangan, kode_tracing) FROM stdin;
    public       postgres    false    213   i�      i          0    173074    beone_hutang_piutang 
   TABLE DATA               �   COPY public.beone_hutang_piutang (hutang_piutang_id, custsup_id, trans_date, nomor, keterangan, valas_trans, idr_trans, valas_pelunasan, idr_pelunasan, tipe_trans, update_by, update_date, flag, status_lunas) FROM stdin;
    public       postgres    false    216   Ϭ      k          0    173079    beone_import_detail 
   TABLE DATA                 COPY public.beone_import_detail (import_detail_id, item_id, qty, satuan_qty, price, pack_qty, satuan_pack, origin_country, volume, netto, brutto, hscode, tbm, ppnn, tpbm, cukai, sat_cukai, cukai_value, bea_masuk, sat_bea_masuk, flag, item_type_id, import_header_id) FROM stdin;
    public       postgres    false    218   �      m          0    173084    beone_import_header 
   TABLE DATA               ]  COPY public.beone_import_header (import_header_id, jenis_bc, car_no, bc_no, bc_date, invoice_no, invoice_date, kontrak_no, kontrak_date, purpose_id, supplier_id, price_type, "from", amount_value, valas_value, value_added, discount, insurance_type, insurace_value, freight, flag, update_by, update_date, status, receive_date, receive_no) FROM stdin;
    public       postgres    false    220   	�      o          0    173089    beone_inventory 
   TABLE DATA               �   COPY public.beone_inventory (intvent_trans_id, intvent_trans_no, item_id, trans_date, keterangan, qty_in, value_in, qty_out, value_out, sa_qty, sa_unit_price, sa_amount, flag, update_by, update_date) FROM stdin;
    public       postgres    false    222   &�      q          0    173094 
   beone_item 
   TABLE DATA               �   COPY public.beone_item (item_id, nama, item_code, saldo_qty, saldo_idr, keterangan, flag, item_type_id, hscode, satuan_id) FROM stdin;
    public       postgres    false    224   C�      s          0    173099    beone_item_type 
   TABLE DATA               O   COPY public.beone_item_type (item_type_id, nama, keterangan, flag) FROM stdin;
    public       postgres    false    226   ��      u          0    173104    beone_kode_trans 
   TABLE DATA               `   COPY public.beone_kode_trans (kode_trans_id, nama, coa_id, kode_bank, in_out, flag) FROM stdin;
    public       postgres    false    228   ��      w          0    173112    beone_konfigurasi_perusahaan 
   TABLE DATA               �   COPY public.beone_konfigurasi_perusahaan (nama_perusahaan, alamat_perusahaan, kota_perusahaan, provinsi_perusahaan, kode_pos_perusahaan, logo_perusahaan) FROM stdin;
    public       postgres    false    230   j�      �          0    198056 	   beone_log 
   TABLE DATA               S   COPY public.beone_log (log_id, log_user, log_tipe, log_desc, log_time) FROM stdin;
    public       postgres    false    272   �      x          0    173118    beone_opname_detail 
   TABLE DATA               �   COPY public.beone_opname_detail (opname_detail_id, opname_header_id, item_id, qty_existing, qty_opname, qty_selisih, status_opname, flag) FROM stdin;
    public       postgres    false    231   �      z          0    173123    beone_opname_header 
   TABLE DATA               �   COPY public.beone_opname_header (opname_header_id, document_no, opname_date, total_item_opname, total_item_opname_match, total_item_opname_plus, total_item_opname_min, update_by, flag, keterangan, update_date) FROM stdin;
    public       postgres    false    233   !�      |          0    173128    beone_peleburan 
   TABLE DATA               �   COPY public.beone_peleburan (peleburan_id, peleburan_no, peleburan_date, keterangan, item_id, qty_peleburan, update_by, update_date, flag) FROM stdin;
    public       postgres    false    235   >�      �          0    197815    beone_po_import_detail 
   TABLE DATA               {   COPY public.beone_po_import_detail (purchase_detail_id, purchase_header_id, item_id, qty, price, amount, flag) FROM stdin;
    public       postgres    false    268   [�      �          0    197807    beone_po_import_header 
   TABLE DATA               �   COPY public.beone_po_import_header (purchase_header_id, purchase_no, trans_date, supplier_id, keterangan, grandtotal, flag, update_by, update_date) FROM stdin;
    public       postgres    false    266   x�      ~          0    173133    beone_purchase_detail 
   TABLE DATA               z   COPY public.beone_purchase_detail (purchase_detail_id, purchase_header_id, item_id, qty, price, amount, flag) FROM stdin;
    public       postgres    false    237   ��      �          0    173138    beone_purchase_header 
   TABLE DATA               �   COPY public.beone_purchase_header (purchase_header_id, purchase_no, trans_date, supplier_id, keterangan, update_by, update_date, flag, ppn, subtotal, ppn_value, grandtotal) FROM stdin;
    public       postgres    false    239   ��      �          0    173143    beone_received_import 
   TABLE DATA               �   COPY public.beone_received_import (received_id, nomor_aju, nomor_dokumen_pabean, tanggal_received, status_received, flag, update_by, update_date, nomor_received, tpb_header_id, kurs, po_import_header_id) FROM stdin;
    public       postgres    false    241   ϳ      �          0    173148 
   beone_role 
   TABLE DATA               	  COPY public.beone_role (role_id, nama_role, keterangan, master_menu, pembelian_menu, penjualan_menu, inventory_menu, produksi_menu, asset_menu, jurnal_umum_menu, kasbank_menu, laporan_inventory, laporan_keuangan, konfigurasi, import_menu, eksport_menu) FROM stdin;
    public       postgres    false    243   �      �          0    198003    beone_role_user 
   TABLE DATA               �  COPY public.beone_role_user (role_id, nama_role, keterangan, master_menu, user_add, user_edit, user_delete, role_add, role_edit, role_delete, customer_add, customer_edit, customer_delete, supplier_add, supplier_edit, supplier_delete, item_add, item_edit, item_delete, jenis_add, jenis_edit, jenis_delete, satuan_add, satuan_edit, satuan_delete, gudang_add, gudang_edit, gudang_delete, master_import, po_add, po_edit, po_delete, tracing, terima_barang, master_pembelian, pembelian_add, pembelian_edit, pembelian_delete, kredit_note_add, kredit_note_edit, kredit_note_delete, master_eksport, eksport_add, eksport_edit, eksport_delete, kirim_barang, menu_penjualan, penjualan_add, penjualan_edit, penjualan_delete, debit_note_add, debit_note_edit, debit_note_delete, menu_inventory, pindah_gudang, stockopname_add, stockopname_edit, stockopname_delete, stockopname_opname, adjustment_add, adjustment_edit, adjustment_delete, pemusnahan_add, pemusnahan_edit, pemusnahan_delete, recal_inventory, menu_produksi, produksi_add, produksi_edit, produksi_delete, menu_asset, menu_jurnal_umum, jurnal_umum_add, jurnal_umum_edit, jurnal_umum_delete, menu_kas_bank, kas_bank_add, kas_bank_edit, kas_bank_delete, menu_laporan_inventory, menu_laporan_keuangan, menu_konfigurasi) FROM stdin;
    public       postgres    false    270   a�      �          0    173153    beone_sales_detail 
   TABLE DATA               q   COPY public.beone_sales_detail (sales_detail_id, sales_header_id, item_id, qty, price, amount, flag) FROM stdin;
    public       postgres    false    245   ��      �          0    173158    beone_sales_header 
   TABLE DATA               �   COPY public.beone_sales_header (sales_header_id, sales_no, trans_date, customer_id, keterangan, ppn, subtotal, ppn_value, grandtotal, update_by, update_date, flag) FROM stdin;
    public       postgres    false    247   ��      �          0    173163    beone_satuan_item 
   TABLE DATA               U   COPY public.beone_satuan_item (satuan_id, satuan_code, keterangan, flag) FROM stdin;
    public       postgres    false    249   ش      �          0    173168    beone_tipe_coa 
   TABLE DATA               ;   COPY public.beone_tipe_coa (tipe_coa_id, nama) FROM stdin;
    public       postgres    false    251   �      �          0    173173    beone_tipe_coa_transaksi 
   TABLE DATA               Q   COPY public.beone_tipe_coa_transaksi (tipe_coa_trans_id, nama, flag) FROM stdin;
    public       postgres    false    253   ��      �          0    173178    beone_transfer_stock 
   TABLE DATA               �   COPY public.beone_transfer_stock (transfer_stock_id, transfer_no, transfer_date, coa_kode_biaya, keterangan, update_by, update_date, flag) FROM stdin;
    public       postgres    false    255   l�      �          0    173181    beone_transfer_stock_detail 
   TABLE DATA               �   COPY public.beone_transfer_stock_detail (transfer_stock_detail_id, transfer_stock_header_id, tipe_transfer_stock, item_id, qty, gudang_id, biaya, update_by, update_date, flag, persen_produksi) FROM stdin;
    public       postgres    false    256   ��      �          0    173188 
   beone_user 
   TABLE DATA               n   COPY public.beone_user (user_id, username, password, nama, role_id, update_by, update_date, flag) FROM stdin;
    public       postgres    false    259   ��      �          0    173201    beone_voucher_detail 
   TABLE DATA               �   COPY public.beone_voucher_detail (voucher_detail_id, voucher_header_id, coa_id_lawan, coa_no_lawan, jumlah_valas, kurs, jumlah_idr, keterangan_detail) FROM stdin;
    public       postgres    false    261   �      �          0    173206    beone_voucher_header 
   TABLE DATA               �   COPY public.beone_voucher_header (voucher_header_id, voucher_number, voucher_date, tipe, keterangan, coa_id_cash_bank, coa_no, update_by, update_date) FROM stdin;
    public       postgres    false    263   *�      �           0    0 "   beone_adjustment_adjustment_id_seq    SEQUENCE SET     Q   SELECT pg_catalog.setval('public.beone_adjustment_adjustment_id_seq', 23, true);
            public       postgres    false    197            �           0    0    beone_coa_coa_id_seq    SEQUENCE SET     C   SELECT pg_catalog.setval('public.beone_coa_coa_id_seq', 1, false);
            public       postgres    false    199            �           0    0 "   beone_coa_jurnal_coa_jurnal_id_seq    SEQUENCE SET     Q   SELECT pg_catalog.setval('public.beone_coa_jurnal_coa_jurnal_id_seq', 12, true);
            public       postgres    false    201            �           0    0    beone_country_country_id_seq    SEQUENCE SET     J   SELECT pg_catalog.setval('public.beone_country_country_id_seq', 3, true);
            public       postgres    false    203            �           0    0    beone_custsup_custsup_id_seq    SEQUENCE SET     K   SELECT pg_catalog.setval('public.beone_custsup_custsup_id_seq', 34, true);
            public       postgres    false    205            �           0    0 (   beone_export_detail_export_detail_id_seq    SEQUENCE SET     W   SELECT pg_catalog.setval('public.beone_export_detail_export_detail_id_seq', 41, true);
            public       postgres    false    207            �           0    0 (   beone_export_header_export_header_id_seq    SEQUENCE SET     W   SELECT pg_catalog.setval('public.beone_export_header_export_header_id_seq', 38, true);
            public       postgres    false    209            �           0    0    beone_fix_asset_akm_akm_id_seq    SEQUENCE SET     L   SELECT pg_catalog.setval('public.beone_fix_asset_akm_akm_id_seq', 3, true);
            public       postgres    false    275            �           0    0     beone_fix_asset_fix_asset_id_seq    SEQUENCE SET     N   SELECT pg_catalog.setval('public.beone_fix_asset_fix_asset_id_seq', 4, true);
            public       postgres    false    273            �           0    0 &   beone_fix_asset_periode_periode_id_seq    SEQUENCE SET     T   SELECT pg_catalog.setval('public.beone_fix_asset_periode_periode_id_seq', 4, true);
            public       postgres    false    277            �           0    0    beone_gl_gl_id_seq    SEQUENCE SET     C   SELECT pg_catalog.setval('public.beone_gl_gl_id_seq', 2416, true);
            public       postgres    false    211            �           0    0 (   beone_gudang_detail_gudang_detail_id_seq    SEQUENCE SET     X   SELECT pg_catalog.setval('public.beone_gudang_detail_gudang_detail_id_seq', 478, true);
            public       postgres    false    214            �           0    0    beone_gudang_gudang_id_seq    SEQUENCE SET     H   SELECT pg_catalog.setval('public.beone_gudang_gudang_id_seq', 4, true);
            public       postgres    false    215            �           0    0 *   beone_hutang_piutang_hutang_piutang_id_seq    SEQUENCE SET     Z   SELECT pg_catalog.setval('public.beone_hutang_piutang_hutang_piutang_id_seq', 383, true);
            public       postgres    false    217            �           0    0 (   beone_import_detail_import_detail_id_seq    SEQUENCE SET     W   SELECT pg_catalog.setval('public.beone_import_detail_import_detail_id_seq', 36, true);
            public       postgres    false    219            �           0    0 (   beone_import_header_import_header_id_seq    SEQUENCE SET     W   SELECT pg_catalog.setval('public.beone_import_header_import_header_id_seq', 30, true);
            public       postgres    false    221            �           0    0 $   beone_inventory_intvent_trans_id_seq    SEQUENCE SET     T   SELECT pg_catalog.setval('public.beone_inventory_intvent_trans_id_seq', 959, true);
            public       postgres    false    223            �           0    0    beone_item_item_id_seq    SEQUENCE SET     E   SELECT pg_catalog.setval('public.beone_item_item_id_seq', 54, true);
            public       postgres    false    225            �           0    0     beone_item_type_item_type_id_seq    SEQUENCE SET     O   SELECT pg_catalog.setval('public.beone_item_type_item_type_id_seq', 15, true);
            public       postgres    false    227            �           0    0 "   beone_kode_trans_kode_trans_id_seq    SEQUENCE SET     Q   SELECT pg_catalog.setval('public.beone_kode_trans_kode_trans_id_seq', 13, true);
            public       postgres    false    229            �           0    0    beone_log_log_id_seq    SEQUENCE SET     D   SELECT pg_catalog.setval('public.beone_log_log_id_seq', 208, true);
            public       postgres    false    271            �           0    0 (   beone_opname_detail_opname_detail_id_seq    SEQUENCE SET     X   SELECT pg_catalog.setval('public.beone_opname_detail_opname_detail_id_seq', 476, true);
            public       postgres    false    232            �           0    0 (   beone_opname_header_opname_header_id_seq    SEQUENCE SET     W   SELECT pg_catalog.setval('public.beone_opname_header_opname_header_id_seq', 14, true);
            public       postgres    false    234            �           0    0     beone_peleburan_peleburan_id_seq    SEQUENCE SET     N   SELECT pg_catalog.setval('public.beone_peleburan_peleburan_id_seq', 6, true);
            public       postgres    false    236            �           0    0 -   beone_po_import_detail_purchase_detail_id_seq    SEQUENCE SET     \   SELECT pg_catalog.setval('public.beone_po_import_detail_purchase_detail_id_seq', 55, true);
            public       postgres    false    267            �           0    0 -   beone_po_import_header_purchase_header_id_seq    SEQUENCE SET     \   SELECT pg_catalog.setval('public.beone_po_import_header_purchase_header_id_seq', 53, true);
            public       postgres    false    265            �           0    0 ,   beone_purchase_detail_purchase_detail_id_seq    SEQUENCE SET     [   SELECT pg_catalog.setval('public.beone_purchase_detail_purchase_detail_id_seq', 96, true);
            public       postgres    false    238            �           0    0 ,   beone_purchase_header_purchase_header_id_seq    SEQUENCE SET     [   SELECT pg_catalog.setval('public.beone_purchase_header_purchase_header_id_seq', 65, true);
            public       postgres    false    240            �           0    0 %   beone_received_import_received_id_seq    SEQUENCE SET     U   SELECT pg_catalog.setval('public.beone_received_import_received_id_seq', 107, true);
            public       postgres    false    242            �           0    0    beone_role_role_id_seq    SEQUENCE SET     D   SELECT pg_catalog.setval('public.beone_role_role_id_seq', 6, true);
            public       postgres    false    244            �           0    0    beone_role_user_role_id_seq    SEQUENCE SET     I   SELECT pg_catalog.setval('public.beone_role_user_role_id_seq', 3, true);
            public       postgres    false    269            �           0    0 &   beone_sales_detail_sales_detail_id_seq    SEQUENCE SET     U   SELECT pg_catalog.setval('public.beone_sales_detail_sales_detail_id_seq', 24, true);
            public       postgres    false    246            �           0    0 &   beone_sales_header_sales_header_id_seq    SEQUENCE SET     U   SELECT pg_catalog.setval('public.beone_sales_header_sales_header_id_seq', 18, true);
            public       postgres    false    248            �           0    0    beone_satuan_item_satuan_id_seq    SEQUENCE SET     M   SELECT pg_catalog.setval('public.beone_satuan_item_satuan_id_seq', 3, true);
            public       postgres    false    250            �           0    0    beone_tipe_coa_tipe_coa_id_seq    SEQUENCE SET     M   SELECT pg_catalog.setval('public.beone_tipe_coa_tipe_coa_id_seq', 1, false);
            public       postgres    false    252            �           0    0 .   beone_tipe_coa_transaksi_tipe_coa_trans_id_seq    SEQUENCE SET     ]   SELECT pg_catalog.setval('public.beone_tipe_coa_transaksi_tipe_coa_trans_id_seq', 20, true);
            public       postgres    false    254            �           0    0 8   beone_transfer_stock_detail_transfer_stock_detail_id_seq    SEQUENCE SET     h   SELECT pg_catalog.setval('public.beone_transfer_stock_detail_transfer_stock_detail_id_seq', 687, true);
            public       postgres    false    257            �           0    0 *   beone_transfer_stock_transfer_stock_id_seq    SEQUENCE SET     Z   SELECT pg_catalog.setval('public.beone_transfer_stock_transfer_stock_id_seq', 316, true);
            public       postgres    false    258            �           0    0    beone_user_user_id_seq    SEQUENCE SET     E   SELECT pg_catalog.setval('public.beone_user_user_id_seq', 22, true);
            public       postgres    false    260                        0    0 *   beone_voucher_detail_voucher_detail_id_seq    SEQUENCE SET     Z   SELECT pg_catalog.setval('public.beone_voucher_detail_voucher_detail_id_seq', 178, true);
            public       postgres    false    262                       0    0 *   beone_voucher_header_voucher_header_id_seq    SEQUENCE SET     Z   SELECT pg_catalog.setval('public.beone_voucher_header_voucher_header_id_seq', 142, true);
            public       postgres    false    264            �           2606    173250 &   beone_adjustment beone_adjustment_pkey 
   CONSTRAINT     o   ALTER TABLE ONLY public.beone_adjustment
    ADD CONSTRAINT beone_adjustment_pkey PRIMARY KEY (adjustment_id);
 P   ALTER TABLE ONLY public.beone_adjustment DROP CONSTRAINT beone_adjustment_pkey;
       public         postgres    false    196            �           2606    173252 &   beone_coa_jurnal beone_coa_jurnal_pkey 
   CONSTRAINT     o   ALTER TABLE ONLY public.beone_coa_jurnal
    ADD CONSTRAINT beone_coa_jurnal_pkey PRIMARY KEY (coa_jurnal_id);
 P   ALTER TABLE ONLY public.beone_coa_jurnal DROP CONSTRAINT beone_coa_jurnal_pkey;
       public         postgres    false    200            �           2606    173254    beone_coa beone_coa_pkey 
   CONSTRAINT     Z   ALTER TABLE ONLY public.beone_coa
    ADD CONSTRAINT beone_coa_pkey PRIMARY KEY (coa_id);
 B   ALTER TABLE ONLY public.beone_coa DROP CONSTRAINT beone_coa_pkey;
       public         postgres    false    198            �           2606    173256     beone_country beone_country_pkey 
   CONSTRAINT     f   ALTER TABLE ONLY public.beone_country
    ADD CONSTRAINT beone_country_pkey PRIMARY KEY (country_id);
 J   ALTER TABLE ONLY public.beone_country DROP CONSTRAINT beone_country_pkey;
       public         postgres    false    202            �           2606    173258 ,   beone_export_detail beone_export_detail_pkey 
   CONSTRAINT     x   ALTER TABLE ONLY public.beone_export_detail
    ADD CONSTRAINT beone_export_detail_pkey PRIMARY KEY (export_detail_id);
 V   ALTER TABLE ONLY public.beone_export_detail DROP CONSTRAINT beone_export_detail_pkey;
       public         postgres    false    206            �           2606    214314 ,   beone_fix_asset_akm beone_fix_asset_akm_pkey 
   CONSTRAINT     n   ALTER TABLE ONLY public.beone_fix_asset_akm
    ADD CONSTRAINT beone_fix_asset_akm_pkey PRIMARY KEY (akm_id);
 V   ALTER TABLE ONLY public.beone_fix_asset_akm DROP CONSTRAINT beone_fix_asset_akm_pkey;
       public         postgres    false    276            �           2606    214322 4   beone_fix_asset_periode beone_fix_asset_periode_pkey 
   CONSTRAINT     z   ALTER TABLE ONLY public.beone_fix_asset_periode
    ADD CONSTRAINT beone_fix_asset_periode_pkey PRIMARY KEY (periode_id);
 ^   ALTER TABLE ONLY public.beone_fix_asset_periode DROP CONSTRAINT beone_fix_asset_periode_pkey;
       public         postgres    false    278            �           2606    214306 $   beone_fix_asset beone_fix_asset_pkey 
   CONSTRAINT     l   ALTER TABLE ONLY public.beone_fix_asset
    ADD CONSTRAINT beone_fix_asset_pkey PRIMARY KEY (fix_asset_id);
 N   ALTER TABLE ONLY public.beone_fix_asset DROP CONSTRAINT beone_fix_asset_pkey;
       public         postgres    false    274            �           2606    173260    beone_gl beone_gl_pkey 
   CONSTRAINT     W   ALTER TABLE ONLY public.beone_gl
    ADD CONSTRAINT beone_gl_pkey PRIMARY KEY (gl_id);
 @   ALTER TABLE ONLY public.beone_gl DROP CONSTRAINT beone_gl_pkey;
       public         postgres    false    210            �           2606    173262 ,   beone_gudang_detail beone_gudang_detail_pkey 
   CONSTRAINT     x   ALTER TABLE ONLY public.beone_gudang_detail
    ADD CONSTRAINT beone_gudang_detail_pkey PRIMARY KEY (gudang_detail_id);
 V   ALTER TABLE ONLY public.beone_gudang_detail DROP CONSTRAINT beone_gudang_detail_pkey;
       public         postgres    false    213            �           2606    173264    beone_gudang beone_gudang_pkey 
   CONSTRAINT     c   ALTER TABLE ONLY public.beone_gudang
    ADD CONSTRAINT beone_gudang_pkey PRIMARY KEY (gudang_id);
 H   ALTER TABLE ONLY public.beone_gudang DROP CONSTRAINT beone_gudang_pkey;
       public         postgres    false    212            �           2606    173266 .   beone_hutang_piutang beone_hutang_piutang_pkey 
   CONSTRAINT     {   ALTER TABLE ONLY public.beone_hutang_piutang
    ADD CONSTRAINT beone_hutang_piutang_pkey PRIMARY KEY (hutang_piutang_id);
 X   ALTER TABLE ONLY public.beone_hutang_piutang DROP CONSTRAINT beone_hutang_piutang_pkey;
       public         postgres    false    216            �           2606    173268 ,   beone_import_detail beone_import_detail_pkey 
   CONSTRAINT     x   ALTER TABLE ONLY public.beone_import_detail
    ADD CONSTRAINT beone_import_detail_pkey PRIMARY KEY (import_detail_id);
 V   ALTER TABLE ONLY public.beone_import_detail DROP CONSTRAINT beone_import_detail_pkey;
       public         postgres    false    218            �           2606    173270 ,   beone_import_header beone_import_header_pkey 
   CONSTRAINT     x   ALTER TABLE ONLY public.beone_import_header
    ADD CONSTRAINT beone_import_header_pkey PRIMARY KEY (import_header_id);
 V   ALTER TABLE ONLY public.beone_import_header DROP CONSTRAINT beone_import_header_pkey;
       public         postgres    false    220            �           2606    173272 $   beone_inventory beone_inventory_pkey 
   CONSTRAINT     p   ALTER TABLE ONLY public.beone_inventory
    ADD CONSTRAINT beone_inventory_pkey PRIMARY KEY (intvent_trans_id);
 N   ALTER TABLE ONLY public.beone_inventory DROP CONSTRAINT beone_inventory_pkey;
       public         postgres    false    222            �           2606    173274    beone_item beone_item_pkey 
   CONSTRAINT     ]   ALTER TABLE ONLY public.beone_item
    ADD CONSTRAINT beone_item_pkey PRIMARY KEY (item_id);
 D   ALTER TABLE ONLY public.beone_item DROP CONSTRAINT beone_item_pkey;
       public         postgres    false    224            �           2606    173276 $   beone_item_type beone_item_type_pkey 
   CONSTRAINT     l   ALTER TABLE ONLY public.beone_item_type
    ADD CONSTRAINT beone_item_type_pkey PRIMARY KEY (item_type_id);
 N   ALTER TABLE ONLY public.beone_item_type DROP CONSTRAINT beone_item_type_pkey;
       public         postgres    false    226            �           2606    173278 &   beone_kode_trans beone_kode_trans_pkey 
   CONSTRAINT     o   ALTER TABLE ONLY public.beone_kode_trans
    ADD CONSTRAINT beone_kode_trans_pkey PRIMARY KEY (kode_trans_id);
 P   ALTER TABLE ONLY public.beone_kode_trans DROP CONSTRAINT beone_kode_trans_pkey;
       public         postgres    false    228            �           2606    198061    beone_log beone_log_pkey 
   CONSTRAINT     Z   ALTER TABLE ONLY public.beone_log
    ADD CONSTRAINT beone_log_pkey PRIMARY KEY (log_id);
 B   ALTER TABLE ONLY public.beone_log DROP CONSTRAINT beone_log_pkey;
       public         postgres    false    272            �           2606    173280 ,   beone_opname_detail beone_opname_detail_pkey 
   CONSTRAINT     x   ALTER TABLE ONLY public.beone_opname_detail
    ADD CONSTRAINT beone_opname_detail_pkey PRIMARY KEY (opname_detail_id);
 V   ALTER TABLE ONLY public.beone_opname_detail DROP CONSTRAINT beone_opname_detail_pkey;
       public         postgres    false    231            �           2606    173282 ,   beone_opname_header beone_opname_header_pkey 
   CONSTRAINT     x   ALTER TABLE ONLY public.beone_opname_header
    ADD CONSTRAINT beone_opname_header_pkey PRIMARY KEY (opname_header_id);
 V   ALTER TABLE ONLY public.beone_opname_header DROP CONSTRAINT beone_opname_header_pkey;
       public         postgres    false    233            �           2606    173284 $   beone_peleburan beone_peleburan_pkey 
   CONSTRAINT     l   ALTER TABLE ONLY public.beone_peleburan
    ADD CONSTRAINT beone_peleburan_pkey PRIMARY KEY (peleburan_id);
 N   ALTER TABLE ONLY public.beone_peleburan DROP CONSTRAINT beone_peleburan_pkey;
       public         postgres    false    235            �           2606    197820 2   beone_po_import_detail beone_po_import_detail_pkey 
   CONSTRAINT     �   ALTER TABLE ONLY public.beone_po_import_detail
    ADD CONSTRAINT beone_po_import_detail_pkey PRIMARY KEY (purchase_detail_id);
 \   ALTER TABLE ONLY public.beone_po_import_detail DROP CONSTRAINT beone_po_import_detail_pkey;
       public         postgres    false    268            �           2606    197812 2   beone_po_import_header beone_po_import_header_pkey 
   CONSTRAINT     �   ALTER TABLE ONLY public.beone_po_import_header
    ADD CONSTRAINT beone_po_import_header_pkey PRIMARY KEY (purchase_header_id);
 \   ALTER TABLE ONLY public.beone_po_import_header DROP CONSTRAINT beone_po_import_header_pkey;
       public         postgres    false    266            �           2606    173286 0   beone_purchase_detail beone_purchase_detail_pkey 
   CONSTRAINT     ~   ALTER TABLE ONLY public.beone_purchase_detail
    ADD CONSTRAINT beone_purchase_detail_pkey PRIMARY KEY (purchase_detail_id);
 Z   ALTER TABLE ONLY public.beone_purchase_detail DROP CONSTRAINT beone_purchase_detail_pkey;
       public         postgres    false    237            �           2606    173288 0   beone_purchase_header beone_purchase_header_pkey 
   CONSTRAINT     ~   ALTER TABLE ONLY public.beone_purchase_header
    ADD CONSTRAINT beone_purchase_header_pkey PRIMARY KEY (purchase_header_id);
 Z   ALTER TABLE ONLY public.beone_purchase_header DROP CONSTRAINT beone_purchase_header_pkey;
       public         postgres    false    239            �           2606    173290 0   beone_received_import beone_received_import_pkey 
   CONSTRAINT     w   ALTER TABLE ONLY public.beone_received_import
    ADD CONSTRAINT beone_received_import_pkey PRIMARY KEY (received_id);
 Z   ALTER TABLE ONLY public.beone_received_import DROP CONSTRAINT beone_received_import_pkey;
       public         postgres    false    241            �           2606    173292    beone_role beone_role_pkey 
   CONSTRAINT     ]   ALTER TABLE ONLY public.beone_role
    ADD CONSTRAINT beone_role_pkey PRIMARY KEY (role_id);
 D   ALTER TABLE ONLY public.beone_role DROP CONSTRAINT beone_role_pkey;
       public         postgres    false    243            �           2606    198008 $   beone_role_user beone_role_user_pkey 
   CONSTRAINT     g   ALTER TABLE ONLY public.beone_role_user
    ADD CONSTRAINT beone_role_user_pkey PRIMARY KEY (role_id);
 N   ALTER TABLE ONLY public.beone_role_user DROP CONSTRAINT beone_role_user_pkey;
       public         postgres    false    270            �           2606    173294 *   beone_sales_detail beone_sales_detail_pkey 
   CONSTRAINT     u   ALTER TABLE ONLY public.beone_sales_detail
    ADD CONSTRAINT beone_sales_detail_pkey PRIMARY KEY (sales_detail_id);
 T   ALTER TABLE ONLY public.beone_sales_detail DROP CONSTRAINT beone_sales_detail_pkey;
       public         postgres    false    245            �           2606    173296 *   beone_sales_header beone_sales_header_pkey 
   CONSTRAINT     u   ALTER TABLE ONLY public.beone_sales_header
    ADD CONSTRAINT beone_sales_header_pkey PRIMARY KEY (sales_header_id);
 T   ALTER TABLE ONLY public.beone_sales_header DROP CONSTRAINT beone_sales_header_pkey;
       public         postgres    false    247            �           2606    173298 (   beone_satuan_item beone_satuan_item_pkey 
   CONSTRAINT     m   ALTER TABLE ONLY public.beone_satuan_item
    ADD CONSTRAINT beone_satuan_item_pkey PRIMARY KEY (satuan_id);
 R   ALTER TABLE ONLY public.beone_satuan_item DROP CONSTRAINT beone_satuan_item_pkey;
       public         postgres    false    249            �           2606    173300 "   beone_tipe_coa beone_tipe_coa_pkey 
   CONSTRAINT     i   ALTER TABLE ONLY public.beone_tipe_coa
    ADD CONSTRAINT beone_tipe_coa_pkey PRIMARY KEY (tipe_coa_id);
 L   ALTER TABLE ONLY public.beone_tipe_coa DROP CONSTRAINT beone_tipe_coa_pkey;
       public         postgres    false    251            �           2606    173302 <   beone_transfer_stock_detail beone_transfer_stock_detail_pkey 
   CONSTRAINT     �   ALTER TABLE ONLY public.beone_transfer_stock_detail
    ADD CONSTRAINT beone_transfer_stock_detail_pkey PRIMARY KEY (transfer_stock_detail_id);
 f   ALTER TABLE ONLY public.beone_transfer_stock_detail DROP CONSTRAINT beone_transfer_stock_detail_pkey;
       public         postgres    false    256            �           2606    173304 .   beone_transfer_stock beone_transfer_stock_pkey 
   CONSTRAINT     {   ALTER TABLE ONLY public.beone_transfer_stock
    ADD CONSTRAINT beone_transfer_stock_pkey PRIMARY KEY (transfer_stock_id);
 X   ALTER TABLE ONLY public.beone_transfer_stock DROP CONSTRAINT beone_transfer_stock_pkey;
       public         postgres    false    255            �           2606    173306    beone_user beone_user_pkey 
   CONSTRAINT     ]   ALTER TABLE ONLY public.beone_user
    ADD CONSTRAINT beone_user_pkey PRIMARY KEY (user_id);
 D   ALTER TABLE ONLY public.beone_user DROP CONSTRAINT beone_user_pkey;
       public         postgres    false    259            �           2606    173310 .   beone_voucher_detail beone_voucher_detail_pkey 
   CONSTRAINT     {   ALTER TABLE ONLY public.beone_voucher_detail
    ADD CONSTRAINT beone_voucher_detail_pkey PRIMARY KEY (voucher_detail_id);
 X   ALTER TABLE ONLY public.beone_voucher_detail DROP CONSTRAINT beone_voucher_detail_pkey;
       public         postgres    false    261            �           2606    173312 .   beone_voucher_header beone_voucher_header_pkey 
   CONSTRAINT     {   ALTER TABLE ONLY public.beone_voucher_header
    ADD CONSTRAINT beone_voucher_header_pkey PRIMARY KEY (voucher_header_id);
 X   ALTER TABLE ONLY public.beone_voucher_header DROP CONSTRAINT beone_voucher_header_pkey;
       public         postgres    false    263            U      x������ � �      W   �  x��Xɒ�8=�_���1����8����iw��i��8	$dՔ#��D"_n�j;�����QJ��h�	�(�\�_��[Fs�]mS�m_�l��0��� �����knmS�����軟Hҁ$�Bhn.��CW+�+�p�͔`��	a.�d�q%K!)�h�Zҳ��{�,���w�g$ᴤ���2�[L�٭����mq��O��kT��_��&��4����op-�b��Y�SN��%'D_�4�ء9%���u͛��Lr��)ͺ�����)�p�����
��*���i���R�3z����Ut};T�tJ'�O{s/����DcjLi���2�<�5ն�G��d�35�ϩ���az������:+ܫk{$��ê�UOg�Ieh�rj"�&���R,#��k��ŔF���9�>��S�5���ՈGs�TR2m�E��bj#��X������/^����ּ�OĈ��
�3�"��\3��}���7$I�)m�@"��ꪹA.�ٺJ=P`�W�یv��sP���Ȉ�cF|;L1�X����B���1��^�Q�j�{Y�P�����Ĳ�,��֣�eC�6�m@'�sN ��tU�{��8����9Ѱ�X��V��}Rۓ���Z7%F��A��paB+
��B,2[2��B�سVš��^?�+Sgez�1	X�.�_�j9�՗�8D[AS	w�J��o�a�;zo���`K~Ew(�����招�d��CR�8��4�T��h�`�3FCas�LY���f���T��8��z���[{��}�16C������a~��r�cP1����k�����i�����B��2�(��Z���Xufr!O�眔D1�,P�F�/"������b�v�R"�<sr��+�2=Ĝpb��:,&�rm2���o[����Q<,dfqsW���"��%5�MI.�� y�Y8(cջ��_��B�^n*r��μ%O�j����2�k�]�"H@b7R��	:L�`ypd�z��3�`Te�V��g�H}��U4U���x.u��W�
���~f����*^�
�H
��<)��٢��Py+OD�@�M�ĢXT{��j��o3m��Q(��C2�+��{t�ަ3��=8���_]qѳ�u��h��f ��JD�Z����#�r)JΨ���
׺Ss0;s@E��qj���eg胻�B+�m���Ȱ�9!�轉Cg�
ᮡs$hM"4Pq�&���r�Y����0�$�q��e})C΃(�N�t�8����	;Cl�B�ŷ��ag���oR|�&�\�o�fm|�Q���S��Q,=��ej�@�]@�C6�r>si���Z�
��R�ͽ�hᵀ@� K2���Azl�X��Ng����|��(K�K*��ӏ�F� �)���zv��Q���o�'��{�K��@��pD���#d����L�K�[|N���w9Diz}x�Naz�}��0=A��e
,Wyz��k�p�������e�J��{,̭�~������o�x%U�,j��p�pl���W5�OSi�*�m`�
�E��G�5����n{����TA�*x�""��,�a`��>:U�,�%�T<��7��V����)��f+Ǔn8,ՓrF���7��KE��R�)�gv�o��N�����}k���,���|�Q��/���<�TpJ�����	F�d�g����M3�B���8��Pw����/��9i�P�O9���E�4�(�Z���-�ͮ�]r���K*}Bi{2���=\�.˽�
��CW�A���B@��}T�P!,Mr
�0�V���&��8�#|����e�^�q-�+�h�[����^�Ĕ��#H����~@?t����7��Xq����e�<�͡�h|�}X�R�WXh-��5KB�ȥ�wY1����Ӗ�Y�2���y�(���Շ{ⶅ�@QA����_�<���v      Y     x�}�Mr� ���)��)Z��e'Z#�%R�E���f��tRE�����ϡ�4���4���hy�K4� rO�G���*�
���Z4�sy�J�B��|xoO��h�RrU���0�|%�q�B�i���{P��L-�u�_[ֻ��)����\hS�\)\�w7>:Cc>/�d�pl,�דw���C�ŎY��NdHoN��61=�.���i��@�P�p�����"���]{�j�va�Od�����9��-����	c����͎��c��      [   >   x�3���quQqq��4�2�t
r����t�t��9=�\��\���.@�=... �pd      ]   t  x��TM��0=;���D��?��G�@�����T�D�V�n����lB�������f�@;�����Ժ��ָ�� /k��ڮи"�("0%Q�	�
����4,���u���y�WhX�8�DJ��,� �o���?!X��oFڦ+tq�ȿ�W(�4W�8�}R��y^V��3��>O ���=,��fcr���/�,�>,��bT4Ϻ�� �vU��rڭд���2�A���8�iSվ{������]���??�9ኆ���>l*#�9SB�p�B*� +Tg�Ys8�`ΤFA�-Py0)^n�b�}O�^E�"Rq�X�(�AQ�('Nd�4�Q�W�æ*�u�o:W�Y����RJpQA�U?B�8||S��+K����%(Q=]�����q>�U�m� �	qr�.�x�����N�x4��<��O����=(��[�	���9��Y��Y����qě��#��Z����
'�s�C�_�>��?�~#�~�y��Ӈ;a4D�6���V�y��TR�_ T��p5�pN#�,0�q��w�9�ψ�þg���A�)�$ �Oc�NTyr*��^��Ç�lp(�J�H�$�@�LA�������P��K�$9AF"��.	�R@�Cv���`�X�f�-T      _      x������ � �      a      x������ � �      �   c   x�=�1� �Ṝ�`
�#��Xu���|��/�B�r^BU7p����A�rPh�Z�}m:�C�vfѠ3�����Bs�1�Kp6��z��{PJ=mO      �   N   x�����0��0���	Y�	���tR%�)�9��}}���c�����E�&�H1��X��T�5�\+Bvf� �K N      �      x�3�H-��OIU020������ 9d�      c      x������ � �      e   m   x�]��� ��3��	LP���V/�?����M�<M+�%�d�p.���a�&��Kr2.s��j�PT�� m?������c���wVT_�6�N g���'��V��B/�      f   V   x�317�4�420��50"N#KNSCKN�`G�x�pGT��21~\&��
��8��--�I2��sNSs#̈���� ��)�      i      x������ � �      k      x������ � �      m      x������ � �      o      x������ � �      q   k  x��V�r� }�_�c�Д�dI�HVbYҀ=N;����	��q�L8{�ݳ�@��i�{�u�Č��S�����'R�|��$��U��~�r��n�t�-�I� }rz������8GZw��ǃ�Z%j�[�/^�I5�nYz�h��r�/�+�a��D�/'�	y9A�q�B����Z���3.x*��	��\�{W��"~��%}ٕţ�|��A+}�k���EI1��jI07ܲb�@�WU�Y�j�7��U���(����7�1����xGt/�C�H��Js�cZ 1�:�Ρ�]epfٙ�;4��_+�׉��[��d7�K����`Ogi�Z�i��F������K�ò#�
<��㓽��I��MZ�ag�e$�ֻ����ʵ�a�Nnʞ��1:-$�� p�'�d�`����U�
	�dZ�p4))T:S�p���*����7�8����c�Xo����� �
ߊ�PQR�86��osk	UA8c� -=:��#sf���͸S�}�p3�<�rf-ުa�nU����n&,=`�jB��B�]Y�i��*�ܒ��}�w~�?_��p�RJ�-w�waM�^��<i�U�[���5YN��.��^�����,F��S�W�y�6���[���2/#��;}K�.�k;�a�,���x��C	�<���=��p7vB��?'�u?�[�;u�d;��$ԕ��hfKK��z�J<�0���:�R|$]A ��c+UM�����  @C�`�z���<���	�S�Á���G�A���zjc.��KB���^=�$p�w?�;�&���+O�JJ��.y}d�[e�!<�1!9����;j��#��R�ao��:GYml��o���U��      s   �   x�M�An1E��)|�B(�v�k�dp";���Ro��KM�����{ߎa��J��P�h��X��$u�{�l��f#�j��R�j����[��2#˹d��/�Тy�V�����D���#�a�A&<�	F<���5���{l�Ls��#Mx����w�<g;|]�����E�^ȸ]|_3n�F�6�I^\������k�_B�cF~      u   �   x�u�1�0Eg�9AE�B;:	R�!T�2����t@ݒ��g��f�5x���3�F�+kFPq�h��T�F�]��P�)ѮM7��bʸڈÊEZ�0*s-L�>�8.i�F�����?eL��S��TXq޿��E!y�^�>��\u�K(JL��l�f>@E�a��-tes�W���9]ٹP2�}1�| ��V      w   m   x��Sq��s�W���uq�Qq�u��K�/�W��K)-.)�L�QH-*NLI�Qp���Vpӵ�Q �������N-*���J,OT��--�434�0������ ��)      �      x������ � �      x      x������ � �      z      x������ � �      |      x������ � �      �      x������ � �      �      x������ � �      ~      x������ � �      �      x������ � �      �      x������ � �      �   e   x�3�p��
u�q�Cbb�1~@�eT�����	�c �!!,�SN�O_��dc��� � G_O?6v]@�e��������`�Ad���qqq �+�      �   -   x�3�q��s�ӆ�rq:�::�z;z"�*�.����� �w-�      �      x������ � �      �      x������ � �      �   1   x�3�pcC.c� 0�i�e������������������ �
      �   ^   x�-�M
� E��pA��+Y�r�E��בA����ٟD'�rq�^��0HZt+�FYS�$�󦡋�QM�%b��:8&暁^�&����	      �   �   x�MP[�� �vN�t���9QPCC\����kh���񌙱mhB�a"��)��,A��� �#f���e[��ʑ<z(QT
*��
%��m8���guL���B���JcM�+���#�c�_�^����Ȫ�n�m�$U�ej��S~�ߤ�o�Ӗ��Bx��7n���lq�]WʒP��iD�7�F.<m.Ȯ^Sװ��1-�L���u]�тSr      �      x������ � �      �      x������ � �      �   W  x�e�Mn1FלS�HꏺB�ЍDQ���0/z�ʅ�&1$h���=�v����ں�8��������y�����h��U,��T��(�e���g���r��Y��st�fR
�P�;m9<���r)��}[�r�y�ʫ�2|�K�Oe��jS�Q���s���0��%3�1��^u���O�YF��~�Q1�ޙo=�U�����cE�T�<�ß���#��!�[��֡yj2z�#VFoY�Ǚv�����%���b��l��V�i|q�>r��k�>��d�.c���`�_��6�YIY�)�&q$P��u�QTO�`�`x_?�j��]	-Մh�����S�'�"��6M�o��4      �      x������ � �      �      x������ � �     